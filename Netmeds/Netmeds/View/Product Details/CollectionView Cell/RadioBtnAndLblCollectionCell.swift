//
//  RadioBtnAndLblCollectionCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 28/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class RadioBtnAndLblCollectionCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    func configCell(rowModel: ProductVariantsFacetValues) {
//        lblName.text = rowModel.value
//        if rowModel.current ?? false {
//            bgView.backgroundColor = UIColor(red: 223/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)
//            lblName.textColor  = UIColor(red: 63/255.0, green: 131/255.0, blue: 135/255.0, alpha: 1.0)
//            ImgView.image = UIImage(named: "checkCircle")
//        } else {
//            bgView.backgroundColor = UIColor(red: 225/255.0, green: 227/255.0, blue: 230/255.0, alpha: 1.0)
//            lblName.textColor = UIColor.lightBlueGrey()
//            ImgView.image = UIImage(named: "unCheckCircle")
//        }
//    }
}
