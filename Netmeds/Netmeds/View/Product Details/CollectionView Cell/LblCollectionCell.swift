//
//  LblCollectionCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class LblCollectionCell: UICollectionViewCell {
    @IBOutlet weak private var lblName: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(name: String) {
        lblName.text = name
        if name == "Rx Required" {
            lblName.backgroundColor = UIColor(red: 223/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)
            lblName.textColor  = UIColor(red: 63/255.0, green: 131/255.0, blue: 135/255.0, alpha: 1.0)
        } else {
            lblName.textColor = UIColor.lightBlueGrey()
            lblName.backgroundColor = UIColor(red: 246/255.0, green: 246/255.0, blue: 247/255.0, alpha: 1.0)
        }
    }
}
