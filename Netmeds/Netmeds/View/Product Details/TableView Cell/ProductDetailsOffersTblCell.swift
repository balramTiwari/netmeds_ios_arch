//
//  ProductDetailsOffersTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsOffersTblCell: UITableViewCell {

    @IBOutlet weak private var lblCouponCode: PaddingLabel!
    @IBOutlet weak private var lblDiscount: PaddingLabel!
    @IBOutlet weak private var lblMessage: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(couponCode: String, discount: String, message: String) {
        lblCouponCode.text = couponCode
        lblDiscount.text = discount
        lblMessage.text = message
    }
}
