//
//  ProductDetailsNotifyMeTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 03/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsNotifyMeTblCell: UITableViewCell {
    
    @IBOutlet weak private var btnNotifyMe: RoundButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String) {
        btnNotifyMe.setTitle(title, for: .normal)
    }
}
