//
//  ProductDetailsExpiryTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsExpiryTblCell: UITableViewCell {
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblSubTitle: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(imgName: String, title: String, subTitle: String) {
        imgView.image = UIImage(named: imgName)
        lblTitle.text = title
        lblSubTitle.text = subTitle
    }
}
