//
//  WebViewTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 03/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit
import WebKit

class WebViewTblCell: UITableViewCell {
    @IBOutlet weak var webView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        webView.scrollView.isScrollEnabled = false
//        webView.tag = 5000
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setWebViewDelegate(delegate: WKNavigationDelegate, urlString: String) {
        if let url = URL(string: urlString) {
            let urlRequest = URLRequest(url: url)
            //            webView.uiDelegate = delegate
            webView.navigationDelegate = delegate
            webView.load(urlRequest)
        }
    }
}
