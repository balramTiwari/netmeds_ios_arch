//
//  ProductDetailsCartTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 03/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsCartTblCell: UITableViewCell {

    @IBOutlet weak private var lblCartCount: PaddingLabel!
    @IBOutlet weak private var btnAddToCart: RoundButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(cartCount: String) {
        lblCartCount.text = cartCount
    }
}

extension ProductDetailsCartTblCell {
    @IBAction private func performIncrementButton() {
    }
    
    @IBAction private func performDecrementButton() {
    }
}
