//
//  ProductDetailsPriceTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsPriceTblCell: UITableViewCell {
    @IBOutlet weak private var lblMessage: PaddingLabel!
    @IBOutlet weak private var lblPackSize: PaddingLabel!
    @IBOutlet weak private var lblPrice: PaddingLabel!
    @IBOutlet weak private var lblStrikePrice: PaddingLabel!
    @IBOutlet weak private var lblOfferPercentage: PaddingLabel!
    @IBOutlet weak private var btnManufacturer: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: ProductDetailsRowModel) {
        resetUI()
        lblPackSize.text = rowModel.packSize
        btnManufacturer.setTitle(rowModel.manufacturerName, for: .normal)
        if rowModel.price > 0 {
            lblPrice.text = "₹ \(rowModel.price)"
            if rowModel.mrp > 0, rowModel.discountRate > 0 {
                let numberValue = NSNumber(value: rowModel.mrp)
                lblStrikePrice.attributedText = numberValue.formatFractionDigits().strikeEffect()
            } else {
                lblPrice.attributedText = NSNumber(value: rowModel.price).formatFractionDigits().setPriceWithMRPText()
            }
        }
        if rowModel.discountRate > 0, let percentage = rowModel.discountRate.convertDoubleToPercentage() {
            lblOfferPercentage.text = "\(percentage)% off"
        }
//        if rowModel.availabilityStatus == "A" {
//            btnAddToCart.isHidden = false
//        }
    }
    
    private func resetUI() {
        lblMessage.text = ""
        lblPackSize.text = ""
        lblPrice.text = ""
        lblStrikePrice.text = ""
        lblOfferPercentage.text = ""
    }
}
