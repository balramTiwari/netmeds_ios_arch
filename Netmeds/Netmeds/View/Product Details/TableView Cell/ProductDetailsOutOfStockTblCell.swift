//
//  ProductDetailsOutOfStockTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 03/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsOutOfStockTblCell: UITableViewCell {

    @IBOutlet weak private var lblMessage: PaddingLabel!
    @IBOutlet weak private var btnOutOfStock: RoundButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, message: String) {
        btnOutOfStock.setTitle(title, for: .normal)
        lblMessage.text = message
    }
}
