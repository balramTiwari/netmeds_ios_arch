//
//  ProductDetailsOffersHeaderTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 04/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsOffersHeaderTblCell: UITableViewCell {

    @IBOutlet weak private var lblTitle: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String) {
        lblTitle.text = title
    }
}
