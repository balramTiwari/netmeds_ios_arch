//
//  ProductDetailsHeaderTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 04/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsHeaderTblCell: UITableViewCell {
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var btnViewAll: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, viewAll: String) {
        lblTitle.text = title
        btnViewAll.setTitle(viewAll, for: .normal)
        btnViewAll.isHidden = !viewAll.isValidString
    }
}
