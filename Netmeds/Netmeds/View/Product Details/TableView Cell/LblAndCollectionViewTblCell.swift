//
//  LblAndCollectionViewTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class LblAndCollectionViewTblCell: UITableViewCell {
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCells()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forSection section: Int = 0, rowModel: ProductDetailsRowModel) {
        lblTitle.text = rowModel.title
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = section
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

extension LblAndCollectionViewTblCell {
    private func registerCollectionViewCells() {
        self.collectionView.registerNibForCell(CollectionViewCellWithImage.self)
    }
}
