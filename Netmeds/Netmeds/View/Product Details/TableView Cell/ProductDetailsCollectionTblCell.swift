//
//  ProductDetailsCollectionTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 04/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class ProductDetailsCollectionTblCell: UITableViewCell {
    @IBOutlet weak private var collectionView: UICollectionView!
    private var headerModel: ProductDetailsHeaderModel = ProductDetailsHeaderModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forSection section: Int = 0, headerModel: ProductDetailsHeaderModel) {
        collectionView.isPagingEnabled = true
        self.headerModel = headerModel
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = section
        collectionView.reloadData()
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.collectionView.layoutIfNeeded()
        switch headerModel.type {
        case .categories:
            return CGSize(width: collectionView.frame.size.width, height: 33)
        case .title:
            return CGSize(width: 50, height: 50)
        case .otcProductImages:
            let value = self.collectionView.collectionViewLayout.collectionViewContentSize
            return CGSize(width: value.width, height: 140)
        default:
            return self.collectionView.frame.size
        }
//        let value = self.collectionView.collectionViewLayout.collectionViewContentSize
//        return CGSize(width: value.width, height: (value.height + 20.0))
    }
}

extension ProductDetailsCollectionTblCell {
    private func registerCollectionViewCell() {
        self.collectionView.registerNibForCell(CollectionViewCellWithImage.self)
        self.collectionView.registerNibForCell(LblCollectionCell.self)
    }
}
