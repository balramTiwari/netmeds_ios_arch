//
//  CartBecomeFirstMemberTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CartBecomeFirstMemberTblViewCell: UITableViewCell {
    @IBOutlet weak private var bgView: RoundView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblSubTitle: PaddingLabel!
    private var gradientLayer: CAGradientLayer!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setGradientLayer()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, subTitle: String) {
        lblTitle.text = title
        lblSubTitle.text = subTitle
    }
}

extension CartBecomeFirstMemberTblViewCell {
    private func setGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bgView.frame
        gradientLayer.colors = [UIColor.getColorFromHexValue(hex: "f85caa", alpha: 1.0).cgColor, UIColor.getColorFromHexValue(hex: "f03479", alpha: 1.0).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.75)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.75)
        gradientLayer.cornerRadius = 8.0
        self.bgView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
