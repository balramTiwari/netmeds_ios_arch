//
//  CartProductTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

enum CartProductTblCellButtonType {
    case chooseQuantity
    case removeItem
    case viewAlternative
}

class CartProductTblViewCell: UITableViewCell {
    @IBOutlet weak private var imgViewProduct: UIImageView!
    @IBOutlet weak private var imgViewRx: UIImageView!
    @IBOutlet weak private var lblName: PaddingLabel!
    @IBOutlet weak private var lblPrice: PaddingLabel!
    @IBOutlet weak private var lblStrikePrice: PaddingLabel!
    @IBOutlet weak private var lblPackSizeType: PaddingLabel!
    @IBOutlet weak private var lblManufacturerName: PaddingLabel!
    @IBOutlet weak private var lblOutOfStock: PaddingLabel!
    @IBOutlet weak private var btnViewAlternative: RoundButton!
    @IBOutlet weak private var btnChooseQuantity: UIButton!
    @IBOutlet weak private var chooseQuantityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var quantitViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var btnRemoveItem: RoundButton!
    @IBOutlet weak var lblSeparatorLine: PaddingLabel!
    @IBOutlet weak private var lblOriginalMedPrice: PaddingLabel!
    @IBOutlet weak private var lblOriginalMedName: PaddingLabel!
    @IBOutlet weak private var btnTapToRevert: UIButton!
    @IBOutlet weak private var revertViewHeightConstraint: NSLayoutConstraint!

//    @IBOutlet weak private var quantityView: RoundView!
    var maxQty: NSNumber = 3
    var qty: NSNumber = 1
    weak var delegate: CartProductTblCellDelegateHandlers?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //    func configButtonTags(section: Int, row: Int) {
    //        let tag = (section * 1000) + row
    //        btnChooseQuantity.tag = tag
    //        btnRemoveItem.tag = tag
    //        btnViewAlternative.tag = tag
    //    }
    
    func configCell(rowModel: CartRowModel, isShowColdStorage: Bool) {
        setDefaultValue()
        if (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) {
            imgViewProduct.image = UIImage(named: "membership_icon" )
        } else if let productImageURL = ConfigManager.shared.configURLPaths?.productImageURL, rowModel.productImagePath.isValidString {
            let url = URL(string: "\(productImageURL)120x120/\(rowModel.productImagePath)")
            self.imgViewProduct.kf.setImage(with: url)
        }
        imgViewRx.isHidden = !rowModel.isRxRequired
        lblName.text = rowModel.productName
        var totalPrice: Float = 0
        if let sellingPrice = rowModel.price as NSNumber? {
            totalPrice = totalPrice + (Float(truncating: sellingPrice) * Float(rowModel.quantity))
            let strPrice = String(format: "%.2f", totalPrice)
            lblPrice.text =  "₹ \(strPrice)"
        }
        if rowModel.discount > 0.0 {
            let strikePrice = NSNumber(value: rowModel.mrp * NSNumber(value: rowModel.quantity).doubleValue)
            lblStrikePrice.attributedText = strikePrice.formatFractionDigits().strikeEffect()
        } else {
            let originalPrice = NSNumber(value: rowModel.price * NSNumber(value: rowModel.quantity).doubleValue)
            lblPrice.attributedText = originalPrice.formatFractionDigits().setPriceWithMRPText()
        }
//        lblPackSizeType.text =  rowModel.packSize
        if rowModel.manufacturerName.isValidString {
            lblManufacturerName.text = kManufacturerShortHand + rowModel.manufacturerName
        }
        btnChooseQuantity.setTitle("Qty \(rowModel.quantity)", for: .normal)
        if (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) {
            imgViewProduct.image = UIImage(named: "membership_icon" )
            lblPackSizeType.text = ""
            showHideChooseQuantityButton(isHidden: true)
            lblManufacturerName.text = ""
        }
        if rowModel.availabilityStatus != "A" || qty.intValue > maxQty.intValue {
            showHideChooseQuantityButton(isHidden: true)
            if rowModel.availabilityStatus != "F" {
                lblOutOfStock.text = "OUT OF STOCK"
                btnViewAlternative.isHidden = !rowModel.isRxRequired
            } else {
                lblOutOfStock.text = ""
                lblManufacturerName.text = ""
            }
        } else {
            lblOutOfStock.text = ""
        }
        if rowModel.isColdStorage && isShowColdStorage {
            lblOutOfStock.text = "COLD STORAGE"
            showHideChooseQuantityButton(isHidden: true)
            btnViewAlternative.isHidden = true
        }
        lblManufacturerName.text = (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) ? "": lblManufacturerName.text
        self.layoutIfNeeded()
    }
    
    //    func configCell(item: CartLineInfoModel, isShowColdStorage: Bool) {
    //        setDefaultValue()
    //        lblName.text = item.displayName
    //        btnChooseQuantity.setTitle("Qty \(item.quantity!)", for: .normal)
    //        var totalPrice: Float = 0
    //        if let sellingPrice = item.sellingPrice as NSNumber? {
    //            totalPrice = totalPrice + (Float(truncating: sellingPrice) * Float(item.quantity ?? 1))
    //            let strPrice = String(format: "%.2f", totalPrice)
    //            lblPrice.text =  "₹ \(strPrice)"
    //        }
    //
    //        if let discount = item.lineProductDiscount, discount > 0.0 {
    //            if let mrp = item.mrp, let qty = item.quantity {
    //                let strikePrice = NSNumber(value: mrp * NSNumber(value: qty).doubleValue)
    //                lblStrikePrice.attributedText = strikePrice.formatFractionDigits().strikeEffect()
    //            }
    //        } else {
    //            lblStrikePrice.isHidden = true
    //            if let sellingPrice = item.sellingPrice, let qty = item.quantity {
    //                let originalPrice = NSNumber(value: sellingPrice * NSNumber(value: qty).doubleValue)
    //                lblPrice.attributedText = originalPrice.formatFractionDigits().setPriceWithMRPText()
    //            }
    //        }
    //        lblPackSizeType.text =  item.packSize
    //        imgViewRx.isHidden = !item.isRxRequired!
    //        if let manufacturerName = item.manufacturerName, manufacturerName.isValidString {
    //            lblManufacturerName.text = "By \(manufacturerName)"
    //        }
    //        if (item.productCode == 2001 || item.productCode == 2002) {
    //            imgViewProduct.image = UIImage(named: "membership_icon" )
    //            //            lblPackSizeType.isHidden = true
    //            quantityView.isHidden = true
    //            lblManufacturerName.isHidden = true
    //        }
    //        //        else if let configUrl = NMUser.shared.configUrl, let imageSuffix = itemData.product_image_path {
    //        //            let url = URL(string: "\(configUrl.result!.product_image_url_base_path!)120x120/\(imageSuffix)")
    //        //            self.thumbnailImage.kf.setImage(with: url)
    //        //        }
    //
    //        if let availableStatus = item.availabilityStatus, availableStatus != "A" || qty.intValue > maxQty.intValue {
    //            quantityView.isHidden = true
    //            if availableStatus != "F" {
    //                lblOutOfStock.text = "OUT OF STOCK"
    //                btnViewAlternative.isHidden = false
    //                //For AlternateSubstitute
    //                //                if let outofStockItemId = item.cartID as NSNumber? {
    //                //                    NMDefault.shared.alternateItemid = outofStockItemId.stringValue
    //                //                }
    //            }
    //        }
    //        if item.isColdStorage ?? false && isShowColdStorage {
    //            lblOutOfStock.text = "COLD STORAGE"
    //            quantityView.isHidden = true
    //        }
    //    }
    
    private func setDefaultValue() {
        lblName.text = ""
        lblPrice.text = ""
        lblStrikePrice.text = ""
        lblPackSizeType.text = ""
        lblManufacturerName.text = ""
        lblOutOfStock.text = ""
//        lblPackSizeType.isHidden = true
        btnViewAlternative.isHidden = true
////        quantityView.isHidden = false
////        lblManufacturerName.isHidden = false
////        lblStrikePrice.isHidden = false
    }
    
    private func showHideChooseQuantityButton(isHidden: Bool) {
        btnChooseQuantity.isHidden = isHidden
        chooseQuantityHeightConstraint.constant = isHidden ? 0.0: 30.0
        quantitViewHeightConstraint.constant = isHidden ? 30.0: 60.0
    }
}

extension CartProductTblViewCell {
    @IBAction private func performChooseQuantityButton(_ sender: UIButton) {
        delegate?.buttonTapped(cell: self, type: .chooseQuantity, sender: sender)
    }
    
    @IBAction private func performRemoveItemButton(_ sender: UIButton) {
        delegate?.buttonTapped(cell: self, type: .removeItem, sender: sender)
    }
    
    @IBAction private func performViewAlternativeButton(_ sender: UIButton) {
        delegate?.buttonTapped(cell: self, type: .viewAlternative, sender: sender)
    }
}
