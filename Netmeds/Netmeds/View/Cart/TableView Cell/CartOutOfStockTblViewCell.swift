//
//  CartOutOfStockTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CartOutOfStockTblViewCell: UITableViewCell {
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var btnTakeAction: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, btnTitle: String) {
        lblTitle.text = title
        btnTakeAction.setTitle(btnTitle, for: .normal)
    }
}
