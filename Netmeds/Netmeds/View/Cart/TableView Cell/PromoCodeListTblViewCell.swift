//
//  PromoCodeListTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 07/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class PromoCodeListTblViewCell: UITableViewCell {
    
    @IBOutlet weak private var layerView: DashLineView!
    @IBOutlet weak private var lblCouponCode: UILabel!
    @IBOutlet weak private var lblOfferDesc: UILabel!
    @IBOutlet weak private var lblYouSave: UILabel!
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblApplied: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(coupon: AllCouponsModel, appliedCouponName: String) {
        lblCouponCode.text = coupon.couponCode
        lblOfferDesc.text = coupon.couponDesc
        lblYouSave.text = "you save \(String(describing: coupon.discount!)) %"
        if coupon.couponCode == appliedCouponName {
            self.layerView.borderColor = UIColor.NMLogoGreenColor()
            self.layerView.dashPattern = [0, 0]
            lblApplied.isHidden = false
            imgView.image = UIImage(named: "selectedPrimary")
        } else {
            self.layerView.borderColor = UIColor.NMSTextColor()
            self.layerView.dashPattern = [10, 4]
            lblApplied.isHidden = true
            imgView.image = UIImage(named: "radio_unchecked")
        }
    }
}
