//
//  CartCouponCodeTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CartCouponCodeTblViewCell: UITableViewCell {

    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var imgViewRighArrow: UIImageView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    @IBOutlet weak private var lblYouSave: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, desc: String, youSaveText: String, isCouponApplied: Bool, isHideRightArrowImg: Bool = false) {
        lblTitle.text = title
        lblDesc.text = desc
        lblYouSave.text = youSaveText
        let imgName: String = isCouponApplied ? "checkCircle": "unCheckCircle"
        imgView.image = UIImage(named: imgName)
        imgViewRighArrow.isHidden = isHideRightArrowImg
        lblTitle.textColor = isCouponApplied ? UIColor.NMSTextColor(with: 1.0): UIColor.NMSTextColor(with: 0.6)
    }
}
