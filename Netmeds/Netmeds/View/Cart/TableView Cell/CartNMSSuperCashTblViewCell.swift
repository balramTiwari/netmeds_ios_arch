//
//  CartNMSSuperCashTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CartNMSSuperCashTblViewCell: UITableViewCell {

    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    @IBOutlet weak private var lblBalance: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
        
    func configCell(title: String, balance: Double, desc: String, isCouponApplied: Bool) {
        lblTitle.text = title
        lblBalance.text = "Use ₹\(balance)"
        lblDesc.text = desc
        let imgName: String = isCouponApplied ? "checkCircle": "unCheckCircle"
        imgView.image = UIImage(named: imgName)
        lblTitle.textColor = isCouponApplied ? UIColor.NMSTextColor(with: 1.0): UIColor.NMSTextColor(with: 0.6)
    }
}
