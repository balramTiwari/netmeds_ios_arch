//
//  PaymentGateWayTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class PaymentGateWayTblCell: UITableViewCell {
    @IBOutlet weak private var imgViewCircle: UIImageView!
    @IBOutlet weak private var imgViewLogo: UIImageView!
    @IBOutlet weak private var lblName: PaddingLabel!
    @IBOutlet weak private var lblBalance: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: PaymentGateWayRowModel) {
        setDefaultValue()
        imgViewCircle.image = UIImage(named: rowModel.isSelected ? "checkCircle": "radio_unchecked")
        imgViewLogo.image = UIImage(named: "net_banking_logo")
        if rowModel.imageURLString.isValidString {
            if let url = URL.init(string: rowModel.imageURLString) {
                imgViewLogo.kf.setImage(with: url)
            }
        }
        lblName.text = rowModel.displayName
        if rowModel.isShowOffer {
            lblDesc.text = rowModel.offer
        }
        if rowModel.key == "MOBIKWIK" || rowModel.key == "FREECHARGE" {
            if rowModel.isLinked, let currentBalance = rowModel.currentBalance as NSNumber? {
                lblBalance.text = "Balance ₹ \(currentBalance.formatFractionDigits())"
            }
        }
    }
    
    private func setDefaultValue() {
        lblName.text = ""
        lblBalance.text = ""
        lblDesc.text = ""
    }
}
