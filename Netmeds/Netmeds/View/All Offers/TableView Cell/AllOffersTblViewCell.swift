//
//  AllOffersTblViewCell.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class AllOffersTblViewCell: UITableViewCell {
    @IBOutlet weak private var imgViewLogo: UIImageView!
    @IBOutlet weak private var imgViewBanner: UIImageView!
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblDesc: UILabel!
    @IBOutlet weak private var lblExpireDate: UILabel!
    @IBOutlet weak private var imgViewBannerHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(offers: AllOffersModel) {
        lblTitle.text = offers.title
        lblDesc.text =  offers.couponDesc
        if let bannerImg = offers.bannerImg, let _ = URL(string: bannerImg) {
            imgViewBannerHeightConstraint.constant = 150
            imgViewBanner.downloadImage(from: bannerImg) { [weak self] (image) in
                self?.imgViewBanner.image = image
                self?.imgViewBanner.layer.cornerRadius = 8
            }
        } else {
            imgViewBannerHeightConstraint.constant = 0
        }
        if let imageURL = offers.image {
            imgViewLogo.downloadImage(from: imageURL) { [weak self] (image) in
                self?.imgViewLogo.image = image
            }
        }
        if let endDate = offers.endDate {
            lblExpireDate.text = endDate.dateString()
        }
    }
}
