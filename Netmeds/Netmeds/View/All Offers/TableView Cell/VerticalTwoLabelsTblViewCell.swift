//
//  VerticalTwoLabelsTblViewCell.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class VerticalTwoLabelsTblViewCell: UITableViewCell {
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblDesc: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(offersDetail: OffersDetailCustomModel) {
        imgView.isHidden = offersDetail.type == .moreDetails ? false: true
        lblTitle.text = offersDetail.title
        lblDesc.text = offersDetail.desc
        if offersDetail.type == .couponCode  {
            lblDesc.textColor =  UIColor.mediumPinkTextColor()
            lblDesc.font = UIFont(name: "Lato-Bold", size: 12)
            lblDesc.alpha = 1.0
        } else {
            lblDesc.textColor =  UIColor.darkGray
            lblDesc.font = UIFont(name: "Lato-Regular", size: 12)
            lblDesc.alpha = 0.6
        }
    }
}
