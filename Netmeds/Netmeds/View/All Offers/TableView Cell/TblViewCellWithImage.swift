//
//  TblViewCellWithImage.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class TblViewCellWithImage: UITableViewCell {

    @IBOutlet weak private var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(imgURL: String) {
        imgView.downloadImage(from: imgURL) { [weak self] (image) in
            self?.imgView.image = image
            self?.imgView.layer.cornerRadius = 8
        }
    }
    
    func configCell(imgName: String) {
        imgView.image = UIImage(named: imgName)
    }
}
