//
//  UploadPrescriptionsCollectionTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class UploadPrescriptionsCollectionTblCell: UITableViewCell {
    @IBOutlet weak private var collectionView: UICollectionView!
    private var rowModel: UploadPrescriptionsRowModel = UploadPrescriptionsRowModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, rowModel: UploadPrescriptionsRowModel = UploadPrescriptionsRowModel()) {
        self.rowModel = rowModel
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = rowModel.type.rawValue
        collectionView.reloadData()
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.collectionView.layoutIfNeeded()
        let width = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        switch rowModel.type {
        case .options:
            let noOfCellsInRow: CGFloat = CGFloat(rowModel.subRowModel.count)
            return CGSize(width: width / noOfCellsInRow - 10, height: 105.0)
        default:
            return CGSize(width: 78.0, height: 132.0)
        }
    }

}

extension UploadPrescriptionsCollectionTblCell {
    private func registerCollectionViewCell() {
        self.collectionView.registerNibForCell(UploadPrescriptionsCollectionCell.self)
        self.collectionView.registerNibForCell(UploadPrescriptionsOptionsCollectionCell.self)
    }
}
