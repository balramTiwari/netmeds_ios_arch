//
//  LabelTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 13/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class LabelTblViewCell: UITableViewCell {

    @IBOutlet weak private var bgView: UIView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(title: String, textAlignment: NSTextAlignment = .left, textColor: UIColor = UIColor.NMSTextColor(with: 1.0), bgColor: UIColor = .white, titleFont: UIFont = UIFont(name: "Lato-Regular", size: 14)!) {
        lblTitle.text = title
        lblTitle.textAlignment = textAlignment
        lblTitle.textColor = textColor
        bgView.backgroundColor = bgColor
    }
}
