//
//  PastPrescriptionsCollectionHeaderCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class PastPrescriptionsCollectionHeaderCell: UICollectionReusableView {
    @IBOutlet weak private var lblTitle: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(title: String) {
        lblTitle.text = title
    }
}
