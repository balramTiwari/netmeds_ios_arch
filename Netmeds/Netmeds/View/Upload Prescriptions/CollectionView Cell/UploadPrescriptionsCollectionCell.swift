//
//  UploadPrescriptionsCollectionCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

//protocol NMSPrescriptionCellDelegate:class {
//    func actionForClearImg(controller:NMSPrescriptionCell, cell: NMSPrescriptionCell)
//}

class UploadPrescriptionsCollectionCell: UICollectionViewCell {
    
    weak var delegate: UploadPrescriptionsCollectionViewCellDelegateHandlers?
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var btnClose: UIButton!
    @IBOutlet weak private var indicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.layer.cornerRadius = 10
        imgView.clipsToBounds = true
        imgView.layer.borderWidth = 3
        imgView.layer.borderColor = UIColor.white.cgColor
    }

    func configCell(imgURL: String = "", image: UIImage? = nil, btnTag: Int) {
        btnClose.tag = btnTag
        if let img = image {
            imgView.image = img
        }
        if imgURL.isValidString {
            indicatorView.startAnimating()
            imgView.downloadImage(from: kDownloadPrescriptionsEndPoint + "\(imgURL)") { [weak self] (image) in
                self?.imgView.image = image
                self?.indicatorView.stopAnimating()
            }
        }
    }
}

extension UploadPrescriptionsCollectionCell {
    @IBAction private func performCloseButton(sender: UIButton) {
        DispatchQueue.main.async {
            if let topMostViewCtrl = UIApplication.topViewController() {
                topMostViewCtrl.showAlertWithTitle("", message: "Are you sure you want to delete this uploaded prescription?", actionTitles: ["No", "Yes"], actionStyles: [UIAlertAction.Style.default, UIAlertAction.Style.cancel]) { (buttonTapped) in
                    if buttonTapped == "Yes" {
//                        guard let cell = (sender as AnyObject).superview?.superview as? UploadPrescriptionsCollectionCell else { return }
                        self.delegate?.btnRemovePrescriptionTapped(cell: self, tag: sender.tag)
                    }
                }
            }
        }
    }
}
