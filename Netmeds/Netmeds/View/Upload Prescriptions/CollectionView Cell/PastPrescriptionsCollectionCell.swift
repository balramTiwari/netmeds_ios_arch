//
//  PastPrescriptionsCollectionCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class PastPrescriptionsCollectionCell: UICollectionViewCell {
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var btnChoose: UIButton!
    @IBOutlet weak private var indicatorView: UIActivityIndicatorView!
    weak var delegate: PastPrescriptionsCollectionViewCellDelegateHandlers?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(imgURL: String, isSelected: Bool = false) {
        btnChoose.isSelected = isSelected
        indicatorView.startAnimating()
        imgView.downloadImage(from: kDownloadPrescriptionsEndPoint + "\(imgURL)") { [weak self] (image) in
            self?.imgView.image = image
            self?.indicatorView.stopAnimating()
        }
    }
    
    @IBAction private func performChooseButton(sender: UIButton) {
        self.delegate?.btnChoosePrescriptionTapped(cell: self)
    }
}
