//
//  UploadPrescriptionsOptionsCollectionCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class UploadPrescriptionsOptionsCollectionCell: UICollectionViewCell {
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(title: String, imgName: String) {
        lblTitle.text = title
        imgView.image = UIImage(named: imgName)
    }
}
