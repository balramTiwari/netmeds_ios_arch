//
//  CartHeaderTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CartHeaderTblViewCell: UITableViewCell {
    
    @IBOutlet weak private var bgView: UIView!
    @IBOutlet weak private var lblName: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundingCorners()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    private func roundingCorners() {
        var bounds = screenSize
        bounds.size.width = screenWidth - 16
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        bgView.layer.mask = maskLayer
    }
    
    func configCell(title: String, bgColor: UIColor = .white, bottomInset:CGFloat = 16.0, textColor: UIColor = UIColor.NMSTextColor(with: 0.6)) {
        lblName.text = title
        bgView.backgroundColor = bgColor
        lblName.bottomInset = bottomInset
        lblName.textColor = textColor
    }

//    func configCell(headerModel: CartHeaderModel) {
//        lblName.text = headerModel.title
//    }
//    
//    func configCell(headerModel: MyOrdersDetailHeaderModel) {
//        lblName.text = headerModel.name
//    }
//    
//    func configCell(headerModel: TrackOrderHeaderModel) {
//        lblName.text = headerModel.name
//    }
}
