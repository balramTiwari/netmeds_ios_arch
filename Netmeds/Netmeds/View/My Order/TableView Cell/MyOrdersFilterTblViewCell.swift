//
//  MyOrdersFilterTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersFilterTblViewCell: UITableViewCell {

    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(status: String, selectedStatus: String) {
        lblTitle.text = status
        imgView.image = status == selectedStatus ? UIImage(named: "checkCircle"): UIImage(named: "radio_unchecked")
    }
}

