//
//  MyOrdersHeaderTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 08/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersHeaderTblViewCell: UITableViewCell {

    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblStatus: UILabel!
    @IBOutlet weak private var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundingCorners()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(headerModel: MyOrdersHeaderModel) {
        lblName.text = headerModel.name
        lblStatus.text = headerModel.statusName
        lblStatus.textColor = headerModel.statusColor
    }
    
    private func roundingCorners() {
        var bounds = screenSize
        bounds.size.width = screenWidth - 16
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        bgView.layer.mask = maskLayer
    }
}
