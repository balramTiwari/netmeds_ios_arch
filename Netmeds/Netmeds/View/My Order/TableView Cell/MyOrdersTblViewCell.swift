//
//  MyOrdersTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 08/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersTblViewCell: UITableViewCell {

    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: MyOrdersRowModel) {
        lblName.text = rowModel.productName
        imgView.image = UIImage(named: rowModel.imgName)
    }
}
