//
//  MyOrdersFooterTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 08/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersFooterTblViewCell: UITableViewCell {
    
    @IBOutlet weak var btnViewDetails: UIButton!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak private var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundingCorners()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(footerModel: MyOrdersFooterModel, section: Int) {
        btnViewDetails.backgroundColor = footerModel.viewDetailsBtnBgColor
        btnViewDetails.setTitleColor(footerModel.viewDetailsBtnTitleColor, for: .normal)
        btnViewDetails.isEnabled = footerModel.isViewDetailsBtnEnabled
        btnTrackOrder.setTitle(footerModel.trackOrderBtnText, for: .normal)
        btnTrackOrder.backgroundColor = footerModel.trackOrderBtnBgColor
        btnTrackOrder.setTitleColor(footerModel.trackOrderBtnTitleColor, for: .normal)
        btnTrackOrder.isEnabled = footerModel.isTrackOrderBtnEnabled
        btnViewDetails.tag = section
        btnTrackOrder.tag = section
    }
    
    private func roundingCorners() {
        var bounds = screenSize
        bounds.size.width = screenWidth - 16
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
//        self.contentView.layer.mask = maskLayer
        self.layer.mask = maskLayer
    }
}
