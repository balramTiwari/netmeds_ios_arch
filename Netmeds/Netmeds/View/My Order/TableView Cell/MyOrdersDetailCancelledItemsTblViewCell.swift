//
//  MyOrdersDetailCancelledItemsTblViewCell.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersDetailCancelledItemsTblViewCell: UITableViewCell {
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblQuantity: UILabel!
    @IBOutlet weak private var lblPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: MyOrdersDetailRowModel) {
        lblName.text = rowModel.name
        lblQuantity.text = rowModel.quantity
        lblPrice.text = rowModel.desc
    }
}
