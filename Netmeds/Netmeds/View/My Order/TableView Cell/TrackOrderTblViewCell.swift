//
//  TrackOrderTblViewCell.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class TrackOrderTblViewCell: UITableViewCell {
    
    @IBOutlet weak private var separatorView: UIView!
    @IBOutlet weak private var lblOrderStatus: UILabel!
    @IBOutlet weak private var lblDate: UILabel!
    @IBOutlet weak private var imgViewStatus: UIImageView!
    @IBOutlet weak private var orderImageBgView: RoundView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: TrackOrderRowModel, row: Int, totalCount: Int) {
        lblOrderStatus.text = rowModel.name
        lblDate.text = rowModel.desc
        imgViewStatus.image = UIImage(named: rowModel.imgName)
        separatorView.isHidden = (row == (totalCount - 1)) ? true : false
        if rowModel.progressStatus {
            lblOrderStatus.font = UIFont(name:"Lato-Bold", size: 14.0)
            lblOrderStatus.textColor = UIColor.NMSTextColor(with: 1.0)
            orderImageBgView.backgroundColor = UIColor.getColorFromHexValue(hex: "24aeb1", alpha: 0.15)
            orderImageBgView.backgroundColor?.withAlphaComponent(0.08)
        } else {
            lblOrderStatus.font = UIFont(name:"Lato-Regular", size: 14.0)
            lblOrderStatus.textColor = UIColor.NMSTextColor()
            orderImageBgView.backgroundColor = UIColor.getColorFromHexValue(hex: "F6F6F7", alpha: 1.0)
            orderImageBgView.alpha = 1.0
        }
    }
}
