//
//  MyOrdersDetailFooterTblViewCell.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersDetailFooterTblViewCell: UITableViewCell {

    @IBOutlet weak private var btnShowDetails: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(footerModel: MyOrdersDetailFooterModel) {
        btnShowDetails.setTitle(footerModel.name, for: .normal)
    }
}
