//
//  FirstMembershipTblCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 21/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class FirstMembershipTblCell: UITableViewCell {
    
    @IBOutlet weak private var imgViewLogo: UIImageView!
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    @IBOutlet weak private var btnImgTitle: UIButton!
    @IBOutlet weak private var btnUpload: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(headerModel: HomeHeaderModel) {
        lblTitle.text = headerModel.title
        lblDesc.text = headerModel.desc
        btnUpload.setTitle(headerModel.btnUploadText, for: .normal)
        btnImgTitle.setTitle(headerModel.orderDurationText, for: .normal)
    }
}
