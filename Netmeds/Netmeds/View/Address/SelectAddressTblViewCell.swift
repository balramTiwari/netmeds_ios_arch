//
//  SelectAddressTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class SelectAddressTblViewCell: UITableViewCell {

    @IBOutlet weak private var  borderView: UIView!
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblAddress: UILabel!
    @IBOutlet weak private var stackViewButtons: UIStackView!
    @IBOutlet weak private var imgViewCheck: UIImageView!
    @IBOutlet weak private var btnEdit: RoundButton!
    @IBOutlet weak private var btnDelete: RoundButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(list: AllAddressInfoModel) {
        lblName.text = "\(list.firstName!) \(list.lastName!)"
        let cityName = "\(list.city!) - \(list.pinCode!)"
        let telephoneNo = "+91 - \(list.mobileNo!)"
        lblAddress.text = "\(list.line1!),\(list.line3!)\n\(cityName),\n\(telephoneNo)"
        if list.isSelected {
            btnEdit.isHidden = false
            btnDelete.isHidden = true
            imgViewCheck.image = UIImage(named: "checkCircle")
            borderView.layer.borderColor = UIColor.NMLogoGreenColor().cgColor
        } else {
            btnEdit.isHidden = false
            btnDelete.isHidden = false
            imgViewCheck.image = UIImage(named: "unCheckCircle")
            borderView.layer.borderColor = UIColor.white.cgColor
        }
    }
}
