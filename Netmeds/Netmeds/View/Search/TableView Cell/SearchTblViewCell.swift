//
//  SearchTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit
import Kingfisher

class SearchTblViewCell: UITableViewCell {
    
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblCategories: UILabel!
    @IBOutlet weak private var lblDrugType: UILabel!
    @IBOutlet weak private var lblPrice: UILabel!
    @IBOutlet weak private var lblStrikePrice: UILabel!
    @IBOutlet weak private var lblPackSizeType: UILabel!
    @IBOutlet weak private var btnManufacturerName: UIButton!
    @IBOutlet weak private var btnAddToCart: UIButton!
    @IBOutlet weak private var lblCartSeparator: UILabel!
    @IBOutlet weak private var strikePriceWidth: NSLayoutConstraint!
    @IBOutlet weak private var rxRequireWidth: NSLayoutConstraint!
    weak var delegate: SearchTblViewCellDelegateHandlers?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(alogliaSearchModel: AlgoliaSearchHitsModel) {
        lblName.text = alogliaSearchModel.displayName
        if alogliaSearchModel.rxRequired == 1 {
            self.lblDrugType.isHidden =  false
            self.rxRequireWidth.constant = 70
        } else{
            self.lblDrugType.isHidden =  true
            self.rxRequireWidth.constant = 0
        }
        if let manufacturerName = alogliaSearchModel.manufacturerName, manufacturerName.isValidString {
            btnManufacturerName.setTitle(kManufacturerShortHand + manufacturerName, for: .normal)
        } else {
            btnManufacturerName.setTitle("", for: .normal)
        }
        var bindURLString: String?
        if let thumbnailURL = alogliaSearchModel.thumbnailURL, thumbnailURL.isValidString {
            if thumbnailURL.hasPrefix("https") {
                bindURLString = thumbnailURL
            } else {
                bindURLString = "https://s1-meds.netmeds.com/" + thumbnailURL
            }
        }
        imgView.image = UIImage(named: "no_experience")
        if let urlString = bindURLString {
            imgView.downloadImage(from: urlString) { [weak self] (image) in
                self?.imgView.image = image
            }
        }
        //        if let imagePathValue = item.thumbnailURL {
        //            if imagePathValue.hasPrefix("https"), let url = URL.init(string: imagePathValue) {
        //                self.thumbnailImage.kf.setImage(with: url, placeholder: UIImage(named: "no_experience"))
        //            } else if let imageBaseURL = NMUser.shared.configUrl?.result?.catalog_image_url_base_path {
        //                if let url = URL(string: imageBaseURL + imagePathValue) {
        //                    self.thumbnailImage.kf.setImage(with: url, placeholder: UIImage(named: "no_experience"))
        //                }
        //            }
        //        }
        
        var sellingPrice: Float = 0.0
        var sellingPriceNum = NSNumber(value: 0)
        if let sellingPriceValue = alogliaSearchModel.sellingPrice {
            let sellingPriceNumber: NSNumber = NSNumber(value: sellingPriceValue)
            sellingPrice = sellingPriceValue
            sellingPriceNum = sellingPriceNumber
            self.lblPrice.text = "₹ \(sellingPriceNumber.formatFractionDigits())"
        }
        self.lblStrikePrice.isHidden = true
        if let strikePrice = alogliaSearchModel.price {
            if sellingPrice == strikePrice {
                self.lblStrikePrice.isHidden = true
                self.strikePriceWidth.constant = 0
                self.lblPrice.attributedText = sellingPriceNum.formatFractionDigits().setPriceWithMRPText()
            } else {
                let strikePriceNumber: NSNumber = NSNumber(value: strikePrice)
                let strikePriceStr = "\(strikePriceNumber.formatFractionDigits())"
                self.lblStrikePrice.isHidden = false
                self.lblStrikePrice.attributedText = strikePriceStr.strikeEffect()
                let size =  self.lblStrikePrice.intrinsicContentSize
                self.strikePriceWidth.constant = size.width
            }
        } else {
            self.lblPrice.attributedText = sellingPriceNum.formatFractionDigits().setPriceWithMRPText()
        }
        self.lblCategories.text = ""
        if let categories = alogliaSearchModel.categories, categories.count > 0 {
            self.lblCategories.text = " \(categories.first ?? "") "
            self.lblCategories.isHidden = false
        } else {
            self.lblCategories.isHidden = true
        }
        if let availableStatus  = alogliaSearchModel.availabilityStatus, availableStatus == "A" {
            self.btnAddToCart.isHidden = false
            self.lblCartSeparator.isHidden = false
        } else {
            self.btnAddToCart.isHidden = true
            self.lblCartSeparator.isHidden = true
        }
        if let packLabel = alogliaSearchModel.packLabel, packLabel.isValidString {
            self.lblPackSizeType.isHidden = false
            self.lblPackSizeType.text = packLabel
        } else {
            self.lblPackSizeType.isHidden = true
        }
    }
    
    func configCellFromProductList(product: ProductsModel) {
        lblName.text = product.displayName
        self.lblDrugType.isHidden = !product.rxRequired!
        self.rxRequireWidth.constant = product.rxRequired! ? 70: 0.0
        btnManufacturerName.setTitle("", for: .normal)
        if let manufacturer = product.manufacturer, let name = manufacturer.name, name.isValidString {
            btnManufacturerName.setTitle(kManufacturerShortHand + name, for: .normal)
        }
        imgView.image = UIImage(named: "no_experience")
        let productImageURL = ConfigManager.shared.configURLPaths?.productImageURL
        let imgPX = "120x120/"
        if let imgStr = product.imagePaths?.first, let productImagePath = productImageURL, let url = URL.init(string: "\(productImagePath)\(imgPX)\(imgStr)") {
            imgView.kf.setImage(with: url, placeholder: UIImage(named: "no_experience"))
        }
        if let price = product.sellingPrice {
            self.lblPrice.text = "₹ \(NSNumber(value: price).formatFractionDigits())"
        }
        if let mrp = product.mrp, let discountRate = product.discountRate, discountRate != 0.0 {
            self.lblStrikePrice.isHidden = false
            let numberValue = NSNumber(value: mrp)
            self.lblStrikePrice.attributedText = numberValue.formatFractionDigits().strikeEffect()
        } else {
            self.lblStrikePrice.isHidden = true
            if let price = product.sellingPrice {
                self.lblPrice.attributedText = NSNumber(value: price).formatFractionDigits().setPriceWithMRPText()
            }
        }

        self.lblCategories.text = ""
        if let categories = product.categories, let firstCategory = categories.first, let breadCrumbs = firstCategory.breadCrumbs {
            var names = [String]()
            for value in breadCrumbs {
                if value.level == 1 || value.level == 2 {
                    if let name = value.name {
                        names.append(name)
                    }
                }
            }
            self.lblCategories.isHidden = !(names.count > 0)
            self.lblCategories.text = names.joined(separator: " ")
        } else {
            self.lblCategories.isHidden = true
        }

        if let availableStatus  = product.availabilityStatus, availableStatus == "A" {
            self.btnAddToCart.isHidden = false
            self.lblCartSeparator.isHidden = false
        } else {
            self.btnAddToCart.isHidden = true
            self.lblCartSeparator.isHidden = true
        }
        if let packLabel = product.packLabel, packLabel.isValidString {
            self.lblPackSizeType.isHidden = false
            self.lblPackSizeType.text = packLabel
        } else {
            self.lblPackSizeType.isHidden = true
        }
    }

}

extension SearchTblViewCell {
    @IBAction private func performAddToCartButton() {
        delegate?.btnAddToCartTapped(cell: self)
    }
    
    @IBAction private func performManufacturerNameButton() {
        delegate?.btnManufacturerNameTapped(cell: self)
    }
}
