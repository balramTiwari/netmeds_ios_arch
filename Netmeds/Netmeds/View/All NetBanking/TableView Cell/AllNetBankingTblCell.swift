//
//  AllNetBankingTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class AllNetBankingTblCell: UITableViewCell {
    @IBOutlet weak private var lblName: PaddingLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(name: String) {
        lblName.text = name
    }
}
