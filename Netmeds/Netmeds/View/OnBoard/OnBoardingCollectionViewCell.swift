//
//  OnBoardingCollectionViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblDescription: UILabel!
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var widthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth
    }

    func configCell(onBoarding: ConfigOnBoardingModel) {
        lblTitle.text = onBoarding.title
        lblDescription.text = onBoarding.desc
        
    }
}
