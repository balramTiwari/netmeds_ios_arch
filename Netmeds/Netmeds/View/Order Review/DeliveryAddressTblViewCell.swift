//
//  DeliveryAddressTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class DeliveryAddressTblViewCell: UITableViewCell {

    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblAddress: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: CartRowModel) {
        lblName.text = rowModel.productName
        lblAddress.text = rowModel.desc
    }
    
    func configCell(rowModel: MyOrdersDetailRowModel) {
        lblName.text = rowModel.name
        lblAddress.text = rowModel.desc
    }
    
    func configCell(rowModel: TrackOrderRowModel) {
        lblName.text = rowModel.name
        lblAddress.text = rowModel.desc
    }
}
