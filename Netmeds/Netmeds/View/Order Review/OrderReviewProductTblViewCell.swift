//
//  CartProductTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit


class OrderReviewProductTblViewCell: UITableViewCell {
    @IBOutlet weak private var imgViewProduct: UIImageView!
    @IBOutlet weak private var imgViewRx: UIImageView!
    @IBOutlet weak private var lblName: PaddingLabel!
    @IBOutlet weak private var lblPrice: PaddingLabel!
    @IBOutlet weak private var lblStrikePrice: PaddingLabel!
    @IBOutlet weak private var lblPackSizeType: PaddingLabel!
    @IBOutlet weak private var lblManufacturerName: PaddingLabel!
    @IBOutlet weak var btnSellerName: UIButton!
    @IBOutlet weak private var lblExpiryDate: PaddingLabel!
    @IBOutlet weak private var lblQuantity: PaddingLabel!
    @IBOutlet weak private var lblDeliveryDate: PaddingLabel!
//    @IBOutlet weak private var lblOutOfStock: PaddingLabel!
    @IBOutlet weak private var lblSeparatorLine: PaddingLabel!
    @IBOutlet weak private var sellerNameHeightConstraint: NSLayoutConstraint!

    var qty: NSNumber = 1
    var maxQty: NSNumber = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(rowModel: CartRowModel, index: Int) {
        setDefaultValue()
        btnSellerName.tag = index
        if (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) {
            imgViewProduct.image = UIImage(named: "membership_icon" )
        } else if let productImageURL = ConfigManager.shared.configURLPaths?.productImageURL, rowModel.productImagePath.isValidString {
            let url = URL(string: "\(productImageURL)120x120/\(rowModel.productImagePath)")
            self.imgViewProduct.kf.setImage(with: url)
        }
        imgViewRx.isHidden = !rowModel.isRxRequired
        lblName.text = rowModel.productName
        var totalPrice: Float = 0
        if let sellingPrice = rowModel.price as NSNumber? {
            totalPrice = totalPrice + (Float(truncating: sellingPrice) * Float(rowModel.quantity))
            let strPrice = String(format: "%.2f", totalPrice)
            lblPrice.text =  "₹ \(strPrice)"
        }
        if rowModel.discount > 0.0 {
            let strikePrice = NSNumber(value: rowModel.mrp * NSNumber(value: rowModel.quantity).doubleValue)
            lblStrikePrice.attributedText = strikePrice.formatFractionDigits().strikeEffect()
        } else {
            lblStrikePrice.text = ""
            let originalPrice = NSNumber(value: rowModel.price * NSNumber(value: rowModel.quantity).doubleValue)
            lblPrice.attributedText = originalPrice.formatFractionDigits().setPriceWithMRPText()
        }
        lblPackSizeType.text = rowModel.packSize
        if rowModel.manufacturerName.isValidString {
            lblManufacturerName.text = kManufacturerShortHand + rowModel.manufacturerName
        }
        if rowModel.sellerName.isValidString {
            let sellerName = "Seller: " + rowModel.sellerName
            let textRange = NSMakeRange(8, rowModel.sellerName.count)
            let attributedString = NSMutableAttributedString(string: sellerName)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor(with: 0.6)],
                                           range: NSRange(location: 0, length: sellerName.count))
            btnSellerName.setAttributedTitle(attributedString, for: .normal)
            sellerNameHeightConstraint.constant = 20
        }
        lblQuantity.text = "Qty \(rowModel.quantity)"
        if rowModel.expiryDate.isValidString {
            lblExpiryDate.text = "Expiry: " + rowModel.expiryDate
            sellerNameHeightConstraint.constant = 20
        }
        if rowModel.deliveryEstimatedDate.isValidString {
            let deliveryEstDate = "Delivery: \(rowModel.deliveryEstimatedDate)"
            lblDeliveryDate.attributedText = deliveryEstDate.changeAttribute(boldText: rowModel.deliveryEstimatedDate)
        }
        let rQuantity = rowModel.quantity
        let rmaxQtyInOrder = rowModel.maxQtyInOrder
        self.qty = NSNumber(value: rQuantity)
        self.maxQty = NSNumber(value: rmaxQtyInOrder)
        if rowModel.availabilityStatus.isValidString, rowModel.isOutOfStock {
            if rowModel.availabilityStatus == "F" || (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) {
//                lblOutOfStock.text = ""
                lblDeliveryDate.text = ""
                lblQuantity.text = ""
                lblManufacturerName.text = ""
                btnSellerName.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
                sellerNameHeightConstraint.constant = 0
                lblExpiryDate.text = ""
            }
            else if (rowModel.availabilityStatus == "S") || self.qty.intValue > self.maxQty.intValue || rowModel.isOutOfStock {
//                lblOutOfStock.text = "OUT OF STOCK"
                lblDeliveryDate.text = ""
                lblQuantity.text = ""
            } else {
//                lblOutOfStock.text = ""
            }
        }
        if (rowModel.productCode == kPrimeProduct2001 || rowModel.productCode == kPrimeProduct2002) {
            lblQuantity.text = ""
            btnSellerName.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
            sellerNameHeightConstraint.constant = 0
            lblExpiryDate.text = ""
            lblDeliveryDate.text = ""
            lblPackSizeType.text = ""
            lblManufacturerName.text = ""
        }
    }
    
    private func setDefaultValue() {
        lblName.text = ""
        lblPrice.text = ""
        lblStrikePrice.text = ""
        lblPackSizeType.text = ""
        lblManufacturerName.text = ""
        btnSellerName.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
        sellerNameHeightConstraint.constant = 0
        lblExpiryDate.text = ""
        lblQuantity.text = ""
        lblDeliveryDate.text = ""
//        lblOutOfStock.text = ""
    }
    
    func configCell(rowModel: MyOrdersDetailRowModel) {
        setDefaultValue()
        lblName.text = rowModel.name
        lblQuantity.text = rowModel.quantity
        lblPrice.text = rowModel.desc
        imgViewRx.isHidden = rowModel.hasRxProduct
    }
}

