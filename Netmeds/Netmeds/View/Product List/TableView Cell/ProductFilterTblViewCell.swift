//
//  ProductFilterTblViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class ProductFilterTblViewCell: UITableViewCell {
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(filterModel: ProductFilterModel) {
        lblTitle.text = filterModel.name
        imgView.image = UIImage(named: "keyboardarrowrightactivate")
    }
    
    func configCell(filterValuesModel: ProductFilterValuesModel) {
        lblTitle.text = filterValuesModel.name
        let imageName: String = filterValuesModel.isSelected ? "checkboxActive": "checkboxDisabled"
        imgView.image = UIImage(named: imageName)
    }
}
