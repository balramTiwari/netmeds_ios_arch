//
//  ProductSortCollectionViewCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class ProductSortCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var cornerView: RoundView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(sortModel: ProductSortModel) {
        lblTitle.text = sortModel.name
        cornerView.layer.borderColor = sortModel.isSelected ? UIColor.NMLogoGreenColor().cgColor: UIColor.NMSTextColor(with: 0.1).cgColor
    }
}
