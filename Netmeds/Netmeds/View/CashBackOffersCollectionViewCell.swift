//
//  CashBackOffersCollectionViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 21/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CashBackOffersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblDesc: UILabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(rowModel: HomeRowModel) {
        lblTitle.text = rowModel.title
        lblDesc.text = rowModel.desc
        imgView.downloadImage(from: rowModel.imgURL) { [weak self] (image) in
            self?.imgView.image = image
        }
    }
}

