//
//  HomeHeaderTblCell.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class HomeHeaderTblCell: UITableViewCell {
    @IBOutlet weak private var lblTitle: PaddingLabel!
    @IBOutlet weak private var lblSubTitle: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    @IBOutlet weak var btnViewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(headerModel: HomeHeaderModel, type: HomeBannersType) {
        updateContentViewBgColor(type: type)
        lblTitle.text = headerModel.title
        lblSubTitle.text = headerModel.subTitle
        lblDesc.text = headerModel.desc
        btnViewAll.setTitle(headerModel.viewAllBtnText, for: .normal)
        btnViewAll.isHidden = !headerModel.viewAllBtnText.isValidString
        btnViewAll.tag = type.rawValue
    }
    
    private func updateContentViewBgColor(type: HomeBannersType) {
        switch type {
        case .bigSales:
            self.contentView.backgroundColor = .clear
        default:
            self.contentView.backgroundColor = .white
        }
    }
}
