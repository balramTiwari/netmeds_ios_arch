//
//  TblCellWithCollectionView.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class TblCellWithCollectionView: UITableViewCell {

    @IBOutlet weak private var collectionView: UICollectionView!
    private var headerModel: HomeHeaderModel = HomeHeaderModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
        
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forSection section: Int = 0, headerModel: HomeHeaderModel = HomeHeaderModel()) {
        self.headerModel = headerModel
        updateContentViewBgColor()
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = section
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.collectionView.layoutIfNeeded()
        let height = headerModel.rowHeight + 8.0
        var width = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        width = headerModel.rowModel.count > 1 ? width - 16: width
        switch headerModel.type {
        case .offers:
            return CGSize(width: width - 40, height: height)
        case .cashBackOffers:
            return CGSize(width: width / 1.2, height: height)
        case .categories:
            return CGSize(width: width / 4 - 10, height: height)
            case .bigSales:
                return CGSize(width: 130.0, height: height)
        case .wellness, .healthConcerns:
            return CGSize(width: height, height: height)
        case .healthExperts:
            return CGSize(width: width / 1.7, height: height)
        case .consultationPopularConcerns, .diagnosticsPopularConcerns:
            return CGSize(width: width / 3.0, height: height)
        default:
            return CGSize(width: width, height: height)
        }
    }
}

extension TblCellWithCollectionView {
    private func registerCollectionViewCell() {
        self.collectionView.registerNibForCell(CollectionViewCellWithImage.self)
        self.collectionView.registerNibForCell(CollectionViewCellWithImageAndLbl.self)
        self.collectionView.registerNibForCell(CollectionViewCellWithVerticalImageAndLbl.self)
        self.collectionView.registerNibForCell(CashBackOffersCollectionViewCell.self)
        self.collectionView.registerNibForCell(HomeCategoriesCollectionViewCell.self)
        self.collectionView.registerNibForCell(HomeRewardsCollectionViewCell.self)
        self.collectionView.registerNibForCell(HomeWinterCollectionViewCell.self)
    }
    
    private func updateContentViewBgColor() {
        switch headerModel.type {
        case .offers, .cashBackOffers, .consultation, .bigSales:
            self.contentView.backgroundColor = .clear
        default:
            self.contentView.backgroundColor = .white
        }
    }
}
