//
//  CollectionViewCellWithVerticalImageAndLbl.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CollectionViewCellWithVerticalImageAndLbl: UICollectionViewCell {

    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(rowModel: HomeRowModel) {
        lblName.text = rowModel.title
        imgView.downloadImage(from: rowModel.imgURL) { [weak self] (image) in
            self?.imgView.image = image
        }
    }
}
