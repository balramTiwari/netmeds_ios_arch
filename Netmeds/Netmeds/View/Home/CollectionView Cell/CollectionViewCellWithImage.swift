//
//  CollectionViewCellWithImage.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class CollectionViewCellWithImage: UICollectionViewCell {

    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(imgURL: String) {
        imgView.downloadImage(from: imgURL) { [weak self] (image) in
            self?.imgView.image = image
        }
    }

    func configCell(imgName: String) {
        imgView.image = UIImage(named: imgName)
    }
    
    func configCell(image: UIImage?) {
        imgView.image = image
    }
    
//    func configCell(rowModel: HomeRowModel) {
//        imgView.downloadImage(from: rowModel.imgURL) { [weak self] (image) in
//            self?.imgView.image = image
//        }
//    }
//
//    func configCell(banner: ProductListBannersInfoModel) {
//        imgView.downloadImage(from: banner.imageURL!) { [weak self] (image) in
//            self?.imgView.image = image
//        }
//    }
//
//    func configCell(rowModel: ProductDetailsRowModel) {
//        imgView.contentMode = .scaleAspectFit
//        imgView.downloadImage(from: rowModel.imgURL) { [weak self] (image) in
//            self?.imgView.image = image
//        }
//    }
}
