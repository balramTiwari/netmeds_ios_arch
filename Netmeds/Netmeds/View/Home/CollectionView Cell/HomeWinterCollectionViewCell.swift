//
//  HomeWinterCollectionViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 21/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class HomeWinterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblOffer: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(rowModel: HomeRowModel) {
        lblName.text = rowModel.title
        lblOffer.text = rowModel.desc
    }
}
