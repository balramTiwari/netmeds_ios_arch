//
//  HomeCategoriesCollectionViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 21/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class HomeCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    func configCell(rowModel: HomeRowModel) {
//        lblTitle.text = rowModel.title
//        imgView.image = UIImage(named: rowModel.imgName)
//    }
    
    func configCell(title: String, imgName: String) {
        lblTitle.text = title
        imgView.image = UIImage(named: imgName)
    }
}
