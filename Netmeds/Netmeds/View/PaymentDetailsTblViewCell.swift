//
//  PaymentDetailsTblViewCell.swift
//  Netmeds
//
//  Created by Netmeds1 on 25/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class PaymentDetailsTblViewCell: UITableViewCell {

    @IBOutlet weak private var bgView: UIView!
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblAmount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(title: String, price: String, type: CartPageRowType) {
        lblName.text = title
        lblAmount.text = price
        if type == .totalSavings {
            let font =  UIFont(name: "Lato-Bold", size: 13)
            lblName.font = font
            lblAmount.font = font
            bgView.backgroundColor = UIColor.lightGreenBgColor()
            lblName.textColor = UIColor.textGreenColor()
            lblAmount.textColor = UIColor.textGreenColor()
        } else {
            let font =  UIFont(name: "Lato", size: 13)
            lblName.font = font
            lblAmount.font = font
            bgView.backgroundColor = UIColor.white
            lblName.textColor = UIColor.black
            lblAmount.textColor = UIColor.black
        }
        if type == .deliveryCharges {
            let separatedArray = price.components(separatedBy: ",")
            if separatedArray.count == 2 {
                lblAmount.attributedText = price.makeStrikePrice(strArray: separatedArray)
            }
        }
    }
    
    func configCell(rowModel: MyOrdersDetailRowModel, row: Int) {
        lblName.text = rowModel.name
        lblAmount.text = rowModel.desc
        bgView.backgroundColor =  row % 2 == 0 ? .white: .clear
    }
}
