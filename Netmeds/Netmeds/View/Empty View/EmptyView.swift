//
//  EmptyView.swift
//  Netmeds
//
//  Created by SANKARLAL on 07/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

enum EmptyViewType: Int {
    case cart = 1
    case search
    case noInternet
    case none
}

class EmptyView: UIView {
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblMessage: PaddingLabel!
    @IBOutlet weak private var lblDesc: PaddingLabel!
    @IBOutlet weak private var btnSearch: RoundButton!
    private var type: EmptyViewType = .none
    
    // MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

// MARK: All Functions
extension EmptyView {
    class func instanceFromNib() -> UIView {
        guard let nibView =  UINib(nibName: "EmptyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {return UIView()}
        return nibView
    }
    
    func updateUI(type: EmptyViewType){
        self.type = type
        switch type {
        case .cart:
            imgView.image = UIImage(named: kEmptyCartImageName)
            lblMessage.text = kEmptyCartMessageText
            lblDesc.text = kEmptyCartDescText
            btnSearch.setTitle(kEmptyCartBtnText, for: .normal)
        default: break
        }
    }
}

// MARK: All Button Functions
extension EmptyView {
    @IBAction private func performSearchButton() {
        switch type {
        case .cart:
            if let topViewCtrl = UIApplication.topViewController() {
                topViewCtrl.navigateToSearchVC()
            }
        default: break
        }
    }
}
