//
//  PaymentGateWayViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum PaymentGateWayPageRowType: Int {
    case nmsSuperCash = 1
    case paymentGateWay
    case creditCard
    case savedCard
    case selectOtherNetbanks
    case paymentDetails
    case vouchers
    case disclaimer
    case none
}

class PaymentGateWayViewModel {
    var paymentGateWayHeaderModel = [PaymentGateWayHeaderModel]()
    weak var delegate: PaymentGateWayDelegateHandlers?
    
    func paymentGateWayService(params: JSONArrayOfDictString) {
        NetworkManager.shared.paymentGateWayService(params: params) { [weak self] (error, response) in
            guard error == nil, let paymentGateWay = response as? PaymentGateWayModel else { return }
            guard paymentGateWay.status?.lowercased() == ResponseStatus.success.rawValue else { self?.parseErrorInfo(errorInfo: paymentGateWay.errorInfo); return }
            guard let gateWayList = paymentGateWay.result?.list else { return }
            self?.parsePaymentGateWayInfo(gateWayList: gateWayList)
        }
    }
}

extension PaymentGateWayViewModel {
    func resetSelectedPaymentGateWay() {
        for (headerIndex, headerValue) in paymentGateWayHeaderModel.enumerated() {
            for (rowIndex, _) in headerValue.rowModel.enumerated() {
                paymentGateWayHeaderModel[headerIndex].rowModel[rowIndex].isSelected = false
            }
        }
    }
}

extension PaymentGateWayViewModel {
    private func parsePaymentGateWayInfo(gateWayList: [PaymentGateWayListModel]) {
        let list = gateWayList.sorted { $0.sequence! < $1.sequence! }
        for item in list {
            var isNetBanking: Bool = false
            if let subList = item.subList, subList.count > 0 {
                var rowModel = [PaymentGateWayRowModel]()
                for subItem in subList {
                    isNetBanking = subItem.parentID == 4 ? true: false
                    rowModel.append(PaymentGateWayRowModel(id: subItem.id!, key: subItem.key!, displayName: subItem.displayName!, imageURLString: subItem.imageURLString!, isShowOffer: subItem.isShowOffer!, offer: subItem.offer!, token: subItem.token!, currentBalance: subItem.currentBalance!, isLinked: subItem.isLinked!, linkToken: subItem.linkToken!, parentID: subItem.parentID!, subID: subItem.subID!, paymentGateWayType: getPaymentGateWayType(parentID: subItem.parentID, subID: subItem.subID), type: .paymentGateWay))
                }
                if isNetBanking {
                    rowModel.append(PaymentGateWayRowModel(displayName: "Select from all other banks", type: .selectOtherNetbanks))
                }
                let headerModel = PaymentGateWayHeaderModel(id: item.id!, title: item.title!, rowModel: rowModel)
                paymentGateWayHeaderModel.append(headerModel)
            }
        }
        
        self.delegate?.successProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}

extension PaymentGateWayViewModel {
    private func getPaymentGateWayType(parentID: Int?, subID: Int?) -> PaymentGateWayType {
        switch (parentID, subID) {
        case (1, 0): // PayPal
            return .payPal
        case (1, 1): // PhonePe
            return .phonePe
        case (2, 1): //MOBIKWIK
            return .mobiKwik
        case (2, 2): //FreeCharge
            return .freeCharge
        case (2, 3): // PAYTM
            return .paytm
        case (2, 4): //AMAZONPAY
            return .amazonPay
        case (2, 5), (1, 5): //OLAPOSTPAID
            return .olaPostPaid
        case (3, _): // CREDIT / DEBIT CARDS
            return .creditAndDebitCards
        case (4, _): // NET BANKING
            return .netBanking
        case (5, _): // COD
            return .cashOnDelivery
        case (6, 0):  //GooglePay
            return .googlePay
        default:
            return .none
        }
    }
}

struct PaymentGateWayHeaderModel {
    var id: Int = 0
    var title: String = ""
    var rowModel: [PaymentGateWayRowModel] = [PaymentGateWayRowModel]()
    
    init(id: Int = 0, title: String = "", rowModel: [PaymentGateWayRowModel] = [PaymentGateWayRowModel]()) {
        self.id = id
        self.title = title
        self.rowModel = rowModel
    }
}

struct PaymentGateWayRowModel {
    var id: String = ""
    var key: String = ""
    var displayName: String = ""
    var imageURLString: String = ""
    var isShowOffer: Bool = false
    var offer: String = ""
    var token: String = ""
    var currentBalance: Double = 0
    var isLinked: Bool = false
    var linkToken: String = ""
    var parentID: Int = 0
    var subID: Int = 0
    var isSelected: Bool = false
    var paymentGateWayType: PaymentGateWayType = .none
    var type: PaymentGateWayPageRowType = .none

    init(id: String = "", key: String = "", displayName: String = "", imageURLString: String = "", isShowOffer: Bool = false, offer: String = "", token: String = "", currentBalance: Double = 0, isLinked: Bool = false, linkToken: String = "", parentID: Int = 0, subID: Int = 0, isSelected: Bool = false, paymentGateWayType: PaymentGateWayType = .none, type: PaymentGateWayPageRowType = .none) {
        self.id = id
        self.key = key
        self.displayName = displayName
        self.imageURLString = imageURLString
        self.isShowOffer = isShowOffer
        self.offer = offer
        self.token = token
        self.currentBalance = currentBalance
        self.isLinked = isLinked
        self.linkToken = linkToken
        self.parentID = parentID
        self.subID = subID
        self.isSelected = isSelected
        self.paymentGateWayType = paymentGateWayType
        self.type = type
    }
}
