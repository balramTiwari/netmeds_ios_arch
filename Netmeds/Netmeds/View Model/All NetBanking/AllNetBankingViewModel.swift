//
//  AllNetBankingViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class AllNetBankingViewModel {
    var allNetBankingHeaderModel = [AllNetBankingHeaderModel]()
    var searchNetBankingHeaderModel = [AllNetBankingHeaderModel]()

    weak var delegate: AllNetBankingDelegateHandlers?
    
    func allNetbankingService() {
        NetworkManager.shared.allNetBankingServcie { [weak self] (error, response) in
            guard error == nil, let netbanksInfo = response as? [AllNetBankingModel] else { return }
            self?.parseNetbanksInfo(netbanksInfo: netbanksInfo)
        }
    }
}

extension AllNetBankingViewModel {
    func searchNetBankNameFrom(text: String) {
        let predicate = NSPredicate(format: "displayName contains[cd] %@", text)
        guard let filtered = (allNetBankingHeaderModel as NSArray).filtered(using: predicate) as? [AllNetBankingHeaderModel] else { return }
        searchNetBankingHeaderModel = filtered
        self.delegate?.successProcess()
    }
}

extension AllNetBankingViewModel {
    private func parseNetbanksInfo(netbanksInfo: [AllNetBankingModel]) {
        var localDict = [String: [AllNetBankingModel]]()
        let netBanksFilterd = netbanksInfo.filter { $0.paymentMethodType == "NB" }
        for netBank in netBanksFilterd {
            let prefix = netBank.desc!.substring(to: 1)
            if localDict.keys.contains(prefix) {
                localDict[prefix]?.append(netBank)
            } else {
                localDict[prefix] = [netBank]
            }
        }
        let sortedBanksInfo = localDict.sorted(by: { $0.key < $1.key })
        for bank in sortedBanksInfo {
            var rowModel = [AllNetBankingRowModel]()
            for rowInfo in bank.value {
                rowModel.append(AllNetBankingRowModel(displayName: rowInfo.desc!, paymentMethod: rowInfo.paymentMethod!))
            }
            allNetBankingHeaderModel.append(AllNetBankingHeaderModel(title: bank.key, rowModel: rowModel))
        }
        self.delegate?.successProcess()
    }
}

struct AllNetBankingHeaderModel {
    var title: String = ""
    var rowModel: [AllNetBankingRowModel] = [AllNetBankingRowModel]()
    
    init(title: String = "", rowModel: [AllNetBankingRowModel] = [AllNetBankingRowModel]()) {
        self.title = title
        self.rowModel = rowModel
    }
}

struct AllNetBankingRowModel {
    var displayName: String = ""
    var paymentMethod: String = ""
    
    init(displayName: String = "", paymentMethod: String = "") {
        self.displayName = displayName
        self.paymentMethod = paymentMethod
    }
}
