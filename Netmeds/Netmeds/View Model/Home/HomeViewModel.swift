//
//  HomeViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

enum HomeBannersType: Int {
    case offers = 1
    case categories
    case uploadPrescriptions
    case bigSales
    case winterSales
    case wellness
    case healthConcerns
    case firstMembership
    case consultation
    case consultationPopularConcerns
    case onlineConsultation
    case diagnostics
    case diagnosticsPopularConcerns
    case rewards
    case cashBackOffers
    case healthExperts
    case none
}

enum HomeBannersRowType {
    case medicine
    case wellness
    case consult
    case labTests
    case others
}

class HomeViewModel {
    private let dispatchGroup = DispatchGroup()
    private var localHomeHeaderModel = [HomeHeaderModel]()
    weak var delegate: HomeDelegateHandlers?
    var homeHeaderModel = [HomeHeaderModel]()
    private var configManager = ConfigManager.shared
    
    func addAllBanners() {
        localHomeHeaderModel.removeAll()
        addOffersBanner()
        addBigSalesBanner()
        addWellnessAndHealthConcernsBanner()
        addCashBackOffersBanner()
        addHealthExpertsBanner()
        dispatchGroup.notify(queue: .main) {
            self.addCategoriesBanner()
            self.addUploadPrescriptionsBanner()
            self.addFirstMembershipBanner()
            self.addConsultationBanner()
            self.addConsultationPopularConcernssBanner()
            self.addOnlineConsultationBanner()
            self.addDiagnosticsPopularConcernsBanner()
            self.addRewardsBanner()
            self.homeHeaderModel.removeAll()
            self.homeHeaderModel.append(contentsOf: self.localHomeHeaderModel)
            self.localHomeHeaderModel.removeAll()
            self.delegate?.successProcess()
        }
    }
    
    private func addOffersBanner() {
        dispatchGroup.enter()
        NetworkManager.shared.homeOffersBannerService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let homeBanners = response as? [HomeOffersBannerModel], homeBanners.count > 0 else { return }
            self?.parseHomeOffersBannerInfo(homeBanners: homeBanners)
        }
    }
    
    private func addBigSalesBanner() {
        guard let hasPromotionBanners = configManager.configDetails?.hasPromotionBanners, hasPromotionBanners else { return }
        dispatchGroup.enter()
        NetworkManager.shared.homeBigSalesBannerService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let promotionBanners = response as? HomeBigSalesBannerModel else { return }
            self?.parseHomeBigSalesBannerInfo(promotionBanners: promotionBanners)
        }
    }
    
    private func addWellnessAndHealthConcernsBanner() {
        dispatchGroup.enter()
        NetworkManager.shared.homeWellnessBannerService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let wellnessAndHealthConcernsBanners = response as? HomeWellnessAndHealthConcernModel else { return }
            self?.parseHomeWellnessAndHealthConcernsBannerInfo(wellnessAndHealthConcernsBanners: wellnessAndHealthConcernsBanners)
        }
    }
    
    private func addCashBackOffersBanner() {
        guard let isGatewayOffersActive = configManager.configDetails?.isGatewayOffersActive, isGatewayOffersActive else { return }
        dispatchGroup.enter()
        NetworkManager.shared.homeCashBackBannerService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let cashBackOffersBanner = response as? [HomeOffersBannerModel], cashBackOffersBanner.count > 0 else { return }
            self?.parseCashBackOffersBannerInfo(cashBackOffersBanner: cashBackOffersBanner)
        }
    }
    
    private func addHealthExpertsBanner() {
        guard let hasFeaturedArticle = configManager.configDetails?.hasFeaturedArticle, hasFeaturedArticle else { return }
        dispatchGroup.enter()
        NetworkManager.shared.homeHealthExpertsBannerService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let healthExpertsBanner = response as? HomeBigSalesBannerModel else { return }
            self?.parseHomeHealthExpertsBannerInfo(healthExpertsBanner: healthExpertsBanner)
        }
    }
    
    private func addCategoriesBanner() {
        var rowModel = [HomeRowModel]()
        rowModel.append(HomeRowModel(title: "Medicine", imgName: "medicine", type: .medicine))
        rowModel.append(HomeRowModel(title: "Wellness", imgName: "wellness", type: .wellness))
        rowModel.append(HomeRowModel(title: "Dr.Consult", imgName: "consulttion", type: .consult))
        rowModel.append(HomeRowModel(title: "Lab Tests", imgName: "diagnostic", type: .labTests))
        let headerModel = HomeHeaderModel(title: "EXPLORE", subTitle: "Categories", hasShowHeader: true, rowHeight: 75.0, type: .categories, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addUploadPrescriptionsBanner() {
        guard let configDetails = configManager.configDetails,
            let m2PrescriptionUploadConfigs = configDetails.m2PrescriptionUploadConfigs,
            let isM2PrescriptionUploadEnabled = configDetails.isM2PrescriptionUploadEnabled, isM2PrescriptionUploadEnabled else { return }
        let headerModel = HomeHeaderModel(title: m2PrescriptionUploadConfigs.header2!.uppercased(), subTitle: m2PrescriptionUploadConfigs.desc!, desc:m2PrescriptionUploadConfigs.header1!, orderDurationText: m2PrescriptionUploadConfigs.orderText!, btnUploadText: m2PrescriptionUploadConfigs.buttonText!, type: .uploadPrescriptions, rowModel: [HomeRowModel]())
        localHomeHeaderModel.append(headerModel)
    }
    
    
    private func addFirstMembershipBanner() {
        guard let homePageConfig = configManager.configDetails?.primeConfig?.homePageConfig,
            let isEnabled = homePageConfig.isEnabled, isEnabled else { return }
        let value = homePageConfig.header!.split(separator: "|")
        var desc: String = ""
        if let first = value.first {
            desc = "• " + String(first)
        }
        if let last = value.last {
            desc += "\n\n•" + String(last)
        }
        let headerModel = HomeHeaderModel(title:homePageConfig.title!, desc: desc, orderDurationText: homePageConfig.desc!, btnUploadText: homePageConfig.buttonText!, type: .firstMembership, rowModel: [HomeRowModel]())
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addConsultationBanner() {
        guard let consultationHomeSliderConfigs = configManager.configDetails?.consultationHomeSliderConfigs,
            let isEnabled = consultationHomeSliderConfigs.isEnabled, isEnabled,
            let slides = consultationHomeSliderConfigs.slide, slides.count > 0 else { return }
        var rowModel = [HomeRowModel]()
        for slide in slides {
            rowModel.append(HomeRowModel(title: slide.title!, imgURL: slide.image!, linkType: slide.linkType!, url: slide.url!))
        }
        let headerModel = HomeHeaderModel(rowHeight: 150.0, type: .consultation, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addConsultationPopularConcernssBanner() {
        guard let consultationPopularConcernConfigs = configManager.configDetails?.consultationPopularConcernConfigs,
            let isEnabled = consultationPopularConcernConfigs.isEnabled, isEnabled,
            let slides = consultationPopularConcernConfigs.slide, slides.count > 0 else { return }
        var rowModel = [HomeRowModel]()
        for slide in slides {
            rowModel.append(HomeRowModel(title: slide.title!, imgURL: slide.image!, linkType: slide.linkType!, url: slide.url!))
        }
        let headerModel = HomeHeaderModel(title: consultationPopularConcernConfigs.title1!, subTitle: consultationPopularConcernConfigs.title2!, desc: consultationPopularConcernConfigs.desc!, viewAllBtnText: "Try now", hasShowHeader: true, rowHeight: 150.0, type: .consultationPopularConcerns, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addOnlineConsultationBanner() {
        guard let configDetails = configManager.configDetails,
            let consultationUploadConfigs = configDetails.consultationUploadConfigs,
            let isEnabled = configDetails.isConsultationUploadEnabled, isEnabled else { return }
        let value = consultationUploadConfigs.desc!.split(separator: "|")
        var desc: String = ""
        if let first = value.first {
            desc = "• " + String(first)
        }
        if let last = value.last {
            desc += "\n\n•" + String(last)
        }
        let headerModel = HomeHeaderModel(title: consultationUploadConfigs.header1!, subTitle: consultationUploadConfigs.header2!, desc:desc, orderDurationText: consultationUploadConfigs.orderText!, btnUploadText: consultationUploadConfigs.buttonText!, rowHeight: 185.0, type: .onlineConsultation, rowModel: [HomeRowModel]())
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addDiagnosticsPopularConcernsBanner() {
        guard let diagnosticsPopularConcernConfigs = configManager.configDetails?.diagnosticsPopularConcernConfigs,
            let isEnabled = diagnosticsPopularConcernConfigs.isEnabled, isEnabled,
            let slides = diagnosticsPopularConcernConfigs.slide, slides.count > 0 else { return }
        var rowModel = [HomeRowModel]()
        for slide in slides {
            rowModel.append(HomeRowModel(title: slide.title!, imgURL: slide.image!, linkType: slide.linkType!, url: slide.url!))
        }
        let headerModel = HomeHeaderModel(title: diagnosticsPopularConcernConfigs.title1!, subTitle: diagnosticsPopularConcernConfigs.title2!, desc: diagnosticsPopularConcernConfigs.desc!, viewAllBtnText: "Try now", hasShowHeader: true, rowHeight: 150.0, type: .diagnosticsPopularConcerns, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func addRewardsBanner() {
        guard let isReferEarnEnabled = configManager.configDetails?.isReferEarnEnabled, isReferEarnEnabled else { return }
        var rowModel = [HomeRowModel]()
        rowModel.append(HomeRowModel(title: "", desc: "", imgURL:""))
        let headerModel = HomeHeaderModel(title: "GRAB YOUR", subTitle: "Instant Rewards", desc: "A simpler way to earn and get discounts", hasShowHeader: true, rowHeight: 115.0, type: .rewards, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
}

extension HomeViewModel {
    private func parseHomeOffersBannerInfo(homeBanners: [HomeOffersBannerModel]) {
        var rowModel = [HomeRowModel]()
        for banners in homeBanners {
            rowModel.append(HomeRowModel(title: banners.title!, imgURL: banners.imageURL!, linkType: banners.linkType!, pageID: banners.newPageID!, url: banners.url!))
        }
        let headerModel = HomeHeaderModel(rowHeight: 150.0, type: .offers, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func parseHomeBigSalesBannerInfo(promotionBanners: HomeBigSalesBannerModel) {
        guard let promoBanners = promotionBanners.list, promoBanners.count > 0 else { return }
        var rowModel = [HomeRowModel]()
        for banners in promoBanners {
            rowModel.append(HomeRowModel(title: banners.name!, imgURL: banners.imageURL!, linkType: banners.linkType!, url: banners.url!))
        }
        let headerModel = HomeHeaderModel(title:  promotionBanners.header!, subTitle: promotionBanners.subHeader!, hasShowHeader: true, rowHeight: 150.0, type: .bigSales, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func parseHomeWellnessAndHealthConcernsBannerInfo(wellnessAndHealthConcernsBanners: HomeWellnessAndHealthConcernModel) {
        if let wellnessBanner = wellnessAndHealthConcernsBanners.wellnessInfo, let wellnessList = wellnessBanner.list, wellnessList.count > 0, let isWellnessProductsActive = configManager.configDetails?.isWellnessProductsActive, isWellnessProductsActive {
            var rowModel = [HomeRowModel]()
            for banners in wellnessList {
                rowModel.append(HomeRowModel(title: banners.name!, imgURL: banners.image!, linkType: banners.linkType!, pageID: String(banners.id!), url: banners.url!))
            }
            let headerModel = HomeHeaderModel(title:  wellnessBanner.header!, subTitle: wellnessBanner.subHeader!, desc: wellnessBanner.tagLine!, viewAllBtnText: "View all", hasShowHeader: true, rowHeight: 70.0, type: .wellness, rowModel: rowModel)
            localHomeHeaderModel.append(headerModel)
        }
        
        if let healthConcernsBanner = wellnessAndHealthConcernsBanners.healthConcernInfo, let healthConcernsList = healthConcernsBanner.list, healthConcernsList.count > 0 {
            var rowModel = [HomeRowModel]()
            for banners in healthConcernsList {
                rowModel.append(HomeRowModel(title: banners.name!, imgURL: banners.image!, linkType: banners.linkType!, pageID: String(banners.id!), url: banners.url!))
            }
            let headerModel = HomeHeaderModel(title:  healthConcernsBanner.header!, subTitle: healthConcernsBanner.subHeader!, desc: healthConcernsBanner.tagLine!, hasShowHeader: true, rowHeight: 70.0, type: .healthConcerns, rowModel: rowModel)
            localHomeHeaderModel.append(headerModel)
        }
    }
    
    private func parseCashBackOffersBannerInfo(cashBackOffersBanner: [HomeOffersBannerModel]) {
        var rowModel = [HomeRowModel]()
        for banners in cashBackOffersBanner {
            rowModel.append(HomeRowModel(title: banners.title!, desc: banners.couponDesc!, imgURL: banners.image!, pageID: banners.pageID!))
        }
        let headerModel = HomeHeaderModel(rowHeight: 75.0, type: .cashBackOffers, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
    
    private func parseHomeHealthExpertsBannerInfo(healthExpertsBanner: HomeBigSalesBannerModel) {
        guard let healthExpertBanners = healthExpertsBanner.articleList, healthExpertBanners.count > 0 else { return }
        var rowModel = [HomeRowModel]()
        for banners in healthExpertBanners {
            rowModel.append(HomeRowModel(title: banners.title!, imgURL: banners.image!, url: banners.url!))
        }
        let headerModel = HomeHeaderModel(title: healthExpertsBanner.header!, subTitle: healthExpertsBanner.subHeader!, desc: healthExpertsBanner.desc!, viewAllBtnText: "View all", hasShowHeader: true, rowHeight: 150.0, type: .healthExperts, rowModel: rowModel)
        localHomeHeaderModel.append(headerModel)
    }
}

struct HomeHeaderModel {
    var title: String = ""
    var subTitle: String = ""
    var desc: String = ""
    var orderDurationText: String = ""
    var btnUploadText: String = ""
    var viewAllBtnText: String = ""
    var hasShowHeader: Bool = false
    var rowHeight: CGFloat = 0.0
    var type: HomeBannersType = .none
    var rowModel = [HomeRowModel]()
    init(title: String = "", subTitle: String = "", desc: String = "", orderDurationText: String = "", btnUploadText: String = "", viewAllBtnText: String = "", hasShowHeader: Bool = false, rowHeight: CGFloat = 0.0, type: HomeBannersType = .none, rowModel:[HomeRowModel] = [HomeRowModel]()) {
        self.title = title
        self.subTitle = subTitle
        self.desc = desc
        self.orderDurationText = orderDurationText
        self.btnUploadText = btnUploadText
        self.viewAllBtnText = viewAllBtnText
        self.hasShowHeader = hasShowHeader
        self.rowHeight = rowHeight
        self.type = type
        self.rowModel = rowModel
    }
    
    static func sequenceByType(lhs: HomeHeaderModel, rhs: HomeHeaderModel) -> Bool {
        return lhs.type.rawValue < rhs.type.rawValue
    }
}

struct HomeRowModel {
    var title: String = ""
    var desc: String = ""
    var imgURL: String = ""
    var linkType: String = ""
    var pageID: String = ""
    var url: String = ""
    var imgName: String = ""
    var type: HomeBannersRowType = .others
    init(title: String = "", desc: String = "", imgURL: String = "", linkType: String = "", pageID: String = "", url: String = "", imgName: String = "", type: HomeBannersRowType = .others) {
        self.title = title
        self.desc = desc
        self.imgURL = imgURL
        self.linkType = linkType
        self.pageID = pageID
        self.url = url
        self.imgName = imgName
        self.type = type
    }
}
