//
//  ProductDetailsViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

enum ProductDetailsHeaderType: Int {
    case title = 1
    case categories
    case otcProductImages
    case coldStorage
    case checkAvailability
    case variants
    case priceInfo
    case cart
    case outOfStock
    case notifyMe
    case highPriceMedicines
    case details
    case offers
    case alternateSalts
    case webView
    case none
}

import Foundation
import InstantSearchClient

class ProductDetailsViewModel {
    private let dispatchGroup = DispatchGroup()
    var productDetailsHeaderModel: [ProductDetailsHeaderModel] = [ProductDetailsHeaderModel]()
    weak var delegate: ProductDetailsDelegateHandlers?

    func productDetailsService(productID: Int) {
        NetworkManager.shared.productDetailsService(productID: productID) { [weak self] (error, response) in
            guard error == nil, let productDetails = response as? ProductDetailsModel else { return }
            self?.parseProductDetailsInfo(productDetails: productDetails)
        }
    }
    
    private func brainSinsInfoService(productID: Int, brainsinsKey: String, templateID: String, customerID: String, headerTitle: String) {
        NetworkManager.shared.brainSinsInfoService(productID: productID, brainsinsKey: brainsinsKey, templateID: templateID, customerID: customerID){ [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let brainSinsInfo = response as? [BrainSinsModel] else { return }
            self?.parseBrainSinsInfo(jsonResult: brainSinsInfo, headerTitle: headerTitle)
        }
    }
    
    private func algoliaSearchService(productID: Int, filter: String, facetFilters: String, headerTitle: String) {
        let query = Query()
        query.hitsPerPage = 1000
        query.page = 0
        query.facets = ["*"]
        query.clickAnalytics = true
        query.filters = filter
        query.facetFilters = ["\(facetFilters)"]
        NetworkManager.shared.algoliaSearchService(query: query) { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let algoliaSearchModel = response as? AlgoliaSearchModel else { return }
            self?.parseAlgoliaHitsInfo(productID: productID, algoliaSearchModel: algoliaSearchModel, headerTitle: headerTitle)
        }
    }
    
    private func loadWebViewInfo(product: ProductsModel) {
        let urlString = APIEndPointConstants.customAPIBaseURL + product.urlPath!
        guard let url = URL(string: urlString) else { return }
        dispatchGroup.enter()
        do {
            let contents = try String(contentsOf: url)
            if (contents.contains("<h1>")) || (contents.contains("<p>")) || (contents.contains("<h2>")) || (contents.contains("<h3>")) || (contents.contains("<li>")) || (contents.contains("<ul>")) {
                var rowModel = [ProductDetailsRowModel]()
                rowModel.append(ProductDetailsRowModel(urlString: urlString))
                let headerModel = ProductDetailsHeaderModel(type: .webView, rowModel: rowModel)
                productDetailsHeaderModel.append(headerModel)
            }
            dispatchGroup.leave()
        } catch { dispatchGroup.leave() }
    }
    
    private func getAllCouponsService() {
        dispatchGroup.enter()
        NetworkManager.shared.getAllCouponsService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let couponsInfo = response as? AllCouponsInfoModel else { return }
            self?.parseCouponsInfo(couponsInfo: couponsInfo)
        }
    }
}

extension ProductDetailsViewModel {
    private func parseProductDetailsInfo(productDetails: ProductDetailsModel) {
        guard productDetails.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        guard let product = productDetails.result?.product else { return }
        parseProductDetailsInfo(product: product)
    }
}

extension ProductDetailsViewModel {
    private func parseProductDetailsInfo(product: ProductsModel) {
        parseAlternateSaltsInfo(product: product)
        loadWebViewInfo(product: product)
        parseOffersInfo(product: product)
        dispatchGroup.notify(queue: .main) {
            self.parseTitleInfo(product: product)
            self.parseCategoriesInfo(product: product)
            self.parseOTCProductImagesInfo(product: product)
            self.parseColdStorageInfo(product: product)
            self.parseCheckAvailabilityInfo(product: product)
            self.parseVariantsInfo(product: product)
            self.parsePriceInfo(product: product)
            self.parseCartInfo(product: product)
            self.parseHighPriceMedicinesInfo(product: product)
            self.parseDetailsInfo(product: product)
            self.delegate?.successProcess()
        }
    }
    
    private func parseTitleInfo(product: ProductsModel) {
        let productImageURL = ConfigManager.shared.configURLPaths?.productImageURL
        var imgURLs: [String] = [String]()
        if product.rxRequired!, let productImagePaths = product.imagePaths, productImagePaths.count > 0 {
            let imgPX = "75x75/"
            for imagePath in productImagePaths {
                if let baseImageURL = productImageURL {
                    imgURLs.append("\(baseImageURL)\(imgPX)\(imagePath)")
                }
            }
        }
        var rowModel = [ProductDetailsRowModel]()
        if imgURLs.count == 0 {
            rowModel.append(ProductDetailsRowModel(title: product.displayName!))
        } else {
            for imgURL in imgURLs {
                rowModel.append(ProductDetailsRowModel(urlString: imgURL, title: product.displayName!))
            }
        }
        let headerModel = ProductDetailsHeaderModel(type: .title, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }
    
    private func parseCategoriesInfo(product: ProductsModel) {
        var categoryNames: [String] = [String]()
        if let categories =  product.categories, let name = categories.first?.name {
            categoryNames.append(name)
        }
        if product.rxRequired! {
            categoryNames.append("Rx Required")
        }
        guard categoryNames.count > 0 else { return }
        var rowModel = [ProductDetailsRowModel]()
        for categoryName in categoryNames {
            rowModel.append(ProductDetailsRowModel(urlString: categoryName))
        }
        let headerModel = ProductDetailsHeaderModel(type: .categories, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }
    
    private func parseOTCProductImagesInfo(product: ProductsModel) {
        guard !product.rxRequired! else { return }
        let productImageURL = ConfigManager.shared.configURLPaths?.productImageURL
        var imgURLs: [String] = [String]()
        if let productImagePaths = product.imagePaths, productImagePaths.count > 0 {
            let imgPX = "240x240/"
            for imagePath in productImagePaths {
                if let baseImageURL = productImageURL {
                    imgURLs.append("\(baseImageURL)\(imgPX)\(imagePath)")
                }
            }
        }
        guard imgURLs.count > 0 else { return }
        var rowModel = [ProductDetailsRowModel]()
        for imgURL in imgURLs {
            rowModel.append(ProductDetailsRowModel(urlString: imgURL))
        }
        let headerModel = ProductDetailsHeaderModel(type: .otcProductImages, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }
    
    private func parseColdStorageInfo(product: ProductsModel) {
        guard product.isColdStorage! else { return }
        let headerModel = ProductDetailsHeaderModel(type: .coldStorage)
        productDetailsHeaderModel.append(headerModel)
    }
    
    private func parseCheckAvailabilityInfo(product: ProductsModel) {
    }

    private func parseVariantsInfo(product: ProductsModel) {
    }

    private func parsePriceInfo(product: ProductsModel) {
        var manufacturerName: String = ""
        if let manufacturer = product.manufacturer, let name = manufacturer.name {
            manufacturerName = kManufacturerShortHand + name
        }
        var rowModel = [ProductDetailsRowModel]()
        rowModel.append(ProductDetailsRowModel(packSize: product.packLabel!, price: product.sellingPrice!, mrp: product.mrp!, discountRate: product.discountRate!, availabilityStatus: product.availabilityStatus!, manufacturerName: manufacturerName, cartCount: "1"))
        let headerModel = ProductDetailsHeaderModel(type: .priceInfo, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }

    private func parseCartInfo(product: ProductsModel) {
        switch product.availabilityStatus! {
        case "A":
            var rowModel = [ProductDetailsRowModel]()
            rowModel.append(ProductDetailsRowModel(title: "1"))
            let headerModel = ProductDetailsHeaderModel(type: .cart, rowModel: rowModel)
            productDetailsHeaderModel.append(headerModel)
        case "C", "T", "R":
            var title: String = "NOT AVAILABLE"
            if product.availabilityStatus! == "R" {
                title = "NOT FOR SALE"
            }
            var rowModel = [ProductDetailsRowModel]()
            rowModel.append(ProductDetailsRowModel(title: title))
            let headerModel = ProductDetailsHeaderModel(type: .outOfStock, rowModel: rowModel)
            productDetailsHeaderModel.append(headerModel)
        case "S":
            var rowModel = [ProductDetailsRowModel]()
            rowModel.append(ProductDetailsRowModel(title: "OUT OF STOCK", subTitle: "Get notified when this medicine is available"))
            let headerModel = ProductDetailsHeaderModel(type: .outOfStock, rowModel: rowModel)
            productDetailsHeaderModel.append(headerModel)
            var notifyMeRowModel = [ProductDetailsRowModel]()
            notifyMeRowModel.append(ProductDetailsRowModel(title: "NOTIFY ME"))
            let notifyMeRowModelHeaderModel = ProductDetailsHeaderModel(type: .notifyMe, rowModel: notifyMeRowModel)
            productDetailsHeaderModel.append(notifyMeRowModelHeaderModel)
        default: break
        }
    }
    
    private func parseHighPriceMedicinesInfo(product: ProductsModel) {
        guard product.sellingPrice! >= 5000.00 else { return }
        let headerModel = ProductDetailsHeaderModel(type: .highPriceMedicines)
        productDetailsHeaderModel.append(headerModel)
    }
    
    private func parseDetailsInfo(product: ProductsModel) {
        var rowModel = [ProductDetailsRowModel]()
        if product.productType! == kRxRequired, let generics = product.generic {
            rowModel.append(ProductDetailsRowModel(imgName: "pill_grey", title: "Content", subTitle: generics.name!))
        }
        if product.availabilityStatus == "A", let stockQty = product.stockQty, let maxQtyInOrder = product.maxQtyInOrder, stockQty > 0, maxQtyInOrder > 0 {
            rowModel.append(ProductDetailsRowModel(imgName: "expiry", title: "Expiry & Estimated Delivery", subTitle: "Check"))
        }
        let headerModel = ProductDetailsHeaderModel(title: "DETAILS", type: .details, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }

    private func parseOffersInfo(product: ProductsModel) {
        if product.productType! == kRxRequired {
            getAllCouponsService()
        } else {
            if let discount = product.discountRate, discount > 0.0 {
                var couponName: String = ""
                if let mrp = product.mrp, let price = product.sellingPrice {
                    let value = NSNumber(value: mrp - price)
                    if let discountPercentage = product.discountRate, discountPercentage != 0.00, let percentage = discountPercentage.convertDoubleToPercentage() {
                        couponName = "You get \(percentage)% OFF on this product"
                    } else {
                        couponName = "You Save ₹\(value.formatFractionDigits())"
                    }
//                    self.offerCouponName.font = UIFont.font(withType: .latoBold, AndSize: 14)
//                    self.offerCouponName.textColor = UIColor.getColorFromHexValue(hex: "378f30", alpha: 1.0)
                }
                var rowModel = [ProductDetailsRowModel]()
                rowModel.append(ProductDetailsRowModel(title: couponName))
                let headerModel = ProductDetailsHeaderModel(title: "Default Discount", type: .offers, rowModel: rowModel)
                productDetailsHeaderModel.append(headerModel)
//                self.offerApplicableView.isHidden = false
//                self.offerMessageName.text = ""
//                self.offerDiscountName.text = ""
//                self.viewAllOffersButton.isHidden = false
//                self.viewAllOffersButton.setTitle("Offer Applied", for: .normal)
//                self.viewAllOffersButton.isUserInteractionEnabled = false
//                self.offerAppliedTitleLabel.text = "Default Discount"//"OFFER APPLIED"
            }
        }
    }

    private func parseCouponsInfo(couponsInfo: AllCouponsInfoModel) {
        guard couponsInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        if let couponsList = couponsInfo.result?.couponList , couponsList.count > 0 {
            let firstCoupon = couponsList.first!
            var rowModel = [ProductDetailsRowModel]()
            rowModel.append(ProductDetailsRowModel(title: firstCoupon.couponCode!, subTitle: "You Save \(firstCoupon.discount!)%", message: firstCoupon.couponDesc!))
            let headerModel = ProductDetailsHeaderModel(title: "OFFERS APPLICABLE", type: .offers, rowModel: rowModel)
            productDetailsHeaderModel.append(headerModel)
        }
        //            let couponInfo = couponList.first!
        //            self.offerApplicableView.isHidden = false
        //            self.offerAppliedTitleLabel.text = "OFFERS APPLICABLE"
        //            self.offerDiscountName.text = "You Save \(couponInfo.discount!)%"
        //            self.offerCouponName.text = couponInfo.couponCode
        //            self.offerMessageName.text = couponInfo.couponDesc
        //            self.viewAllOffersButton.isHidden = couponList.count > 1 ? false: true

    }

    private func parseAlternateSaltsInfo(product: ProductsModel) {
        dispatchGroup.enter()
        let configDetails = ConfigManager.shared.configDetails
        let brainsinsKey: String = configDetails?.brainsinsKey ?? ""
        let templateID: String = configDetails?.brainSinConfigValues?.peopleAlsoView?.templateID ?? ""
        let customerID: String = UserDefaultsModel.getUserID()
        let headerTitle = (product.productType! == kRxRequired) ? "ALTERNATE SALTS" : "PEOPLE ALSO VIEWED"
        let brainsinFlag = (configDetails?.hasBrainsinStatus ?? false) && (configDetails?.brainSinConfigValues?.peopleAlsoView?.isStatusEnabled ?? false) && (product.productType != kRxRequired)
        guard brainsinFlag else {
            guard let formulationType = product.formulationType, let dosageId = product.genericWithDosageID else { return }
            let filterQueryString = "\"formulation_type\"" + ":" + "\"\(formulationType)\"" + " AND " + "\"availability_status\"" + ":" + "\"A\"" + " OR " +  "\"availability_status\"" + ":" + "\"S\""
            self.algoliaSearchService(productID: product.productCode!, filter: filterQueryString, facetFilters: "generic_with_dosage_id:\(dosageId)", headerTitle: headerTitle)
            return
        }
        self.brainSinsInfoService(productID: product.productCode!, brainsinsKey: brainsinsKey.decodeKey(), templateID: templateID, customerID: customerID, headerTitle: headerTitle)
    }
    
    private func parseBrainSinsInfo(jsonResult:[BrainSinsModel], headerTitle: String) {
        var updateJSON = [[String: Any]]()
        for brainSins in jsonResult {
            updateJSON.append(["product_code": Int(brainSins.productCode ?? "") as Any,
                 "display_name": brainSins.displayName as Any,
                 "availability_status": "A" as Any,
                 "selling_price": Float(brainSins.sellingPrice ?? "") as Any,
                 "thumbnail_url": brainSins.imageURL as Any,
                 "mrp": Float(brainSins.price ?? "") as Any])
        }
        guard let updateJSONResult = [AlgoliaSearchHitsModel].decode(json: updateJSON as AnyObject) else { return }
        updateAlteranteSaltsInfo(alternateSaltsInfo: updateJSONResult, headerTitle: headerTitle)
    }
    
    private func parseAlgoliaHitsInfo(productID: Int, algoliaSearchModel: AlgoliaSearchModel, headerTitle: String) {
        guard algoliaSearchModel.nbHits! > 0 else { return }
        guard var hits = algoliaSearchModel.hits, hits.count > 0 else { return }
        if let getIndex = hits.index(where: { $0.productCode == productID }) {
            hits.remove(at: getIndex)
        }
        let priceSorted = hits.sorted { $0.price! < $1.price! }
        let availabilityStatusSorted = priceSorted.sorted { $0.availabilityStatus! < $1.availabilityStatus! }
        updateAlteranteSaltsInfo(alternateSaltsInfo: availabilityStatusSorted, headerTitle: headerTitle)
    }
    
    private func updateAlteranteSaltsInfo(alternateSaltsInfo: [AlgoliaSearchHitsModel], headerTitle: String) {
        var rowModel = [ProductDetailsRowModel]()
        if alternateSaltsInfo.count > 3 {
            rowModel.append(ProductDetailsRowModel(algoliaSearchHitsModel: alternateSaltsInfo[0]))
            rowModel.append(ProductDetailsRowModel(algoliaSearchHitsModel: alternateSaltsInfo[1]))
            rowModel.append(ProductDetailsRowModel(algoliaSearchHitsModel: alternateSaltsInfo[2]))
        } else {
            for alternateSalt in alternateSaltsInfo {
                rowModel.append(ProductDetailsRowModel(algoliaSearchHitsModel: alternateSalt))
            }
        }
        guard rowModel.count > 0 else { return }
        let headerModel = ProductDetailsHeaderModel(title: headerTitle, type: .alternateSalts, rowModel: rowModel)
        productDetailsHeaderModel.append(headerModel)
    }
}

class ProductDetailsHeaderModel {
    var title: String = ""
    var rowModel: [ProductDetailsRowModel] = [ProductDetailsRowModel]()
    var type: ProductDetailsHeaderType = .none
    init(title: String = "", type: ProductDetailsHeaderType = .none, rowModel: [ProductDetailsRowModel] = [ProductDetailsRowModel]()) {
        self.title = title
        self.type = type
        self.rowModel = rowModel
    }
    
    static func sequenceByType(lhs: ProductDetailsHeaderModel, rhs: ProductDetailsHeaderModel) -> Bool {
        return lhs.type.rawValue < rhs.type.rawValue
    }

}

class ProductDetailsRowModel {
    var productCode: Int = 0
    var imgName: String = ""
    var urlString: String = ""
    var title: String = ""
    var subTitle: String = ""
    var message: String = ""
    var packSize: String = ""
    var price: Double = 0.0
    var mrp: Double = 0.0
    var discountRate: Double = 0.0
    var availabilityStatus: String = ""
    var manufacturerName: String = ""
    var cartCount: String = ""
    var type: ProductDetailsHeaderType = .none
    var algoliaSearchHitsModel: AlgoliaSearchHitsModel?
    init(productCode: Int = 0, imgName: String = "", urlString: String = "", title: String = "", subTitle: String = "", message: String = "", packSize: String = "", price: Double = 0.0, mrp: Double = 0.0, discountRate: Double = 0.0, availabilityStatus: String = "", manufacturerName: String = "", cartCount: String = "", algoliaSearchHitsModel: AlgoliaSearchHitsModel? = nil) {
        self.productCode = productCode
        self.imgName = imgName
        self.urlString = urlString
        self.title = title
        self.subTitle = subTitle
        self.message = message
        self.packSize = packSize
        self.price = price
        self.mrp = mrp
        self.discountRate = discountRate
        self.availabilityStatus = availabilityStatus
        self.manufacturerName = manufacturerName
        self.cartCount = cartCount
        self.algoliaSearchHitsModel = algoliaSearchHitsModel
    }
}
