//
//  AddToCartViewModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class AddToCartViewModel {
    
    var productDetails: ProductDetailsModel?
    weak var delegate: AddToCartDelegateHandlers?
    
    func productDetailsService(productID: Int) {
        NetworkManager.shared.productDetailsService(productID: productID) { [weak self] (error, response) in
            guard error == nil, let productDetails = response as? ProductDetailsModel else { return }
            self?.parseProductDetailsInfo(productDetails: productDetails)
        }
    }
    
    func addToCartService(productID: Int, quantity: Int, cartID: Int = 0) {
        guard UserDefaultsModel.getCartID() > 0 else {
            createCartService { [weak self] (isSuccess) in
                self?.invokeAddToCartService(productID: productID, quantity: quantity, cartID: cartID)
            }
            return
        }
        invokeAddToCartService(productID: productID, quantity: quantity, cartID: cartID)
    }
   
    private func createCartService(completionHandler: @escaping ((_ isSuccess: Bool) -> ())) {
        NetworkManager.shared.createCartService { (error, response) in
            guard error == nil, let createCartInfo = response as? CreateCartModel else { return }
            guard createCartInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
            completionHandler(true)
        }
    }

    private func invokeAddToCartService(productID: Int, quantity: Int, cartID: Int = 0) {
        NetworkManager.shared.addToCartService(productID: productID, quantity: quantity, cartID: cartID) { [weak self] (error, response) in
            guard error == nil, let addToCartInfo = response as? AddToCartModel else { return }
            self?.parseAddToCartInfo(addToCartInfo: addToCartInfo, productID: productID, quantity: quantity, cartID: cartID)
        }
    }
}

extension AddToCartViewModel {
    private func parseProductDetailsInfo(productDetails: ProductDetailsModel) {
        guard productDetails.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        self.productDetails = productDetails
        self.delegate?.productDetailsSuccessProcess()
    }
    
    private func parseAddToCartInfo(addToCartInfo: AddToCartModel, productID: Int, quantity: Int, cartID: Int = 0) {
        guard addToCartInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
           guard let message = addToCartInfo.errorInfo?.code, message.contains("CART_NOT_FOUND") else {
            parseErrorInfo(errorInfo: addToCartInfo.errorInfo); return
            }
            createCartService { [weak self] (isSuccess) in
                self?.invokeAddToCartService(productID: productID, quantity: quantity, cartID: cartID)
            }
            return
        }
        self.delegate?.addToCartSuccessProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
