//
//  SearchViewModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import InstantSearchClient

class SearchViewModel {
    
    weak var delegate: SearchDelegateHandlers?
    var algoliaSearchModel = [AlgoliaSearchHitsModel]()
    var totalPages: Int = 0
    var queryID: String = ""
    var hasRecentSearchInfo: Bool = false

    func fetechRecentSearchItemsFromDB() {
        algoliaSearchModel.removeAll()
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        guard let results = CoreDataManager.shared.fetchRequestResults(forEntityName: DBRecentSearches.entityName(), sortDescriptor: sortDescriptor) as? [DBRecentSearches] else { return }
        for result in results {
            if let decode = AlgoliaSearchHitsModel.decode(data: result.hitItem.dataWithJSONObject())  {
                algoliaSearchModel.append(decode)
            }
        }
        hasRecentSearchInfo = algoliaSearchModel.count > 0 ? true: false
        self.delegate?.recentSearchItemsSuccessProcess()
    }

    func algoliaSearchService(searchText: String, page: UInt) {
        hasRecentSearchInfo = false
        let query = Query(query: searchText)
        query.queryType = .prefixAll
        query.hitsPerPage = 10
        query.clickAnalytics = true
        query.page = page
        NetworkManager.shared.algoliaSearchService(query: query) { [weak self] (error, response) in
            guard error == nil, let algoliaSearchModel = response as? AlgoliaSearchModel else { return }
            self?.parseAlgoliaHitsInfo(algoliaSearchModel: algoliaSearchModel)
        }
    }
    
    func addRecentSearchIntoDB(item: AlgoliaSearchHitsModel) {
        guard let encodeData = item.encode(), let jSONResponse = encodeData.jsonObjectWithData() else { return }
        guard let productCode = item.productCode, !isSearchItemExistsFromDB(productCode: String(productCode)) else { return }
        CoreDataManager.shared.insertObject(forEntityName: DBRecentSearches.entityName(), attributes:
            ["hitItem": jSONResponse,
             "date": Date(),
             "productCode": String(productCode)]
        )
    }
    
    func deleteAllRecentSearchInfoFromDB() {
        CoreDataManager.shared.deleteAll(forEntityName: DBRecentSearches.entityName())
        algoliaSearchModel.removeAll()
        hasRecentSearchInfo = false
        delegate?.recentSearchItemsSuccessProcess()
    }
    
    private func isSearchItemExistsFromDB(productCode: String) -> Bool {
        let predicate : NSPredicate = NSPredicate(format: "productCode = %@", productCode)
        guard let result = CoreDataManager.shared.fetchByPredicate(forEntityName: DBRecentSearches.entityName(), predicate: predicate), result.count > 0 else { return false }
        return true
    }
}

extension SearchViewModel {
    private func parseAlgoliaHitsInfo(algoliaSearchModel: AlgoliaSearchModel) {
        totalPages = algoliaSearchModel.totalPage!
        queryID = algoliaSearchModel.queryID!
        guard let hits = algoliaSearchModel.hits, hits.count > 0 else {
            self.delegate?.algoliaNoHitsProcess(); return }
        guard !hasRecentSearchInfo else { return }
        self.algoliaSearchModel.append(contentsOf: hits)
        self.delegate?.algoliaHitsSuccessProcess()
    }
}
