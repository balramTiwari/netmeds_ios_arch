//
//  ProductFilterViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import InstantSearchClient

class ProductFilterViewModel {
    weak var delegate: ProductFilterDelegateHandlers?
    var filterItems = [ProductFilterModel]()
    var searchFilterItems = [ProductFilterValuesModel]() {
        didSet {
            delegate?.productSearchFilterSuccessProcess()
        }
    }
    private var selectedFilterItems = [ProductFilterModel]()
    private var subCategories = [SubCategoriesModel]()
    private let DIVIDE_VALUE: Int = 5

    func algoliaSearchService(facetFilters: [Any]?) {
        let query = Query()
        query.page = 0
        query.facets = ["*"]
        query.clickAnalytics = true
        query.facetFilters = facetFilters
        NetworkManager.shared.algoliaSearchService(query: query) { [weak self] (error, response) in
            guard error == nil, let algoliaSearchModel = response as? AlgoliaSearchModel, let facets = algoliaSearchModel.facets else { return }
            self?.parseAlgoliaFilterResponse(facets: facets)
        }
    }
}

extension ProductFilterViewModel {
    private func parseAlgoliaFilterResponse(facets: [String: [String: Int]]) {
        let facetsKeys = facets.keys
        for key in facetsKeys {
            if key == FacetsParseKeysType.inStock.rawValue {
                if let facetsValue = facets[key], facetsValue.count > 0 {
                    addAvailability(subKey: facetsValue.first!.key)
                }
            } else {
                let getKey = key
                let namesToBind = getDisplayAndAlgoliaSearchKeyName(parseKey: getKey)
                if let displayName = namesToBind.displayName, let algoliaSearchKeyName = namesToBind.algoliaSearchKeyName {
                    let filterModel = ProductFilterModel()
                    filterModel.key = algoliaSearchKeyName
                    filterModel.name = displayName
                    let facetsValues = facets[getKey]
                    var arrayFilterValues = [ProductFilterValuesModel]()
                    if let facetsValue = facetsValues{
                        for (name, _) in facetsValue {
                            let filterValuesModel = ProductFilterValuesModel()
                            filterValuesModel.name = name
                            arrayFilterValues.append(filterValuesModel)
                        }
                    }
                    filterModel.values = arrayFilterValues
                    if filterModel.values.count > 0 {
                        filterItems.append(filterModel)
                    }
                }
            }
            updateTableViewData()
        }
    }
    
    private func updateCategoryList() {
        let filterModel = ProductFilterModel()
        filterModel.key = FacetsAlgoliaSearchKeysType.categories.rawValue
        filterModel.name = FacetsDisplayNameType.categories.rawValue
        var arrayFilterValues = [ProductFilterValuesModel]()
        let filteredSubCategories = self.subCategories.sorted { $0.productCount! > $1.productCount! }
        for name in filteredSubCategories {
            let filterValuesModel = ProductFilterValuesModel()
            filterValuesModel.name = name.name ?? ""
            arrayFilterValues.append(filterValuesModel)
        }
        filterModel.values = arrayFilterValues
        if filterModel.values.count > 0 {
            filterItems.append(filterModel)
        }
    }
    
    private func updateDiscountPrice() {
        handlePriceAndDiscountLogic(name: FacetsDisplayNameType.discountPrice.rawValue)
    }
    
    private func updateSellingPrice() {
        handlePriceAndDiscountLogic(name: FacetsDisplayNameType.sellingPrice.rawValue)
    }
    
    private func handlePriceAndDiscountLogic(name: String) {
        guard let index = filterItems.index( where:{ $0.name == name }) else { return }
        let maxValue = filterItems[index].values.map{ Float($0.name) ?? 0 }.max() ?? 0
        guard maxValue > 0 else { return }
        let divided = (maxValue / Float(DIVIDE_VALUE)).rounded(.toNearestOrEven)
        var fromValue: Float = 0
        var toValue = divided
        var arrayFilterValues = [ProductFilterValuesModel]()
        for _ in 0..<DIVIDE_VALUE {
            let filterValuesModel = ProductFilterValuesModel()
            let fromIntValue = Int(fromValue)
            let toIntValue = Int(toValue)
            if name == FacetsDisplayNameType.sellingPrice.rawValue {
                filterValuesModel.name = "Rs.\(fromIntValue) - \(toIntValue)"
            } else {
                filterValuesModel.name = "\(fromIntValue)% - \(toIntValue)%"
            }
            arrayFilterValues.append(filterValuesModel)
            fromValue = toValue + 1
            toValue = toValue + divided
        }
        filterItems[index].values = arrayFilterValues
    }
    
    private func addAvailability(subKey: String = "") {
        setUpDataForSingleItem(key: FacetsParseKeysType.inStock.rawValue, name: "Exclude out of stock", subKey: subKey)
    }
    
    private func setUpDataForSingleItem(key: String, name: String, subKey: String = "") {
        let filterModel = ProductFilterModel()
        let namesToBind = getDisplayAndAlgoliaSearchKeyName(parseKey: key)
        if let displayName = namesToBind.displayName, let algoliaSearchKeyName = namesToBind.algoliaSearchKeyName {
            filterModel.key = algoliaSearchKeyName
            filterModel.name = displayName
            var arrayFilterValues = [ProductFilterValuesModel]()
            let filterValuesModel = ProductFilterValuesModel()
            filterValuesModel.name = name
            filterValuesModel.key = subKey
            arrayFilterValues.append(filterValuesModel)
            filterModel.values = arrayFilterValues
            if filterModel.values.count > 0 {
                filterItems.append(filterModel)
            }
        }
    }
}

extension ProductFilterViewModel {
    func assignSelectedFacetsInfo(selectedFacets: [ProductFilterModel]?, subCategories: [SubCategoriesModel]) {
        self.selectedFilterItems = selectedFacets ?? [ProductFilterModel]()
        self.subCategories = subCategories
    }
    
    private func updateTableViewData() {
        updateCategoryList()
        sortArray()
        updateDiscountPrice()
        updateSellingPrice()
        compareSelectedAndNewFacets()
        delegate?.algoliaHitsSuccessProcess()
    }
    
    private func sortArray() {
        filterItems.sort { $0.name < $1.name }
        for value in filterItems {
            value.values = value.values.sorted { $0.name < $1.name }
        }
    }
    
    private func compareSelectedAndNewFacets() {
        for newFacets in filterItems {
            let filtered = selectedFilterItems.filter{$0.key == newFacets.key}
            for selectedFacets in filtered {
                for newValues in newFacets.values {
                    for selectedValues in selectedFacets.values {
                        if newValues.name == selectedValues.name {
                            newValues.isSelected = selectedValues.isSelected
                        }
                    }
                }
            }
        }
    }
    
    func hasSelectedFilter() -> Bool {
        for filter in filterItems {
            for values in filter.values where values.isSelected == true {
                return true
            }
        }
        return false
    }
    
    private func getSearchFiltersBy(text: String, selectedIndex: Int) -> [ProductFilterValuesModel] {
        let filtered = filterItems[selectedIndex].values.filter{$0.name.localizedCaseInsensitiveContains(text)}
        return filtered.sorted { $0.name < $1.name }
    }
    
    func resetSelectedFilter() {
        for facets in filterItems {
            for value in facets.values where value.isSelected == true {
                value.isSelected = false
            }
        }
    }
    
    func searchFilterBy(name: String, selectedIndex: Int) {
        let filterModel = filterItems[selectedIndex]
        if filterModel.name == FacetsDisplayNameType.manufacturer.rawValue {
            searchFilterItems = getSearchFiltersBy(text: name, selectedIndex: selectedIndex)
        } else if filterModel.name == FacetsDisplayNameType.brand.rawValue {
            searchFilterItems = getSearchFiltersBy(text: name, selectedIndex: selectedIndex)
        } else if filterModel.name == FacetsDisplayNameType.categories.rawValue {
            searchFilterItems = getSearchFiltersBy(text: name, selectedIndex: selectedIndex)
        }
    }
    
    private func getDisplayAndAlgoliaSearchKeyName(parseKey: String) -> (displayName: String?, algoliaSearchKeyName: String?) {
        switch parseKey {
        case FacetsParseKeysType.brand.rawValue:
            return (FacetsDisplayNameType.brand.rawValue, FacetsAlgoliaSearchKeysType.brand.rawValue)
        case FacetsParseKeysType.discountPrice.rawValue:
            return (FacetsDisplayNameType.discountPrice.rawValue, FacetsAlgoliaSearchKeysType.discountPrice.rawValue)
        case FacetsParseKeysType.sellingPrice.rawValue:
            return (FacetsDisplayNameType.sellingPrice.rawValue, FacetsAlgoliaSearchKeysType.sellingPrice.rawValue)
        case FacetsParseKeysType.manufacturer.rawValue:
            return (FacetsDisplayNameType.manufacturer.rawValue, FacetsAlgoliaSearchKeysType.manufacturer.rawValue)
        case FacetsParseKeysType.inStock.rawValue:
            return (FacetsDisplayNameType.inStock.rawValue, FacetsAlgoliaSearchKeysType.inStock.rawValue)
        default:
            return (nil, nil)
        }
    }
}
