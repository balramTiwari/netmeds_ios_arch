//
//  ProductListViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import InstantSearchClient

class ProductListViewModel {
    
    var categoryListDetails: ProductListDetailsModel?
    var productList: [ProductsModel] = [ProductsModel]()
    var algoliaSearchList: [AlgoliaSearchHitsModel] = [AlgoliaSearchHitsModel]()
    var productListBanners: [ProductListBannersInfoModel] = [ProductListBannersInfoModel]()
    var totalCount: Int = 0
    weak var delegate: ProductListDelegateHandlers?
    
    func getCategoryProductListService(categoryID: Int, pageNo: Int) {
        NetworkManager.shared.getCategoryListService(categoryID: categoryID, pageNo: pageNo) { [weak self] (error, response) in
            guard error == nil, let categoryList = response as? ProductListModel else { return }
            self?.parseCategoryList(categoryList: categoryList)
        }
    }
    
    func getManufacturerProductListService(manufacturerID: Int, pageNo: Int) {
        NetworkManager.shared.getManufacturerListService(manufacturerID: manufacturerID, pageNo: pageNo) { [weak self] (error, response) in
            guard error == nil, let manufacturerList = response as? ProductListModel else { return }
            self?.parseManufacturerList(manufacturerList: manufacturerList)
        }
    }

    func getCategoryProductBannersService(categoryID: Int) {
        NetworkManager.shared.getCategoryBannersService(categoryID: categoryID) { [weak self] (error, response) in
            guard error == nil, let bannerDetails = response as? ProductListBannersModel else { return }
            self?.parseProductListBanners(bannerDetails: bannerDetails)
        }
    }
    
    func getManufacturerProductBannersService(manufacturerID: Int) {
        NetworkManager.shared.getManufacturerBannersService(manufacturerID: manufacturerID) { [weak self] (error, response) in
            guard error == nil, let bannerDetails = response as? ProductListBannersModel else { return }
            self?.parseProductListBanners(bannerDetails: bannerDetails)
        }
    }

    func algoliaSearchService(index: String = "", filters: String, facetFilters: [Any]?, page: UInt) {
        let query = Query()
        query.hitsPerPage = 10
        query.page = page
        query.facets = ["*"]
        query.clickAnalytics = true
        query.filters = filters
        query.facetFilters = facetFilters
        NetworkManager.shared.algoliaSearchService(index: index, query: query) { [weak self] (error, response) in
            guard error == nil, let algoliaSearchModel = response as? AlgoliaSearchModel else { return }
            self?.parseAlgoliaHitsInfo(algoliaSearchModel: algoliaSearchModel)
        }
    }
}

extension ProductListViewModel {
    private func parseCategoryList(categoryList: ProductListModel) {
        guard categoryList.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: categoryList.errorInfo); return }
        guard let categoryDetails = categoryList.result?.categoryDetails, let productsList = categoryDetails.products else { return }
        self.categoryListDetails = categoryDetails
        self.totalCount = categoryDetails.productCount!
        self.productList.append(contentsOf: productsList)
        self.delegate?.productListSuccessProcess()
    }
    
    private func parseManufacturerList(manufacturerList: ProductListModel) {
        guard manufacturerList.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: manufacturerList.errorInfo); return }
        guard let manufacturerDetails = manufacturerList.result?.manufacturerDetails, let productsList = manufacturerDetails.products else { return }
        self.categoryListDetails = manufacturerDetails
        self.totalCount = manufacturerDetails.productCount!
        self.productList.append(contentsOf: productsList)
        self.delegate?.productListSuccessProcess()
    }

    private func parseProductListBanners(bannerDetails: ProductListBannersModel) {
        guard bannerDetails.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: bannerDetails.errorInfo); return }
        guard let banners = bannerDetails.result?.categoryBanner else { return }
        self.productListBanners = banners
        self.delegate?.productListBannersSuccessProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}

extension ProductListViewModel {
    private func parseAlgoliaHitsInfo(algoliaSearchModel: AlgoliaSearchModel) {
        totalCount = algoliaSearchModel.nbHits!
        guard let hits = algoliaSearchModel.hits else { return }
        self.algoliaSearchList.append(contentsOf: hits)
        self.delegate?.algoliaHitsSuccessProcess()
    }
}
