//
//  ProductSortViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//


import Foundation

class ProductSortViewModel {
    
    var sortItems: [ProductSortModel] = [ProductSortModel]()
    var selectedSortItems: [ProductSortModel] = [ProductSortModel]()
    weak var delegate: ProductSortDelegateHandlers?
    
    func setUpSortData() {
        sortItems.removeAll()
        sortItems.append(ProductSortModel(index: kAlgoliaSortPopularitySearchIndex, name: SortDisplayNameType.popularity.rawValue))
        sortItems.append(ProductSortModel(index: kAlgoliaSortDiscountSearchIndex, name: SortDisplayNameType.discount.rawValue))
        sortItems.append(ProductSortModel(index: kAlgoliaSortPriceLowToHighSearchIndex, name: SortDisplayNameType.priceLowtoHigh.rawValue))
        sortItems.append(ProductSortModel(index: kAlgoliaSortPriceHighToLowSearchIndex, name: SortDisplayNameType.priceHightoLow.rawValue))
        sortItems.append(ProductSortModel(index: kAlgoliaSortNameAToZSearchIndex, name: SortDisplayNameType.nameAtoZ.rawValue))
        sortItems.append(ProductSortModel(index: kAlgoliaSortNameZToASearchIndex, name: SortDisplayNameType.nameZtoA.rawValue))
        compareSelectedAndNewSortItems()
        if !hasSelectedSortItems() {
            sortItems[0].isSelected = true
        }
        self.delegate?.productSortSuccessProcess()
    }
    
    private func compareSelectedAndNewSortItems() {
        for newItem in sortItems {
            for selectedItem in selectedSortItems where newItem.name == selectedItem.name {
                newItem.isSelected = selectedItem.isSelected
            }
        }
    }
    
    func hasSelectedSortItems() -> Bool {
        for item in sortItems where item.isSelected == true {
            return true
        }
        return false
    }
    
    func resetSelectedItems() {
        let selectedItems = sortItems.filter{$0.isSelected == true}
        for item in selectedItems {
            item.isSelected = false
        }
    }
}
