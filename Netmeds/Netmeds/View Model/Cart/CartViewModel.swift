//
//  CartViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

enum CartViewModelPageType: Int {
    case cart = 1
    case orderReview
    case none
}

enum CartPageHeaderType: Int {
    case becomeFirstMember = 1
    case undeliverable
    case medicinesOutOfStock
    case products
    case prescription
    case promoCode
    case deliveryAddress
    case paymentDetails
    case disclaimer
    case none
}

enum CartPageRowType: Int {
    case promoCode
    case separatorView
    case superCash
    case totalSavings
    case deliveryCharges
    case none
}

class CartViewModel {
    private var totalAmount: String = ""
    private var shippingAddressID: Int = 0
    private var hasPrimeProduct: Bool = false
    private var hasColdStorageProduct: Bool = false
    private var isPrimeUser: Bool = false
    private var walletBalance: Double = 0
    private var orderReviewDiscount: Double = 0.0
    private var isNMSSuperCashApplied: Bool = false
    private var configDetails = ConfigManager.shared.configDetails
    private let dispatchGroup = DispatchGroup()
    private var localCartHeaderModel = [CartHeaderModel]()
    private var superCash: AllCouponsModel?
    private var pageType: CartViewModelPageType = .none
    weak var delegate: CartDelegateHandlers?
    weak var promoCodeListDelegate: PromoCodeListDelegateHandlers?
    var cartHeaderModel = [CartHeaderModel]()
    var allCoupons = [AllCouponsModel]()
    var appliedCouponName: String = ""
    var isUsedSuperCash: Bool = false
    var hasRxProduct: Bool = false
    var hasOutOfStockProduct: Bool = false
    // WARNING: TO BE START REMOVED
    var cartID: Int = 0
    // WARNING: TO BE END REMOVED

    func getCartInfoService(pageType: CartViewModelPageType = .none) {
        self.pageType = pageType
        switch pageType {
        case .cart:
            getCartDetailsService()
            getCustomerDetailsService()
            getAllCouponsService()
            getWalletBalanceService()
        case .orderReview:
            getCartDetailsService()
        default: break
        }
        dispatchGroup.notify(queue: .main) {
            self.addOtherInfo()
        }
    }
    
    //   private func getCartDetailsService(deliveryAddress: AllAddressInfoModel? = nil) {
    private func getCartDetailsService() {
        dispatchGroup.enter()
        localCartHeaderModel.removeAll()
        NetworkManager.shared.getCartService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let cartInfo = response as? CartModel else { return }
            //            if let selectedDeliveryAddress = deliveryAddress {
            //                self?.addDeliveryAddressInfo(list: selectedDeliveryAddress)
            //            }
            self?.parseCartDetails(cartInfo: cartInfo)
        }
    }
    
    func removeItemFromCartService(productCode: String, quantity: String) {
        NetworkManager.shared.removeItemFromCartService(productCode: productCode, quantity: quantity) { [unowned self] (error, response) in
            guard error == nil, let cartInfo = response as? SuccessModel else { return }
            guard cartInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
            self.getCartInfoService(pageType: self.pageType)
        }
    }
    
    func addItemToCartService(productCode: String, quantity: String) {
        NetworkManager.shared.addItemToCartService(productCode: productCode, quantity: quantity) { [unowned self] (error, response) in
            guard error == nil, let cartInfo = response as? SuccessModel else { return }
            guard cartInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
            self.getCartInfoService(pageType: self.pageType)
        }
    }
    
    func applyCouponCodeService(couponCode: String) {
        NetworkManager.shared.applyCouponCodeService(couponCode: couponCode) { [weak self] (error, response) in
            guard error == nil, let couponResponse = response as? SuccessModel else { return }
            guard couponResponse.status?.lowercased() == ResponseStatus.success.rawValue else { self?.parseErrorInfo(errorInfo: couponResponse.errorInfo); return }
            self?.promoCodeListDelegate?.successProcess(isAppliedPromoCode: true)
        }
    }
    
    func unApplyCouponCodeService(couponCode: String) {
        NetworkManager.shared.unApplyCouponCodeService(couponCode: couponCode) { [weak self] (error, response) in
            guard error == nil, let couponResponse = response as? SuccessModel else { return }
            guard couponResponse.status?.lowercased() == ResponseStatus.success.rawValue else { self?.parseErrorInfo(errorInfo: couponResponse.errorInfo); return }
            self?.promoCodeListDelegate?.successProcess(isAppliedPromoCode: false)
        }
    }
    
    private func getCustomerDetailsService() {
        dispatchGroup.enter()
        NetworkManager.shared.customerDetailsService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let customerInfo = response as? CustomerInfoModel else { return }
            self?.parseCustomerInfo(response: customerInfo)
        }
    }
    
    private func getAllCouponsService() {
        dispatchGroup.enter()
        NetworkManager.shared.getAllCouponsService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let couponsInfo = response as? AllCouponsInfoModel else { return }
            self?.parseCouponsInfo(couponsInfo: couponsInfo)
        }
    }
    
    private func getWalletBalanceService() {
        dispatchGroup.enter()
        NetworkManager.shared.getWalletBalanceService { [weak self] (error, response) in
            self?.dispatchGroup.leave()
            guard error == nil, let walletBalanceInfo = response as? WalletBalanceModel else { return }
            self?.parseWalletBalanceInfo(response: walletBalanceInfo)
        }
    }
    
    func applySuperCashService() {
        NetworkManager.shared.applySuperCashService { [weak self] (error, response) in
            guard error == nil, let superCashResponse = response as? SuccessModel else { return }
            guard superCashResponse.status?.lowercased() == ResponseStatus.success.rawValue else { self?.parseErrorInfo(errorInfo: superCashResponse.errorInfo); return }
            self?.delegate?.applySuperCashProcess()
        }
    }
    
   private func getDeliveryEstimationService() {
        guard let params = getDeliveryEstimationParams() else { return }
        NetworkManager.shared.getDeliveryEstimationDateService(params: params) { [weak self] (error, response) in
            guard error == nil, let deliveryEstimationInfo = response as? [DeliveryEstimationModel] else { return }
            self?.parsedeliveryEstimationInfo(deliveryEstimationInfo: deliveryEstimationInfo)
        }
    }
}

extension CartViewModel {
    private func getDeliveryEstimationParams() -> JSONDictAny? {
        guard let index = cartHeaderModel.index( where:{ $0.type == .products }) else { return nil }
        let rowModel = cartHeaderModel[index].rowModel
        var drugList = [Any]()
        for item in rowModel {
            var cItem = [String: Any]()
            cItem["itemcode"] = item.productCode
            cItem["Qty"] = item.quantity
            drugList.append(cItem)
        }
        guard drugList.count > 0 else { return nil }
        let params = [
            "lstdrug" : drugList,
            "pincode" : "600004",
            "do_not_split": true,
            "calling_to_route": false
            ] as JSONDictAny
        return params
        //        if drugList.count > 0 {
        ////            if let selectedAddress = passedSelectedAddress.pin {
        //                let params = [
        //                    "lstdrug" : drugList,
        ////                    "pincode" : "\(selectedAddress)",
        //                    "pincode" : "600004",
        //                    "do_not_split": true,
        //                    "calling_to_route": false
        //                    ] as JSONDictAny
        ////                self.getDeliveryEstimationSplitPost(url: NSObjectCommon.shared.DELIVERYDATEAPI, params: params)
        ////            }
        //        }
    }
}

extension CartViewModel {
    private func parseCartDetails(cartInfo: CartModel) {
        guard cartInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
            self.parseErrorInfo(errorInfo: cartInfo.errorInfo); return }
        self.resetValues()
        guard let items = cartInfo.result?.cart?.lines else { return }
        self.parseNMSSuperCashInfo(cartInfo: cartInfo)
        self.addPaymentDetailsInfo(cartInfo: cartInfo)
        self.parseProductsInfo(items: items)
        // WARNING: TO BE START REMOVED
        self.cartID = cartInfo.result?.cart?.id ?? 0
        // WARNING: TO BE END REMOVED
    }
    
    private func parseNMSSuperCashInfo(cartInfo: CartModel) {
        guard let cart = cartInfo.result?.cart else { return }
        guard let walletSavings = cart.usedWalletAmounts, let superCash = walletSavings.superCash, superCash > 0 else { return }
        isNMSSuperCashApplied = true
    }
    
    private func parseProductsInfo(items: [CartLineInfoModel]) {
        var rowModel = [CartRowModel]()
        for item in items {
            parseCartLineInfo(item: item)
            rowModel.append(CartRowModel(productName: item.displayName!, price: item.sellingPrice!, manufacturerName: item.manufacturerName!, quantity: item.quantity!, packSize: item.packSize!, packSizeType: item.productType!, isOutOfStock: item.isOutOfStock!, imgURL: item.urlPath!, discount: item.lineProductDiscount!, mrp: item.mrp!, isRxRequired: item.isRxRequired!, productCode: item.productCode!, isColdStorage: item.isColdStorage!, availabilityStatus: item.availabilityStatus!, maxQtyInOrder: item.maxQtyInOrder!, productImagePath: item.productImagePath!))
        }
        let headerModel = CartHeaderModel(title: "PRODUCTS", type: .products, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func parseCustomerInfo(response: CustomerInfoModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue, let customerDetails = response.result?.details else { return }
        UserManager.shared.resetUserDetails(userInfo: customerDetails)
        isPrimeUser = UserManager.shared.userDetails?.isPrimeUser ?? false
    }
    
    private func parseCouponsInfo(couponsInfo: AllCouponsInfoModel) {
        guard couponsInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        self.allCoupons.removeAll()
        self.superCash = nil
        if let couponsList = couponsInfo.result?.couponList {
            self.allCoupons = couponsList
        }
        if let superCash = couponsInfo.result?.superCash {
            self.superCash = superCash
        }
    }
    
    private func parseWalletBalanceInfo(response: WalletBalanceModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        walletBalance = 0
        guard let cashbackUseableBalance = response.result?.cashbackUseableBalance, cashbackUseableBalance > 0 else { return }
        walletBalance = cashbackUseableBalance
    }
    
    private func parseCartPageInfo() {
        guard let index = localCartHeaderModel.index( where:{ $0.type == .products }), localCartHeaderModel[index].rowModel.count > 0 else {
            self.delegate?.emptyCartProcess(); return }
        addBecomeFirstMemberInfo()
        addUnDeliverableInfo()
        addMedicinesOutOfStockInfo()
        addCouponCodeInfo()
        addDisclaimerInfo()
        //        updateSelectedCouponFlag()
        localCartHeaderModel.sort(by: CartHeaderModel.sequenceByType)
        self.cartHeaderModel.removeAll()
        self.cartHeaderModel.append(contentsOf: self.localCartHeaderModel)
        self.localCartHeaderModel.removeAll()
        self.delegate?.successProcess(totalAmount: self.totalAmount, shippingAddressID: self.shippingAddressID)
    }
    
    private func parseOrderReviewPageInfo() {
        addUnDeliverableInfo()
        addMedicinesOutOfStockInfo()
        addOrderReviewCouponCodeInfo()
        addDisclaimerInfo()
        localCartHeaderModel.sort(by: CartHeaderModel.sequenceByType)
        self.cartHeaderModel.removeAll()
        self.cartHeaderModel.append(contentsOf: self.localCartHeaderModel)
        self.localCartHeaderModel.removeAll()
        self.delegate?.successProcess(totalAmount: self.totalAmount, shippingAddressID: self.shippingAddressID)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.getDeliveryEstimationService()
        }
    }
    
    private func parseCartLineInfo(item: CartLineInfoModel) {
        // Prime User Check
        if item.productCode! == kPrimeProduct2001 || item.productCode! == kPrimeProduct2002 {
            hasPrimeProduct = true
        }
        // OutOfStock Check
        if item.isOutOfStock! || item.quantity! > item.maxQtyInOrder! {
            hasOutOfStockProduct = true
        }
        // Cold Storage Check
        if item.isColdStorage! {
            hasColdStorageProduct = true
        }
        // Rx Product Check
        if item.isRxRequired! {
            hasRxProduct = true
        }
        orderReviewDiscount = orderReviewDiscount + item.lineCouponDiscount!
    }
    
    private func parsedeliveryEstimationInfo(deliveryEstimationInfo: [DeliveryEstimationModel]) {
        guard let index = cartHeaderModel.index( where:{ $0.type == .products }) else { return }
        var rowModel = cartHeaderModel[index].rowModel
        for deliveryInfo in deliveryEstimationInfo {
            for (rowIndex, value) in rowModel.enumerated() {
                if deliveryInfo.productCode!.isValidString {
                    if Int(deliveryInfo.productCode!) ?? 0 == value.productCode {
                        rowModel[rowIndex].sellerName = deliveryInfo.sellerName!
                        rowModel[rowIndex].deliveryEstimatedDate = deliveryInfo.deliveryDate!
                        rowModel[rowIndex].expiryDate = deliveryInfo.expiryDate!
                        rowModel[rowIndex].sellerAddress = deliveryInfo.sellerAddress!
                    }
                }
            }
        }
        cartHeaderModel[index].rowModel = rowModel
        self.delegate?.deliveryEstimationSuccessProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
        self.promoCodeListDelegate?.failureProcess(errorMessage: errorMessage)
    }
}

extension CartViewModel {
    private func resetValues() {
        totalAmount = ""
        shippingAddressID = 0
        hasPrimeProduct = false
        hasColdStorageProduct = false
        orderReviewDiscount = 0.0
        isNMSSuperCashApplied = false
        appliedCouponName = ""
        isUsedSuperCash = false
        hasRxProduct = false
        hasOutOfStockProduct = false
    }
    
    private func addOtherInfo() {
        switch self.pageType {
        case .cart:
            parseCartPageInfo()
        case .orderReview:
            parseOrderReviewPageInfo()
        default: break
        }
    }
    
    private func addBecomeFirstMemberInfo() {
        guard let isEnabled = configDetails?.primeConfig?.isEnabled, isEnabled else { return }
        guard !isPrimeUser, !hasPrimeProduct else { return }
        guard let shoppingCart = configDetails?.primeConfig?.shoppingCart, let title = shoppingCart.header, let subTitle = shoppingCart.desc else { return }
        var rowModel = [CartRowModel]()
        rowModel.append(CartRowModel(title: title, subTitle: subTitle))
        let headerModel = CartHeaderModel(title: "", type: .becomeFirstMember, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addUnDeliverableInfo() {
        guard hasColdStorageProduct else { return }
        var rowModel = [CartRowModel]()
//        rowModel.append(CartRowModel(title: "UNDELIVERABLE", subTitle: "One or more medicines cannot be delivered at your location. Please remove the medicines."))
        rowModel.append(CartRowModel(title: "UNDELIVERABLE", subTitle: "One or more items are not available at your location. Please remove the medicines(s)."))
        let headerModel = CartHeaderModel(title: "", type: .undeliverable, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addMedicinesOutOfStockInfo() {
        guard hasOutOfStockProduct else { return }
        var rowModel = [CartRowModel]()
        rowModel.append(CartRowModel(title: "Some medicines have gone out of stock.\nHow would you like to proceed?", btnTitle: "Take Action"))
        let headerModel = CartHeaderModel(title: "", type: .medicinesOutOfStock, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addCouponCodeInfo() {
        var rowModel = [CartRowModel]()
        let headerTitle: String = appliedCouponName.isValidString ? "PROMOCODE APPLIED": "APPLY PROMO CODE"
        let couponTitle: String = appliedCouponName.isValidString ? appliedCouponName: "APPLY PROMO CODE"
        var couponDesc: String = kPromoCodeDefaultDescriptionText
        var discountPercentage: String = ""
        if appliedCouponName.isValidString {
            couponDesc = ""
            let filter = allCoupons.filter { $0.couponCode! == appliedCouponName }
            if filter.count > 0, let firstCoupon = filter.first {
                if let description = firstCoupon.couponDesc {
                    couponDesc = description
                }
                if let discount = firstCoupon.discount {
                    discountPercentage = "You Save \(discount)%"
                }
            }
        }
        rowModel.append(CartRowModel(type: .promoCode, title: couponTitle, isOutOfStock: appliedCouponName.isValidString, sellerName: discountPercentage, desc: couponDesc))
        // Add Super Cash Info
        if walletBalance > 0 {
            rowModel.append(CartRowModel(type: .separatorView))
            var superCashDesc: String = ""
            if let couponDesc = superCash?.couponDesc {
                superCashDesc = couponDesc
            }
            rowModel.append(CartRowModel(type: .superCash, title: "NMS SUPER CASH", price: walletBalance, isOutOfStock: isUsedSuperCash, desc: superCashDesc))
        }
        let headerModel = CartHeaderModel(title: headerTitle, type: .promoCode, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addPaymentDetailsInfo(cartInfo: CartModel) {
        guard let cart = cartInfo.result?.cart else { return }
        self.shippingAddressID = cart.shippingAddressID!
        self.isUsedSuperCash = cart.isUsedSuperCash!
        self.appliedCouponName = cart.appliedCouponName!
        var additionalDiscount: Double = 0.0
        var netmedsDiscount: Double = 0.0
        var walletDiscount: Double = 0.0
        var voucherDiscount: Double = 0.0
        var totalPriceWithDiscount: Double = 0.0
        var rowModel = [CartRowModel]()
        for item in cart.lines ?? [CartLineInfoModel]() {
            additionalDiscount = additionalDiscount + item.lineProductDiscount!
            netmedsDiscount = netmedsDiscount + item.lineCouponDiscount!
        }
        totalPriceWithDiscount = cart.netPayable!
        if cart.subTotal! > 0 {
            rowModel.append(CartRowModel(productName: "MRP Total", paymentDetailsPrice: "₹ \(String(format: "%.2f", cart.subTotal!))"))
        }
        let discount = cart.shippingDiscount!
        var discountValue = String(format: "₹ %.2f", discount)
        if discountValue == "₹ 0.00"{
            discountValue = ""
        }
        if (cart.shippingDiscount! == cart.shippingCharges!) || cart.shippingDiscount! == 0 {
            rowModel.append(CartRowModel(type: .deliveryCharges, productName: "Delivery Charges", paymentDetailsPrice: " ₹ \(String(format: "%.2f", cart.shippingCharges!))"))
        } else {
            rowModel.append(CartRowModel(type: .deliveryCharges, productName: "Delivery Charges", paymentDetailsPrice: "\(discountValue), ₹ \(String(format: "%.2f", cart.shippingCharges!))"))
        }
        if cart.productDiscountTotal! > 0 {
            rowModel.append(CartRowModel(productName: "Additional Discount", paymentDetailsPrice: "₹ -\(String(format: "%.2f", additionalDiscount))"))
        }
        if cart.couponDiscountTotal! > 0 {
            rowModel.append(CartRowModel(productName: "Netmeds Discount", paymentDetailsPrice: "₹ -\(String(format: "%.2f", netmedsDiscount))"))
        }
        if let walletSavings = cart.usedWalletAmounts {
            walletDiscount += walletSavings.superCash!
            walletDiscount += walletSavings.walletCash!
            walletDiscount = walletSavings.total!
            if walletDiscount > 0 {
                rowModel.append(CartRowModel(productName: "Wallet Discount", paymentDetailsPrice: "₹ -\(String(format: "%.2f", walletDiscount))"))
            }
        }
        if cart.usedVoucherAmount! > 0 {
            voucherDiscount = cart.usedVoucherAmount!
            rowModel.append(CartRowModel(productName: "Voucher Discount", paymentDetailsPrice: "₹ -\(String(format: "%.2f", voucherDiscount))"))
        }
        rowModel.append(CartRowModel(productName: "Total Amount*", paymentDetailsPrice: "₹ \(String(format: "%.2f", totalPriceWithDiscount))"))
        self.totalAmount = "₹ \(NSNumber(value: totalPriceWithDiscount).formatFractionDigits())"
        //            if let m2 = NMDefault.shared.Methodtwo, m2 {
        //                self.bottomTotalamount.text = "₹ -"
        //            } else {
        //                self.bottomTotalamount.text = "₹ \(NSNumber(value: self.totalPriceWithDiscount).formatFractionDigits())"
        //            }
        if cart.totalSavings! > 0 {
            rowModel.append(CartRowModel(type: .totalSavings, productName: "TOTAL SAVINGS", paymentDetailsPrice: "₹ \(String(format: "%.2f", cart.totalSavings!))"))
        }
        let headerModel = CartHeaderModel(title: "PAYMENT DETAILS", type: .paymentDetails, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addDisclaimerInfo() {
        var rowModel = [CartRowModel]()
        rowModel.append(CartRowModel())
        let headerModel = CartHeaderModel(title: "", type: .disclaimer, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addOrderReviewCouponCodeInfo() {
        let headerTitle: String = "PROMOCODE APPLIED"
        var discountPercentage: String = ""
        var couponTitle: String = ""
        var isCoupCodeSelected: Bool = false
        if appliedCouponName.isValidString {
            couponTitle = appliedCouponName
            discountPercentage = orderReviewDiscount > 0 ? "You save \(String(format: "%.2f", orderReviewDiscount))": ""
            isCoupCodeSelected = true
        } else {
            if isNMSSuperCashApplied {
                couponTitle = "NMS SUPERCASH"
                isCoupCodeSelected = true
            }
        }
        guard couponTitle.isValidString else { return }
        var rowModel = [CartRowModel]()
        rowModel.append(CartRowModel(type: .promoCode, title: couponTitle, isOutOfStock: isCoupCodeSelected, sellerName: discountPercentage))
        let headerModel = CartHeaderModel(title: headerTitle, type: .promoCode, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    private func addDeliveryAddressInfo(list: AllAddressInfoModel) {
        let name = "\(list.firstName!) \(list.lastName!)"
        let cityName = "\(list.city!) - \(list.pinCode!)"
        let telephoneNo = "+91 - \(list.mobileNo!)"
        let address = "\(list.line1!),\(list.line3!)\n\(cityName),\n\(telephoneNo)"
        var rowModel = [CartRowModel]()
        rowModel.append(CartRowModel(productName: name, desc: address))
        let headerModel = CartHeaderModel(title: "DELIVERY ADDRESS", type: .deliveryAddress, rowModel: rowModel)
        localCartHeaderModel.append(headerModel)
    }
    
    //    private func updateSelectedCouponFlag() {
    //        for (index, coupon) in allCoupons.enumerated() {
    //            if coupon.couponCode! == appliedCouponName {
    //                allCoupons[index].isSelected = true
    //                break
    //            }
    //        }
    //    }
    
}

struct CartHeaderModel {
    var title: String = ""
    var type: CartPageHeaderType = .none
    var rowModel: [CartRowModel] = [CartRowModel]()
    
    init(title: String = "", type: CartPageHeaderType = .none, rowModel: [CartRowModel] = [CartRowModel]()) {
        self.title = title
        self.type = type
        self.rowModel = rowModel
    }
    
    static func sequenceByType(lhs: CartHeaderModel, rhs: CartHeaderModel) -> Bool {
        return lhs.type.rawValue < rhs.type.rawValue
    }
}

struct CartRowModel {
    var type: CartPageRowType = .none
    var title: String = ""
    var subTitle: String = ""
    var btnTitle: String = ""
    var productName: String = ""
    var price: Double = 0
    var manufacturerName: String = ""
    var quantity: Int = 0
    var packSize: String = ""
    var packSizeType: String = ""
    var isOutOfStock: Bool = false
    var imgURL: String = ""
    var discount: Double = 0
    var mrp: Double = 0
    var isRxRequired: Bool = false
    var productCode: Int = 0
    var isColdStorage: Bool = false
    var availabilityStatus: String = ""
    var paymentDetailsPrice: String = ""
    var maxQtyInOrder: Int = 0
    var sellerName: String = ""
    var sellerAddress: String = ""
    var expiryDate: String = ""
    var deliveryEstimatedDate: String = ""
    var desc: String = ""
    var productImagePath: String = ""
    
    init(type: CartPageRowType = .none, title: String = "", subTitle: String = "", btnTitle: String = "", productName: String = "", price: Double = 0, manufacturerName: String = "", quantity: Int = 0, packSize: String = "", packSizeType: String = "", isOutOfStock: Bool = false, imgURL: String = "", discount: Double = 0, mrp: Double = 0, isRxRequired: Bool = false, productCode: Int = 0, isColdStorage: Bool = false, availabilityStatus: String = "", paymentDetailsPrice: String = "", maxQtyInOrder: Int = 0, sellerName: String = "", sellerAddress: String = "", expiryDate: String = "", deliveryEstimatedDate: String = "", desc: String = "", productImagePath: String = "") {
        self.type = type
        self.title = title
        self.subTitle = subTitle
        self.btnTitle = btnTitle
        self.productName = productName
        self.price = price
        self.manufacturerName = manufacturerName
        self.quantity = quantity
        self.packSize = packSize
        self.packSizeType = packSizeType
        self.isOutOfStock = isOutOfStock
        self.imgURL = imgURL
        self.discount = discount
        self.mrp = mrp
        self.isRxRequired = isRxRequired
        self.productCode = productCode
        self.isColdStorage = isColdStorage
        self.availabilityStatus = availabilityStatus
        self.paymentDetailsPrice = paymentDetailsPrice
        self.maxQtyInOrder = maxQtyInOrder
        self.sellerName = sellerName
        self.sellerAddress = sellerAddress
        self.expiryDate = expiryDate
        self.deliveryEstimatedDate = deliveryEstimatedDate
        self.desc = desc
        self.productImagePath = productImagePath
    }
}
