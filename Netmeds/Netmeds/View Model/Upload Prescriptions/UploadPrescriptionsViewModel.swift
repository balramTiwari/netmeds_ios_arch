//
//  UploadPrescriptionsViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 13/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum UploadPrescriptionsHeaderType: Int {
    case upload = 1
    case prescriptionsGuide
    case none
}

enum UploadPrescriptionsRowType: Int {
    case title = 1
    case options
    case prescriptions
    case prescriptionsGuide
    case none
}

enum UploadPrescriptionsSubRowType {
    case camera
    case gallery
    case pastRx
    case prescriptions
    case none
}

class UploadPrescriptionsViewModel {
    private var recentRxIDPrescriptions = [String]()
    var uploadPrescriptionsHeaderModel = [UploadPrescriptionsHeaderModel]()
    var pastPrescriptionsHeaderModel = [PastPrescriptionsHeaderModel]()
    weak var delegate: UploadPrescriptionsDelegateHandlers?
}

extension UploadPrescriptionsViewModel {
    func addDefaultInfo() {
        addDefaultUploadSection()
        addDefaultGuideSection()
        delegate?.successProcess(isEnabled: false)
    }
    
    func method2CartsService() {
        NetworkManager.shared.method2CartsService { [weak self] (error, response) in
            guard error == nil, let method2Carts = response as? Method2CartsModel else { return }
            self?.parseMethod2CartsInfo(method2Carts: method2Carts)
        }
    }
    
    func detachAndUploadPrescriptions(params: [Any]?) {
        var rxIDs = [String]()
        if let subRowModel = getAllRxIDsPrescriptions() {
            for model in subRowModel {
                rxIDs.append(model.imgName)
            }
        }
        let toDetachRxIDs = recentRxIDPrescriptions.subtracting(from: rxIDs)
        if toDetachRxIDs.count > 0 {
            detachPrescriptionsService(detatchRxIDs: toDetachRxIDs, params: params)
        } else {
            uploadSelectedPrescriptionsService(params: params)
        }
    }
    
   private func uploadSelectedPrescriptionsService(params: [Any]?) {
        NetworkManager.shared.uploadSelectedPrescriptionsService(params: params) { [weak self] (error, response) in
            guard error == nil, let cartInfo = response as? CartModel else { return }
            self?.parseUploadedPrescriptionsInfo(cartInfo: cartInfo)
        }
    }
    
   private func detachPrescriptionsService(detatchRxIDs: [String], params: [Any]?) {
        var detatchPrescriptions = ""
        for (index, rxID) in detatchRxIDs.enumerated() {
            if index >= 9 {
                detatchPrescriptions = detatchPrescriptions + "&pid\(index + 1)=\(rxID)"
            } else {
                detatchPrescriptions = detatchPrescriptions + "&pid0\(index + 1 )=\(rxID)"
            }
        }
        NetworkManager.shared.detachPrescriptionsService(detatchPrescriptions: detatchPrescriptions) { [weak self] (error, response) in
            guard error == nil, let successInfo = response as? SuccessModel else { return }
            guard successInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
            self?.uploadSelectedPrescriptionsService(params: params)
        }
    }
    
    private func getCartDetailsService() {
        NetworkManager.shared.getCartService { [weak self] (error, response) in
            guard error == nil, let cartInfo = response as? CartModel else { return }
            self?.parseCartDetails(cartInfo: cartInfo)
        }
    }
}

extension UploadPrescriptionsViewModel {
    private func parseMethod2CartsInfo(method2Carts: Method2CartsModel) {
        guard method2Carts.status?.lowercased() == ResponseStatus.success.rawValue else {
            //            self.parseErrorInfo(errorInfo: cartInfo.errorInfo);
            return }
        guard let result = method2Carts.result, let cartID = result.first else { return }
        MedicineFlowManager.shared.cartID = cartID
        getCartDetailsService()
    }
    
    private func parseCartDetails(cartInfo: CartModel) {
        guard cartInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
            //            self.parseErrorInfo(errorInfo: cartInfo.errorInfo);
            return }
        guard let prescriptions = cartInfo.result?.cart?.prescriptions else { return }
        recentRxIDPrescriptions.removeAll()
        MedicineFlowManager.shared.cartID = cartInfo.result?.cart?.id ?? 0
        recentRxIDPrescriptions.append(contentsOf: prescriptions)
        addPrescriptions(rxIDs: prescriptions)
    }
    
    private func parseUploadedPrescriptionsInfo(cartInfo: CartModel) {
        guard cartInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
            //            self.parseErrorInfo(errorInfo: cartInfo.errorInfo);
            return }
        guard let prescriptions = cartInfo.result?.cart?.prescriptions else { return }
        recentRxIDPrescriptions.removeAll()
        MedicineFlowManager.shared.cartID = cartInfo.result?.cart?.id ?? 0
        recentRxIDPrescriptions.append(contentsOf: prescriptions)
        addUploadedPrescriptions(rxIDs: prescriptions)
    }
}

extension UploadPrescriptionsViewModel {
    private func addDefaultUploadSection() {
        var rowModel = [UploadPrescriptionsRowModel]()
        rowModel.append(UploadPrescriptionsRowModel(title: "Please upload images of valid prescription from your doctor.", type: .title))
        var optionsSubRowModel = [UploadPrescriptionsSubRowModel]()
        optionsSubRowModel.append(UploadPrescriptionsSubRowModel(title: "Camera", imgName: "cameraActive", type: .camera))
        optionsSubRowModel.append(UploadPrescriptionsSubRowModel(title: "Gallery", imgName: "gallery_active", type: .gallery))
        optionsSubRowModel.append(UploadPrescriptionsSubRowModel(title: "Past Rx", imgName: "download", type: .pastRx))
        rowModel.append(UploadPrescriptionsRowModel(type: .options, subRowModel: optionsSubRowModel))
        let headerModel = UploadPrescriptionsHeaderModel(title: "UPLOAD", type: .upload, rowModel: rowModel)
        uploadPrescriptionsHeaderModel.append(headerModel)
    }
    
    private func addDefaultGuideSection() {
        var rowModel = [UploadPrescriptionsRowModel]()
        rowModel.append(UploadPrescriptionsRowModel(title: "Image should be sharp and contain below mentioned 4 points", type: .title))
        rowModel.append(UploadPrescriptionsRowModel(imgName: "group_320", type: .prescriptionsGuide))
        let headerModel = UploadPrescriptionsHeaderModel(title: "VALID PRESCRIPTION GUIDE", type: .prescriptionsGuide, rowModel: rowModel)
        uploadPrescriptionsHeaderModel.append(headerModel)
    }
    
    func addPrescription(image: UIImage? = nil, rxID: String = "") {
        guard let headerIndex = getIndexFromHeader() else { return }
        guard let rowIndex = getIndexFromRow() else {
            let prescriptionSubRowModel = UploadPrescriptionsSubRowModel(imgName: rxID, image: image, type: .prescriptions)
            let rowModel = UploadPrescriptionsRowModel(type: .prescriptions, subRowModel: [prescriptionSubRowModel])
            uploadPrescriptionsHeaderModel[headerIndex].rowModel.append(rowModel)
            delegate?.successProcess(isEnabled: true)
            return }
        let prescriptionSubRowModel = UploadPrescriptionsSubRowModel(imgName: rxID, image: image, type: .prescriptions)
        uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.append(prescriptionSubRowModel)
        delegate?.successProcess(isEnabled: true)
    }
    
    func addPrescriptions(rxIDs: [String]) {
        for rxID in rxIDs {
            addPrescription(rxID: rxID)
        }
    }
    
    func addUploadedPrescriptions(rxIDs: [String]) {
        guard let headerIndex = getIndexFromHeader() else { return }
        guard let rowIndex = getIndexFromRow() else { return }
        uploadPrescriptionsHeaderModel[headerIndex].rowModel.remove(at: rowIndex)
        var prescriptionsSubRowModel = [UploadPrescriptionsSubRowModel]()
        for rxID in rxIDs {
            prescriptionsSubRowModel.append(UploadPrescriptionsSubRowModel(imgName: rxID, type: .prescriptions))
        }
        let rowModel = UploadPrescriptionsRowModel(type: .prescriptions, subRowModel: prescriptionsSubRowModel)
        uploadPrescriptionsHeaderModel[headerIndex].rowModel.append(rowModel)
        delegate?.successProcess(isEnabled: true)
    }
    
    func getPrescriptionsRowModelFrom(index: Int) -> UploadPrescriptionsRowModel? {
        return getRowModelFrom(index: index)
    }
    
    func getPrescriptionsRowModelFrom() -> UploadPrescriptionsRowModel? {
        return getRowModelFrom()
    }
    
    func getAllRxIDsPrescriptions() -> [UploadPrescriptionsSubRowModel]? {
        guard let headerIndex = getIndexFromHeader() else { return nil }
        guard let rowIndex = getIndexFromRow() else { return nil }
        return uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.filter { $0.image == nil }
    }

    func resetSelectedPrescriptions(rxIDs: [String]) {
        removeAllRxIDsPrescriptions()
        addPrescriptions(rxIDs: rxIDs)
        delegate?.successProcess(isEnabled: true)
    }
    
    func removePrescription(index: Int) {
        guard let headerIndex = getIndexFromHeader() else { return }
        guard let rowIndex = getIndexFromRow() else { return }
        uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.remove(at: index)
        if uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.count == 0 {
            uploadPrescriptionsHeaderModel[headerIndex].rowModel.remove(at: rowIndex)
            delegate?.successProcess(isEnabled: false)
        }
    }
    
    func removeAllPrescriptions() {
        guard let headerIndex = getIndexFromHeader() else { return }
        guard let rowIndex = getIndexFromRow() else { return }
        uploadPrescriptionsHeaderModel[headerIndex].rowModel.remove(at: rowIndex)
        delegate?.successProcess(isEnabled: false)
    }
    
    func removeAllRxIDsPrescriptions() {
        guard let headerIndex = getIndexFromHeader() else { return }
        guard let rowIndex = getIndexFromRow() else { return }
        uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.removeAll { $0.image == nil }
    }
    
    //    func removeRxIDPrescriptionsImage() {
    //        guard let headerIndex = getUploadHeaderIndex() else { return }
    //        let headerModel = uploadPrescriptionsHeaderModel[headerIndex]
    //        guard let rowIndex = headerModel.rowModel.index( where:{ $0.type == .prescriptions }) else { return }
    //        var rowModel = headerModel.rowModel[rowIndex]
    //        for subModel in rowModel.subRowModel {
    //            if subModel.imgName.isValidString {
    ////                uploadPrescriptionsHeaderModel[headerIndex].rowModel[rowIndex].subRowModel.remove(element: subModel)
    //            }
    //        }
    //    }
}

extension UploadPrescriptionsViewModel {
    private func getIndexFromHeader(type: UploadPrescriptionsHeaderType = .upload) -> Int? {
        guard let headerIndex = uploadPrescriptionsHeaderModel.index( where:{ $0.type == type }) else { return nil }
        return headerIndex
    }
    
    private func getIndexFromRow(type: UploadPrescriptionsRowType = .prescriptions, headerType: UploadPrescriptionsHeaderType = .upload) -> Int? {
        guard let headerIndex = getIndexFromHeader(type: headerType) else { return nil }
        let headerModel = uploadPrescriptionsHeaderModel[headerIndex]
        guard let rowIndex = headerModel.rowModel.index( where:{ $0.type == type }) else { return nil }
        return rowIndex
    }
    
    private func getRowModelFrom(index: Int, headerType: UploadPrescriptionsHeaderType = .upload) -> UploadPrescriptionsRowModel? {
        guard let headerIndex = getIndexFromHeader(type: headerType) else { return nil }
        let headerModel = uploadPrescriptionsHeaderModel[headerIndex]
        guard let rowIndex = headerModel.rowModel.index( where:{ $0.type.rawValue == index }) else { return nil }
        return headerModel.rowModel[rowIndex]
    }
    
    private func getRowModelFrom(type: UploadPrescriptionsRowType = .prescriptions, headerType: UploadPrescriptionsHeaderType = .upload) -> UploadPrescriptionsRowModel? {
        guard let headerIndex = getIndexFromHeader(type: headerType) else { return nil }
        let headerModel = uploadPrescriptionsHeaderModel[headerIndex]
        guard let rowIndex = headerModel.rowModel.index( where:{ $0.type == type }) else { return nil }
        return headerModel.rowModel[rowIndex]
    }
    
    //    func getRowModelsFrom(type: UploadPrescriptionsRowType) -> [UploadPrescriptionsRowModel]? {
    //        guard let headerIndex = getUploadHeaderIndex() else { return nil }
    //        let headerModel = uploadPrescriptionsHeaderModel[headerIndex]
    //        return headerModel.rowModel.filter { $0.type == type }
    //    }
}

struct UploadPrescriptionsHeaderModel {
    var title: String = ""
    var type: UploadPrescriptionsHeaderType = .none
    var rowModel: [UploadPrescriptionsRowModel] = [UploadPrescriptionsRowModel]()
    
    init(title: String = "", type: UploadPrescriptionsHeaderType = .none, rowModel: [UploadPrescriptionsRowModel] = [UploadPrescriptionsRowModel]()) {
        self.title = title
        self.type = type
        self.rowModel = rowModel
    }
    
    static func sequenceByType(lhs: UploadPrescriptionsHeaderModel, rhs: UploadPrescriptionsHeaderModel) -> Bool {
        return lhs.type.rawValue < rhs.type.rawValue
    }
    
}

struct UploadPrescriptionsRowModel {
    var title: String = ""
    var imgName: String = ""
    var type: UploadPrescriptionsRowType = .none
    var subRowModel: [UploadPrescriptionsSubRowModel] = [UploadPrescriptionsSubRowModel]()
    
    init(title: String = "", imgName: String = "", type: UploadPrescriptionsRowType = .none, subRowModel: [UploadPrescriptionsSubRowModel] = [UploadPrescriptionsSubRowModel]()) {
        self.title = title
        self.imgName = imgName
        self.type = type
        self.subRowModel = subRowModel
    }
}

struct UploadPrescriptionsSubRowModel {
    var title: String = ""
    var imgName: String = ""
    var image: UIImage? = nil
    var type: UploadPrescriptionsSubRowType = .none
    
    init(title: String = "", imgName: String = "", image: UIImage? = nil, type: UploadPrescriptionsSubRowType = .none) {
        self.title = title
        self.imgName = imgName
        self.image = image
        self.type = type
    }
}
