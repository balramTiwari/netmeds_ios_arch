//
//  PastPrescriptionsViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum PastPrescriptionsHeaderType: Int {
    case verified = 1
    case recent
    case none
}

class PastPrescriptionsViewModel {
    var pastPrescriptionsHeaderModel = [PastPrescriptionsHeaderModel]()
    var subRowModel = [UploadPrescriptionsSubRowModel]()
    weak var delegate: PastPrescriptionsDelegateHandlers?

    func pastPrescriptionsService(params: JSONArrayOfDictString, subRowModel: [UploadPrescriptionsSubRowModel]) {
        self.subRowModel = subRowModel
        NetworkManager.shared.pastPrescriptionsService(params: params) { [weak self] (error, response) in
            guard error == nil, let pastPrescriptions = response as? PastPrescriptionsModel else { return }
            self?.parsePastPrescriptionsInfo(pastPrescriptions: pastPrescriptions)
            self?.addDefaultSelectPrescription()
        }
    }
}

extension PastPrescriptionsViewModel {
    private func parsePastPrescriptionsInfo(pastPrescriptions: PastPrescriptionsModel) {
        guard pastPrescriptions.status?.lowercased() == ResponseStatus.success.rawValue else {
            //                self?.parseErrorInfo(errorInfo: paymentGateWay.errorInfo);
            return }
        guard let result = pastPrescriptions.result else { return }
        self.addVerifiedPrescriptionsInfo(result: result)
        self.addRecentPrescriptionsInfo(result: result)
    }
    
}

extension PastPrescriptionsViewModel {
    func updateSelectedPrescription(section: Int, row: Int) {
        let headerModel = pastPrescriptionsHeaderModel[section]
        var rowModel = headerModel.rowModel[row]
        rowModel.isSelected = !rowModel.isSelected
        pastPrescriptionsHeaderModel[section].rowModel[row] = rowModel
        self.delegate?.successProcess()
    }
    
    func addDefaultSelectPrescription() {
        for model in subRowModel {
            for (headerInedex, headerModel) in pastPrescriptionsHeaderModel.enumerated() {
                for (rowInedex, rowModel) in headerModel.rowModel.enumerated() {
                    if model.imgName == rowModel.imgURL {
                        pastPrescriptionsHeaderModel[headerInedex].rowModel[rowInedex].isSelected = true
                    }
                }
            }
        }
        self.delegate?.successProcess()
    }
}

extension PastPrescriptionsViewModel {
    private func addVerifiedPrescriptionsInfo(result: PastPrescriptionsResultModel) {
        guard let digitizedRxList = result.digitizedPrescriptionList?.rxIDs else { return }
        var rowModel = [PastPrescriptionsRowModel]()
        for rxID in digitizedRxList {
            rowModel.append(PastPrescriptionsRowModel(imgURL: rxID))
        }
        guard rowModel.count > 0 else { return }
        let headerModel = PastPrescriptionsHeaderModel(title: "Verified Prescription", type: .verified, rowModel: rowModel)
        pastPrescriptionsHeaderModel.append(headerModel)
    }
    
    private func addRecentPrescriptionsInfo(result: PastPrescriptionsResultModel) {
        guard let unDigitizedRxList = result.pastOneWeekUnDigitizedRxList?.rxIDs else { return }
        var rowModel = [PastPrescriptionsRowModel]()
        for rxID in unDigitizedRxList {
            rowModel.append(PastPrescriptionsRowModel(imgURL: rxID))
        }
        guard rowModel.count > 0 else { return }
        let headerModel = PastPrescriptionsHeaderModel(title: "Recent Upload", type: .recent, rowModel: rowModel)
        pastPrescriptionsHeaderModel.append(headerModel)
    }
}

struct PastPrescriptionsHeaderModel {
    var title: String = ""
    var type: PastPrescriptionsHeaderType = .none
    var rowModel: [PastPrescriptionsRowModel] = [PastPrescriptionsRowModel]()
    
    init(title: String = "", type: PastPrescriptionsHeaderType = .none, rowModel: [PastPrescriptionsRowModel] = [PastPrescriptionsRowModel]()) {
        self.title = title
        self.type = type
        self.rowModel = rowModel
    }
}

struct PastPrescriptionsRowModel {
    var imgURL: String = ""
    var isSelected: Bool = false
    
    init(imgURL: String = "", isSelected: Bool = false) {
        self.imgURL = imgURL
        self.isSelected = isSelected
    }
}
