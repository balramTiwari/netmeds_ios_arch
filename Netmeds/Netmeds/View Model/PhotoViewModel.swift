//
//  PhotoViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class PhotoViewModel: NSObject {
    @objc dynamic var photos: [PhotoRecord] = [PhotoRecord]()

    func fetchPhotoFrom(searchText: String) {
        NetworkManager.shared.fetchPhotosFrom(searchText: searchText) { [weak self] (error, response) in
            if error == nil {
                self?.updateResponse(response: response)
            }
        }
    }
    
    private func updateResponse(response: Any?) {
        guard let searchPhoto = response as? SearchPhotoModel, let photo = searchPhoto.photos, let photos = photo.photo, photos.count > 0 else { return }
        for value in photos {
            let url = URL(string: "https://farm\(value.farm!).staticflickr.com/\(value.server!)/\(value.photoId!)_\(value.secret!)_m.jpg")
            self.photos.append(PhotoRecord(name: value.title!, url: url!))
        }
    }
}
