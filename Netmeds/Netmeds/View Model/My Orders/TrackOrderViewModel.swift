//
//  TrackOrderViewModel.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum TrackOrderType: Int {
    case orderStatus = 1
    case banner
    case deliveryAddress
    case none
}

class TrackOrderViewModel {
    var trackOrderHeaderModel = [TrackOrderHeaderModel]()
    weak var delegate: TrackOrdersDetailDelegateHandlers?

    func getTrackOrderDetailsService(params: JSONArrayOfDictString) {
        NetworkManager.shared.getTrackOrderDetailsService(params: params) { [weak self] (error, response) in
            guard error == nil, let trackOrderDetailsInfo = response as? TrackOrderInfoModel else { return }
            self?.parseTrackOrdersDetailInfo(trackOrderDetailsInfo: trackOrderDetailsInfo)
        }
    }
}

extension TrackOrderViewModel {
    private func parseTrackOrdersDetailInfo(trackOrderDetailsInfo: TrackOrderInfoModel) {
        guard trackOrderDetailsInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        guard let trackOrder = trackOrderDetailsInfo.result?.orderTracking else { return }
        parseOrderStatusInfo(trackOrder: trackOrder)
        parseDeliveryAddressInfo(trackOrder: trackOrder)
        self.delegate?.successProcess()
    }
    
    private func parseOrderStatusInfo(trackOrder: TrackOrderModel) {
        guard let trackOrderDetails = trackOrder.trackDetails, trackOrderDetails.count > 0 else { return }
        let trackDetail = trackOrderDetails[0]
        guard let trackStatus = trackDetail.trackStatusDetails, trackStatus.count > 0 else { return }
        var rowModel = [TrackOrderRowModel]()
        for status in trackStatus {
            rowModel.append(TrackOrderRowModel(name: status.status!, desc: status.date!.getLocalizedDateString(), imgName: getOrderStatusImgName(status: status.shortStatus!)))
        }
        let hasCancelledStatus = (trackDetail.drugStatus == kCancelledKey || trackDetail.drugStatus == kDeclinedKey)
        var remainingStatus = [String]()
        if !hasCancelledStatus {
            let orderID = trackDetail.orderID!
            if trackDetail.drugStatusCode!.isValidString { // "P" to check all the status.
                remainingStatus = getRemainingStatusNames(statusCode: trackDetail.drugStatusCode!, orderID: orderID, drugStatus: trackOrder.orderStatus)
                } else if let trackShortStatus = trackDetail.trackShortStatus, let lastStatusCode = trackShortStatus.last {
                remainingStatus = getRemainingStatusNames(statusCode: "\(lastStatusCode)", orderID: orderID, drugStatus: trackOrder.orderStatus)
            }
        }
        for rStatus in remainingStatus {
            rowModel.append(TrackOrderRowModel(name: rStatus, desc: "", imgName: getOrderStatusImgName(status: rStatus), progressStatus: false))
        }
        let headerModel = TrackOrderHeaderModel(name: "ORDER STATUS", type: .orderStatus, rowModel: rowModel)
        trackOrderHeaderModel.append(headerModel)
    }
    
    private func parseDeliveryAddressInfo(trackOrder: TrackOrderModel) {
        guard let addressInfo = trackOrder.orderBillingInfo else { return }
        let addressDetails = getAddressInfo(addressInfo: addressInfo)
        var rowModel = [TrackOrderRowModel]()
        rowModel.append(TrackOrderRowModel(name: addressDetails.name, desc: addressDetails.address))
        let headerModel = TrackOrderHeaderModel(name: "DELIVERY ADDRESS", type: .deliveryAddress, rowModel: rowModel)
        trackOrderHeaderModel.append(headerModel)
    }
}

extension TrackOrderViewModel {
    private func getOrderStatusImgName(status: String) -> String {
        var imgName: String = ""
        switch status {
        case TrackOrderStatus.orderPlaced.rawValue:
            imgName = "shoppingCartAddedActive"
        case TrackOrderStatus.prescriptionVerificationPending.rawValue, TrackOrderStatus.prescriptionVerified.rawValue:
            imgName = "rx_primary_active"
        case TrackOrderStatus.orderInProcess.rawValue:
            imgName = "order_in_process_active"
        case TrackOrderStatus.shipped.rawValue:
            imgName = "rocketActive"
        case TrackOrderStatus.delivered.rawValue:
            imgName = "deliveryActive"
        case TrackOrderStatus.paymentPending.rawValue, TrackOrderStatus.paymentReceived.rawValue, TrackOrderStatus.paymentPending.description, TrackOrderStatus.paymentReceived.rawValue:
            imgName = "rupee_active"
        case TrackOrderStatus.orderPlaced.description:
            imgName = "shoppingCartAddedInactive"
        case TrackOrderStatus.prescriptionVerificationPending.description:
            imgName = "rx_primary_inactive"
        case TrackOrderStatus.orderInProcess.description:
            imgName = "order_in_process_inactive"
        case TrackOrderStatus.shipped.description:
            imgName = "rocketInactive"
        case TrackOrderStatus.delivered.description:
            imgName = "deliveryInactive"
        default:
            imgName = "cancelRed"
        }
        return imgName
    }
    
    private func getRemainingStatusNames(statusCode: String, orderID: String?, drugStatus: String?) -> [String] {
        if statusCode == MyOrdersStatusType.orderPlaced.rawValue {
            return [TrackOrderStatus.prescriptionVerificationPending.description, TrackOrderStatus.orderInProcess.description, TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
        } else if statusCode == MyOrdersStatusType.prescriptionVerificationPending.rawValue {
            if let drugStatus = drugStatus, drugStatus == "Consultation Scheduled" {
                return [TrackOrderStatus.orderInProcess.description, TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
            } else {
                if let id = orderID {
                    if id.contains("P") && (statusCode == MyOrdersStatusType.paymentPending.rawValue) {
                        return [TrackOrderStatus.orderInProcess.description, TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
                    } else {
                        return [TrackOrderStatus.paymentPending.description, TrackOrderStatus.orderInProcess.description, TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
                    }
                }
            }
        } else if statusCode == MyOrdersStatusType.paymentPending.rawValue {
            return [TrackOrderStatus.orderInProcess.description, TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
        } else if  statusCode == MyOrdersStatusType.orderInProcess.rawValue {
            return [TrackOrderStatus.shipped.description, TrackOrderStatus.delivered.description]
        } else if statusCode == MyOrdersStatusType.shipped.rawValue {
            return [TrackOrderStatus.delivered.description]
        }
        return []
    }
}

extension TrackOrderViewModel {
    private func getAddressInfo(addressInfo: MyOrderBillingInfoModel) -> (name: String, address: String) {
        var name: String = ""
        var address: String = ""
        if let firstName = addressInfo.firstName, firstName.isValidString {
            name = firstName
        }
        if let lastName = addressInfo.lastName, lastName.isValidString {
            name = name + "  " + lastName
        }
        if let address1 = addressInfo.address1, address1.isValidString {
            address = address + address1
        }
        if let address2 = addressInfo.address2, address2.isValidString {
            address = address + ", " + address2
        }
        if let city = addressInfo.city, city.isValidString, let value = city.components(separatedBy: ",").first {
            address = address + ",\n" + value
        }
        if let state = addressInfo.state, state.isValidString {
            address = address + ", " + state
        }
        if let zipcode = addressInfo.zipCode, zipcode.isValidString {
            address = address + "-" + zipcode + "."
        }
        if let phoneNumber = addressInfo.phoneNumber, phoneNumber.isValidString {
            address = address + "\n+91 - " + phoneNumber
        }
        return (name: name, address: address)
    }
}

struct TrackOrderHeaderModel {
    var name: String = ""
    var type: TrackOrderType = .none
    var rowModel: [TrackOrderRowModel] = [TrackOrderRowModel]()
    
    init(name: String = "", type: TrackOrderType = .none, rowModel: [TrackOrderRowModel] = [TrackOrderRowModel]()) {
        self.name = name
        self.type = type
        self.rowModel = rowModel
    }
}

struct TrackOrderRowModel {
    var name: String = ""
    var desc: String = ""
    var imgName: String = ""
    var progressStatus: Bool = true
    init(name: String = "", desc: String = "", imgName: String = "", progressStatus: Bool = true) {
        self.name = name
        self.desc = desc
        self.imgName = imgName
        self.progressStatus = progressStatus
    }
}
