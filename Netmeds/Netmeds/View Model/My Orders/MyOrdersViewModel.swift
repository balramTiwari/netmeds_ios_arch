//
//  MyOrdersViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 08/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum MyOrdersStatusType: String {
    case orderPlaced = "Z"
    case prescriptionVerificationPending, prescriptionVerified = "P"
    //    case prescriptionVerified = "P" // "N"
    case paymentPending = "N"
    case orderInProcess = "S"
    case processing = "H"
    case shipped = "I" // INTRANSIT
    case delivered = "D"
    case awaitingPrescription = "A"
    case declined = "R" // "DECLINED"
    case cancelled = "C" // "CANCELLED"
    case returned = "U" //  "RETURNED"
    case consultationScheduled = "F" // Consultation Scheduled
    case orderReceived = "Order Received" // temp order status
    case paymentConfirmationPending = "Payment Confirmation Pending" // payment yet to receive from bank
    case paymentFailed = "Payment Failed" // payment failed from webhook
    case none
}

enum MyOrdersButtonType: String {
    case reOrder = "REORDER"
    case retry = "RETRY"
    case payNow = "PAY NOW"
    case needHelp = "NEED HELP"
    case trackOrder = "TRACK ORDER"
}


class MyOrdersViewModel {
    var myOrdersHeaderModel = [MyOrdersHeaderModel]()
    var myOrdersStatusModel = [String]()
    var totalOrders: Int = 0
    weak var delegate: MyOrdersDelegateHandlers?
    
    func getMyOrdersService(params: JSONArrayOfDictString) {
        NetworkManager.shared.getMyOrdersService(params: params) { [weak self] (error, response) in
            guard error == nil, let myOrdersInfo = response as? MyOrdersModel else { return }
            self?.parseMyOrdersInfo(myOrdersInfo: myOrdersInfo)
        }
    }
    
    func getMyOrdersFilterService(params: JSONArrayOfDictString) {
        myOrdersHeaderModel = [MyOrdersHeaderModel]()
        delegate?.successProcess()
        getMyOrdersService(params: params)
    }
}

extension MyOrdersViewModel {
    private func parseMyOrdersInfo(myOrdersInfo: MyOrdersModel) {
        guard myOrdersInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        guard let ordersList = myOrdersInfo.result?.orderList, ordersList.count > 0 else { return }
        parseMyOrdersList(ordersList: ordersList)
        guard let statusList = myOrdersInfo.result?.statusList, statusList.count > 0 else { return }
        parseMyOrdersStatusList(statusList: statusList)
    }
    
    private func parseMyOrdersList(ordersList: [MyOrdersListModel]) {
        for list in ordersList {
            var rowModel = [MyOrdersRowModel]()
            if let items = list.items, items.count > 0 {
                for (index, item) in items.enumerated() {
                    if index > 1 {
                        rowModel.append(MyOrdersRowModel(productName: "\(items.count - 2) more products"))
                        break
                    }
                    var imgName: String = "experience_masters"
                    if item.sku! == kPrimeProduct2001 || item.sku! == kPrimeProduct2002 {
                        imgName = list.isPrimeCustomerOrder! ? "membership_super_member": "membership_icon"
                    }
                    rowModel.append(MyOrdersRowModel(productName: item.brandName!, imgName: imgName))
                }
            }
            var statusColor: UIColor = .black
            if list.statusColor!.isValidString {
                statusColor = UIColor.getColorFromHexValue(hex: list.statusColor!, alpha: 1.0)
            }
            var statusName: String = ""
            if let statusDescription = list.displayStatus {
                if let isPrimeProductOrder = list.isPrimeMembershipOrder, isPrimeProductOrder {
                    statusName = ""
                } else {
                    statusName = statusDescription.uppercased()
                }
            }
            let statusType = getStatusType(orderStatus: list.orderStatus!, statusDescription: list.statusDesc!)
            let footerModel = MyOrdersFooterModel(trackOrderBtnText: statusType.type.rawValue, viewDetailsBtnBgColor: statusType.viewDetailsBtnBgColor, trackOrderBtnBgColor: statusType.trackOrderBtnBgColor, viewDetailsBtnTitleColor: statusType.viewDetailsBtnTitleColor, trackOrderBtnTitleColor: statusType.trackOrderBtnTitleColor, isViewDetailsBtnEnabled: statusType.isViewDetailsBtnEnabled, isTrackOrderBtnEnabled: statusType.isTrackOrderBtnEnabled)
            let headerModel = MyOrdersHeaderModel(orderID: list.orderID!, name: list.name!, statusName: statusName, statusColor: statusColor, statusType: .none, rowModel: rowModel, footerModel: footerModel)
            myOrdersHeaderModel.append(headerModel)
        }
        delegate?.successProcess()
    }

    private func getStatusType(orderStatus: String, statusDescription: String) -> (type: MyOrdersButtonType, viewDetailsBtnBgColor: UIColor, trackOrderBtnBgColor: UIColor, viewDetailsBtnTitleColor: UIColor, trackOrderBtnTitleColor: UIColor, isViewDetailsBtnEnabled: Bool, isTrackOrderBtnEnabled: Bool) {
        var defaultTrackOrderBtnBgColor: UIColor = UIColor.NMLogoGreenColor()
        var defaultTrackOrderBtnTitleColor: UIColor = .white
        var isDefaultTrackOrderBtnEnabled: Bool = true
        var defaultViewDetailsBtnBgColor: UIColor = UIColor.NMSTextColor(with: 0.04)
        var defaultViewDetailsBtnTitleColor: UIColor = UIColor.NMSTextColor(with: 0.6)
        var isDefaultViewDetailsBtnEnabled: Bool = true
        switch orderStatus {
        case MyOrdersStatusType.cancelled.rawValue, MyOrdersStatusType.declined.rawValue, MyOrdersStatusType.returned.rawValue, MyOrdersStatusType.delivered.rawValue:
            if statusDescription == MyOrdersStatusType.paymentConfirmationPending.rawValue {
                defaultTrackOrderBtnBgColor = UIColor.getColorFromHexValue(hex: "f5f6f7", alpha: 1.0)
                defaultTrackOrderBtnTitleColor = UIColor.getColorFromHexValue(hex: "151b39", alpha: 0.38)
                isDefaultTrackOrderBtnEnabled = false
                return (type: .retry, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
            } else if statusDescription == MyOrdersStatusType.paymentFailed.rawValue {
                return (type: .retry, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
            }
            return (type: .reOrder, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
        case MyOrdersStatusType.paymentPending.rawValue:
                if statusDescription.lowercased() == "Payment Pending".lowercased() {
                    return (type: .payNow, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
                } else if statusDescription.lowercased() == "Consultation Scheduled".lowercased() {
                    return (type: .needHelp, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
                }
            return (type: .trackOrder, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
        case MyOrdersStatusType.orderReceived.rawValue:
            defaultTrackOrderBtnBgColor = UIColor.getColorFromHexValue(hex: "f5f6f7", alpha: 1.0)
            defaultTrackOrderBtnTitleColor = UIColor.getColorFromHexValue(hex: "151b39", alpha: 0.38)
            isDefaultTrackOrderBtnEnabled = false
            defaultViewDetailsBtnBgColor = UIColor.getColorFromHexValue(hex: "f5f6f7", alpha: 1.0)
            defaultViewDetailsBtnTitleColor = UIColor.getColorFromHexValue(hex: "151b39", alpha: 0.38)
            isDefaultViewDetailsBtnEnabled = false
            return (type: .trackOrder, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
        default:
            return (type: .trackOrder, viewDetailsBtnBgColor: defaultViewDetailsBtnBgColor, trackOrderBtnBgColor: defaultTrackOrderBtnBgColor, viewDetailsBtnTitleColor: defaultViewDetailsBtnTitleColor, trackOrderBtnTitleColor: defaultTrackOrderBtnTitleColor, isViewDetailsBtnEnabled: isDefaultViewDetailsBtnEnabled, isTrackOrderBtnEnabled: isDefaultTrackOrderBtnEnabled)
        }
    }
    
    private func parseMyOrdersStatusList(statusList: [MyOrdersStatusListModel]) {
        totalOrders = 0
        myOrdersStatusModel = [String]()
        myOrdersStatusModel.append("Recent")
        for status in statusList {
            totalOrders = totalOrders + (Int(status.count ?? "0") ?? 0)
            myOrdersStatusModel.append(status.title!)
        }
        myOrdersStatusModel.append("All")
    }
}


struct MyOrdersHeaderModel {
    var orderID: String = ""
    var name: String = ""
    var statusName: String = ""
    var statusColor: UIColor = .black
    var statusType: MyOrdersStatusType = .none
    var rowModel: [MyOrdersRowModel] = [MyOrdersRowModel]()
    var footerModel: MyOrdersFooterModel = MyOrdersFooterModel()
    
    init(orderID: String = "", name: String = "", statusName: String = "", statusColor: UIColor = .black, statusType: MyOrdersStatusType = .none, rowModel: [MyOrdersRowModel] = [MyOrdersRowModel](), footerModel: MyOrdersFooterModel = MyOrdersFooterModel()) {
        self.orderID = orderID
        self.name = name
        self.statusName = statusName
        self.statusColor = statusColor
        self.statusType = statusType
        self.rowModel = rowModel
        self.footerModel = footerModel
    }
}

struct MyOrdersRowModel {
    var productName: String = ""
    var imgName: String = ""
    
    init(productName: String = "", imgName: String = "experience_masters") {
        self.productName = productName
        self.imgName = imgName
    }
}

struct MyOrdersFooterModel {
    var trackOrderBtnText: String = ""
    var viewDetailsBtnBgColor: UIColor = .white
    var trackOrderBtnBgColor: UIColor = .white
    var viewDetailsBtnTitleColor: UIColor = .white
    var trackOrderBtnTitleColor: UIColor = .white
    var isViewDetailsBtnEnabled: Bool = true
    var isTrackOrderBtnEnabled: Bool = true

    init(trackOrderBtnText: String = "", viewDetailsBtnBgColor: UIColor = .white, trackOrderBtnBgColor: UIColor = .white, viewDetailsBtnTitleColor: UIColor = .white, trackOrderBtnTitleColor: UIColor = .white, isViewDetailsBtnEnabled: Bool = true, isTrackOrderBtnEnabled: Bool = true) {
        self.trackOrderBtnText = trackOrderBtnText
        self.viewDetailsBtnBgColor = viewDetailsBtnBgColor
        self.trackOrderBtnBgColor = trackOrderBtnBgColor
        self.viewDetailsBtnTitleColor = viewDetailsBtnTitleColor
        self.trackOrderBtnTitleColor = trackOrderBtnTitleColor
        self.isViewDetailsBtnEnabled = isViewDetailsBtnEnabled
        self.isTrackOrderBtnEnabled = isTrackOrderBtnEnabled
    }
}
