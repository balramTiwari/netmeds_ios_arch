//
//  MyOrdersDetailViewModel.swift
//  Netmeds
//
//  Created by Netmedsian on 11/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum TrackOrderStatus: String {
    case orderPlaced = "Z"
    case prescriptionVerificationPending, prescriptionVerified = "P" // "N"
    case paymentPending = "N"
    case paymentReceived = "M"
    case orderInProcess = "S"
    case shipped = "I"
    case delivered = "D"
    
    var description: String {
        switch self {
        case .orderPlaced: return "Order Placed"
        case .prescriptionVerificationPending: return "Prescription Verification Pending"
        case .prescriptionVerified: return "Prescription Verified"
        case .paymentPending: return "Payment Pending"
        case .paymentReceived: return "Payment Received"
        case .orderInProcess: return "Order in Process"
        case .shipped: return "Shipped"
        case .delivered: return "Delivered"
        }
    }
}

enum MyOrdersDetailType: Int {
    case banner = 1
    case orderDetails
    case orders
    case prescriptons
    case paymentDetails
    case cancelledItems
    case deliveryAddress
    case none
}

class MyOrdersDetailViewModel {
    private var shippingChargeAmount = Double()
    var myOrdersDetailHeaderModel = [MyOrdersDetailHeaderModel]()
    weak var delegate: MyOrdersDetailDelegateHandlers?

    func getMyOrdersDetailService(params: JSONArrayOfDictString) {
        NetworkManager.shared.getMyOrdersDetailService(params: params) { [weak self] (error, response) in
            guard error == nil, let myOrdersDetailInfo = response as? MyOrdersDetailInfoModel else { return }
            self?.parseMyOrdersDetailInfo(myOrdersDetailInfo: myOrdersDetailInfo)
        }
    }
}

extension MyOrdersDetailViewModel {
    private func parseMyOrdersDetailInfo(myOrdersDetailInfo: MyOrdersDetailInfoModel) {
        guard myOrdersDetailInfo.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        guard let myOrdersDetailData = myOrdersDetailInfo.result?.orderDetailsData else { return }
        parseBannerInfo(myOrdersDetailData: myOrdersDetailData)
        parseOrderDetailsInfo(myOrdersDetailData: myOrdersDetailData)
        parseOrdersInfo(myOrdersDetailData: myOrdersDetailData)
        parsePrescriptionsInfo(myOrdersDetailData: myOrdersDetailData)
        parsePaymentDetailsInfo(myOrdersDetailData: myOrdersDetailData)
        parseCancelledItemsInfo(myOrdersDetailData: myOrdersDetailData)
        parseDeliveryAddressInfo(myOrdersDetailData: myOrdersDetailData)
        self.delegate?.successProcess()
    }
    
    private func parseBannerInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        
    }
    
    private func parseOrderDetailsInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        var rowModel = [MyOrdersDetailRowModel]()
        var patientName: String = ""
        if let billingInfo = myOrdersDetailData.orderBillingInfo {
            if let firstName = billingInfo.firstName {
                patientName = firstName
            }
            if let lastName = billingInfo.lastName {
                 patientName = patientName + " " + lastName
            }
        }
        var orderPlacedDate: String = ""
        if let deliveryDate = myOrdersDetailData.orderDate {
            orderPlacedDate = deliveryDate.getDateAndTime()
        }
        rowModel.append(MyOrdersDetailRowModel(name: "Patient Name", desc: patientName))
        rowModel.append(MyOrdersDetailRowModel(name: "Order ID", desc: myOrdersDetailData.orderID!))
        rowModel.append(MyOrdersDetailRowModel(name: "Order Placed", desc: orderPlacedDate))
        let headerModel = MyOrdersDetailHeaderModel(name: "ORDER DETAILS", type: .orderDetails, rowModel: rowModel)
        myOrdersDetailHeaderModel.append(headerModel)
    }
    
    private func parseOrdersInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        guard let orders = myOrdersDetailData.orderDetail else { return }
        for (index, order) in orders.enumerated() {
            if let drugDetails = order.drugDetails {
                var rowModel = [MyOrdersDetailRowModel]()
                for drug in drugDetails {
                    let quantity: String = getQuantity(drug: drug)
                    let price: String = getPrice(drug: drug, orderStatus: myOrdersDetailData.orderStatus!)
                    let hasRxProduct = (drug.prescriptionNeeded == "N") ? true : false
                    rowModel.append(MyOrdersDetailRowModel(drugCode: drug.drugCode!, name: drug.brandName!, desc: price, quantity: quantity, hasRxProduct: hasRxProduct))
                }
                if rowModel.count > 0 {
                    let footerName: String = getFooterName(statusCode: myOrdersDetailData.orderStatusCode!, orderStatus: myOrdersDetailData.orderStatus!)
                    let footerModel = MyOrdersDetailFooterModel(name: footerName)
                    let headerName: String = getHeaderName(ordersCount: orders.count, section: index)
                    let headerModel = MyOrdersDetailHeaderModel(name: headerName, type: .orders, rowModel: rowModel, footerModel: footerModel)
                    myOrdersDetailHeaderModel.append(headerModel)
                }
            }
        }
    }
    
    private func parsePrescriptionsInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        guard let rxIDs = myOrdersDetailData.rxIDs else { return }
        var rowModel = [MyOrdersDetailRowModel]()
        for rxID in rxIDs {
            rowModel.append(MyOrdersDetailRowModel(name: rxID))
        }
        guard rowModel.count > 0 else { return }
        let headerModel = MyOrdersDetailHeaderModel(name: "PRESCRIPTION UPLOADED", type: .prescriptons, rowModel: rowModel)
        myOrdersDetailHeaderModel.append(headerModel)
    }
    
    private func parsePaymentDetailsInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        guard let _ = myOrdersDetailData.orderDetail, let _ = myOrdersDetailData.orderStatusCode, let _ = myOrdersDetailData.isCancelStatus else { return }
        var rowModel = [MyOrdersDetailRowModel]()
        let subTotalValue = getSubTotal(myOrdersDetailData: myOrdersDetailData)
        let subTotal = subTotalValue != 0 ? subTotalValue.doubleValue.withTwoDecimalValue: ""
        if subTotal.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Sub Total", desc: subTotal))
        }
        let shippingCharges = getShippingCharges(myOrdersDetailData: myOrdersDetailData)
        if shippingCharges.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Shipping Charges", desc: shippingCharges))
        }
        let additionalDiscount = getAdditionalDiscount(myOrdersDetailData: myOrdersDetailData)
        if additionalDiscount.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Additional Discount", desc: additionalDiscount))
        }
        let couponDiscount = getCouponDiscount(myOrdersDetailData: myOrdersDetailData)
        if couponDiscount.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Coupon Discount", desc: couponDiscount))
        }
        let voucherDiscount = getVoucherDiscount(myOrdersDetailData: myOrdersDetailData)
        if voucherDiscount.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Voucher Discount", desc: voucherDiscount))
        }
        let walletDiscount = getWalletDiscount(myOrdersDetailData: myOrdersDetailData)
        if walletDiscount.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Wallet Discount", desc: walletDiscount))
        }
        let netAmount = getNetAmount(myOrdersDetailData: myOrdersDetailData)
        if netAmount.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Net Amount", desc: netAmount))
        }
        let paymentMode = getPaymentMode(myOrdersDetailData: myOrdersDetailData)
        if paymentMode.isValidString {
            rowModel.append(MyOrdersDetailRowModel(name: "Payment Mode", desc: paymentMode))
        }
        let headerModel = MyOrdersDetailHeaderModel(name: "PAYMENT DETAILS", type: .paymentDetails, rowModel: rowModel)
        myOrdersDetailHeaderModel.append(headerModel)
    }
    
    private func parseCancelledItemsInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        guard let cancelledItems = myOrdersDetailData.cancelledItems else { return }
        var rowModel = [MyOrdersDetailRowModel]()
        for item in cancelledItems {
            rowModel.append(MyOrdersDetailRowModel(name: item.drugName!, desc: item.mrp!, quantity: item.quantity!))
        }
        guard rowModel.count > 0 else { return }
        let headerModel = MyOrdersDetailHeaderModel(name: "CANCELLATION / REFUND DETAILS", type: .cancelledItems, rowModel: rowModel)
        myOrdersDetailHeaderModel.append(headerModel)
    }
    
    private func parseDeliveryAddressInfo(myOrdersDetailData: MyOrdersDetailDataModel) {
        guard let addressInfo = myOrdersDetailData.orderBillingInfo else { return }
        let addressDetails = getAddressInfo(addressInfo: addressInfo)
        var rowModel = [MyOrdersDetailRowModel]()
        rowModel.append(MyOrdersDetailRowModel(name: addressDetails.name, desc: addressDetails.address))
        let headerModel = MyOrdersDetailHeaderModel(name: "DELIVERY ADDRESS", type: .deliveryAddress, rowModel: rowModel)
        myOrdersDetailHeaderModel.append(headerModel)
    }
}

extension MyOrdersDetailViewModel {
    private func getQuantity(drug: MyOrdersDrugDetailsModel) -> String {
        var quantity: String = ""
        if let purchaseQuantity = drug.purchaseQuantity {
            if  drug.drugCode == kPrimeProduct2001 || drug.drugCode == kPrimeProduct2002 { // Prime product
                quantity = ""
            } else {
                quantity = "Quantity: \(purchaseQuantity)"
            }
        }
        return quantity
    }
    
    private func getPrice(drug: MyOrdersDrugDetailsModel, orderStatus: String) -> String {
        var price: String = ""
        if let discountedPrice = drug.purchasePrice {
            price = "₹ \(NSNumber(value: discountedPrice).formatFractionDigits())"
            if orderStatus.lowercased() == TrackOrderStatus.paymentPending.description.lowercased() {
                if let quantity = drug.purchaseQuantity {
                    let finalPrice = NSNumber(value: Double(quantity) * discountedPrice)
                    price = "₹ \(finalPrice.formatFractionDigits())"
                } else {
                    price = "₹ \(NSNumber(value: discountedPrice).formatFractionDigits())"
                }
            }
        }
        return price
    }
    
    private func getFooterName(statusCode: String, orderStatus: String) -> String {
        var footerName: String = "SHOW STATUS DETAILS"
        if statusCode == MyOrdersStatusType.paymentPending.rawValue, orderStatus.uppercased() == "PAYMENT PENDING" {
            footerName = "REVIEW / EDIT ORDER"
        }
        return footerName
    }
    
    private func getHeaderName(ordersCount: Int, section: Int) -> String {
        return ordersCount > 1 ? "ORDER \(section + 1) OF \(ordersCount)": "ORDERS"
    }
}

extension MyOrdersDetailViewModel {
    private func getSubTotal(myOrdersDetailData: MyOrdersDetailDataModel) -> NSNumber {
        var subTotalValue = NSNumber(value: 0)
        if myOrdersDetailData.orderStatus == TrackOrderStatus.paymentPending.description {
            if let orders = myOrdersDetailData.orderDetail {
                var drugs = [MyOrdersDrugDetailsModel]()
                for order in orders {
                    if let drugDetails = order.drugDetails {
                        drugs = drugs + drugDetails
                    }
                }
                for drug in drugs {
                    if let price = drug.purchasePrice {
                        if let qty = drug.purchaseQuantity {
                            subTotalValue = NSNumber(value: (subTotalValue.doubleValue + (price * Double(qty))))
                        }
                    }
                }
            }
        } else {
            if let productAmount = myOrdersDetailData.productAmount, productAmount > 0 {
                subTotalValue = NSNumber(value: productAmount)
                if let couponDiscount = myOrdersDetailData.couponDiscount {
                    let value = NSNumber(value: (productAmount + ((couponDiscount < 0) ? couponDiscount * -1 : couponDiscount)))
                    subTotalValue = value
                }
            }
        }
        return subTotalValue
    }
    
    private func getShippingCharges(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var shippingCharges = ""
        if let shippingAmount = myOrdersDetailData.shippingAmount {
            shippingCharges = "₹ \(NSNumber(value: shippingAmount).formatFractionDigits())"
        }
        return shippingCharges
    }
    
    private func getAdditionalDiscount(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var additionalDiscount = ""
        if let discountAmount = myOrdersDetailData.discountAmount, discountAmount > 0 {
            additionalDiscount = "- ₹ \(NSNumber(value:discountAmount).formatFractionDigits())"
        }
        return additionalDiscount
    }

    private func getCouponDiscount(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var couponDiscount = ""
        if let discountAmount = myOrdersDetailData.couponDiscount, discountAmount > 0 {
            couponDiscount = "- ₹ \(NSNumber(value:discountAmount).formatFractionDigits())"
        }
        return couponDiscount
    }

    private func getVoucherDiscount(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var voucherDiscount = ""
        if let discountAmount = myOrdersDetailData.voucherAmount, discountAmount > 0 {
            voucherDiscount = "- ₹ \(NSNumber(value:discountAmount).formatFractionDigits())"
        }
        return voucherDiscount
    }
    
    private func getWalletDiscount(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var walletDiscount = ""
        if let discountAmount = myOrdersDetailData.usedWalletAmount, discountAmount > 0 {
            walletDiscount = "- ₹ \(NSNumber(value:discountAmount).formatFractionDigits())"
        }
        return walletDiscount
    }

    private func getNetAmount(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        var netAmount = NSNumber(value: 0)
        if myOrdersDetailData.orderStatus == TrackOrderStatus.paymentPending.description {
            let subTotalValue = getSubTotal(myOrdersDetailData: myOrdersDetailData)
            netAmount = subTotalValue
            if let couponDiscount = myOrdersDetailData.couponDiscount, couponDiscount > 0 {
                netAmount = NSNumber(value: subTotalValue.doubleValue - couponDiscount)
            }
            netAmount = NSNumber(value: subTotalValue.doubleValue + shippingChargeAmount)
        } else {
            if let totalAmount = myOrdersDetailData.transactionAmount {
                netAmount = NSNumber(value: totalAmount)
            }
        }
        return "₹ \(netAmount.formatFractionDigits())"
    }

    private func getPaymentMode(myOrdersDetailData: MyOrdersDetailDataModel) -> String {
        let paymentMode = myOrdersDetailData.paymentMode!
        return paymentMode.isValidString ? paymentMode.uppercased(): ""
    }
}

extension MyOrdersDetailViewModel {
    private func getAddressInfo(addressInfo: MyOrderBillingInfoModel) -> (name: String, address: String) {
        var name: String = ""
        var address: String = ""
        if let firstName = addressInfo.firstName, firstName.isValidString {
            name = firstName
        }
        if let lastName = addressInfo.lastName, lastName.isValidString {
            name = name + "  " + lastName
        }
        if let address1 = addressInfo.address1, address1.isValidString {
            address = address + address1
        }
        if let address2 = addressInfo.address2, address2.isValidString {
            address = address + ", " + address2
        }
        if let city = addressInfo.city, city.isValidString, let value = city.components(separatedBy: ",").first {
            address = address + ",\n" + value
        }
        if let state = addressInfo.state, state.isValidString {
            address = address + ", " + state
        }
        if let zipcode = addressInfo.zipCode, zipcode.isValidString {
            address = address + "-" + zipcode + "."
        }
        if let phoneNumber = addressInfo.phoneNumber, phoneNumber.isValidString {
            address = address + "\n+91 - " + phoneNumber
        }
        return (name: name, address: address)
    }
}

struct MyOrdersDetailHeaderModel {
    var name: String = ""
    var type: MyOrdersDetailType = .none
    var rowModel: [MyOrdersDetailRowModel] = [MyOrdersDetailRowModel]()
    var footerModel: MyOrdersDetailFooterModel = MyOrdersDetailFooterModel()
    
    init(name: String = "", type: MyOrdersDetailType = .none, rowModel: [MyOrdersDetailRowModel] = [MyOrdersDetailRowModel](), footerModel: MyOrdersDetailFooterModel = MyOrdersDetailFooterModel()) {
        self.name = name
        self.type = type
        self.rowModel = rowModel
        self.footerModel = footerModel
    }
}

struct MyOrdersDetailRowModel {
    var drugCode: Int = 0
    var name: String = ""
    var desc: String = ""
    var quantity: String = ""
    var hasRxProduct: Bool = false
    
    init(drugCode: Int = 0, name: String = "", desc: String = "", quantity: String = "", hasRxProduct: Bool = false) {
        self.drugCode = drugCode
        self.name = name
        self.desc = desc
        self.quantity = quantity
        self.hasRxProduct = hasRxProduct
    }
}

struct MyOrdersDetailFooterModel {
    var name: String = ""

    init(name: String = "") {
        self.name = name
    }
}
