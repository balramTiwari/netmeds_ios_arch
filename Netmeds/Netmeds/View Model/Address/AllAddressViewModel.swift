//
//  AllAddressViewModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 26/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class AllAddressViewModel {
    
    weak var delegate: SelectAddressDelegateHandlers?
    var addressList: [AllAddressInfoModel] = [AllAddressInfoModel]()
    
    func getAllAddressService() {
        NetworkManager.shared.getAllAddressService { [weak self] (error, response) in
            guard error == nil, let allAddress = response as? AllAddressModel else { return }
            self?.parseAllAddressInfo(allAddress: allAddress)
        }
    }
}

extension AllAddressViewModel {
    private func parseAllAddressInfo(allAddress: AllAddressModel) {
        guard allAddress.status?.lowercased() == ResponseStatus.success.rawValue else { return }
        guard let list = allAddress.result?.addressList else { return }
        self.addressList = list
        delegate?.allAddressSuccessProcess()
    }
}
