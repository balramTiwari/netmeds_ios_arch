//
//  ChangePasswordViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct ChangePasswordViewModel {
    var delegate: ChangePasswordDelegateHandlers?

    func reSendOTPService(randomKey: String) {
        NetworkManager.shared.reSendOTPService(randomKey: randomKey) { (error, response) in
            guard error == nil, let logInInfo = response as? LogInModel else { return }
            self.parseOTPInfo(response: logInInfo)
        }
    }
    
    func completeChangePasswordService(params: JSONDictAny) {
        NetworkManager.shared.completeChangePasswordService(params: params) { (error, response) in
            guard error == nil, let changePassword = response as? CompleteChangePasswordModel else { return }
            self.parseChangePasswordInfo(response: changePassword)
        }
    }
}

extension ChangePasswordViewModel {
    private func parseChangePasswordInfo(response: CompleteChangePasswordModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: response.errorInfo); return }
        guard response.result == "true" else { return }
        self.delegate?.changePasswordSuccessProcess()
    }

    private func parseOTPInfo(response: LogInModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.parseRandomKey(response: response)
    }
    
    private func parseRandomKey(response: LogInModel) {
        guard let randomKey = response.result?.otpDetails?.randomKey else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.delegate?.resendOTPSuccessProcess(randomKey: randomKey)
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
