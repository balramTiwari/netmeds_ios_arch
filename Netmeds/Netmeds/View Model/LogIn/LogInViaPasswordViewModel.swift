//
//  LogInViaPasswordViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct LogInViaPasswordViewModel {
    var delegate: LogInViaPasswordDelegateHandlers?
    
    func logInViaPasswordService(params: JSONDictAny) {
        NetworkManager.shared.logInViaPasswordService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            guard logInInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
                self.parseErrorInfo(errorInfo: logInInfo.errorInfo); return }
            guard let sessionInfo = logInInfo.result?.session else { return }
            UserDefaultsModel.saveSessionDetails(sessionInfo: sessionInfo)
            self.getCustomerDetailsService()
        }
    }
    
    func sendOTPService(mobileNo: String) {
        NetworkManager.shared.sendOTPService(mobileNo: mobileNo) { (error, response) in
            guard error == nil, let logInInfo = response as? LogInModel else { return }
            self.parseOTPInfo(response: logInInfo, pageSourceType: .otp)
        }
    }
    
    func initiateChangePasswordService(params: JSONDictAny) {
        NetworkManager.shared.initiateChangePasswordService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? LogInModel else { return }
            self.parseOTPInfo(response: logInInfo, pageSourceType:.changePassword)
        }
    }
    
    func getCustomerDetailsService() {
        NetworkManager.shared.customerDetailsService { (error, response) in
            guard error == nil, let customerInfo = response as? CustomerInfoModel else { return }
            self.parseCustomerInfo(response: customerInfo)
        }
    }
}

extension LogInViaPasswordViewModel {
    private func parseOTPInfo(response: LogInModel, pageSourceType: PageSourceType) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.parseRandomKey(response: response, pageSourceType: pageSourceType)
    }
    
    private func parseRandomKey(response: LogInModel, pageSourceType: PageSourceType) {
        guard let randomKey = response.result?.otpDetails?.randomKey else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.delegate?.otpSuccessProcess(randomKey: randomKey, pageSourceType: pageSourceType)
    }
    
    private func parseCustomerInfo(response: CustomerInfoModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue, let customerDetails = response.result?.details else { return }
        UserManager.shared.resetUserDetails(userInfo: customerDetails)
        self.delegate?.homePageProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
