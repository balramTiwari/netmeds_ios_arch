//
//  LogInViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

struct LogInViewModel {
    
    var delegate: LogInDelegateHandlers?
    
    func loginService(mobileNo: String) {
        NetworkManager.shared.logInService(mobileNo: mobileNo) { (error, response) in
            guard error == nil, let logInInfo = response as? LogInModel else { return }
            self.parseLogInInfo(response: logInInfo)
        }
    }
    
    func socialLogInService(socialLogInInfo: SocialLogInInfoViewModel) {
        let params = setUpParamsForSocialLogIn(socialLogInInfo:socialLogInInfo)
        NetworkManager.shared.socialLogInService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            self.parseSocialLogInInfo(response: logInInfo)
        }
    }
    
    func getCustomerDetailsService() {
        NetworkManager.shared.customerDetailsService { (error, response) in
            guard error == nil, let customerInfo = response as? CustomerInfoModel else { return }
            self.parseCustomerInfo(response: customerInfo)
        }
    }
}

extension LogInViewModel {
    private func parseLogInInfo(response: LogInModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseRandomKey(response: response); return }
        self.delegate?.logInSuccessProcess()
    }
    
    private func parseRandomKey(response: LogInModel) {
        guard let randomKey = response.result?.otpDetails?.randomKey else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.delegate?.signUpProcessUsing(randomKey: randomKey)
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
    
    private func parseCustomerInfo(response: CustomerInfoModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue, let customerDetails = response.result?.details else { return }
        UserManager.shared.resetUserDetails(userInfo: customerDetails)
        self.delegate?.homePageProcess()
    }
    
    private func setUpParamsForSocialLogIn(socialLogInInfo: SocialLogInInfoViewModel) -> JSONArrayOfDictString {
        return [
            [
                "signature": "",
                "payload": "",
                "accessToken": socialLogInInfo.accessToken,
                "socialFlag": socialLogInInfo.socialFlag,
                "source": kSourceApp,
                "mobileNumber": socialLogInInfo.mobileNumber,
                "randomkey": socialLogInInfo.randomkey]
        ]
    }
    
    private func parseSocialLogInInfo(response: CreateSessionModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { self.parseErrorInfo(errorInfo: response.errorInfo); return }
        if response.result?.code?.lowercased() == ResponseStatus.usernotexists.rawValue {
            self.delegate?.socialLogInSuccessProcess()
        } else if let sessionInfo = response.result?.session {
            UserDefaultsModel.saveSessionDetails(sessionInfo: sessionInfo)
            self.getCustomerDetailsService()
        }
    }
}


struct SocialLogInInfoViewModel {
    var accessToken: String = ""
    var socialFlag: String = ""
    var mobileNumber: String = ""
    var randomkey: String = ""
}
