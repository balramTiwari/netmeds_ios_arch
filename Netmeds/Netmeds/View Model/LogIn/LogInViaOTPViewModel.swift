//
//  LogInViaOTPViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct LogInViaOTPViewModel {
    var delegate: LogInViaOTPDelegateHandlers?
    
    func logInViaOTP(mobileNo: String, randomKey: String, otp: String) {
        NetworkManager.shared.logInViaOTPService(mobileNo: mobileNo, randomKey: randomKey, otp: otp) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            guard logInInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
                self.parseErrorInfo(errorInfo: logInInfo.errorInfo); return }
            guard let sessionInfo = logInInfo.result?.session else { return }
            UserDefaultsModel.saveSessionDetails(sessionInfo: sessionInfo)
            self.getCustomerDetailsService()
        }
    }
    
    func reSendOTPService(randomKey: String) {
        NetworkManager.shared.reSendOTPService(randomKey: randomKey) { (error, response) in
            guard error == nil, let logInInfo = response as? LogInModel else { return }
            self.parseOTPInfo(response: logInInfo)
        }
    }
    
    func socialLogInService(socialLogInInfo: SocialLogInInfoViewModel) {
        let params = setUpParamsForSocialLogIn(socialLogInInfo:socialLogInInfo)
        NetworkManager.shared.socialLogInService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            self.parseSocialLogInInfo(response: logInInfo)
        }
    }
    
    func getCustomerDetailsService() {
        NetworkManager.shared.customerDetailsService { (error, response) in
            guard error == nil, let customerInfo = response as? CustomerInfoModel else { return }
            self.parseCustomerInfo(response: customerInfo)
        }
    }
}

extension LogInViaOTPViewModel {
    private func parseOTPInfo(response: LogInModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.parseRandomKey(response: response)
    }
    
    private func parseRandomKey(response: LogInModel) {
        guard let randomKey = response.result?.otpDetails?.randomKey else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.delegate?.resendOTPSuccessProcess(randomKey: randomKey)
    }
    
    private func parseCustomerInfo(response: CustomerInfoModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue, let customerDetails = response.result?.details else { return }
        UserManager.shared.resetUserDetails(userInfo: customerDetails)
        self.delegate?.homePageProcess()
    }
    
    private func setUpParamsForSocialLogIn(socialLogInInfo: SocialLogInInfoViewModel) -> JSONArrayOfDictString {
        return [
            [
                "signature": "",
                "payload": "",
                "accessToken": socialLogInInfo.accessToken,
                "socialFlag": socialLogInInfo.socialFlag,
                "source": kSourceApp,
                "mobileNumber": socialLogInInfo.mobileNumber,
                "randomkey": socialLogInInfo.randomkey]
        ]
    }
    
    private func parseSocialLogInInfo(response: CreateSessionModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { self.parseErrorInfo(errorInfo: response.errorInfo); return }
        if let randomKey = response.result?.randomKey, randomKey > 0 {
            self.delegate?.resendOTPSuccessProcess(randomKey: String(randomKey))
        } else if let sessionInfo = response.result?.session {
            UserDefaultsModel.saveSessionDetails(sessionInfo: sessionInfo)
            self.getCustomerDetailsService()
        }
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
