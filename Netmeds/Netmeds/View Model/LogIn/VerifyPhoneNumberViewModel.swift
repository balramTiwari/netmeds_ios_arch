//
//  VerifyPhoneNumberViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct VerifyPhoneNumberViewModel {
    
    var delegate: VerifyPhoneNoDelegateHandlers?
    
    func socialLogInService(socialLogInInfo: SocialLogInInfoViewModel) {
        let params = setUpParamsForSocialLogIn(socialLogInInfo:socialLogInInfo)
        NetworkManager.shared.socialLogInService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            self.parseSocialLogInInfo(response: logInInfo)
        }
    }
}

extension VerifyPhoneNumberViewModel {
    private func setUpParamsForSocialLogIn(socialLogInInfo: SocialLogInInfoViewModel) -> JSONArrayOfDictString {
        return [
            [
                "signature": "",
                "payload": "",
                "accessToken": socialLogInInfo.accessToken,
                "socialFlag": socialLogInInfo.socialFlag,
                "source": kSourceApp,
                "mobileNumber": socialLogInInfo.mobileNumber,
                "randomkey": socialLogInInfo.randomkey]
        ]
    }
    
    private func parseSocialLogInInfo(response: CreateSessionModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { self.parseErrorInfo(errorInfo: response.errorInfo); return }
        if let randomKey = response.result?.randomKey, randomKey > 0 {
            self.delegate?.successProcess(randomKey: String(randomKey))
        } else if response.result?.code?.lowercased() == ResponseStatus.userexists.rawValue, let message = response.result?.message, message.isValidString {
            self.delegate?.failureProcess(errorMessage: message)
        }
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
