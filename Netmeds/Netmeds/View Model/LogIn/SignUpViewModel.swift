//
//  SignUpViewModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct SignUpViewModel {

    var delegate: SignUpDelegateHandlers?
    
    func signUpService(params: JSONDictAny) {
        NetworkManager.shared.signUpService(params: params) { (error, response) in
            guard error == nil, let logInInfo = response as? CreateSessionModel else { return }
            guard logInInfo.status?.lowercased() == ResponseStatus.success.rawValue else {
                self.parseErrorInfo(errorInfo: logInInfo.errorInfo); return }
            guard let sessionInfo = logInInfo.result?.session else { return }
            UserDefaultsModel.saveSessionDetails(sessionInfo: sessionInfo)
            self.getCustomerDetailsService()
        }
    }
    
    func reSendOTPService(randomKey: String) {
          NetworkManager.shared.reSendOTPService(randomKey: randomKey) { (error, response) in
              guard error == nil, let logInInfo = response as? LogInModel else { return }
              self.parseOTPInfo(response: logInInfo)
          }
      }
    
    func getCustomerDetailsService() {
        NetworkManager.shared.customerDetailsService { (error, response) in
            guard error == nil, let customerInfo = response as? CustomerInfoModel else { return }
            self.parseCustomerInfo(response: customerInfo)
        }
    }
}

extension SignUpViewModel {
    
    private func parseOTPInfo(response: LogInModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.parseRandomKey(response: response)
    }
    
    private func parseRandomKey(response: LogInModel) {
        guard let randomKey = response.result?.otpDetails?.randomKey else { parseErrorInfo(errorInfo: response.errorInfo); return }
        self.delegate?.resendOTPSuccessProcess(randomKey: randomKey)
    }

    private func parseCustomerInfo(response: CustomerInfoModel) {
        guard response.status?.lowercased() == ResponseStatus.success.rawValue, let customerDetails = response.result?.details else { return }
        UserManager.shared.resetUserDetails(userInfo: customerDetails)
        self.delegate?.homePageProcess()
    }
    
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
        self.delegate?.failureProcess(errorMessage: errorMessage)
    }
}
