//
//  OffersDetailViewModel.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum OffersDetailType {
    case couponCode
    case image
    case moreDetails
    case others
}

class OffersDetailViewModel {
    var offersDetailCustomModel = [OffersDetailCustomModel]()
    weak var delegate: OffersDetailDelegateHandlers?

    func getOffersDetailService(pageID: String) {
        NetworkManager.shared.getAllOffersService(pageID: pageID) { [weak self] (error, response) in
            guard error == nil, let offersDetailInfo = response as? [AllOffersModel], offersDetailInfo.count > 0 else {
                if pageID != "0" { self?.delegate?.failureProcess()}; return }
            self?.parseOffersDetail(offersDetailInfo: offersDetailInfo.first!)
        }
    }
}

extension OffersDetailViewModel {
    func parseOffersDetail(offersDetailInfo: AllOffersModel) {
        offersDetailCustomModel.removeAll()
        if let bannerImg = offersDetailInfo.bannerImg, bannerImg.isValidString {
            offersDetailCustomModel.append(OffersDetailCustomModel(title: "", desc: bannerImg, type: .image))
        }
        if let couponCode = offersDetailInfo.couponCode, couponCode.isValidString {
            offersDetailCustomModel.append(OffersDetailCustomModel(title: "COUPON CODE", desc: couponCode, type: .couponCode))
        }
        if let eligibility = offersDetailInfo.eligibility, eligibility.isValidString {
            offersDetailCustomModel.append(OffersDetailCustomModel(title: "ELIGIBILITY", desc: eligibility))
        }
        if let howToGet = offersDetailInfo.howToGet, howToGet.isValidString {
            offersDetailCustomModel.append(OffersDetailCustomModel(title: "HOW DO YOU GET IT?", desc: howToGet))
        }
        if let conditions = offersDetailInfo.conditions, conditions.isValidString {
            offersDetailCustomModel.append(OffersDetailCustomModel(title: "CONDITIONS IN CASE OF CANCELLATION", desc: conditions))
        }
        offersDetailCustomModel.append(OffersDetailCustomModel(title: "FOR MORE DETAILS", desc: "", type: .moreDetails))
        delegate?.successProcess(offersDetailsModel: offersDetailInfo)
    }
}

struct OffersDetailCustomModel {
    var title: String = ""
    var desc: String = ""
    var type: OffersDetailType = .others
    init(title: String, desc: String, type: OffersDetailType = .others) {
        self.title = title
        self.desc = desc
        self.type = type
    }
}
