//
//  AllOffersViewModel.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum AllOffersSelectionType: String {
    case medicine = "M"
    case diagnostics = "D"
    case consultation = "C"
}

class AllOffersViewModel {
    private var allOffersInfoModel = [AllOffersModel]()
    var allOffersModel = [AllOffersModel]()
    var sectionType: AllOffersSelectionType = .medicine
    weak var delegate: AllOffersDelegateHandlers?

    func getAllOffersService() {
        NetworkManager.shared.getAllOffersService(pageID: "0") { [weak self] (error, response) in
            guard error == nil, let allOffers = response as? [AllOffersModel] else { return }
            self?.parseAllOffers(allOffers: allOffers)
        }
    }
}

extension AllOffersViewModel {
    private func parseAllOffers(allOffers: [AllOffersModel]) {
        allOffersInfoModel = allOffers
        filterBy()
    }
    
    func filterBy(selectionType type: AllOffersSelectionType = .medicine) {
        allOffersModel = allOffersInfoModel.filter { $0.sectionType!.contains(type.rawValue) }
        delegate?.successProcess()
    }
}
