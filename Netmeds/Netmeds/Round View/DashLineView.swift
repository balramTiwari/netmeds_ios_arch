//
//  DashLineView.swift
//  Netmeds
//
//  Created by SANKARLAL on 07/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class DashLineView: UIView {
    
    var borderColor: UIColor?
    var dashPattern: [CGFloat] = [10, 4]

    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
        
        if let color = borderColor {
            color.setStroke()
        } else {
            UIColor.gray.setStroke()
        }
        
        path.lineWidth = 1
        
        path.setLineDash(dashPattern, count: 2, phase: 0)
        path.stroke()
    }
}
