//
//  DiagnosticsSelectSlotViewController.swift
//  Netmeds
//
//  Created by NETMEDS on 07/06/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit
import NetmedsServices
import SVProgressHUD

class DiagnosticsSelectSlotViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    // Week/Day View
    @IBOutlet weak var weekView: UIView!
    
    //Bottom View outlets
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var nextButton: NMSRoundButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
        
    var selectedDay = [String: Any]()
    var slotsAvailable = [[String: Any]]()
    var slotTimes = [[String: Any]]()
    var totalAmount: String?
    var selectedSlotTime: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarForChildViewController()

        let tableViewCellNib = UINib(nibName: "DiagnosticsPopularTestTableViewCell", bundle: nil)
        self.tableView.register(tableViewCellNib, forCellReuseIdentifier: "DiagnosticsPopularTestTableViewCell")
        
        getNextSevenDate()
        getLabSlots()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.layer.cornerRadius = 8
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        guard let slotTime = selectedSlotTime else {
            addNotificationView(textlabel: "Please select slot time!.")
            return
        }
        
        
    }
    
    
    func getNextSevenDate() {
        var value = [Date]()
        for i in 1...7 {
            if let date = Calendar.current.date(byAdding: .day, value: i, to: Date()) {
                value.append(date)
            }
        }
        print(value)
    }
    
    
    func getLabSlots() {
        SVProgressHUD.show()
        let request = DiagnosticsGetLabSlotsServiceRequest(delegate: self as NMServiceDelegate)
        request.getlabSlotsServiceRequest(pincode: "110023", labID: "1002")
    }
    
}

// MARK:- Tableview Delegate and dataSource
extension DiagnosticsSelectSlotViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slotTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiagnosticsPopularTestTableViewCell", for: indexPath) as! DiagnosticsPopularTestTableViewCell
        let slotTime = slotTimes[indexPath.row]
        
        cell.testNameLabel.text = (slotTime["timeRange"] as? String)?.lowercased()
        cell.separatorView.isHidden = true
        
        if let slotTime = slotTime["timeRange"] as? String, let selectedSlot = selectedSlotTime, slotTime.lowercased() == selectedSlot.lowercased() {
            cell.checkCircleButton.setImage( UIImage(named: "checkCircle"), for: .normal) // selectedPrimary
        } else {
            cell.checkCircleButton.setImage(UIImage(named: "radio_unchecked"), for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let slotTime = slotTimes[indexPath.row]
        if let slot = slotTime["timeRange"] as? String {
            selectedSlotTime = slot.lowercased()
        }
        tableView.reloadData()
    }
    
}

//MARK:-  CollectionView Delegate
extension DiagnosticsSelectSlotViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slotsAvailable.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NMSubscriptionCollectionViewCell", for: indexPath) as! NMSubscriptionCollectionViewCell
        
        let weekday = slotsAvailable[indexPath.row]
        cell.monthLabel.text = weekday["slotDayDate"] as? String ?? ""
        
        if let selectedDay = selectedDay["slotDayDate"] as? String, let day = weekday["slotDayDate"] as? String {
            cell.highLightView.isHidden = (day == selectedDay) ? false : true
        } else {
            cell.highLightView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDay = slotsAvailable[indexPath.row]
        collectionView.reloadData()
        
        if let slotTimes =  selectedDay["slotTime"] as? [[String: Any]] {
            self.slotTimes = slotTimes
            self.selectedSlotTime = nil
        }
        
        tableView.reloadData()
    }
}

// MARK: - API Service Delegate Methods
extension DiagnosticsSelectSlotViewController: NMServiceDelegate {
    func serviceResponse(serviceInfo: [AnyHashable : Any], urlResponse: URLResponse, serviceType: ServiceType) {
        
        if serviceType == .DiagnosticsLabSlots, let result = serviceInfo["result"] as? [String: Any], let slotsAvailable = result["slotsAvailable"] as? [[String: Any]] {
            if SVProgressHUD.isVisible() {
                SVProgressHUD.dismiss()
            }
            
            self.slotsAvailable = slotsAvailable
            collectionView.reloadData()
            
            if let firstSlotTime = slotsAvailable.first, let slotTimes = firstSlotTime["slotTime"] as? [[String: Any]]  {
                selectedDay = firstSlotTime
                self.slotTimes = slotTimes
                tableView.reloadData()
            }
        }
    }
    
    func serviceFailedWithError(error: NSError!, urlResponse: URLResponse?, serviceType: ServiceType) {
        print(error)
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */
