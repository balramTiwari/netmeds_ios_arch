//
//  CustomSegmentedControl.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UIView {
    var segmentCount = 0
    var segmentedControl = UISegmentedControl()
    let buttonBar = UIView()
    let separatorBar = UIView()
    var selectedIndex = 0
    var isFromOffersPage = false
    private var action :((Int) -> ())?
    
    // Closure Method
    func handleSegmentedControlValueChangedAction(ForAction action:@escaping (Int) -> () ) {
        self.action = action
    }
    
    func loadView() {
        self.backgroundColor = .white
        // Add segments
        if isFromOffersPage {
            segmentedControl.insertSegment(withTitle: "Medicine ", at: 0, animated: true)
            segmentedControl.insertSegment(withTitle: "Diagnostics ", at: 1, animated: true)
            segmentedControl.insertSegment(withTitle: "Consultation ", at: 2, animated: true)
        } else {
            if !(segmentedControl.numberOfSegments > 0) {
                for segment in 1...segmentCount {
                    segmentedControl.insertSegment(withTitle: "ORDER \(segment) OF \(segmentCount)", at: segment, animated: true)
                }
            }
        }
        segmentedControl.selectedSegmentIndex = selectedIndex
        segmentedControl.backgroundColor = .clear
        segmentedControl.tintColor = .clear
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        if let font: UIFont = UIFont(name: "Lato-Bold", size: 12) {
            segmentedControl.setTitleTextAttributes([ NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor() ], for: .normal)
            
            if isFromOffersPage {
                segmentedControl.setTitleTextAttributes([ NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: UIColor.NMLogoGreenColor()], for: .selected)
            } else {
                segmentedControl.setTitleTextAttributes([ NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor(with: 1.0) ], for: .selected)
            }
        }
        segmentedControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(_:)), for: UIControl.Event.valueChanged)
        // Selected bar
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = isFromOffersPage ? UIColor.NMLogoGreenColor() : UIColor.mediumPinkTextColor()
        buttonBar.layer.cornerRadius = 2
        let widthOfOneBar = self.frame.width / CGFloat(segmentCount)
        let centerOfOneBar = widthOfOneBar / 2
        let xPosition = widthOfOneBar * CGFloat(segmentedControl.selectedSegmentIndex)
        let centerXPosition = xPosition + centerOfOneBar
        if isFromOffersPage {
            buttonBar.frame = CGRect(x: centerXPosition - 22, y: 41, width: 30, height: 4)
        } else {
            buttonBar.frame = CGRect(x: centerXPosition - 6, y: 33, width: 15, height: 4)
        }
        if !isFromOffersPage {
            // Separator View
            separatorBar.frame = CGRect(x: widthOfOneBar, y: 0, width: 2, height: 30)
            separatorBar.backgroundColor = UIColor.NMSTextColor(with: 0.1)
            for count in 0...segmentCount {
                if !(count == 0 || count == segmentCount) {
                    let view = createSeparatorView(viewCount: count)
                    self.addSubview(view)
                }
            }
        }
        self.addSubview(segmentedControl)
        self.addSubview(buttonBar)
        // set constraints
        segmentedControl.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        segmentedControl.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        segmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        self.action?(sender.selectedSegmentIndex) ?? ()
        let widthOfOneBar = self.frame.width / CGFloat(segmentCount)
        let centerOfOneBar = widthOfOneBar / 2
        let xPosition = widthOfOneBar * CGFloat(sender.selectedSegmentIndex)
        let centerXPosition = xPosition + centerOfOneBar
        self.buttonBar.frame.origin.x = centerXPosition
        if isFromOffersPage {
            self.buttonBar.frame.origin.x = centerXPosition - 15
        }
    }
    
    func createSeparatorView(viewCount: Int ) -> UIView {
        let separatorView = UIView()
        let widthOfOneBar = self.frame.width / CGFloat(segmentCount)
        let xPosition = widthOfOneBar * CGFloat(viewCount)
        separatorView.frame = CGRect(x: xPosition, y: 0, width: 2, height: 30)
        separatorView.backgroundColor = UIColor.NMSTextColor(with: 0.1)
        return separatorView
    }
}
