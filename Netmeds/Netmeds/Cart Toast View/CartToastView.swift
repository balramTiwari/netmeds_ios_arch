//
//  CartToastView.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

import UIKit

class CartToastView: UIView {
    @IBOutlet weak var txtlabel: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartButtonWidthConstraint: NSLayoutConstraint!
    var action :(() -> Void)?
    
    func actionViewCartHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.action = action
    }

    @objc private func triggerActionHandleBlock() {
        self.action?() ?? ()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.nibSetup()
    }
    
     override init(frame: CGRect) {
        super.init(frame: frame)
        self.nibSetup()
    }
    
    private func nibSetup() {
        let view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
        self.viewCartBtn.addTarget(self, action: #selector(self.triggerActionHandleBlock), for: .touchUpInside)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
}
