//
//  NMSConstants.swift
//  Netmeds
//
//  Created by Netmeds on 29/11/18.
//  Copyright © 2018 Netmeds. All rights reserved.
//

import Foundation

//TrueCaller
let NMTrueCallerAPPKEY = "w9Rqic233586d022149b6842bc4ac36d8b539";
let NMTrueCallerAPPLinks = "https://sic18291e5161843d68f9278441fef5c54.truecallerdevs.com";

//Google
let googleClientID = "667464643048-q86upv8aa07isqb79sa6qnip9cpd5222.apps.googleusercontent.com"
let googleReversedClientID = "com.googleusercontent.apps.667464643048-q86upv8aa07isqb79sa6qnip9cpd5222"

//MARK:- onboard screen sliders
let onboardSlideXibID = "OnBoardSlide"

let slider1Title = "Buy Medicine @ 20% off"
let slider1SubTitle =  "100 yrs’ of excellence  -  100% authenticity  -  Trusted by 3 million+ "

let slider2Title = "Easy Order & Fast Delivery"
let slider2Subtitle = "Prescription Upload  -  Verification  -  Doorstep Delivery  -  Get Medicine every month"

let slider3Title = "There’s a lot more..."
let slider3Subtitle = "Consult Doctor  -  Home LabTests  -  Save Health Records"


//MARK:- Language Selection Screen

var bundleKey = "0"
let languageStrings = ["English", "Hindi", "Tamil"] //"Telugu", "Kannada", "Malayalam"]
let LanguageCodeStrings = ["English", "हिंदी", "தமிழ்"] //"తెలుగు", , "తెలుగు", "മലയാളം"]
let languageCodes = ["en", "hi", "ta"] //"te", "kn", "ml"]

let LanguageSaveKey = "currentLanguageKey"
let cellID = "NMSLanguageCell"


//MARK:- Login
let signinTitletext = "Sign in"
let signinSubtitleText = "Sign in to access your Orders, Medicines, and Offers."

let verifyTitleText = "Verify"
let verifySubtitleText = "Enter number to access your Orders, Medicines, and Offers."

let termsAndPolicyText = "By continuing you agree to our Terms or service and Privacy & Legal Policy."

let otpTimerMaxCountText = 180



//SEGUE Identifiers
let loginSegueID = "segueLoginViewController"
