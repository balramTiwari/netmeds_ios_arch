//
//  Json.swift
//  Netmeds
//
//  Created by NETMEDS on 10/12/18.
//  Copyright © 2018 Netmeds. All rights reserved.
//

import Foundation
import HandyJSON
import UIKit

class Json: NSObject {
    static func onboardingJson() -> [Onboard?] {
        
        let jsonString = """
                  {
                  "serviceStatus": {
                    "status": "Success",
                    "statusCode": 200,
                    "message": ""
                  },
                  "result": [
                    {
                      "onBoardingImage": "onboarding_one",
                      "onBoardingTitle": "Buy Medicine @ 20% off",
                      "onBoardingDescription": "100 yrs' of excellence  -  100% authenticity  -  Trusted by 3 million+"
                    },
                    {
                      "onBoardingImage": "onboarding_two",
                      "onBoardingTitle": "Easy Order Fast Delivery",
                      "onBoardingDescription": "Prescription Upload  -  Verification  -  Doorstep Delivery  -  Get Medicine every month"
                    },
                    {
                      "onBoardingImage": "onboarding_three",
                      "onBoardingTitle": "There's a lot more...",
                      "onBoardingDescription": "Consult Doctor  -  Home LabTests  -  Save Health Records"
                    }
                  ]
                }
                """
        
        if let onboards = [Onboard].deserialize(from: jsonString, designatedPath: "result") {
            return onboards
        }
        return [Onboard()]
    }
}

//MARK:- Onboard slide Data class
class Onboard: HandyJSON {
    var onBoardingImage: String?
    var onBoardingTitle: String?
    var onBoardingDescription: String?
    
    required init() {}
}
