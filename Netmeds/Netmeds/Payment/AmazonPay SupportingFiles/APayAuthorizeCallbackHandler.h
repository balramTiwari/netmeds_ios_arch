//
//  APayAuthorizeCallbackHandler.h
//  SilentPayDemoiOS
//

#import <Foundation/Foundation.h>
#import <PWAINSilentPayiOSSDK/APayAuthorizeCallbackDelegate.h>
#import "AuthorizeAndGetBalanceDelegate.h"

@interface APayAuthorizeCallbackHandler : NSObject<APayAuthorizeCallbackDelegate>

@property (nonatomic, weak) id <AuthorizeAndGetBalanceDelegate> delegate;

@end
