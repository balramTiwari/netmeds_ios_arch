//
//  APayAuthorizeCallbackHandler.m
//  SilentPayDemoiOS
//

#import "APayAuthorizeCallbackHandler.h"
#import "SilentPayHelper.h"
#import <SafariServices/SafariServices.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@implementation APayAuthorizeCallbackHandler

- (void)onCancel {
    [SilentPayHelper showMessage:@"User cancelled the transaction"];
}

- (void)onMobileSDKError:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onNetworkUnavailable {
    [SilentPayHelper showMessage:@"Network Unavailable"];
}

- (void)onFailure:(NSError *)error {
    [SilentPayHelper showMessage:@"Could not connect Amazon Pay Account, Please try again"];
}

- (void)onSuccess {
    if(_delegate){
        [_delegate authorizationSuccess];
    }
}

@end
