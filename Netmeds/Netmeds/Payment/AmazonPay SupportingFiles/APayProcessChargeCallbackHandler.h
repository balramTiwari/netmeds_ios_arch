//
//  APayProcessChargeCallbackHandler.h
//  SilentPayDemoiOS
//

#import <Foundation/Foundation.h>
#import <PWAINSilentPayiOSSDK/APayProcessChargeCallbackDelegate.h>

@interface APayProcessChargeCallbackHandler : NSObject<APayProcessChargeCallbackDelegate>


@end
