//
//  SilentPayHelper.h
//  SilentPayDemoiOS


#import <UIKit/UIKit.h>
#import <PWAINSilentPayiOSSDK/AmazonPay.h>
#import <PWAINSilentPayiOSSDK/GetBalanceRequest.h>

@interface SilentPayHelper : NSObject

+ (void) callProcessCharge: (NSString *) amount
                 isSandbox:(BOOL)isSandbox;

+(void) validateResponse:(ProcessChargeResponse *)response;

+(void) callGetChargeStatus: (NSString *)transactionId;

+(void) validateChargeStatusResponse:(GetChargeStatusResponse *)response;

+ (void ) showMessage:(NSString *)message;

+ (void ) showToast:(NSString *)message;

@end
