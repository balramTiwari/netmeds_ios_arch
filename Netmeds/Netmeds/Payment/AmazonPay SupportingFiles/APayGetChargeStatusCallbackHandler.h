//
//  APayGetChargeStatusCallbackHandler.h
//  SilentPayDemoiOS
//


#import <UIKit/UIKit.h>
#import <PWAINSilentPayiOSSDK/APayGetChargeStatusCallbackDelegate.h>

@interface APayGetChargeStatusCallbackHandler : NSObject<APayGetChargeStatusCallbackDelegate>

@end
