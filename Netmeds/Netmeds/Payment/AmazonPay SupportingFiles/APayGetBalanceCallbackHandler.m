//
//  APayGetBalanceCallbackHandler.m
//  SilentPayDemoiOS
//
//  Created by A R, Deepthi on 12/20/17.
//  Copyright © 2017 A R, Deepthi. All rights reserved.
//

#import "APayGetBalanceCallbackHandler.h"
#import <SafariServices/SafariServices.h>
#import "SilentPayHelper.h"

@implementation APayGetBalanceCallbackHandler

//on getbalance failure
- (void)onFailure:(id)response error:(NSError *)error {
    if(_delegate){
        [_delegate didRecieveBalanceData:nil];
    }
}

- (void)onMobileSDKError:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onNetworkUnavailable {
    [SilentPayHelper showMessage:@"Network Unavailable"];
}

//on getbalance success
- (void)onSuccess:(GetBalanceResponse *)response {
    if(_delegate){
        //call did recieve balance
        [_delegate didRecieveBalanceData:[self buildMessageWithGetBalanceResponse:response]];
    }
}

- (NSString *)buildMessageWithGetBalanceResponse:(GetBalanceResponse *)response{
    NSString *message = [NSString stringWithFormat:@"%@", response.balance];
    message = [message stringByAppendingString:[NSString stringWithFormat:@" %@",response.currencyCode]];
    return message;
}

@end
