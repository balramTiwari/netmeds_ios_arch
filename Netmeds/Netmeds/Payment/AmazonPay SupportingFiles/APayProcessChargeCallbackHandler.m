//
//  APayProcessChargeCallbackHandler.m
//  SilentPayDemoiOS
//
//  Created by A R, Deepthi on 12/20/17.
//  Copyright © 2017 A R, Deepthi. All rights reserved.
//

#import "APayProcessChargeCallbackHandler.h"
#import "APayProcessChargeCallbackHandler.h"
#import "SilentPayHelper.h"

@implementation APayProcessChargeCallbackHandler

- (void)onCancel {
    [SilentPayHelper showMessage:@"Process charge canceled"];
}

- (void)onFailure:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onMobileSDKError:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onNetworkUnavailable {
    [SilentPayHelper showMessage:@"Network Unavailable"];
}

- (void)onSuccess:(ProcessChargeResponse *)response {
    [SilentPayHelper showToast:@"validating process charge response...."];
    [SilentPayHelper validateResponse:response];
}

@end
