//
//  APayGetBalanceCallbackHandler.h
//  SilentPayDemoiOS
//
//  Created by A R, Deepthi on 12/20/17.
//  Copyright © 2017 A R, Deepthi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <PWAINSilentPayiOSSDK/APayGetBalanceCallbackDelegate.h>
#import "AuthorizeAndGetBalanceDelegate.h"

@interface APayGetBalanceCallbackHandler : NSObject<APayGetBalanceCallbackDelegate>
@property (nonatomic, weak) id <AuthorizeAndGetBalanceDelegate> delegate;
@end
