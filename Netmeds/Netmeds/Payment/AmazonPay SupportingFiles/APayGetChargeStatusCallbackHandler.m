//
//  APayGetChargeStatusCallbackHandler.m
//  SilentPayDemoiOS
//

#import "APayGetChargeStatusCallbackHandler.h"
#import <Foundation/Foundation.h>
#import "APayProcessChargeCallbackHandler.h"
#import "SilentPayHelper.h"

@implementation APayGetChargeStatusCallbackHandler

- (void)onFailure:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onMobileSDKError:(NSError *)error {
    [SilentPayHelper showMessage:error.description];
}

- (void)onNetworkUnavailable {
    [SilentPayHelper showMessage:@"Network Unavailable while calling get charge status"];
}

- (void)onSuccess:(GetChargeStatusResponse *)response {
    if(response.transactionStatus == SUCCESS){
        [SilentPayHelper validateChargeStatusResponse:response];
    }else if(response.transactionStatus == FAILURE){
        [SilentPayHelper validateChargeStatusResponse:response];
    }else{
        //retry after 2 seconds
        [SilentPayHelper showToast:@"Pending..."];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SilentPayHelper callGetChargeStatus:response.transactionId];
        });
    }
}

@end
