//
//  SilentPayHelper.m
//  SilentPayDemoiOS
//

#import "SilentPayHelper.h"
#import <PWAINSilentPayiOSSDK/APayAuthorizeCallbackDelegate.h>
#import <PWAINSilentPayiOSSDK/APayGetBalanceCallbackDelegate.h>
#import "APayGetBalanceCallbackHandler.h"
#import <PWAINSilentPayiOSSDK/EncryptedRequest.h>
#import "APayProcessChargeCallbackHandler.h"
#import "APayGetChargeStatusCallbackHandler.h"

@interface SilentPayHelper () <NSURLSessionDelegate>

@end

@implementation SilentPayHelper

NSString* ENDPOINT = @"http://ec2-35-162-20-220.us-west-2.compute.amazonaws.com";
NSString* PROCESS_CHARGE_PATH1 = @"/prod/signAndEncrypt.jsp";
NSString* VALIDATION_PATH = @"/prod/verifySignature.jsp";
NSString* GET_CHARGE_STATUS_PATH = @"/prod/signAndEncryptForOperation.jsp";

typedef void (^NSURLSessionDataTaskHandler)(NSData *, NSURLResponse *, NSError *);

NSURLSession *urlSession;

+(void) callGetChargeStatus: (NSString *)transactionId{
    
    [self showToast:@"getting charge status..."];
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   transactionId, @"transactionId",
                                   @"TRANSACTION_ID",@"transactionIdType"
                                   , nil];
    
    NSURL *url  = [self buildURLForHost:ENDPOINT path:GET_CHARGE_STATUS_PATH protocol:nil shouldEncodeQueryParams:YES urlParams:params];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    [self executeRequest:request onRequestCompletion:^(NSData *data, NSError *error){
        if(data){
            NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            newStr = [newStr substringFromIndex:[newStr rangeOfString:@"?" options:NSCaseInsensitiveSearch].location+1 ];
            
            NSString *decoded = [[self getURLDecodedString:newStr] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSMutableDictionary* params = [self getParamsFromQueryString:decoded];
            
            APayGetChargeStatusCallbackHandler *apayCallback = [[APayGetChargeStatusCallbackHandler alloc] init];
            
            NSString *payload = [params[@"payload"] substringToIndex:[params[@"payload"] rangeOfString:@"<br/>" options:NSCaseInsensitiveSearch].location];
            
            BOOL isSandbox = ([transactionId containsString:@"S04"])?YES:NO;
            
            EncryptedRequest *getChargeStatusRequest = [EncryptedRequest
                                                        build:^(id<EncryptedRequestBuilder> builder){
                                                            [builder withIV:params[@"iv"]];
                                                            [builder withKey:params[@"key"]];
                                                            [builder withPayload:payload];
                                                            [builder withIsSandbox:isSandbox];
                                                        }];
            
            
            [[AmazonPay sharedInstance] getChargeStatus:getChargeStatusRequest apayGetChargeStatusCallback:apayCallback];
            
        }else{
            [SilentPayHelper showMessage:@"Error while encypting get charge status request"];
        }
    }];
    
    
}

+ (void) callProcessCharge: (NSString *) amount
                 isSandbox:(BOOL)isSandbox
{
    
    [self showToast:@"Encrypting request ...."];
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   amount,@"orderTotalAmount",
                                   @"INR",@"orderTotalCurrencyCode",
                                   @"some note", @"sellerNote",
                                   @"sampleName", @"sellerStoreName",
                                   [[NSUUID UUID] UUIDString], @"sellerOrderId", nil];
    NSLog(@"process charge path is %s",[PROCESS_CHARGE_PATH1 UTF8String]);
    
    NSURL *url  = [self buildURLForHost:ENDPOINT path:PROCESS_CHARGE_PATH1 protocol:nil shouldEncodeQueryParams:YES urlParams:params];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSLog(@"this is the url %s",[[url absoluteString] UTF8String]);
    
    [self executeRequest:request onRequestCompletion:^(NSData *data, NSError *error){
        if(data){
            NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSString *decoded = [[self getURLDecodedString:newStr] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            
            NSMutableDictionary* params = [self getParamsFromQueryString:decoded];
            
            APayProcessChargeCallbackHandler *apayCallback = [[APayProcessChargeCallbackHandler alloc] init];
            
            
            
            EncryptedRequest *processChargeRequest = [EncryptedRequest
                                                      build:^(id<EncryptedRequestBuilder> builder){
                                                          [builder withIV:params[@"iv"]];
                                                          [builder withKey:params[@"key"]];
                                                          [builder withPayload:params[@"payload"]];
                                                          [builder withIsSandbox:isSandbox];
                                                          
                                                      }];
            
            NSLog(@"created the ency req");
            
            [self showToast:@"calling process charge....."];
            [[AmazonPay sharedInstance] processCharge:processChargeRequest apayProcessChargeCallback:apayCallback];
            
        }else{
            [SilentPayHelper showMessage:@"Error while executing request"];
        }
    }];
}



+(void) validateResponse:(ProcessChargeResponse *)response{
    [self showToast:@"validating process charge response..."];
    NSURLRequest *request = [self postURLRequest:VALIDATION_PATH urlParams:[response getVerificationParameters]];
    
//    NSDictionary *dictionary = [response getVerificationParameters];
    
    [self executeRequest:request onRequestCompletion:^(NSData *data, NSError *error){
        if(data){
            NSString* newStr = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if([newStr isEqualToString:@"true"]){
                [self callGetChargeStatus:response.amazonOrderId];
                
            }else{
                [self showMessage:@"ProcessCharge Validation failed!"];
            }
            
        }else{
            [self showMessage:error.description];
        }
    }];
}


+(void) validateChargeStatusResponse:(GetChargeStatusResponse *)response{
    NSURLRequest *request = [self postURLRequest:VALIDATION_PATH urlParams:[response getVerificationParameters]];
 //   NSDictionary *dictionary = [response getVerificationParameters];
    [self executeRequest:request onRequestCompletion:^(NSData *data, NSError *error){
        if(data){
            NSString* newStr = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([newStr isEqualToString:@"true"]){
                NSArray *arr = [NSArray arrayWithObjects:response.transactionId, response.transactionStatusDesciption, response.transactionValue, response.transactionCurrencyCode, response.transactionDate, response.merchantTransactionId, nil];
                
                NSString *str = [arr componentsJoinedByString:@"\n"];
                
                [self showMessage:str];
                
            }else{
                [self showMessage:@"Validate charge status failed"];
            }
            
        }else{
            [self showMessage:error.description];
        }
    }
     ];
}



+ (NSURLRequest *)postURLRequest:(NSString *)pathname
                       urlParams: (NSMutableDictionary *)params

{
    NSURL *url  = [self buildURLForHost:ENDPOINT path:pathname protocol:nil shouldEncodeQueryParams:YES urlParams:params];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    return request;
}
//helper methods

+ (NSURL *) buildURLForHost:(NSString *)host
                       path:(NSString *)path
                   protocol:(NSString *)protocl
    shouldEncodeQueryParams:(BOOL) shouldEncodeQueryParams
                  urlParams:(NSMutableDictionary *)urlParams
{
    NSLog(@"this is the path we have %s",[path UTF8String]);
    
    //append the protocol string in front of the host string
    host = ( protocl == nil)? host: [[protocl stringByAppendingString: @"://"] stringByAppendingString:host];
    
    //create as modifiedhost/path
    NSURL *baseUrl=  [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", host, path]];
    
    
    return [self buildURLWithBaseUrl:baseUrl
             shouldEncodeQueryParams:shouldEncodeQueryParams
                           urlParams:urlParams];
    
}

+ (NSURL *) buildURLWithBaseUrl:(NSURL *)baseUrl
        shouldEncodeQueryParams:(BOOL) shouldEncodeQueryParams
                      urlParams:(NSMutableDictionary *)urlParams
{
    
    if ([urlParams count] > 0)
    {
        NSMutableArray *paramArray = [[NSMutableArray alloc] init];
        
        [urlParams enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop)
         {
             [paramArray addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
         }];
        
        NSString *queryString = [[paramArray componentsJoinedByString:@"&"] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if(shouldEncodeQueryParams){
            queryString = [self getEncodedString:queryString];
        }
        NSString *urlString =[[baseUrl absoluteString] stringByAppendingString:[NSString stringWithFormat:@"?%@",queryString]];
        NSURL *url =  [NSURL URLWithString:urlString];
        return url;
    }else
    {
        return baseUrl;
    }
}


+ (NSMutableDictionary *) getParamsFromQueryString: (NSString *)queryString{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    for (id object in [queryString componentsSeparatedByString:@"&"]){
        NSString *key = [object substringToIndex:[object rangeOfString:@"=" options:NSCaseInsensitiveSearch].location];
        NSString *value = [object substringFromIndex:[object rangeOfString:@"=" options:NSCaseInsensitiveSearch].location + 1];
        params[key] = value;
    }
    return params;
}


+ (NSString *) getEncodedString:(NSString *)decodedString
{
    NSString *urlEncodedString = [decodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    urlEncodedString = [urlEncodedString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return urlEncodedString;
}


+ (NSString *) getURLDecodedString:(NSString *)encodedString
{
    NSString *urlDecodedString = [encodedString stringByRemovingPercentEncoding];
    return urlDecodedString;
}


+ (void) executeRequest:(NSURLRequest *)request
    onRequestCompletion:(void (^)(NSData *, NSError *))onRequestCompletion
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    urlSession = [NSURLSession sessionWithConfiguration:config];
    
    [self dispatchAsync:request
    onRequestCompletion:onRequestCompletion];
}

+ (void) dispatchAsync:(NSURLRequest *)request
   onRequestCompletion:(void (^)(NSData *, NSError *))onRequestCompletion
{
    @try
    {
        
        NSURLSessionDataTaskHandler handler = ^(NSData *data, NSURLResponse *response, NSError *urlError)
        {
            if(urlError != nil)
            {
                onRequestCompletion(data, nil);
                return;
            } else if (!response)
            {
                onRequestCompletion(data, nil);
                return;
            }
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSInteger statusCode = [httpResponse statusCode];
            if(statusCode>=200 && statusCode<300){
                onRequestCompletion(data, urlError);
                return;
            }
        };
        
        NSURLSessionDataTask *dataTask = [urlSession dataTaskWithRequest:request completionHandler:handler];
        [dataTask resume];
    } @catch(NSException *exception)
    {
        onRequestCompletion(nil,nil);
        return;
    }
}

+ (void) URLSession:(NSURLSession *)session
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
  completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    if ([challenge.protectionSpace.authenticationMethod isEqual:NSURLAuthenticationMethodServerTrust])
    {
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        return;
    } else
    {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
        return;
    }
}


+ (void ) showToast:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *penultimateViewController = topViewController;
    
    while (topViewController.presentedViewController)
    {
        penultimateViewController = topViewController;
        topViewController = topViewController.presentedViewController;
    }
    // the topmost controller would be safari controller which would be dead by the time callback methods are invoked
    // hence using the 2nd topmost controller
    dispatch_async(dispatch_get_main_queue(), ^{
        [penultimateViewController presentViewController:alertController animated:YES completion:nil];
    });
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alertController dismissViewControllerAnimated:YES completion:nil];
    });
    
}

+ (void ) showMessage:(NSString *)message
{
    UIAlertController * alertController = [UIAlertController
                                           alertControllerWithTitle:@"Transaction Response"
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your ok please button action here
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle cancel, thanks button
                                   }];
    
    [alertController addAction:okButton];
    [alertController addAction:cancelButton];
    
    
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *penultimateViewController = topViewController;
    
    while (topViewController.presentedViewController)
    {
        penultimateViewController = topViewController;
        topViewController = topViewController.presentedViewController;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [penultimateViewController presentViewController:alertController animated:YES completion:nil];
    });
}

@end
