//
//  AuthorizeAndGetBalanceDelegate.h
//  SilentPayDemoiOS
//
//  Created by A R, Deepthi on 12/20/17.
//  Copyright © 2017 A R, Deepthi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AuthorizeAndGetBalanceDelegate

-(void) didRecieveBalanceData: (NSString *) balance;

-(void) authorizationSuccess;

@end
