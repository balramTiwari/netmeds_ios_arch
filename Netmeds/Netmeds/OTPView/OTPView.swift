//
//  OTPView.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import UIKit

class OTPView: UIView, UITextFieldDelegate {
    @IBOutlet weak var otpBox1: UITextField!
    @IBOutlet weak var otpBox2: UITextField!
    @IBOutlet weak var otpBox3: UITextField!
    @IBOutlet weak var otpBox4: UITextField!
    @IBOutlet weak var otpBox5: UITextField!
    @IBOutlet weak var otpBox6: UITextField!
    
    //MARK:- Accessor methods
    func otpText() -> String {
        var finalString = ""
        if let string1 = otpBox1.text , let string2 = otpBox2.text, let string3 = otpBox3.text, let string4 = otpBox4.text, let string5 = otpBox5.text, let string6 = otpBox6.text {
            finalString = string1 + string2 + string3 + string4 + string5 + string6
        }
        return finalString
    }
    
    func updateOTPTimer(with count: Int) -> String {
        var countDownText: String = ""
        
        if count >= 0 {
            let minute = count / 60
            let seconds = count % 60
            
            switch (minute, seconds) {
            case (1, 2..<59):
                countDownText = "\(minute) minute \(seconds) seconds"
            case (0, 2..<59):
                countDownText = "\(seconds) seconds"
            case (0,1):
                countDownText = "\(seconds) second"
            case (0,0):
                countDownText = "\(seconds) second"
            default:
                countDownText = "\(minute) minutes \(seconds) seconds"
            }
        }
        return countDownText
    }
    
    @IBAction func textFieldDidChange(_ textField: UITextField) {
        let otpCode = textField.text
        if #available(iOS 12.0, *) {
            if textField.textContentType == .oneTimeCode {
                
                if let otp = otpCode, otp.count > 3 {
                    let otpText = Array(otp)
                    otpBox1.text = String(otpText[0])
                    otpBox2.text = String(otpText[1])
                    otpBox3.text = String(otpText[2])
                    otpBox4.text = String(otpText[3])
                    otpBox5.text = String(otpText[4])
                    otpBox6.text = String(otpText[5])
                }
            }
        }
        
        let text = textField.text
        if text?.utf16.count == 1 {
            moveToNextTextField(textField: textField)
        } else {
            moveToPreviousTextField(textField: textField)
        }
    }
    
    func moveToNextTextField(textField: UITextField) {
        switch textField {
        case otpBox1:
            otpBox2.becomeFirstResponder()
        case otpBox2:
            otpBox3.becomeFirstResponder()
        case otpBox3:
            otpBox4.becomeFirstResponder()
        case otpBox4:
            otpBox5.becomeFirstResponder()
        case otpBox5:
            otpBox6.becomeFirstResponder()
        case otpBox6:
            otpBox6.resignFirstResponder()
        default:
            break
        }
    }
    
    func moveToPreviousTextField(textField: UITextField) {
        switch textField {
        case otpBox6:
            otpBox5.becomeFirstResponder()
        case otpBox5:
            otpBox4.becomeFirstResponder()
        case otpBox4:
            otpBox3.becomeFirstResponder()
        case otpBox3:
            otpBox2.becomeFirstResponder()
        case otpBox2:
            otpBox1.becomeFirstResponder()
        case otpBox1:
            otpBox1.resignFirstResponder()
        default:
            break
        }
    }
    
    func clearTextField() {
        otpBox1.text = ""
        otpBox2.text = ""
        otpBox3.text = ""
        otpBox4.text = ""
        otpBox5.text = ""
        otpBox6.text = ""
    }
    
    //MARK:- TextFiele Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        moveToNextTextField(textField: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 1 && string.count == 1 {
            textField.text = string
            moveToNextTextField(textField: textField)
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == otpBox1 {
            clearTextField()
        }
    }
}
