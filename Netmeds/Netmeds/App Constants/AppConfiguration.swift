//
//  AppConfiguration.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

@objcMembers
public class ConsultationConfig: NSObject {
    
    @objc public static let consultationUrl = kConsultationBaseURL
    @objc public static let consultationImageUrl = "https://justdoc.com/img/"
    @objc public static let consultationAttachmentUrl = "https://s3-us-west-2.amazonaws.com/jdnmimgs/Attachments"
}
