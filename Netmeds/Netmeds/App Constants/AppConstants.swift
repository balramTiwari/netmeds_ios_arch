//
//  AppConstants.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

//TYPE_ALISING
typealias JSONDictAny = [String: Any]
typealias JSONDictString = [String: String]
typealias JSONArrayOfDictString = [[String: String]]
typealias JSONArrayOfDictAny = [[String: Any]]

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

enum RequestType: String {
    case appConfig
    case configURLPaths
    case logIn
    case socialLogIn
    case customerDetails
    case logInViaPassword
    case sendOTP
    case reSendOTP
    case initiateChangePassword
    case completeChangePassword
    case logInViaOTP
    case signUp
    case homeOffersBanner
    case homeBigSalesBanner
    case homeWellnessBanner
    case homeCashBackBanner
    case homeHealthExperts
    case downloadImage
    case algoliaSearch
    case productDetails
    case brainSins
    case createCart
    case addToCart
    case getCart
    case removeItemFromCart
    case addItemToCart
    case getWalletBalance
    case getAllCoupons
    case applyCouponCode
    case unApplyCouponCode
    case applySuperCash
    case deliveryEstimation
    case getAllAddress
    case paymentGateWayDetails
    case allNetBankingDetails
    case getCategoryProductList
    case getManufacturerProductList
    case getCategoryProductBanners
    case getManufacturerProductBanners
    case allOffers
    case getMyOrders
    case getMyOrdersDetail
    case getTrackOrderDetails
    case searchPhoto
    case method2Carts
    case uploadSelectedPrescriptions
    case detachPrescriptions
    case pastPrescriptions
    case jusPayOrderCreate
    case juspayOrderStatus
    case juspayOrderComplete
    case none
}

enum ResponseStatus: String {
    case success
    case fail
    case failed
    case usernotexists
    case userexists
    func type() -> String {
        return self.rawValue
    }
}

enum StoryBoardNameType: String {
    case logIn = "LogIn"
    case home = "Home"
    case search = "Search"
    case cart = "Cart"
    case paymentGateWay = "PaymentGateWay"
    case products = "Products"
    case allOffers = "AllOffers"
    case myOrders = "MyOrders"
    case uploadPrescriptions = "UploadPrescriptions"
//    case rxAttach = "AttachPrescription"
//    case pastRX = "PastPrescription"
//    case deliveryAddress = "DeliveryAddress"
//    case myOrder = "MyOrder"
//    case myAccount = "MyAccount"
//    case subscription = "Subscription"
//    case noInternet = "noInternet"
//    case categoryList = "CategoryList"
//    case webView = "webview"
//    case offers = "Offers"
//    case onlineConsultation = "OnlineConsultation"
//    case wellNess = "wellnessViewController"
//    case wellNessAllCategories = "WellnessAllCategories"
//    case referAndEarn = "ReferAndEarn"
//    case methodTwo = "MethodTwo"
//    case diagnostics = "Diagnostics"
//    case primeInfo = "NMPrimeMembership"
//    case orderReviewManually = "OrderReviewManually"
//    case orderPlacedSuccessfully = "OrderPlacedSuccessfully"
//    case addAddress = "AddAddress"
}

enum PageSourceType {
    case search
    case changePassword
    case otp
    case none
}

let kITUNESAPPURL = "itms-apps://itunes.apple.com/in/app/netmeds/id1011070011?mt=8"
let kManufacturerShortHand = "Mfr: "
let kRxRequired = "P"

let kPrimeProduct2001: Int = 2001
let kPrimeProduct2002: Int = 2002

// Decrytion  Keys
let kAESKey = "4u7x!A%D*F-JaNdR"
let kAESIV  = "3794118009232073"

// MARK:- User Default Keys
let kUserLoggedInKey: String = "kUserLoggedInKey"
let kSessionIDKey: String = "kSessionIDKey"
let kUserIDKey: String = "kUserIDKey"
let kLoganSessionIDKey: String = "kLoganSessionIDKey"
let kSessionExpireTSKey: String = "kSessionExpireTSKey"
let kCartIDKey: String = "kCartIDKey"

// GoogleAnalytics Track Id
let kGoogleAnalyticsTrackingId = "UA-63910444-2"

let kOTPTimerMaxSeconds = 45

let kToastViewDuration: TimeInterval = 2.0
let kPhoneNumberCharLength: Int = 10
let kTermsAndPolicyText1 = "By continuing you agree to our Terms of service"
let kTermsAndPolicyText2 = "and Privacy & Legal Policy."

//Google Keys
let kGoogleClientID = "667464643048-q86upv8aa07isqb79sa6qnip9cpd5222.apps.googleusercontent.com"
let googleReversedClientID = "com.googleusercontent.apps.667464643048-q86upv8aa07isqb79sa6qnip9cpd5222"
let kSourceApp = "app-ios"
let kGoogleSocialFlag = "g"
let kFBSocialFlag = "f"
//let kConsultationBaseURL = "https://s1-consult.netmeds.com"

let kTuneAdvertiserID = "189096"
let kTuneConversionKey = "5029898a847ed45a4fc0aff616fcaf1f"
let kGAIAppVersion  = "kGAIAppVersion"

let kPushMessageTypeForKapChat = "KapChat"
let kSchemesIdentifier = "netmeds"

// MARK:- All Text
//MARK:- LogIn Page
let kEnterMobileNoText = "Please enter your Mobile Number"
let kEnterValidMobileNoText = "Please enter a valid Mobile Number"
let kGoogleText = "Google"
let kFaceBookText = "Facebook"
let kORText = "OR"
let kPhoneNumberText = "PHONE NUMBER"
let kPermissionDeniedText = "Permission Denied"

//MARK:- LogIn With Password Page
let kLogInText = "LOG IN"
let kLogInUsingOTPText = "LOG IN USING OTP"
let kPasswordText = "PASSWORD"
let kChangeText = "Change"
let kForgotPasswordText = "Forgot?"
let kEnterPasswordText = "Please enter your Password!"

//MARK:- LogIn With OTP Page
let kVerifyText = "VERIFY"
let kVerifySubTitle = "Please verify your number by entering the OTP sent to you."
let kSignInText = "Sign in"
let kSignInSubTitle = "Sign up or Sign in to access your orders, special offers, health tips and more!"
let kLogInUsingPasswordText = "LOG IN USING PASSWORD"
let kEnterOTPText = "Please enter OTP!"

// MARK:- Change Password Page
let kPasswordWarningText = "Password must be alphanumeric and at least 8 characters!"
let kPwdAndConfirmPwdNotMatchWarningText = "Password and confirm password should be same"

// MARK:- SignUp Page
let kPasswordTooShortText = "\"New Password\" too short. At least 8 characters expected"
let kEnterEmailIDText = "Please enter your E-mail ID!"
let kEnterValidEmailIDText = "Please enter a valid E-mail ID!"
let kEnterFirstNameText = "Please enter your FirstName!"
let kEnterValidFirstNameText = "Please enter a valid FirstName!"
let kEnterLastNameText = "Please enter your LastName!"
let kEnterValidLastNameText = "Please enter a valid LastName!"

// MARK:- Product Sort Page
// Algolia Index For Sort
var kAlgoliaSortPopularitySearchIndex = ""
var kAlgoliaSortDiscountSearchIndex = ""
var kAlgoliaSortPriceLowToHighSearchIndex = ""
var kAlgoliaSortPriceHighToLowSearchIndex = ""
var kAlgoliaSortNameAToZSearchIndex = ""
var kAlgoliaSortNameZToASearchIndex = ""

// MARK:- Cart Page
//Error Message For Promo Code
let kPromoCodeEnterValidText = "Please enter your Promo Code!"
let kPromoCodeAppliedSuccessText = "Promo code applied successfully"
let kPromoRemovedSuccessText = "Promo code removed successfully"
let kSupercashAppliedSuccessText = "NMS Supercash applied successfully"
let kPromoCodeDefaultDescriptionText = "Get flat discount! Vouchers applicable in payment options."
let kEmptyCartMessageText = "Your Cart is empty!"
let kEmptyCartDescText = "You have no items added in the cart.\nExplore and add products you like!"
let kEmptyCartBtnText = "GO TO SEARCH"
let kEmptyCartImageName = "group_30"

// MARK:- Track Order Details Page
let kDeliveredKey = "Delivered"
let kCancelledKey = "Cancelled"
let kReturnedKey = "Returned"
let kDeclinedKey = "Declined"
let kInTransitKey = "Intransit"

//"SkipKey" = "SKIP" ;
//"ContinueKey" ="CONTINUE";
//
////Login
//
//
//"PasswordWarningKey" = "Password must be alphanumeric and at least 8 characters!";
//"PwdAndConfirmPwdNotMatchWarningKey" = "Password and confirm password should be same";
//
//"SignUpButtonKey" = "SIGN UP";
//"ResetButtonKey" = "RESET" ;
//
//
//"UploadPrescriptionKey" = "UPLOAD PRESCRIPTION";

//API ENDPOINTS
struct APIEndPointConstants {
    //MARK:- BASEURL
    
    #if DEVELOPMEN
    //static let custom_Api_Base_URL = "https://s1-meds.netmeds.com/" Exist
    static let baseURL = "https://d1-mstar.netmeds.com/mst/rest/v1/"
    static let customAPIBaseURL = "https://d1-apiv2.netmeds.com/"
    static let diagnosticsJustPayBaseURL = "https://d1-apiv2.netmeds.com//"
    static let consultationPaymentBaseURL = "https://d1-consult.netmeds.com/"
    static let brainSinsBaseURL = "https://api.brainsins.com/RecSinsAPI/api/recomm/"

    #elseif STAGING
    static let baseURL = "https://s1-apiv2.netmeds.com/mst/rest/v1/"
    static let customAPIBaseURL = "https://s1-apiv2.netmeds.com/" //New
    static let diagnosticsJustPayBaseURL = "https://s1-apiv2.netmeds.com/"
    static let consultationPaymentBaseURL = "https://s1-consult.netmeds.com/"
    static let brainSinsBaseURL = "https://api.brainsins.com/RecSinsAPI/api/recomm/"

    #else //Live
    static let baseURL = "https://apiv2.netmeds.com/mst/rest/v1/"
    static let customAPIBaseURL = "https://apiv2.netmeds.com/" //New
    static let diagnosticsJustPayBaseURL = "https://apiv2.netmeds.com/"
    static let consultationPaymentBaseURL = "https://consult.netmeds.com/"
    static let brainSinsBaseURL = "https://api.brainsins.com/RecSinsAPI/api/recomm/"
    #endif
    
}

#if DEVELOPMENT
let kBaseURL = "https://s1-apiv2.netmeds.com/" //New
var kAlgoliaSearchIndex = "s1india_nmsappstore_products"
var kAlgolia_Diagnostics_Index = "stage_diagnosis_index_v3"
var kAlgolia_DiagnosticsSearch_Index = "stage_diagnosis_index_v3"
let CFFaqUrl = "https://s1-apiv2.netmeds.com/pub/faq.php"
let kConsultationBaseURL = "https://s1-consult.netmeds.com"
let kNMHealthExpertURL = "https://s1-apiv2.netmeds.com/health-library"
let kDiagnosticsBaseURL = "https://s1-labs.netmeds.com/"

#elseif STAGING
let kBaseURL = "https://s1-apiv2.netmeds.com/" //New
var kAlgoliaSearchIndex = "s1india_nmsappstore_products"
var kAlgolia_Diagnostics_Index = "stage_diagnosis_index_v3"
var kAlgolia_DiagnosticsSearch_Index = "stage_diagnosis_index_v3"
let CFFaqUrl = "https://s1-apiv2.netmeds.com/pub/faq.php"
let kConsultationBaseURL = "https://s1-consult.netmeds.com"
let kNMHealthExpertURL = "https://s1-apiv2.netmeds.com/health-library"
let kDiagnosticsBaseURL = "https://s1-labs.netmeds.com/"
#else

let kBaseURL = "https://apiv2.netmeds.com/" //New
var kAlgoliaSearchIndex = "nmsp01_nmsappstore_products"
var kAlgolia_Diagnostics_Index = "prod_diagnosis_index_v3"//"stage_diagnosis_index"
var kAlgolia_DiagnosticsSearch_Index = "prod_diagnosis_index_v3"
//let CFPrivacyPolicyUrl = "https://apiv2.netmeds.com/pub/privacy.php"
//let CFTermsAndConditionUrl = "https://apiv2.netmeds.com/pub/terms.php"
let CFFaqUrl = "https://apiv2.netmeds.com/pub/faq.php"
let kConsultationBaseURL = "https://consult.netmeds.com"
let kNMHealthExpertURL = "https://apiv2.netmeds.com/health-library"
let kDiagnosticsBaseURL = "https://labs.netmeds.com/" // change this to actual URL when its goes to production
#endif

let kAppDetailsURL = "http://itunes.apple.com/lookup?bundleId="
let kDownloadPrescriptionsEndPoint = "\(APIEndPointConstants.baseURL)cart/download_prescription/"
