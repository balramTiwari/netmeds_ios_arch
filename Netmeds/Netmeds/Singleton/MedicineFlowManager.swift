//
//  MedicineFlowManager.swift
//  Netmeds
//
//  Created by SANKARLAL on 07/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

enum MedicineFlow {
    case universalCart
    case methodTwo
    case subscription
    case subscriptionEditOrder
}

class MedicineFlowManager: NSObject {
    static let shared = MedicineFlowManager()
    var flow: MedicineFlow = .universalCart
    var cartID: Int = 0
}
