//
//  UserManager.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class UserManager: NSObject {
    static let shared = UserManager()
    var userDetails: CustomerDetailsModel?
}

extension UserManager {
    func updateUserDetails() {
        guard let userDetailsInfo = CoreDataManager.shared.fetchAllAttributes(forEntityName: DBUserDetails.entityName()) as? [DBUserDetails], userDetailsInfo.count > 0 else { return }
        guard let response = userDetailsInfo.first?.response, let userInfo = CustomerDetailsModel.decode(json: response as AnyObject) else { return }
        self.userDetails = userInfo
    }

    func resetUserDetails(userInfo: CustomerDetailsModel) {
        CoreDataManager.shared.deleteAll(forEntityName: DBUserDetails.entityName())
        guard let encodeData = userInfo.encode(), let jSONResponse = encodeData.jsonObjectWithData() else { return }
        CoreDataManager.shared.insertObject(forEntityName: DBUserDetails.entityName(), attributes: ["response": jSONResponse])
        self.updateUserDetails()
    }
}
