//
//  ConfigManager.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class ConfigManager: NSObject {
    static let shared = ConfigManager()
    var configDetails: ConfigDetailsModel?
    var configURLPaths: ConfigURLPathsResultModel?
}

extension ConfigManager {
    func updateConfigDetails() {
        guard let appConfigDetails = CoreDataManager.shared.fetchAllAttributes(forEntityName: DBAppConfiguration.entityName()) as? [DBAppConfiguration], appConfigDetails.count > 0 else { return }
        guard let response = appConfigDetails.first?.response, let configInfo = ConfigDetailsModel.decode(json: response as AnyObject) else { return }
        self.configDetails = configInfo
        self.updateAlgoliaIndexValue(configInfo: configInfo)
    }

    func resetConfigDetails(configInfo: ConfigDetailsModel) {
        CoreDataManager.shared.deleteAll(forEntityName: DBAppConfiguration.entityName())
        guard let encodeData = configInfo.encode(), let jSONResponse = encodeData.jsonObjectWithData() else { return }
        CoreDataManager.shared.insertObject(forEntityName: DBAppConfiguration.entityName(), attributes: ["response": jSONResponse])
        self.updateConfigDetails()
    }
    
    private func updateAlgoliaIndexValue(configInfo: ConfigDetailsModel) {
        guard let algMedIndex = configInfo.algolia?.medIndex, algMedIndex.isValidString else { return }
        kAlgoliaSortPopularitySearchIndex = algMedIndex + "_products_popularity"
        kAlgoliaSortDiscountSearchIndex = algMedIndex + "_products_discount"
        kAlgoliaSortPriceLowToHighSearchIndex = algMedIndex + "_products_price_asc"
        kAlgoliaSortPriceHighToLowSearchIndex = algMedIndex + "_products_price_desc"
        kAlgoliaSortNameAToZSearchIndex = algMedIndex + "_products_name_asc"
        kAlgoliaSortNameZToASearchIndex = algMedIndex + "_products_name_desc"
    }
}
