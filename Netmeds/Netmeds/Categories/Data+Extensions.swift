//
//  Data+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 13/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

extension Data {
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
    
    func utf8Encoding() -> String? {
        return String(data: self, encoding: .utf8)
    }
}
