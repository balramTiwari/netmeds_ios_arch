//
//  ViewController+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import UIKit

enum LinkType: String {
    case offers = "offers"
    case custom = "custom"
    case webview = "webview"
    case netmedsWebview = "netmeds_webview"
    case externalURL = "external_url"
    case consultation = "consultation_history"
    case referEarn = "refer_earn"
    case productDetail = "product_detail"
    case manufacturer = "manufacturer"
    case search = "search"
    case diagnostic = "diagnostic_home"
    case prime = "primeMemberShipActivity"
    case subscription = "Subscription"
    case otc = "otc"
    case prescription = "prescription"
    case viewOfferDetails = "view_offer_details"
    case otcCategoryList = "otc_category_list"
    case specialOffers = "special-offers"
    case wellness = "wellness"
    case wellnessHome = "wellness_home"
}

extension UIViewController {
    class func instantiate<T: UIViewController>(storyboardName: StoryBoardNameType) -> T {
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        let identifier = String(describing: self)
        let viewCtrl = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        viewCtrl.hidesBottomBarWhenPushed = true
        return viewCtrl
    }
}

extension UIViewController {
    @discardableResult func bannerRedirection(linkType: String, pageID: String = "", url: String) -> Bool {
        switch linkType {
        case LinkType.offers.rawValue:
            navigateToOffersDetailVC(pageID: pageID)
        case LinkType.webview.rawValue, LinkType.netmedsWebview.rawValue:
            navigateToWebVC(title: "Netmeds", urlString: url)
        case LinkType.custom.rawValue:
            return parseCustomLinkType(url: url)
        default:
            break
        }
        return true
    }
    
    @discardableResult func parseCustomLinkType(url: String) -> Bool {
        guard url.isValidString else { return false }
        let separater = url.components(separatedBy: "/")
        if separater.contains(LinkType.netmedsWebview.rawValue) {
            navigateToWebVC(title: "Netmeds", urlString: "\(kBaseURL)health-library/post/\(separater.last ?? "")")
        } else if separater.contains(LinkType.externalURL.rawValue) {
            openExternalURL(url: url)
        } else if separater.contains(LinkType.offers.rawValue) {
            navigateToAllOffersVC()
        } else if separater.contains(LinkType.viewOfferDetails.rawValue) {
            navigateToOffersDetailVC(pageID: separater.last ?? "")
        } else if separater.contains(LinkType.otcCategoryList.rawValue) {
            let productID = Int(separater.last ?? "0") ?? 0
            navigateToProductListVC(productID: productID, isCategoryProduct: false)
        } else if separater.contains(LinkType.consultation.rawValue) {
        } else if separater.contains(LinkType.diagnostic.rawValue) {
        } else if separater.contains(LinkType.subscription.rawValue) {
        } else if separater.contains(LinkType.netmedsWebview.rawValue) {
            navigateToWebVC(title: "Netmeds", urlString: "\(kBaseURL)health-library/post/\(separater.last ?? "")")
        } else if separater.contains(LinkType.productDetail.rawValue) {
        } else if separater.contains(LinkType.referEarn.rawValue) {
        } else if separater.contains(LinkType.prime.rawValue) {
        } else if separater.contains(LinkType.search.rawValue) {
            navigateToSearchVC(searchText: separater.last ?? "")
        } else if separater.contains(LinkType.manufacturer.rawValue) {
            let productID = Int(separater[separater.count - 2]) ?? 0
            navigateToProductListVC(productID: productID, isCategoryProduct: false)
//        } else if separater.contains(LinkType.otc.rawValue) {
//            let productID = Int(separater[separater.count - 2]) ?? 0
//            navigateToProductListVC(productID: productID, isCategoryProduct: false)
        } else if separater.contains(LinkType.prescription.rawValue) {
            let productID = Int(separater[separater.count - 2]) ?? 0
            navigateToProductListVC(productID: productID, isCategoryProduct: false)
            } else if separater.contains(LinkType.specialOffers.rawValue) {
                navigateToWebVC(title: "Netmeds", urlString: url)
        } else if filterCategoryBy(separater: separater) {
            let productID = Int(separater[separater.count - 2]) ?? 0
            navigateToProductListVC(productID: productID)
        } else {
            // DEEP LINK INTEGRATION
            return false
        }
        return true
    }
    
    private func filterCategoryBy(separater: [String]) -> Bool {
        let predicate = NSPredicate(format: "SELF = 'otc_productlist' OR SELF = 'drug_list' OR SELF = 'manufacturer'")
        let filter = (separater as NSArray).filtered(using: predicate)
        if filter.count > 0 {
            return true
        }
        return false
    }
            
    private func openExternalURL(url: String) {
        if let range = url.range(of: "external_url/") {
            let weburl = url[range.upperBound...]
            if let url = URL(string: "https://\(weburl)") {
                UIApplication.shared.open(url)
            }
        }
    }
}

//MARK:- App Navigation Handlers
extension UIViewController {
    func setHomeRootVC() {
        guard let appDelegate  = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.setHomeRootViewController()
    }

    func popViewController(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func popToRootViewController(animated: Bool = true) {
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    func navigateToSignUpVC(randomKey key: String) {
        guard let vc = SignUpViewController.instantiate(storyboardName: .logIn) as? SignUpViewController else { return }
        vc.randomKey = key
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToVerifyPhoneNumberVC(socialLogInInfo: SocialLogInInfoViewModel) {
        guard let vc = VerifyPhoneNumberViewController.instantiate(storyboardName: .logIn) as? VerifyPhoneNumberViewController else { return }
        vc.socialLogInInfo = socialLogInInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToLoginWithPasswordVC(phoneNo: String = "") {
        guard let vc = LoginWithPasswordViewController.instantiate(storyboardName: .logIn) as? LoginWithPasswordViewController else { return }
        vc.phoneNo = phoneNo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToLoginWithOTPVC(randomKey key: String, isFromVerifyPhoneNumber: Bool = false, socialLogInInfo: SocialLogInInfoViewModel = SocialLogInInfoViewModel()) {
        guard let vc = LoginWithOTPViewController.instantiate(storyboardName: .logIn) as? LoginWithOTPViewController else { return }
        vc.randomKey = key
        vc.isFromVerifyPhoneNumber = isFromVerifyPhoneNumber
        vc.socialLogInInfo = socialLogInInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToChangePasswordVC(randomKey key: String) {
        guard let vc = ChangePasswordViewController.instantiate(storyboardName: .logIn) as? ChangePasswordViewController else { return }
        vc.randomKey = key
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToOffersDetailVC(pageID: String) {
        guard pageID.isValidString else { return }
        guard let vc = OffersDetailViewController.instantiate(storyboardName: .allOffers) as? OffersDetailViewController else { return }
        vc.pageID = pageID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToWebVC(title: String, urlString: String) {
        guard let vc = WebViewController.instantiate(storyboardName: .allOffers) as? WebViewController else { return }
        vc.titleString = title
        vc.urlString = urlString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToProductListVC(productID: Int, isCategoryProduct: Bool = true, title: String = "") {
        guard let vc = ProductListViewController.instantiate(storyboardName: .products) as? ProductListViewController else { return }
        vc.productID = productID
        vc.isCategoryProduct = isCategoryProduct
        vc.titleString = title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAddToCartVC(productID: Int) {
        self.view.endEditing(true)
        guard let vc = AddToCartViewController.instantiate(storyboardName: .search) as? AddToCartViewController else { return }
        vc.productID = productID
        vc.modalPresentationStyle = .overFullScreen
        vc.actionAddCartHandle {
//            self.addSuccessViewCart()
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    func navigateToAllOffersVC() {
         guard let vc = AllOffersViewController.instantiate(storyboardName: .allOffers) as? AllOffersViewController else { return }
         self.navigationController?.pushViewController(vc, animated: true)
     }
    
    func navigateToSearchVC(searchText: String = "") {
        guard let vc = SearchViewController.instantiate(storyboardName: .search) as? SearchViewController else { return }
        vc.searchText = searchText
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToCartVC() {
        guard let vc = CartViewController.instantiate(storyboardName: .cart) as? CartViewController else { return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigatToOrdersDetailVC(orderID: String) {
        guard let vc = MyOrdersDetailViewController.instantiate(storyboardName: .myOrders) as? MyOrdersDetailViewController else { return }
        vc.orderID = orderID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigatToTrackOrderVC(orderID: String) {
        guard let vc = TrackOrderViewController.instantiate(storyboardName: .myOrders) as? TrackOrderViewController else { return }
        vc.orderID = orderID
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToOfferListVC(offers: [AllOffersModel], title: String) {
        guard let vc = OfferListViewController.instantiate(storyboardName: .allOffers) as? OfferListViewController else { return }
        vc.offerListModel = offers
        vc.offerName = title
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToOffersDetailVC(offers: AllOffersModel) {
        guard let vc = OffersDetailViewController.instantiate(storyboardName: .allOffers) as? OffersDetailViewController else { return }
        vc.offersDetailsModel = offers
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSelectAddressVC(shippingAddressID: Int) {
        guard let vc = SelectAddressViewController.instantiate(storyboardName: .cart) as? SelectAddressViewController else { return }
        vc.shippingAddressID = shippingAddressID
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToOrderReviewVC(selectedAddress: AllAddressInfoModel? = nil) {
        guard let vc = OrderReviewViewController.instantiate(storyboardName: .cart) as? OrderReviewViewController else { return }
        vc.selectedAddress = selectedAddress
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToProductDetailsVC(productCode: Int = 0) {
        guard let vc = ProductDetailsViewController.instantiate(storyboardName: .products) as? ProductDetailsViewController else { return }
        vc.productCode = productCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSellerAddressPopUpVC(addressInfo: (title: String, address: String)) {
        guard let vc = SellerAddressPopUpViewController.instantiate(storyboardName: .cart) as? SellerAddressPopUpViewController else { return }
        vc.addressInfo = addressInfo
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func navigateToPaymentGateWayVC(cartID: String, orderValue: String) {
        guard let vc = PaymentGateWayViewController.instantiate(storyboardName: .paymentGateWay) as? PaymentGateWayViewController else { return }
        vc.cartID = cartID
        vc.orderValue = orderValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAllNetBankingVC() {
        guard let vc = AllNetBankingViewController.instantiate(storyboardName: .paymentGateWay) as? AllNetBankingViewController else { return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToUploadPrescriptionsVC() {
        guard let vc = UploadPrescriptionsViewController.instantiate(storyboardName: .uploadPrescriptions) as? UploadPrescriptionsViewController else { return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
