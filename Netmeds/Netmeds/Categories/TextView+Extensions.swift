//
//  TextView+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    func handleTermsAndServiceTextView(termsAndConditionUrl: String) {
        let attributedString = NSMutableAttributedString(string:  kTermsAndPolicyText1)
        attributedString.addAttribute(NSAttributedString.Key.link,
                                      value: termsAndConditionUrl,
                                      range: (kTermsAndPolicyText1 as NSString).range(of: "Terms of service")
        )
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor(with: 0.6)],
                                       range: NSRange(location: 0, length: kTermsAndPolicyText1.count))
        self.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mediumPinkTextColor()]
        self.attributedText = attributedString
        self.textAlignment = .center
    }
    
    
    func handlePrivacyPolicyTextView(privacyPolicyUrl: String) {
        let attributedString = NSMutableAttributedString(string:  kTermsAndPolicyText2)
        attributedString.addAttribute(NSAttributedString.Key.link,
                                      value: privacyPolicyUrl,
                                      range: (kTermsAndPolicyText2 as NSString).range(of: "Privacy & Legal Policy")
        )
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor(with: 0.6)],
                                       range: NSRange(location: 0, length: kTermsAndPolicyText2.count))
        self.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mediumPinkTextColor()]
        self.attributedText = attributedString
        self.textAlignment = .center
    }
}
