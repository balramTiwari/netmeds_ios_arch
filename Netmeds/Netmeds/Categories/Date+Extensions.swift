//
//  Date+Extensions.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

extension Date {
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    
    func dateToString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "dd MMM yyyy"
        return dateFormatter.string(from: self)
    }
}
