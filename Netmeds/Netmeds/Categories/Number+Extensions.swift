//
//  Number+Extensions.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

extension NSNumber {
    
    func formatFractionDigits() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        let result = formatter.string(from: self)
        return result!
    }
    
    func formatDigits() -> NSNumber {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        let result = formatter.number(from: self.stringValue)
        return result!
    }
}
