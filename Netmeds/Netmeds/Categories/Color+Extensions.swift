//
//  Color+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func getColorFromHexValue(hex: String?, alpha: Float?) -> UIColor {
        guard let hex = hex else {return .NMSTextColor()}
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return .NMSTextColor()
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha ?? 1.0)
        )
    }
    
    static func NMSTextColor(with alpha: CGFloat) -> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: alpha)
    }
    
    static func NMSTextColor() -> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 0.6)
    }
    
    static func mediumPinkTextColor() -> UIColor {
        return UIColor(red: 239/255.0, green: 66/255.0, blue: 129/255.0, alpha: 1.0) /* #ef4281 */
    }
    
    static func warningRedColor(with alpha: CGFloat)-> UIColor {
        return UIColor(red: 226.0/255.0, green: 57.0/255.0, blue: 17.0/255.0, alpha: alpha)
    }
    
    static func separatorViewColor()-> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 0.04)
    }
    
    static func netmedsGreenColor()-> UIColor {
        return UIColor(red: 2/255, green: 183/255, blue: 194/255, alpha: 1.0)
    }
    
    static func NMLogoGreenColor() -> UIColor {
        return UIColor(red: 36/255.0, green: 174/255.0, blue: 177/255.0, alpha: 1.0) // 24aeb1
    }
    
    static func NMorderStatusGreenColor() -> UIColor {
        return UIColor(red: 132/255.0, green: 190/255.0, blue: 82/255.0, alpha: 1.0)
    }
    
    static func NMorderStatusYellowColor() -> UIColor {
        return UIColor(red: 245/255.0, green: 166/255.0, blue: 35/255.0, alpha: 1.0)
    }
    
    static func borderViewColor()-> UIColor {
        return UIColor(red: 246/255, green: 246/255, blue: 247/255, alpha: 1.0)
    }
    
    static func addNoteBorderColor()-> UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
    }
    
    static func lastCellGreenColor()-> UIColor {
        return UIColor(red: 132/255, green: 190/255, blue: 82/255, alpha: 0.1)
    }
    
    static func darkishGreenTextColor() -> UIColor {
        return UIColor(red: 55/255, green: 143/255, blue: 48/255, alpha: 1.0) //378f30
    }
    
    static func btndisableAshColor()-> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 0.38)
    }
    
    static func lineAshColor()-> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 0.0399999991059303)
    }
    
    
    
    static func btnEnableNetmedsGreenColor()-> UIColor {
        return UIColor(red: 36/255.0, green: 174/255.0, blue: 177/255.0, alpha: 1.0)
    }
    
    static func turtleGreenColor() -> UIColor {
        return UIColor(red: 132/255.0, green: 190/255.0, blue: 82/255.0, alpha: 1.0) // 84be52
    }
    
    static func walletTransactionRedColor() -> UIColor {
        return UIColor(red: 226/255.0, green: 57/255.0, blue: 17/255.0, alpha: 1.0) // e23911
    }
    
    static func imageBorderColorForWellnessAllCategories() -> UIColor {
        return UIColor(red: 237/255.0, green: 238/255.0, blue: 240/255.0, alpha: 1.0) //edeef0
    }
    
    static func imageBorderColorForWellnessSelectSortAllCategories() -> UIColor {
        return UIColor(red: 36/255.0, green: 174/255.0, blue: 177/255.0, alpha: 1.0) //edeef0
    }
    
    static func referAndEarnBorderColor() -> UIColor {
        return UIColor(red: 237/255.0, green: 238/255.0, blue: 240/255.0, alpha: 1.0) //edeef0
    }
    
    static func orderConfirmationMethodTwoGreenColor() -> UIColor {
        return UIColor(red: 36/255.0, green: 174/255.0, blue: 177/255.0, alpha: 1.0)
    }
    
    static func orderConfirmationMethodTwoAsaColor() -> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 0.4)
    }
    
    static func paymentGateWayFooterView() -> UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 249/255, alpha: 1.0)
    }
    
    static func lightBlueGrey() -> UIColor {
        return  UIColor(red: 111/255.0, green: 114/255.0, blue: 130/255.0, alpha: 1.0) //dff6f6
    }
    
    static func orderConfirmationMethodTwoAsaColorPure() -> UIColor {
        return UIColor(red: 21/255, green: 27/255, blue: 57/255, alpha: 1.0)
    }
    
    static func lightGreenBgColor() -> UIColor {
        return #colorLiteral(red: 0.8980392157, green: 0.9529411765, blue: 0.862745098, alpha: 1)
    }
    
    static func textGreenColor() -> UIColor {
        return #colorLiteral(red: 0.5647058824, green: 0.7254901961, blue: 0.537254902, alpha: 1)
    }
    
    static func textBlueColor() -> UIColor {
        return #colorLiteral(red: 0.007843137255, green: 0.7176470588, blue: 0.7607843137, alpha: 1)
    }
}

