//
//  Double+Extensions.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

extension Double {
    var withTwoDecimalValue:String {
        return String(format: "₹ %.2f", self)
    }
    
    func convertDoubleToPercentage() -> String? {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 0
        return formatter.string(for: self)
    }
}
