//
//  View+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static var nameOfClass: String {
        return String(describing: self)
    }
    
    var nameOfClass: String {
        return String(describing: type(of: self))
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.frame = self.bounds
        self.layer.mask = mask
    }
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
    }
}

//MARK:- App Link Banner Redirection
extension UIView {
    func appLinkBannerRedirection(linkType: String, pageID: String = "", url: String) {
        guard let topViewCtrl = UIApplication.topViewController() else { return }
        switch linkType {
        case LinkType.offers.rawValue:
            topViewCtrl.navigateToOffersDetailVC(pageID: pageID)
        case LinkType.webview.rawValue, LinkType.netmedsWebview.rawValue:
            topViewCtrl.navigateToWebVC(title: "Netmeds", urlString: url)
        case LinkType.custom.rawValue:
             topViewCtrl.parseCustomLinkType(url: url)
        default: break
        }
    }
}
