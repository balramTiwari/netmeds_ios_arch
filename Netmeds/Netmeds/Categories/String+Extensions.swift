//
//  String+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import CryptoSwift

let EMAIL_CHARACTERS : String = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
// at least one Lower / Upper / Special Char,
// at least one digit
// 8 characters total
let PASSOWRD_CHARACTERS: String = "^(?=.*\\d)(?=.*[A-Za-z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,30}$"

extension String {
    
    var addingPercentEncoding: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
    
    var isValidString: Bool {
        return (self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)).count > 0 ? true: false
    }
    
    var isValidEmail: Bool {
        return (NSPredicate(format: "SELF MATCHES %@", EMAIL_CHARACTERS)).evaluate(with: self)
    }
    
    var isValidPhoneNumber: Bool {
        return (self.count == kPhoneNumberCharLength) ? true : false
    }
    
    var isValidPassword: Bool {
        return (NSPredicate(format: "SELF MATCHES %@", PASSOWRD_CHARACTERS)).evaluate(with: self)
    }
    
    var isValidName: Bool {
        let value = self.replacingOccurrences(of: " ", with: "")
        return (value.count >= 2 && value.count <= 30 && !value.trimmingCharacters(in: .whitespaces).isEmpty) ? true : false
    }
    
    func limitCharactersLength(maxLength: Int = kPhoneNumberCharLength, range: NSRange, replacement: String) -> Bool {
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: self) else { return false }
        // add their new text to the existing text
        let updatedText = self.replacingCharacters(in: stringRange, with: replacement)
        // make sure the result is under 16 characters
        return updatedText.count <= maxLength
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func decodeKey() -> String {
        if let decry = try? self.aesDecrypt(key: kAESKey, iv: kAESIV) {
            return decry
        }
        return ""
    }
    
    private func aesDecrypt(key: String, iv: String) throws -> String {
        guard let data = Data(base64Encoded: self) else { return "" }
        let decrypted = try AES(key: key, iv: iv, padding: .pkcs7).decrypt([UInt8](data))
        return String(bytes: Data(decrypted).bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    func setPriceWithMRPText() -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let fontSize :[NSAttributedString.Key : Any] = [ NSAttributedString.Key.font: UIFont(name: "Lato-Bold", size: 10) as Any ]
        mutableAttributedString.append(NSAttributedString(string: "MRP ", attributes: fontSize))
        mutableAttributedString.append(NSAttributedString(string: "₹ \(self)"))
        return mutableAttributedString
    }
    
    func strikeEffect() -> NSMutableAttributedString {
        let strokeEffect: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.strikethroughColor: UIColor.NMSTextColor()
        ]
        let value = NSMutableAttributedString()
        let fontSize :[NSAttributedString.Key : Any] = [ NSAttributedString.Key.font: UIFont(name: "Lato-Bold", size: 10) as Any ]
        value.append(NSAttributedString(string: "MRP ", attributes: fontSize))
        value.append(NSAttributedString(string: "₹ \(self)", attributes: strokeEffect))
        return value
    }
    
    func replace(string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func dateString() -> String {
        guard let date = self.stringToDate() else { return "" }
        guard let dateString = date.dateToString() else { return "" }
        return dateString
    }
    
    func getExpiryDate()  -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"    // "2020-08-31"
        guard let date = dateFormatter.date(from: self) else { return "" }
        dateFormatter.dateFormat = "MMM yyyy" // Apr 2018
        return dateFormatter.string(from: date)
    }
    
    func stringToDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = dateFormat
        return dateFormatter.date(from: self)
    }
    
    func changeAttribute(boldText: String, regularFont: UIFont = UIFont(name: "Lato-Regular", size: 13)!, boldFont: UIFont = UIFont(name: "Lato-Bold", size: 13)!) -> NSMutableAttributedString {
        let mutableString = NSMutableAttributedString(string: self)
        let boldRange = (self as NSString).range(of: boldText)
        let regularRange = (self as NSString).range(of: self)
        mutableString.addAttribute(NSAttributedString.Key.font, value: regularFont, range: regularRange)
        mutableString.addAttribute(NSAttributedString.Key.font, value: boldFont, range: boldRange)
//        mutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Lato-Regular", size: 13) as Any, range: regularRange)
//        mutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Lato-Bold", size: 13) as Any, range: boldRange)
        return mutableString
    }
    
    func addStrikeStyle(strikeText: String) -> NSMutableAttributedString? {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        let strikeTextRange = (self as NSString).range(of: strikeText)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: strikeTextRange)
        return attributeString
    }

    func makeStrikePrice(strArray: [String]) -> NSMutableAttributedString {
        let combination = NSMutableAttributedString()
        if strArray.count == 2 {
            let attributeString1: NSMutableAttributedString =  NSMutableAttributedString(string: strArray[0])
            let attributeString2: NSMutableAttributedString =  NSMutableAttributedString(string: strArray[1])
            attributeString1.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString1.length))
            attributeString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.NMSTextColor() , range: NSMakeRange(0, attributeString1.length))
            combination.append(attributeString1)
            combination.append(attributeString2)
        }
        return combination
    }

    func changeAttribute(colourText: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        let colourTextRange = (self as NSString).range(of: colourText)
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: colourTextRange)
        attributeString.addAttribute(NSAttributedString.Key.font, value: font, range: colourTextRange)
        return attributeString
    }
    
    func getDateAndTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // 2019-09-14T15:58:10
        guard let date = dateFormatter.date(from: self) else {return ""}
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a" // Apr 01, 2019 03:56 AM
        return dateFormatter.string(from: date)
    }
    
    func getLocalizedDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: self) else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            if let date = dateFormatter.date(from: self) {
                dateFormatter.dateFormat = "dd MMM, h:mm a"
                return dateFormatter.string(from: date)
            } else { return "" }
        }
        dateFormatter.dateFormat = "dd MMM, h:mm a" // 30 Nov, 6:12 PM
        return dateFormatter.string(from: date)
    }
    
    func deliveryDateFormat(toDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date1 = dateFormatter.date(from: self), let date2 = dateFormatter.date(from: toDate) {
            if date1.isInSameMonth(date: date2) {
                dateFormatter.dateFormat = "dd"
                let dateFromDate1 = dateFormatter.string(from: date1)
                dateFormatter.dateFormat = "dd-MMM"
                let dateAndMonthFromDate2 = dateFormatter.string(from: date2)
                return "\(dateFromDate1) to \(dateAndMonthFromDate2)"
            } else {
                dateFormatter.dateFormat = "dd-MMM"
                let dateFromDate1 = dateFormatter.string(from: date1)
                let dateAndMonthFromDate2 = dateFormatter.string(from: date2)
                return "\(dateFromDate1) to \(dateAndMonthFromDate2)"
            }
        }
        return ""
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
}
