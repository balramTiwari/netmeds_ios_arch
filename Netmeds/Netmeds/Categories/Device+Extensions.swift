//
//  Device+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import AVFoundation

extension UIDevice {
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}
