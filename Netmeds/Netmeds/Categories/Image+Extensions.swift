//
//  Image+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: quality.rawValue)
    }
}
