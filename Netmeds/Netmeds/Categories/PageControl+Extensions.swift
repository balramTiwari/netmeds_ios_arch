//
//  PageControl+Extensions.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import UIKit

extension UIPageControl {
    func customPageControl(dotWidth: CGFloat) {
        self.subviews.forEach { (dotView) in
            dotView.frame.size = CGSize.init(width: dotWidth, height: 2)
            dotView.layer.cornerRadius = 1
        }
    }
}
