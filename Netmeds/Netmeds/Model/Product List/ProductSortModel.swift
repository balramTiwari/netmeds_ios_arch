//
//  ProductSortModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class ProductSortModel {
    var index: String = ""
    var name: String = ""
    var isSelected: Bool = false
    init(index: String, name: String, isSelected: Bool = false) {
        self.index = index
        self.name = name
        self.isSelected = isSelected
    }
}
