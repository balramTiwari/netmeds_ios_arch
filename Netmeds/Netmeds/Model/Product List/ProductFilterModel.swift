//
//  ProductFilterModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class ProductFilterModel {
    var key: String = ""
    var name: String = ""
    var values: [ProductFilterValuesModel] = [ProductFilterValuesModel]()
}

class ProductFilterValuesModel {
    var key: String = ""
    var name: String = ""
    var isSelected: Bool = false
}
