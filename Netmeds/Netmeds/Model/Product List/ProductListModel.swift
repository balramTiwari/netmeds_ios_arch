//
//  ProductListModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APICategoryProductList: APIBaseRequestResponse {
    
    var categoryID: Int = 0
    var pageNo: Int = 0
    
    override func route() -> String {
        return "/catalog/category/get_details/\(categoryID)?return_products=Y&return_facets=Y&page_no=\(pageNo)&page_size=20&product_fieldset_name=all&sub_category_depth=2"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ProductListModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIManufacturerProductList: APIBaseRequestResponse {
    
    var manufacturerID: Int = 0
    var pageNo: Int = 0
    
    override func route() -> String {
        return "catalog/manufacturer/get_details/\(manufacturerID)?return_products=y&return_facets=N&page_no=\(pageNo)&page_size=20&product_fieldset_name=all"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ProductListModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APICategoryProductBanners: APIBaseRequestResponse {
    
    var categoryID: Int = 0
    
    override func route() -> String {
        return "api/v1/catalog/banners?type=category&catId=\(categoryID)&manufId="
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ProductListBannersModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIManufacturerProductBanners: APIBaseRequestResponse {
    
    var manufacturerID: Int = 0
    
    override func route() -> String {
        return "api/v1/catalog/banners?type=manufacture&catId=&manufId=\(manufacturerID)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ProductListBannersModel.decode(data: jsonResult.dataWithJSONObject())
    }
}


struct ProductListModel: Codable {
    var status: String?
    var result: ProductListResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(ProductListResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct ProductListResultModel: Codable {
    var categoryDetails: ProductListDetailsModel?
    var manufacturerDetails: ProductListDetailsModel?

    public enum CodingKeys: String, CodingKey {
        case categoryDetails = "category_details"
        case manufacturerDetails = "manufacturer_details"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            categoryDetails = (try? values.decode(ProductListDetailsModel.self, forKey: .categoryDetails)) ?? nil
            manufacturerDetails = (try? values.decode(ProductListDetailsModel.self, forKey: .manufacturerDetails)) ?? nil
        } catch {}
    }
}

struct ProductListDetailsModel: Codable {
    var breadCrumbs: [ProductBreadCrumbsModel]?
    var filters: FiltersModel?
    var level: Int?
    var mastheadIimagePath,name: String?
    var products: [ProductsModel]?
    var productCount: Int?
    var thumbnailImagePath, urlPath: String?
    var subCategories: [SubCategoriesModel]?

    public enum CodingKeys: String, CodingKey {
        case breadCrumbs = "bread_crumbs"
        case mastheadIimagePath = "masthead_image_path"
        case productCount = "product_count"
        case thumbnailImagePath = "thumbnail_image_path"
        case urlPath = "url_path"
        case subCategories = "sub_categories"
        case filters, level, name, products
        
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            breadCrumbs = (try? values.decode([ProductBreadCrumbsModel].self, forKey: .breadCrumbs)) ?? nil
            filters = (try? values.decode(FiltersModel.self, forKey: .filters)) ?? nil
            level = (try? values.decode(Int.self, forKey: .level)) ?? 0
            mastheadIimagePath = (try? values.decode(String.self, forKey: .mastheadIimagePath)) ?? ""
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            products = (try? values.decode([ProductsModel].self, forKey: .products)) ?? nil
            productCount = (try? values.decode(Int.self, forKey: .productCount)) ?? 0
            thumbnailImagePath = (try? values.decode(String.self, forKey: .thumbnailImagePath)) ?? ""
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            subCategories = (try? values.decode([SubCategoriesModel].self, forKey: .subCategories)) ?? nil
        } catch {}
    }
}

struct FiltersModel: Codable {
    var facets: FacetsModel?
    
    public enum CodingKeys: String, CodingKey {
        case facets
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            facets = (try? values.decode(FacetsModel.self, forKey: .facets)) ?? nil
        } catch {}
    }
}


struct FacetsModel: Codable {
    var availability: [AvailabilityModel]?
    var brand: [AvailabilityModel]?
    var manufacturer: [AvailabilityModel]?

    public enum CodingKeys: String, CodingKey {
        case availability,brand,manufacturer
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            availability = (try? values.decode([AvailabilityModel].self, forKey: .availability)) ?? nil
            brand = (try? values.decode([AvailabilityModel].self, forKey: .brand)) ?? nil
            manufacturer = (try? values.decode([AvailabilityModel].self, forKey: .manufacturer)) ?? nil
        } catch {}
    }
}

struct AvailabilityModel: Codable {
    var count, id: Int?
    var name,urlPath: String?

    public enum CodingKeys: String, CodingKey {
        case count, id, name
        case urlPath = "url_path"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            count = (try? values.decode(Int.self, forKey: .count)) ?? 0
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
        } catch {}
    }
}

struct SubCategoriesModel: Codable {
    var id: Int?
    var mastheadIimagePath, name: String?
    var productCount: Int?
    var thumbnailImagePath, urlPath: String?
    var maxDiscountPct: Int?
    var subCategories: [SubCategoriesModel]?

    public enum CodingKeys: String, CodingKey {
        case mastheadIimagePath = "masthead_image_path"
        case productCount = "product_count"
        case thumbnailImagePath = "thumbnail_image_path"
        case urlPath = "url_path"
        case maxDiscountPct = "max_discount_pct"
        case subCategories = "sub_categories"
        case id, name
        
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            mastheadIimagePath = (try? values.decode(String.self, forKey: .mastheadIimagePath)) ?? ""
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            productCount = (try? values.decode(Int.self, forKey: .productCount)) ?? 0
            thumbnailImagePath = (try? values.decode(String.self, forKey: .thumbnailImagePath)) ?? ""
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            maxDiscountPct = (try? values.decode(Int.self, forKey: .maxDiscountPct)) ?? 0
            subCategories = (try? values.decode([SubCategoriesModel].self, forKey: .subCategories)) ?? nil
        } catch {}
    }
}


struct ProductListBannersModel: Codable {
    var status: String?
    var result: ProductListBannersResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(ProductListBannersResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct ProductListBannersResultModel: Codable {
    var categoryBanner: [ProductListBannersInfoModel]?

    public enum CodingKeys: String, CodingKey {
        case categoryBanner
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            categoryBanner = (try? values.decode([ProductListBannersInfoModel].self, forKey: .categoryBanner)) ?? nil
        } catch {}
    }
}


struct ProductListBannersInfoModel: Codable {
    var name: String?
    var imageURL: String?
    var linkType: String?
    var url: String?

    public enum CodingKeys: String, CodingKey {
        case name,url
        case imageURL = "imageUrl"
        case linkType = "linktype"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            imageURL = (try? values.decode(String.self, forKey: .imageURL)) ?? ""
            linkType = (try? values.decode(String.self, forKey: .linkType)) ?? ""
            url = (try? values.decode(String.self, forKey: .url)) ?? ""
        } catch {}
    }
}
