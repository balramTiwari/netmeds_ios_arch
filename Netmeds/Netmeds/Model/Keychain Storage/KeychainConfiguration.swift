//
//  KeychainConfiguration.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

struct KeychainConfiguration {
    static let serviceName: String = Bundle.main.bundleIdentifier ?? "com.NetmedsMarketplace.Netmeds"
    static let accessGroup: String? = nil
    static let accountName: String = "NETMEDS_UUID_STRING"
}
