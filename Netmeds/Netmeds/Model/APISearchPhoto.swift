//
//  APISearchPhoto.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class APISearchPhoto: APIBaseRequestResponse {
   
    var searchText: String = ""
    
    override func route() -> String {
        return "services/rest/?method=flickr.photos.search&api_key=0f2d0ea339f5fe0a65f6c47b32268f1b&tags=\(searchText)&format=json&nojsoncallback=1"
    }
        
//    override func responseFrom(data: Data) -> Any? {
//        return SearchPhotoModel.decode(data: data)
//    }
}

class SearchPhotoModel: NSObject, Codable {
    var photos: PhotosModel?
    
    private enum CodingKeys: String, CodingKey {
        case photos = "photos"
    }
    
    required init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            photos = (try? values.decode(PhotosModel.self, forKey: .photos)) ?? nil
        } catch {
            
        }
    }
}

class PhotosModel: NSObject, Codable {
    var photo: [PhotoModel]?
    
    private enum CodingKeys: String, CodingKey {
        case photo = "photo"
    }
    
    required init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            photo = (try? values.decode([PhotoModel].self, forKey: .photo)) ?? [PhotoModel]()
        } catch {
            
        }
    }
}

class PhotoModel: NSObject, Codable {
    var title: String?
    var photoId: String?
    var farm: Int?
    var secret: String?
    var server: String?

    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case photoId = "id"
        case farm = "farm"
        case secret = "secret"
        case server = "server"
    }
    
    required init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            photoId = (try? values.decode(String.self, forKey: .photoId)) ?? ""
            farm = (try? values.decode(Int.self, forKey: .farm)) ?? 0
            secret = (try? values.decode(String.self, forKey: .secret)) ?? ""
            server = (try? values.decode(String.self, forKey: .server)) ?? ""

        } catch {
            
        }
    }
}
