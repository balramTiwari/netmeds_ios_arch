//
//  HomeModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 20/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APIHomeOffersBanner: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/homesection/mainbanner"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny, let homeBanners = result["homeBanners"] as? JSONArrayOfDictAny else { return nil }
        return [HomeOffersBannerModel].decode(data: homeBanners.dataWithJSONObject())
    }
}

class APIHomeBigSalesBanner: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/homesection/promotionalbanner"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny, let promotionBanners = result["promotionBanners"] as? JSONDictAny else { return nil }
        return HomeBigSalesBannerModel.decode(data: promotionBanners.dataWithJSONObject())
    }
}


class APIHomeWellnessBanner: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/homesection/wellnessproducts"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny else { return nil }
        return HomeWellnessAndHealthConcernModel.decode(data: result.dataWithJSONObject())
    }
}

class APIHomeCashBackBanner: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/offers/gatewayoffers"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny, let alloffers = result["alloffers"] as? JSONArrayOfDictAny else { return nil }
        return [HomeOffersBannerModel].decode(data: alloffers.dataWithJSONObject())
    }
}

class APIHomeHealthExpertsBanner: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/homesection/healtharticle"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny else { return nil }
        return HomeBigSalesBannerModel.decode(data: result.dataWithJSONObject())
    }
}

struct HomeOffersBannerModel: Codable {
    var title: String?
    var imageURL: String?
    var id: Int?
    var url: String?
    var linkType: String?
    var linkPage: String?
    var newPageID: String?
    var name: String?
    var image: String?
    var pageID: String?
    var couponDesc: String?
    
    private enum CodingKeys: String, CodingKey {
        case title, id, url, name, image
        case imageURL = "imageUrl"
        case linkType = "linktype"
        case linkPage = "linkpage"
        case newPageID = "newPageId"
        case pageID = "pageId"
        case couponDesc = "couponDescription"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            imageURL = (try? values.decode(String.self, forKey: .imageURL)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            url = (try? values.decode(String.self, forKey: .url)) ?? ""
            linkType = (try? values.decode(String.self, forKey: .linkType)) ?? ""
            linkPage = (try? values.decode(String.self, forKey: .linkPage)) ?? ""
            newPageID = (try? values.decode(String.self, forKey: .newPageID)) ?? ""
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            image = (try? values.decode(String.self, forKey: .image)) ?? ""
            pageID = (try? values.decode(String.self, forKey: .pageID)) ?? ""
            couponDesc = (try? values.decode(String.self, forKey: .couponDesc)) ?? ""
        } catch {}
    }
}

struct HomeBigSalesBannerModel: Codable {
    var header: String?
    var subHeader: String?
    var tagLine: String?
    var desc: String?
    var list: [HomeOffersBannerModel]?
    var articleList: [HomeOffersBannerModel]?

    private enum CodingKeys: String, CodingKey {
        case header, subHeader, list, articleList
        case tagLine = "tagline"
        case desc = "description"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            header = (try? values.decode(String.self, forKey: .header)) ?? ""
            subHeader = (try? values.decode(String.self, forKey: .subHeader)) ?? ""
            list = (try? values.decode([HomeOffersBannerModel].self, forKey: .list)) ?? nil
            tagLine = (try? values.decode(String.self, forKey: .tagLine)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
            articleList = (try? values.decode([HomeOffersBannerModel].self, forKey: .articleList)) ?? nil
        } catch {}
    }
}

struct HomeWellnessAndHealthConcernModel: Codable {
    var wellnessInfo: HomeBigSalesBannerModel?
    var healthConcernInfo: HomeBigSalesBannerModel?

    private enum CodingKeys: String, CodingKey {
        case wellnessInfo = "wellnessproducts"
        case healthConcernInfo = "healthConcernCategory"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            wellnessInfo = (try? values.decode(HomeBigSalesBannerModel.self, forKey: .wellnessInfo)) ?? nil
            healthConcernInfo = (try? values.decode(HomeBigSalesBannerModel.self, forKey: .healthConcernInfo)) ?? nil
        } catch {}
    }
}

