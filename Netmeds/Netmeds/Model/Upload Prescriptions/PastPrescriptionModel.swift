//
//  PastPrescriptionModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIPastPrescriptions: APIBaseRequestResponse {

    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/prescription/recentprescription"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }

    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return PastPrescriptionsModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct PastPrescriptionsModel: Codable {
    var status: String?
    var result: PastPrescriptionsResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(PastPrescriptionsResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct PastPrescriptionsResultModel: Codable {
    var digitizedPrescriptionCount: Int?
    var pastOneWeekUnDigitizedRxCount: Int?
    var pastOneWeekUnDigitizedRxList: PastOneWeekPrescriptionsModel?
    var digitizedPrescriptionList: PastOneWeekPrescriptionsModel?

    private enum CodingKeys: String, CodingKey {
        case digitizedPrescriptionCount, pastOneWeekUnDigitizedRxCount
        case pastOneWeekUnDigitizedRxList, digitizedPrescriptionList
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            digitizedPrescriptionCount = (try? values.decode(Int.self, forKey: .digitizedPrescriptionCount)) ?? 0
            pastOneWeekUnDigitizedRxCount = (try? values.decode(Int.self, forKey: .pastOneWeekUnDigitizedRxCount)) ?? 0
            pastOneWeekUnDigitizedRxList = (try? values.decode(PastOneWeekPrescriptionsModel.self, forKey: .pastOneWeekUnDigitizedRxList)) ?? nil
            digitizedPrescriptionList = (try? values.decode(PastOneWeekPrescriptionsModel.self, forKey: .digitizedPrescriptionList)) ?? nil
        } catch {}
    }
}

struct PastOneWeekPrescriptionsModel: Codable {
    var rxIDs: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case rxIDs = "rxId"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            rxIDs = (try? values.decode([String].self, forKey: .rxIDs)) ?? nil
        } catch {}
    }
}
