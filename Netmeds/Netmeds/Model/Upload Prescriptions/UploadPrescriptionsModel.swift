//
//  UploadPrescriptionsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIMethod2Carts: APIBaseRequestResponse {
    
    override func route() -> String {
        return "cart/get_method2_carts"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return Method2CartsModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIUploadSelectedPrescriptions: APIBaseRequestResponse {
    
    var params: [Any]?
    
    override func route() -> String {
        let prescriptions = "cart/upload_prescriptions"
        return MedicineFlowManager.shared.cartID == 0 ? "\(prescriptions)?new_method2_cart=y" : "\(prescriptions)?cart_id=\(MedicineFlowManager.shared.cartID)"
    }
    
    override func uploadPrescriptionParameters() -> [Any]? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CartModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIDetachPrescriptions: APIBaseRequestResponse {
    
    var detatchPrescriptions: String = ""
    
    override func route() -> String {
       return "cart/detach_prescriptions?cart_id=\(MedicineFlowManager.shared.cartID)\(detatchPrescriptions)"
    }
        
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct Method2CartsModel: Codable {
    var status: String?
    var result: [Int]?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode([Int].self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}
