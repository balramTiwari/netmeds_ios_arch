//
//  PaymentGateWayModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIPaymentGateWay: APIBaseRequestResponse {

    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/gateway/paymentlist"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }

    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return PaymentGateWayModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct PaymentGateWayModel: Codable {
    var status: String?
    var result: PaymentGateWayResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(PaymentGateWayResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct PaymentGateWayResultModel: Codable {
    var list: [PaymentGateWayListModel]?
    
    private enum CodingKeys: String, CodingKey {
        case list
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            list = (try? values.decode([PaymentGateWayListModel].self, forKey: .list)) ?? nil
        } catch {}
    }
}

struct PaymentGateWayListModel: Codable {
    var sequence: String?
    var title: String?
    var orientation: String?
    var imageURLString: String?
    var isEnabled: Bool?
    var id: Int?
    var subList: [PaymentGateWaySubListModel]?

    private enum CodingKeys: String, CodingKey {
        case sequence, title, orientation, id, subList
        case imageURLString = "image"
        case isEnabled = "enabled"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            sequence = (try? values.decode(String.self, forKey: .sequence)) ?? ""
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            orientation = (try? values.decode(String.self, forKey: .orientation)) ?? ""
            imageURLString = (try? values.decode(String.self, forKey: .imageURLString)) ?? ""
            isEnabled = (try? values.decode(Bool.self, forKey: .isEnabled)) ?? false
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            subList = (try? values.decode([PaymentGateWaySubListModel].self, forKey: .subList)) ?? nil
        } catch {}
    }
}

struct PaymentGateWaySubListModel: Codable {
    var sequence: Int?
    var id: String?
    var key: String?
    var displayName: String?
    var offer: String?
    var isEnabled: Bool?
    var imageURLString: String?
    var token: String?
    var currentBalance: Double?
    var isLinked: Bool?
    var linkToken: String?
    var isShowOffer: Bool?
    var parentID: Int?
    var subID: Int?
    var preferID: Int?
    var isDisplay: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case sequence, displayName, offer, id, key
        case token, currentBalance, linkToken
        case imageURLString = "image"
        case isEnabled = "enabled"
        case isLinked = "linked"
        case isShowOffer = "showOffer"
        case parentID = "parentId"
        case subID = "subid"
        case preferID = "prefer_id"
        case isDisplay = "display"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            sequence = (try? values.decode(Int.self, forKey: .sequence)) ?? 0
            id = (try? values.decode(String.self, forKey: .id)) ?? ""
            key = (try? values.decode(String.self, forKey: .key)) ?? ""
            displayName = (try? values.decode(String.self, forKey: .displayName)) ?? ""
            offer = (try? values.decode(String.self, forKey: .offer)) ?? ""
            isEnabled = (try? values.decode(Bool.self, forKey: .isEnabled)) ?? false
            imageURLString = (try? values.decode(String.self, forKey: .imageURLString)) ?? ""
            token = (try? values.decode(String.self, forKey: .token)) ?? ""
            currentBalance = (try? values.decode(Double.self, forKey: .currentBalance)) ?? 0
            isLinked = (try? values.decode(Bool.self, forKey: .isLinked)) ?? false
            linkToken = (try? values.decode(String.self, forKey: .linkToken)) ?? ""
            isShowOffer = (try? values.decode(Bool.self, forKey: .isShowOffer)) ?? false
            parentID = (try? values.decode(Int.self, forKey: .parentID)) ?? 0
            subID = (try? values.decode(Int.self, forKey: .subID)) ?? 0
            preferID = (try? values.decode(Int.self, forKey: .preferID)) ?? 0
            isDisplay = (try? values.decode(Bool.self, forKey: .isDisplay)) ?? false
        } catch {}
    }
}
