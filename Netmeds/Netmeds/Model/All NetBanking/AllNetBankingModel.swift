//
//  AllNetBankingModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIAllNetBanking: APIBaseRequestResponse {
        
    override func route() -> String {
        return "https://api.juspay.in/merchants/netmeds/paymentmethods"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let paymentMethods = jsonResult["payment_methods"] as? JSONArrayOfDictAny else { return nil }
        return [AllNetBankingModel].decode(data: paymentMethods.dataWithJSONObject())
    }
}

struct AllNetBankingModel: Codable {
    var paymentMethodType: String?
    var paymentMethod: String?
    var desc: String?

    private enum CodingKeys: String, CodingKey {
        case paymentMethodType = "payment_method_type"
        case paymentMethod = "payment_method"
        case desc = "description"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            paymentMethodType = (try? values.decode(String.self, forKey: .paymentMethodType)) ?? ""
            paymentMethod = (try? values.decode(String.self, forKey: .paymentMethod)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        } catch {}
    }
}
