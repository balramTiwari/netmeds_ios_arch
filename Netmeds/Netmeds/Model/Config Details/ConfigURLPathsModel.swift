//
//  ConfigURLPathsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 27/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIConfigURLPaths: APIBaseRequestResponse {
    
    override func route() -> String {
        return "config/url_paths"
    }
        
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ConfigURLPathsModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct ConfigURLPathsModel: Codable {
    var status: String?
    var result: ConfigURLPathsResultModel?

    private enum CodingKeys: String, CodingKey {
        case status, result
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(ConfigURLPathsResultModel.self, forKey: .result)) ?? nil
        } catch {}
    }
}

struct ConfigURLPathsResultModel: Codable {
    var catalogImageURL, manufacturerImageURL, productImageURL, urlRoot: String?

    private enum CodingKeys: String, CodingKey {
        case catalogImageURL = "catalog_image_url_base_path"
        case manufacturerImageURL = "manufacturer_image_url_base_path"
        case productImageURL = "product_image_url_base_path"
        case urlRoot = "url_root_path"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            catalogImageURL = (try? values.decode(String.self, forKey: .catalogImageURL)) ?? ""
            manufacturerImageURL = (try? values.decode(String.self, forKey: .manufacturerImageURL)) ?? ""
            productImageURL = (try? values.decode(String.self, forKey: .productImageURL)) ?? ""
            urlRoot = (try? values.decode(String.self, forKey: .urlRoot)) ?? ""
        } catch {}
    }
}
