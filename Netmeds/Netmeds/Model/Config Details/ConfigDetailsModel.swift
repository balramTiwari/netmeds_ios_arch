//
//  ConfigDetailsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class APIAppConfig: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/appconfig/get"
    }
        
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny, let configDetails = result["configDetails"] as? JSONDictAny else { return nil }
        return ConfigDetailsModel.decode(data: configDetails.dataWithJSONObject())
    }
}

struct ConfigDetailsModel: Codable {
    var uploadPrescriptionLimit: Int?
    var iOSForceUpdateVersion: String?
    var appUpdateTitle: String?
    var appUpdateMesssage: String?
    var pinCodeURL: String?
    var stateCityURL: String?
    var deliveryDateAPI: String?
    var privacy: String?
    var terms: String?
    var referralTerms: String?
    var isWellnessProductsActive: Bool?
    var isGatewayOffersActive: Bool?
    var hasPromotionBanners: Bool?
    var hasFeaturedArticle: Bool?
    var cancelOrderReason: [ConfigCancelOrderReasonModel]?
    var m2Coupon: ConfigM2CouponModel?
    var defaultDeliveryCharges: String?
    var m2DeliveryCharges: ConfigM2DeliveryChargesModel?
    var onBoarding: [ConfigOnBoardingModel]?
    var enquiryMobileNo: String?
    var algolia: ConfigAlgoliaModel?
    var hasSubscriptionStatus: Bool?
    var subscriptionCouponList: ConfigSubscriptionCouponListModel?
    var subscriptionBannersList: [ConfigSubscriptionBannersListModel]?
    var referEarn: ConfigReferEarnModel?
    var refillSectionValues: ConfigRefillSectionValuesModel?
    var isReferEarnEnabled: Bool?
    var isOrderSuccessBannerEnabled: Bool?
    var orderSuccessBlockBanners: ConfigRefillSectionValuesModel?
    var isDiagnosisEnabled: Bool?
    var isConsultIconEnabled: Bool?
    var hasBrainsinStatus: Bool?
    var brainsinsKey: String?
    var brainSinConfigValues: ConfigBrainSinConfigValuesModel?
    var isM2PrescriptionUploadEnabled: Bool?
    var m2PrescriptionUploadConfigs: ConfigM2PrescriptionUploadConfigsModel?
    var isConsultationUploadEnabled: Bool?
    var consultationUploadConfigs: ConfigM2PrescriptionUploadConfigsModel?
    var consultationHomeSliderConfigs: ConfigConsultationHomeSliderConfigsModel?
    var consultationPopularConcernConfigs: ConfigConsultationHomeSliderConfigsModel?
    var diagnosticsPopularConcernConfigs: ConfigConsultationHomeSliderConfigsModel?
    var diagnosisText: String?
    var productDetailsFlags: ConfigProductDetailsFlagsModel?
    var primeConfig: ConfigPrimeConfigModel?
    var deliveryContent: ConfigDeliveryContentModel?
    var orderConfirm: ConfigOrderConfirmModel?
    var orderAuthConfirm: ConfigOrderConfirmModel?
    var transactionFailure: String?
    var diagnosticsContent: String?
    var isAltCartEnabled: Bool?
    var isCartAlternateEnabled: Bool?
    var outofStockContent: ConfigOrderConfirmModel?
    var nmscashTermsConditions: String?
    var nmssupercashTermsConditions: String?
    var isKaptureChatEnabled: Bool?
    var isInAppForceUpdateEnabled: Bool?
    var isInAppSoftUpdateEnabled: Bool?
    var inAppSoftUpdateTitle: String?
    var inAppSoftUpdateDesc: String?

    private enum CodingKeys: String, CodingKey {
        case uploadPrescriptionLimit
        case iOSForceUpdateVersion = "iosForceUpdateVersion"
        case appUpdateTitle
        case appUpdateMesssage
        case pinCodeURL = "pinCodeUrl"
        case stateCityURL = "stateCityUrl"
        case deliveryDateAPI = "deliveryDateApi"
        case privacy
        case terms
        case referralTerms
        case isWellnessProductsActive = "wellnessProductsActiveFlag"
        case isGatewayOffersActive = "gatewayOffersActiveFlag"
        case hasPromotionBanners = "promotionBannerFlag"
        case hasFeaturedArticle = "featuredArticleFlag"
        case cancelOrderReason
        case m2Coupon
        case defaultDeliveryCharges
        case m2DeliveryCharges
        case onBoarding
        case enquiryMobileNo
        case algolia
        case hasSubscriptionStatus = "subscriptionStatus"
        case subscriptionCouponList
        case subscriptionBannersList
        case referEarn
        case refillSectionValues
        case isReferEarnEnabled = "referEarnEnableFlag"
        case isOrderSuccessBannerEnabled = "orderSuccessBannerEnableFlag"
        case orderSuccessBlockBanners
        case isDiagnosisEnabled = "diagnosisEnableFlag"
        case isConsultIconEnabled = "consultIconEnableFlag"
        case hasBrainsinStatus = "brainsinStatusFlag"
        case brainsinsKey
        case brainSinConfigValues
        case isM2PrescriptionUploadEnabled = "m2PrescriptionUploadEnableFlag"
        case m2PrescriptionUploadConfigs
        case isConsultationUploadEnabled = "consultationUploadEnableFlag"
        case consultationUploadConfigs
        case consultationHomeSliderConfigs
        case consultationPopularConcernConfigs
        case diagnosticsPopularConcernConfigs
        case diagnosisText
        case productDetailsFlags
        case primeConfig
        case deliveryContent
        case orderConfirm
        case orderAuthConfirm
        case transactionFailure
        case diagnosticsContent
        case isAltCartEnabled = "altCartEnableFlag"
        case isCartAlternateEnabled = "cartAlternateEnableFlag"
        case outofStockContent
        case nmscashTermsConditions
        case nmssupercashTermsConditions
        case isKaptureChatEnabled = "kapture_chat_ios"
        case isInAppForceUpdateEnabled = "inAppForceUpdateEnableIosFlag"
        case isInAppSoftUpdateEnabled = "inAppSoftUpdateEnableIosFlag"
        case inAppSoftUpdateTitle
        case inAppSoftUpdateDesc
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            uploadPrescriptionLimit = (try? values.decode(Int.self, forKey: .uploadPrescriptionLimit)) ?? 0
            iOSForceUpdateVersion = (try? values.decode(String.self, forKey: .iOSForceUpdateVersion)) ?? ""
            appUpdateTitle = (try? values.decode(String.self, forKey: .appUpdateTitle)) ?? ""
            appUpdateMesssage = (try? values.decode(String.self, forKey: .appUpdateMesssage)) ?? ""
            pinCodeURL = (try? values.decode(String.self, forKey: .pinCodeURL)) ?? ""
            stateCityURL = (try? values.decode(String.self, forKey: .stateCityURL)) ?? ""
            deliveryDateAPI = (try? values.decode(String.self, forKey: .deliveryDateAPI)) ?? ""
            privacy = (try? values.decode(String.self, forKey: .privacy)) ?? ""
            terms = (try? values.decode(String.self, forKey: .terms)) ?? ""
            referralTerms = (try? values.decode(String.self, forKey: .referralTerms)) ?? ""
            isWellnessProductsActive = (try? values.decode(Bool.self, forKey: .isWellnessProductsActive)) ?? false
            isGatewayOffersActive = (try? values.decode(Bool.self, forKey: .isGatewayOffersActive)) ?? false
            hasPromotionBanners = (try? values.decode(Bool.self, forKey: .hasPromotionBanners)) ?? false
            hasFeaturedArticle = (try? values.decode(Bool.self, forKey: .hasFeaturedArticle)) ?? false
            cancelOrderReason = (try? values.decode([ConfigCancelOrderReasonModel].self, forKey: .cancelOrderReason)) ?? nil
            m2Coupon = (try? values.decode(ConfigM2CouponModel.self, forKey: .m2Coupon)) ?? nil
            defaultDeliveryCharges = (try? values.decode(String.self, forKey: .defaultDeliveryCharges)) ?? ""
            m2DeliveryCharges = (try? values.decode(ConfigM2DeliveryChargesModel.self, forKey: .m2DeliveryCharges)) ?? nil
            onBoarding = (try? values.decode([ConfigOnBoardingModel].self, forKey: .onBoarding)) ?? nil
            enquiryMobileNo = (try? values.decode(String.self, forKey: .enquiryMobileNo)) ?? ""
            algolia = (try? values.decode(ConfigAlgoliaModel.self, forKey: .algolia)) ?? nil
            hasSubscriptionStatus = (try? values.decode(Bool.self, forKey: .hasSubscriptionStatus)) ?? false
            subscriptionCouponList = (try? values.decode(ConfigSubscriptionCouponListModel.self, forKey: .subscriptionCouponList)) ?? nil
            subscriptionBannersList = (try? values.decode([ConfigSubscriptionBannersListModel].self, forKey: .subscriptionBannersList)) ?? nil
            referEarn = (try? values.decode(ConfigReferEarnModel.self, forKey: .referEarn)) ?? nil
            refillSectionValues = (try? values.decode(ConfigRefillSectionValuesModel.self, forKey: .refillSectionValues)) ?? nil
            isReferEarnEnabled = (try? values.decode(Bool.self, forKey: .isReferEarnEnabled)) ?? false
            isOrderSuccessBannerEnabled = (try? values.decode(Bool.self, forKey: .isOrderSuccessBannerEnabled)) ?? false
            orderSuccessBlockBanners = (try? values.decode(ConfigRefillSectionValuesModel.self, forKey: .orderSuccessBlockBanners)) ?? nil
            isDiagnosisEnabled = (try? values.decode(Bool.self, forKey: .isDiagnosisEnabled)) ?? false
            isConsultIconEnabled = (try? values.decode(Bool.self, forKey: .isConsultIconEnabled)) ?? false
            hasBrainsinStatus = (try? values.decode(Bool.self, forKey: .hasBrainsinStatus)) ?? false
            brainsinsKey = (try? values.decode(String.self, forKey: .brainsinsKey)) ?? ""
            brainSinConfigValues = (try? values.decode(ConfigBrainSinConfigValuesModel.self, forKey: .brainSinConfigValues)) ?? nil
            isM2PrescriptionUploadEnabled = (try? values.decode(Bool.self, forKey: .isM2PrescriptionUploadEnabled)) ?? false
            m2PrescriptionUploadConfigs = (try? values.decode(ConfigM2PrescriptionUploadConfigsModel.self, forKey: .m2PrescriptionUploadConfigs)) ?? nil
            isConsultationUploadEnabled = (try? values.decode(Bool.self, forKey: .isConsultationUploadEnabled)) ?? false
            consultationUploadConfigs = (try? values.decode(ConfigM2PrescriptionUploadConfigsModel.self, forKey: .consultationUploadConfigs)) ?? nil
            consultationHomeSliderConfigs = (try? values.decode(ConfigConsultationHomeSliderConfigsModel.self, forKey: .consultationHomeSliderConfigs)) ?? nil
            consultationPopularConcernConfigs = (try? values.decode(ConfigConsultationHomeSliderConfigsModel.self, forKey: .consultationPopularConcernConfigs)) ?? nil
            diagnosticsPopularConcernConfigs = (try? values.decode(ConfigConsultationHomeSliderConfigsModel.self, forKey: .diagnosticsPopularConcernConfigs)) ?? nil
            diagnosisText = (try? values.decode(String.self, forKey: .diagnosisText)) ?? ""
            productDetailsFlags = (try? values.decode(ConfigProductDetailsFlagsModel.self, forKey: .productDetailsFlags)) ?? nil
            primeConfig = (try? values.decode(ConfigPrimeConfigModel.self, forKey: .primeConfig)) ?? nil
            deliveryContent = (try? values.decode(ConfigDeliveryContentModel.self, forKey: .deliveryContent)) ?? nil
            orderConfirm = (try? values.decode(ConfigOrderConfirmModel.self, forKey: .orderConfirm)) ?? nil
            orderAuthConfirm = (try? values.decode(ConfigOrderConfirmModel.self, forKey: .orderAuthConfirm)) ?? nil
            transactionFailure = (try? values.decode(String.self, forKey: .transactionFailure)) ?? ""
            diagnosticsContent = (try? values.decode(String.self, forKey: .diagnosticsContent)) ?? ""
            isAltCartEnabled = (try? values.decode(Bool.self, forKey: .isAltCartEnabled)) ?? false
            isCartAlternateEnabled = (try? values.decode(Bool.self, forKey: .isCartAlternateEnabled)) ?? false
            outofStockContent = (try? values.decode(ConfigOrderConfirmModel.self, forKey: .outofStockContent)) ?? nil
            nmscashTermsConditions = (try? values.decode(String.self, forKey: .nmscashTermsConditions)) ?? ""
            nmssupercashTermsConditions = (try? values.decode(String.self, forKey: .nmssupercashTermsConditions)) ?? ""
            isKaptureChatEnabled = (try? values.decode(Bool.self, forKey: .isKaptureChatEnabled)) ?? false
            isInAppForceUpdateEnabled = (try? values.decode(Bool.self, forKey: .isInAppForceUpdateEnabled)) ?? false
            isInAppSoftUpdateEnabled = (try? values.decode(Bool.self, forKey: .isInAppSoftUpdateEnabled)) ?? false
            inAppSoftUpdateTitle = (try? values.decode(String.self, forKey: .inAppSoftUpdateTitle)) ?? ""
            inAppSoftUpdateDesc = (try? values.decode(String.self, forKey: .inAppSoftUpdateDesc)) ?? ""
        } catch {}
    }
}

struct ConfigCancelOrderReasonModel: Codable {
    var key: Int?
    var value: String?
    
    private enum CodingKeys: String, CodingKey {
        case key
        case value
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            key = (try? values.decode(Int.self, forKey: .key)) ?? 0
            value = (try? values.decode(String.self, forKey: .value)) ?? ""
        } catch {}
    }
}

struct ConfigM2CouponModel: Codable {
    var couponCode: String?
    var couponDescription: String?
    var discount: String?
    
    private enum CodingKeys: String, CodingKey {
        case couponCode
        case couponDescription = "description"
        case discount
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            couponCode = (try? values.decode(String.self, forKey: .couponCode)) ?? ""
            couponDescription = (try? values.decode(String.self, forKey: .couponDescription)) ?? ""
            discount = (try? values.decode(String.self, forKey: .discount)) ?? ""
        } catch {}
    }
}

struct ConfigM2DeliveryChargesModel: Codable {
    var orderCompleteContent: String?
    var mrpContent: String?
    var deliveryContent: String?
    var freeShipEligibilityValue: String?
    var cutOffValue: String?
    var minShippingCharges: String?
    var maxShippingCharges: String?
    
    private enum CodingKeys: String, CodingKey {
        case orderCompleteContent
        case mrpContent
        case deliveryContent
        case freeShipEligibilityValue
        case cutOffValue
        case minShippingCharges
        case maxShippingCharges
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderCompleteContent = (try? values.decode(String.self, forKey: .orderCompleteContent)) ?? ""
            mrpContent = (try? values.decode(String.self, forKey: .mrpContent)) ?? ""
            deliveryContent = (try? values.decode(String.self, forKey: .deliveryContent)) ?? ""
            freeShipEligibilityValue = (try? values.decode(String.self, forKey: .freeShipEligibilityValue)) ?? ""
            cutOffValue = (try? values.decode(String.self, forKey: .cutOffValue)) ?? ""
            minShippingCharges = (try? values.decode(String.self, forKey: .minShippingCharges)) ?? ""
            maxShippingCharges = (try? values.decode(String.self, forKey: .maxShippingCharges)) ?? ""
        } catch {}
    }
}

struct ConfigOnBoardingModel: Codable {
    var title: String?
    var desc: String?
    var image: String?
    
    private enum CodingKeys: String, CodingKey {
        case title = "onBoardingTitle"
        case desc = "onBoardingDescription"
        case image = "onBoardingImage"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
            image = (try? values.decode(String.self, forKey: .image)) ?? ""
        } catch {}
    }
}

struct ConfigAlgoliaModel: Codable {
    var medIndex: String?
    var diagIndex: String?
    var appId: String?
    var hits: String?
    var medDetail1: String?
    var diagDetail1: String?
    
    private enum CodingKeys: String, CodingKey {
        case medIndex = "algMedIndex"
        case diagIndex = "algDiagIndex"
        case appId = "algoliaAppId"
        case hits = "algoliaHits"
        case medDetail1 = "algMedDetail1"
        case diagDetail1 = "algDiagdetail1"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            medIndex = (try? values.decode(String.self, forKey: .medIndex)) ?? ""
            diagIndex = (try? values.decode(String.self, forKey: .diagIndex)) ?? ""
            appId = (try? values.decode(String.self, forKey: .appId)) ?? ""
            hits = (try? values.decode(String.self, forKey: .hits)) ?? ""
            medDetail1 = (try? values.decode(String.self, forKey: .medDetail1)) ?? ""
            diagDetail1 = (try? values.decode(String.self, forKey: .diagDetail1)) ?? ""
        } catch {}
    }
}

struct ConfigSubscriptionCouponListModel: Codable {
    var couponCode: String?
    var discount: String?
    
    private enum CodingKeys: String, CodingKey {
        case couponCode
        case discount
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            couponCode = (try? values.decode(String.self, forKey: .couponCode)) ?? ""
            discount = (try? values.decode(String.self, forKey: .discount)) ?? ""
        } catch {}
    }
}

struct ConfigSubscriptionBannersListModel: Codable {
    var title: String?
    var desc: String?
    var image: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case desc = "description"
        case image
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
            image = (try? values.decode(String.self, forKey: .image)) ?? ""
        } catch {}
    }
}

struct ConfigReferEarnModel: Codable {
    var invite: ConfigReferEarnInfoModel?
    var cashBack: ConfigReferEarnInfoModel?
    var offer: ConfigReferEarnInfoModel?
    var orderSuccess: ConfigOrderSuccessModel?
    
    private enum CodingKeys: String, CodingKey {
        case invite
        case cashBack
        case offer
        case orderSuccess
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            invite = (try? values.decode(ConfigReferEarnInfoModel.self, forKey: .invite)) ?? nil
            cashBack = (try? values.decode(ConfigReferEarnInfoModel.self, forKey: .cashBack)) ?? nil
            offer = (try? values.decode(ConfigReferEarnInfoModel.self, forKey: .offer)) ?? nil
            orderSuccess = (try? values.decode(ConfigOrderSuccessModel.self, forKey: .orderSuccess)) ?? nil
        } catch {}
    }
}

struct ConfigReferEarnInfoModel: Codable {
    var title: String?
    var content: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case content
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            content = (try? values.decode(String.self, forKey: .content)) ?? ""
        } catch {}
    }
}

struct ConfigOrderSuccessModel: Codable {
    var header: String?
    var subTitle: String?
    var desc: String?
    var buttonDescription: String?
    var amount: String?
    var referAmountImg: String?
    var referImg: String?
    
    private enum CodingKeys: String, CodingKey {
        case header
        case subTitle
        case desc = "description"
        case buttonDescription
        case amount
        case referAmountImg
        case referImg
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            header = (try? values.decode(String.self, forKey: .header)) ?? ""
            subTitle = (try? values.decode(String.self, forKey: .subTitle)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
            buttonDescription = (try? values.decode(String.self, forKey: .buttonDescription)) ?? ""
            amount = (try? values.decode(String.self, forKey: .amount)) ?? ""
            referAmountImg = (try? values.decode(String.self, forKey: .referAmountImg)) ?? ""
            referImg = (try? values.decode(String.self, forKey: .referImg)) ?? ""
        } catch {}
    }
}

struct ConfigRefillSectionValuesModel: Codable {
    var header: String?
    var subHeader: String?
    //    var banners

    private enum CodingKeys: String, CodingKey {
        case header
        case subHeader
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            header = (try? values.decode(String.self, forKey: .header)) ?? ""
            subHeader = (try? values.decode(String.self, forKey: .subHeader)) ?? ""
        } catch {}
    }
}

struct ConfigBrainSinConfigValuesModel: Codable {
    var productTryAndBuy: ConfigProductTryAndBuyModel?
    var cartTryAndBuy: ConfigProductTryAndBuyModel?
    var peopleAlsoView: ConfigProductTryAndBuyModel?

    private enum CodingKeys: String, CodingKey {
        case productTryAndBuy
        case cartTryAndBuy
        case peopleAlsoView
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            productTryAndBuy = (try? values.decode(ConfigProductTryAndBuyModel.self, forKey: .productTryAndBuy)) ?? nil
            cartTryAndBuy = (try? values.decode(ConfigProductTryAndBuyModel.self, forKey: .cartTryAndBuy)) ?? nil
            peopleAlsoView = (try? values.decode(ConfigProductTryAndBuyModel.self, forKey: .peopleAlsoView)) ?? nil
        } catch {}
    }
}


struct ConfigProductTryAndBuyModel: Codable {
    var templateID: String?
    var isStatusEnabled: Bool?
    var title: String?
    var subTitle: String?
    var desc: String?

    private enum CodingKeys: String, CodingKey {
        case isStatusEnabled = "EnableStatus"
        case subTitle
        case templateID = "TemplateId"
        case title = "Title"
        case desc = "Description"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            templateID = (try? values.decode(String.self, forKey: .templateID)) ?? ""
            isStatusEnabled = (try? values.decode(Bool.self, forKey: .isStatusEnabled)) ?? false
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            subTitle = (try? values.decode(String.self, forKey: .subTitle)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        } catch {}
    }
}

struct ConfigM2PrescriptionUploadConfigsModel: Codable {
    var header1: String?
    var header2: String?
    var orderText: String?
    var buttonText: String?
    var desc: String?

    private enum CodingKeys: String, CodingKey {
        case header1, header2, orderText, buttonText
        case desc = "description"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            header1 = (try? values.decode(String.self, forKey: .header1)) ?? ""
            header2 = (try? values.decode(String.self, forKey: .header2)) ?? ""
            orderText = (try? values.decode(String.self, forKey: .orderText)) ?? ""
            buttonText = (try? values.decode(String.self, forKey: .buttonText)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        } catch {}
    }
}

struct ConfigConsultationHomeSliderConfigsModel: Codable {
    var isEnabled: Bool?
    var title1: String?
    var title2: String?
    var slide: [ConfigSlideModel]?
    var desc: String?

    private enum CodingKeys: String, CodingKey {
        case title1, title2, slide
        case isEnabled = "enableFlag"
        case desc = "descp"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            isEnabled = (try? values.decode(Bool.self, forKey: .isEnabled)) ?? false
            title1 = (try? values.decode(String.self, forKey: .title1)) ?? ""
            title2 = (try? values.decode(String.self, forKey: .title2)) ?? ""
            slide = (try? values.decode([ConfigSlideModel].self, forKey: .slide)) ?? nil
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        } catch {}
    }
}

struct ConfigSlideModel: Codable {
    var title: String?
    var linkType: String?
    var image: String?
    var url: String?

    private enum CodingKeys: String, CodingKey {
        case title, image, url
        case linkType = "linktype"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            linkType = (try? values.decode(String.self, forKey: .linkType)) ?? ""
            image = (try? values.decode(String.self, forKey: .image)) ?? ""
            url = (try? values.decode(String.self, forKey: .url)) ?? ""
        } catch {}
    }
}

struct ConfigProductDetailsFlagsModel: Codable {
    var hasPackOfBuy: Bool?
    var packOfBuyTitle: String?
    var isComboEnabled: Bool?
    var comboEnableTitle: String?
    var isSimilarEnable: Bool?
    var similarEnableTitle: String?

    private enum CodingKeys: String, CodingKey {
        case packOfBuyTitle, comboEnableTitle, similarEnableTitle
        case hasPackOfBuy = "packOfBuy"
        case isComboEnabled = "comboEnable"
        case isSimilarEnable = "similarEnable"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            hasPackOfBuy = (try? values.decode(Bool.self, forKey: .hasPackOfBuy)) ?? false
            packOfBuyTitle = (try? values.decode(String.self, forKey: .packOfBuyTitle)) ?? ""
            isComboEnabled = (try? values.decode(Bool.self, forKey: .isComboEnabled)) ?? false
            comboEnableTitle = (try? values.decode(String.self, forKey: .comboEnableTitle)) ?? ""
            isSimilarEnable = (try? values.decode(Bool.self, forKey: .isSimilarEnable)) ?? false
            similarEnableTitle = (try? values.decode(String.self, forKey: .similarEnableTitle)) ?? ""
        } catch {}
    }
}

struct ConfigPrimeConfigModel: Codable {
    var isEnabled: Bool?
    var header: String?
    var memberText: String?
    var faq: String?
    var shoppingCart: ConfigPrimeConfigDetailsModel?
    var orderHeader: ConfigPrimeConfigDetailsModel?
    var homePageConfig: ConfigPrimeConfigDetailsModel?
    var netmedsMembership: ConfigPrimeConfigDetailsModel?

    private enum CodingKeys: String, CodingKey {
        case isEnabled = "primeEnableFlag"
        case header = "primeHeader"
        case memberText = "primeMemberText"
        case faq = "primeFaq"
        case orderHeader = "orderheader"
        case shoppingCart, homePageConfig, netmedsMembership
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            isEnabled = (try? values.decode(Bool.self, forKey: .isEnabled)) ?? false
            header = (try? values.decode(String.self, forKey: .header)) ?? ""
            memberText = (try? values.decode(String.self, forKey: .memberText)) ?? ""
            faq = (try? values.decode(String.self, forKey: .faq)) ?? ""
            shoppingCart = (try? values.decode(ConfigPrimeConfigDetailsModel.self, forKey: .shoppingCart)) ?? nil
            orderHeader = (try? values.decode(ConfigPrimeConfigDetailsModel.self, forKey: .orderHeader)) ?? nil
            homePageConfig = (try? values.decode(ConfigPrimeConfigDetailsModel.self, forKey: .homePageConfig)) ?? nil
            netmedsMembership = (try? values.decode(ConfigPrimeConfigDetailsModel.self, forKey: .netmedsMembership)) ?? nil
        } catch {}
    }
}

struct ConfigPrimeConfigDetailsModel: Codable {
    var title: String?
    var header: String?
    var desc: String?
    var buttonText: String?
    var isEnabled: Bool?
    var memberShipIcons: [ConfigMemberShipIconsModel]?
    
    private enum CodingKeys: String, CodingKey {
        case header, title, memberShipIcons
        case desc = "description"
        case buttonText = "buttontext"
        case isEnabled = "enableFlag"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            header = (try? values.decode(String.self, forKey: .header)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
            buttonText = (try? values.decode(String.self, forKey: .buttonText)) ?? ""
            isEnabled = (try? values.decode(Bool.self, forKey: .isEnabled)) ?? false
            memberShipIcons = (try? values.decode([ConfigMemberShipIconsModel].self, forKey: .memberShipIcons)) ?? nil
        } catch {}
    }
}

struct ConfigMemberShipIconsModel: Codable {
    var linkPage: String?
    var linkType: String?
    var imageURL: String?

    private enum CodingKeys: String, CodingKey {
        case linkPage = "linkpage"
        case linkType = "linktype"
        case imageURL = "imageUrl"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            linkPage = (try? values.decode(String.self, forKey: .linkPage)) ?? ""
            linkType = (try? values.decode(String.self, forKey: .linkType)) ?? ""
            imageURL = (try? values.decode(String.self, forKey: .imageURL)) ?? ""
        } catch {}
    }
}

struct ConfigDeliveryContentModel: Codable {
    var productCannotBeDelivered: String?
    var productCannotBeDeliveredColdStorage: String?
    var productCannotBeDeliveredToEstimatedDelivery: String?
    var pinBlockedErrorMessage: String?

    private enum CodingKeys: String, CodingKey {
        case productCannotBeDelivered, productCannotBeDeliveredColdStorage, productCannotBeDeliveredToEstimatedDelivery, pinBlockedErrorMessage
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            productCannotBeDelivered = (try? values.decode(String.self, forKey: .productCannotBeDelivered)) ?? ""
            productCannotBeDeliveredColdStorage = (try? values.decode(String.self, forKey: .productCannotBeDeliveredColdStorage)) ?? ""
            productCannotBeDeliveredToEstimatedDelivery = (try? values.decode(String.self, forKey: .productCannotBeDeliveredToEstimatedDelivery)) ?? ""
            pinBlockedErrorMessage = (try? values.decode(String.self, forKey: .pinBlockedErrorMessage)) ?? ""
        } catch {}
    }
}

struct ConfigOrderConfirmModel: Codable {
    var title: String?
    var desc: String?

    private enum CodingKeys: String, CodingKey {
        case title
        case desc = "descp"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        } catch {}
    }
}




