//
//  LogInViaOTPModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APILogInViaOTP: APIBaseRequestResponse {
    
    var mobileNo: String = ""
    var randomKey: String = ""
    var otp: String = ""
    
    override func route() -> String {
        return "session/complete/using_mobileno_n_otp?mobile_no=\(mobileNo)&rk=\(randomKey)&otp=\(otp)&channel=\(kSourceApp)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CreateSessionModel.decode(data: jsonResult.dataWithJSONObject())
    }
}
