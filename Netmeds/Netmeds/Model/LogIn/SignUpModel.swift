//
//  SignUpModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APISignUp: APIBaseRequestResponse {
    
    var params: JSONDictAny?
    override func route() -> String {
        return "register?channel=\(kSourceApp)"
    }
    
    override func parameters() -> JSONDictAny? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CreateSessionModel.decode(data: jsonResult.dataWithJSONObject())
    }
}
