//
//  CustomerDetailsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APICustomerDetails: APIBaseRequestResponse {
    
    override func route() -> String {
        return "entity/customer/get_details"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CustomerInfoModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct CustomerInfoModel: Codable {
    var status: String?
    var result: CustomerResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(CustomerResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct CustomerResultModel: Codable {
    var details: CustomerDetailsModel?
    
    private enum CodingKeys: String, CodingKey {
        case details = "your_details"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            details = (try? values.decode(CustomerDetailsModel.self, forKey: .details)) ?? nil
        } catch {}
    }
}

struct CustomerDetailsModel: Codable {
    var juspayID: String?
    var updatedTime: String?
    var firstName: String?
    var isMobileNoVerified: Bool?
    var gender: String?
    var universalCartID: Int?
    var dob: String?
    var paytmCustomerToken: String?
    var mobileNo: String?
    var preferredBillingAddress: Int?
    var segments: String?
    var primeValidTime: String?
    var password: String?
    var someType: String?
    var id: Int?
    var email: String?
    var isPrimeUser: Bool?
    var createdTime: String?
    var registrationSource:String?
    var isEmailVerified: Bool?
    var groupName: String?
    var shortName: String?
    var registeredChannel: String?
    var nmsCustomerID: String?
    var lastname: String?
    var fullName: String?
    var preferredShippingAddress: Int?
    
    private enum CodingKeys: String, CodingKey {
        case juspayID = "juspay_id"
        case updatedTime = "updated_time"
        case firstName = "firstname"
        case isMobileNoVerified = "mobile_no_verified"
        case gender = "gender"
        case universalCartID = "universal_cart_id"
        case dob = "date_of_birth"
        case paytmCustomerToken = "paytm_customer_token"
        case mobileNo = "mobile_no"
        case preferredBillingAddress = "preferred_billing_address"
        case segments = "segments"
        case primeValidTime = "prime_valid_till_time"
        case password = "password"
        case someType = "some_type"
        case id = "id"
        case email = "email"
        case isPrimeUser = "prime"
        case createdTime = "created_time"
        case registrationSource = "registration_source"
        case isEmailVerified = "email_verified"
        case groupName = "group_name"
        case shortName = "display_name_short"
        case registeredChannel = "registered_channel"
        case nmsCustomerID = "nms_customer_id"
        case lastname = "lastname"
        case fullName = "display_name_full"
        case preferredShippingAddress = "preferred_shipping_address"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            juspayID = (try? values.decode(String.self, forKey: .juspayID)) ?? ""
            updatedTime = (try? values.decode(String.self, forKey: .updatedTime)) ?? ""
            firstName = (try? values.decode(String.self, forKey: .firstName)) ?? ""
            isMobileNoVerified = (try? values.decode(Bool.self, forKey: .isMobileNoVerified)) ?? false
            gender = (try? values.decode(String.self, forKey: .gender)) ?? ""
            universalCartID = (try? values.decode(Int.self, forKey: .universalCartID)) ?? 0
            dob = (try? values.decode(String.self, forKey: .dob)) ?? ""
            paytmCustomerToken = (try? values.decode(String.self, forKey: .paytmCustomerToken)) ?? ""
            mobileNo = (try? values.decode(String.self, forKey: .mobileNo)) ?? ""
            preferredBillingAddress = (try? values.decode(Int.self, forKey: .preferredBillingAddress)) ?? 0
            segments = (try? values.decode(String.self, forKey: .segments)) ?? ""
            primeValidTime = (try? values.decode(String.self, forKey: .primeValidTime)) ?? ""
            password = (try? values.decode(String.self, forKey: .password)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            email = (try? values.decode(String.self, forKey: .email)) ?? ""
            isPrimeUser = (try? values.decode(Bool.self, forKey: .isPrimeUser)) ?? false
            createdTime = (try? values.decode(String.self, forKey: .createdTime)) ?? ""
            registrationSource = (try? values.decode(String.self, forKey: .registrationSource)) ?? ""
            isEmailVerified = (try? values.decode(Bool.self, forKey: .isEmailVerified)) ?? false
            groupName = (try? values.decode(String.self, forKey: .groupName)) ?? ""
            shortName = (try? values.decode(String.self, forKey: .shortName)) ?? ""
            registeredChannel = (try? values.decode(String.self, forKey: .registeredChannel)) ?? ""
            nmsCustomerID = (try? values.decode(String.self, forKey: .nmsCustomerID)) ?? ""
            lastname = (try? values.decode(String.self, forKey: .lastname)) ?? ""
            fullName = (try? values.decode(String.self, forKey: .fullName)) ?? ""
            preferredShippingAddress = (try? values.decode(Int.self, forKey: .preferredShippingAddress)) ?? 0
        } catch {}
    }
}
