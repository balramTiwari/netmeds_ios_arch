//
//  LogInModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class APILogIn: APIBaseRequestResponse {
    
    var mobileNo: String = ""
    override func route() -> String {
        return "id/details/" + mobileNo
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return LogInModel.decode(data: jsonResult.dataWithJSONObject())
    }
}


struct LogInModel: Codable {
    var status: String?
    var result: LogInResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(LogInResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct LogInResultModel: Codable {
    var otpDetails: LogInOTPDetailsModel?
    
    private enum CodingKeys: String, CodingKey {
        case otpDetails = "otp_details"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            otpDetails = (try? values.decode(LogInOTPDetailsModel.self, forKey: .otpDetails)) ?? nil
        } catch {}
    }
}

struct LogInOTPDetailsModel: Codable {
    var randomKey: String?
    
    private enum CodingKeys: String, CodingKey {
        case randomKey = "random_key"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            randomKey = (try? values.decode(String.self, forKey: .randomKey)) ?? ""
        } catch {}
    }
}

struct ErrorInfoModel: Codable {
    var message: String?
    var code: String?
    
    private enum CodingKeys: String, CodingKey {
        case message = "reason_eng"
        case code = "reason_code"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            message = (try? values.decode(String.self, forKey: .message)) ?? ""
            code = (try? values.decode(String.self, forKey: .code)) ?? ""
        } catch {}
    }
}
