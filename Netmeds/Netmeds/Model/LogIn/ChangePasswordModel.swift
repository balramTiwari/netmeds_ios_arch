//
//  ChangePasswordModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APIInitiateChangePassword: APIBaseRequestResponse {
    
    var params: JSONDictAny?
    override func route() -> String {
        return "session/initiate/change_password"
    }
    
    override func parameters() -> JSONDictAny? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return LogInModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APICompleteChangePassword: APIBaseRequestResponse {
    
    var params: JSONDictAny?
    override func route() -> String {
        return "session/complete/change_password"
    }
    
    override func parameters() -> JSONDictAny? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CompleteChangePasswordModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct CompleteChangePasswordModel: Codable {
    var status: String?
    var result: String?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(String.self, forKey: .result)) ?? ""
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

