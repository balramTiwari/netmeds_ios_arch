//
//  LogInViaPasswordModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
class APILogInViaPassword: APIBaseRequestResponse {
    
    var params:  JSONDictAny?
    override func route() -> String {
        return "session/create/using_userid_n_passwd"
    }
    
    override func parameters() -> JSONDictAny? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CreateSessionModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APISendOTP: APIBaseRequestResponse {
    
    var mobileNo: String = ""
    override func route() -> String {
        return "session/initiate/using_mobileno_n_otp?mobile_no=" + mobileNo
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return LogInModel.decode(data: jsonResult.dataWithJSONObject())
    }
}


class APIReSendOTP: APIBaseRequestResponse {
    
    var randomKey: String = ""
    override func route() -> String {
        return "session/resend/same_otp?rk=" + randomKey
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return LogInModel.decode(data: jsonResult.dataWithJSONObject())
    }
}
