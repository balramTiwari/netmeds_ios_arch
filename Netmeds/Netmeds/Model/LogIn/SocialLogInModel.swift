//
//  SocialLogInModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 13/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class APISocialLogIn: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    override func route() -> String {
        return "api/v1/login/sociallogin"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CreateSessionModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct CreateSessionModel: Codable {
    var status: String?
    var result: SessionResultModel?
    var responseTime: String?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case responseTime = "response_time"
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(SessionResultModel.self, forKey: .result)) ?? nil
            responseTime = (try? values.decode(String.self, forKey: .responseTime)) ?? ""
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct SessionResultModel: Codable {
    var session: SessionModel?
    var code, message: String?
    var randomKey: Int?
    
    public enum CodingKeys: String, CodingKey {
        case session
        case code, message
        case randomKey = "random"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            session = (try? values.decode(SessionModel.self, forKey: .session)) ?? nil
            code = (try? values.decode(String.self, forKey: .code)) ?? ""
            message = (try? values.decode(String.self, forKey: .message)) ?? ""
            randomKey = (try? values.decode(Int.self, forKey: .randomKey)) ?? 0
        } catch {}
    }
}


struct SessionModel: Codable {
    var createdTime: String?
    var customerId: Int?
    var id: String?
    var isValid: Bool?
    var loganSessionId: String?
    var validTill: String?
    
    public enum CodingKeys: String, CodingKey {
        case createdTime = "created_time"
        case customerId = "customer_id"
        case id
        case isValid = "is_valid"
        case loganSessionId = "logan_session_id"
        case validTill = "valid_till"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            createdTime = (try? values.decode(String.self, forKey: .createdTime)) ?? ""
            customerId = (try? values.decode(Int.self, forKey: .customerId)) ?? 0
            id = (try? values.decode(String.self, forKey: .id)) ?? ""
            isValid = (try? values.decode(Bool.self, forKey: .isValid)) ?? false
            loganSessionId = (try? values.decode(String.self, forKey: .loganSessionId)) ?? ""
            validTill = (try? values.decode(String.self, forKey: .validTill)) ?? ""
        } catch {}
    }
}
