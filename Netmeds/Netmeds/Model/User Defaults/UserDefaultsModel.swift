//
//  UserDefaultsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import UIKit

class UserDefaultsModel: NSObject {
    
    private class func userDefaults() -> UserDefaults {
        return UserDefaults.standard
    }
    
    class func setObject (_ object : Any?, forKey : String) {
        userDefaults().set(object, forKey: forKey)
        userDefaults().synchronize()
    }
    
    class func removeObjectForKey(_ key : String) {
        userDefaults().removeObject(forKey: key)
        userDefaults().synchronize()
    }
    
    class func isUserLoggedIn() -> Bool {
        return userDefaults().bool(forKey: kUserLoggedInKey)
    }

    class func getSessionID() -> String {
        return userDefaults().string(forKey: kSessionIDKey) ?? ""
    }
    
    class func getUserID() -> String {
        return userDefaults().string(forKey: kUserIDKey) ?? ""
    }
    
    class func getLoganSessionID() -> String {
        return userDefaults().string(forKey: kLoganSessionIDKey) ?? ""
    }
    
    class func getSessionExpireTS() -> String {
        return userDefaults().string(forKey: kSessionExpireTSKey) ?? ""
    }
    
    class func getCartID() -> Int {
        return userDefaults().integer(forKey: kCartIDKey)
    }
}

extension UserDefaultsModel {
    class func saveSessionDetails(sessionInfo: SessionModel) {
        self.setObject(sessionInfo.customerId, forKey: kUserIDKey)
        self.setObject(sessionInfo.id, forKey: kSessionIDKey)
        self.setObject(sessionInfo.loganSessionId, forKey: kLoganSessionIDKey)
        self.setObject(sessionInfo.validTill, forKey: kSessionExpireTSKey)
    }
}
