//
//  CartModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APICart: APIBaseRequestResponse {
    
    override func route() -> String {
        let cartGet = "cart/get"
        let cartRoute = MedicineFlowManager.shared.flow == .universalCart ? cartGet: "\(cartGet)?cart_id=\(MedicineFlowManager.shared.cartID)"
        return cartRoute
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CartModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIRemoveItemFromCart: APIBaseRequestResponse {
    
    var productCode: String = ""
    var quantity: String = ""
    
    override func route() -> String {
        return "cart/remove_item?product_code=\(productCode)&qty=\(quantity)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIAddItemToCart: APIBaseRequestResponse {
    
    var productCode: String = ""
    var quantity: String = ""
    
    override func route() -> String {
        return "cart/add_item?product_code=\(productCode)&qty=\(quantity)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIDeliveryEstimation: APIBaseRequestResponse {
    
    var params:  JSONDictAny?
    override func route() -> String {
        return ConfigManager.shared.configDetails?.deliveryDateAPI ?? ""
    }
    
    override func parameters() -> JSONDictAny? {
        return params
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return parseDeliveryEstimationInfo(jsonResult: jsonResult)
    }
    
    private func parseDeliveryEstimationInfo(jsonResult: JSONDictAny) -> [DeliveryEstimationModel]? {
        var cartDeliveryInfo: JSONArrayOfDictString = JSONArrayOfDictString()
        if let status = jsonResult["Status"] as? Bool, status {
            if let mainResult = jsonResult["Result"] as? JSONDictAny {
                if let subResults = mainResult["result"] as? JSONArrayOfDictAny {
                    for subResult in subResults {
                        var productsCode: [String] = [String]()
                        if let products = subResult["products"] as? [String] {
                            for product in products {
                                productsCode.append(product)
                            }
                            if let productDetails = subResult["product_details"] as? JSONDictAny {
                                for productCode in productsCode {
                                    var deliveryInfo: JSONDictString = JSONDictString()
                                    if let productsInfo = productDetails[productCode] as? [Any], let firstProductInfo = productsInfo.first as? JSONDictAny {
                                        if let deliveryEstimate = subResult["delivery_estimate"] as? JSONDictAny, let dates = deliveryEstimate["in_dates"] as? JSONDictAny, let fromDate = dates["on_or_after"] as? String, let toDate = dates["on_or_before"] as? String {
                                            deliveryInfo["deliveryDate"] = fromDate.deliveryDateFormat(toDate: toDate)
                                        }
                                        deliveryInfo["sellerName"] = subResult["fc_name"] as? String ?? ""
                                        deliveryInfo["sellerAddress"] = subResult["fc_address"] as? String ?? ""
                                        if let expiryDate = firstProductInfo["expiry_date"] as? String {
                                            deliveryInfo["expiryDate"] = expiryDate.getExpiryDate()
                                        }
                                        deliveryInfo["productCode"] = productCode
                                        cartDeliveryInfo.append(deliveryInfo)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        guard cartDeliveryInfo.count > 0 else { return nil }
        return [DeliveryEstimationModel].decode(data: cartDeliveryInfo.dataWithJSONObject())
    }
}

struct CartModel: Codable {
    var status: String?
    var result: CartResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(CartResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct CartResultModel: Codable {
    var cart: CartInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case cart
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cart = (try? values.decode(CartInfoModel.self, forKey: .cart)) ?? nil
        } catch {}
    }
}

struct CartInfoModel: Codable {
    var id: Int?
    var cartStatus: String?
    var appliedCouponName: String?
    var netPayable: Double?
    var subTotal: Double?
    var lines: [CartLineInfoModel]?
    var shippingDiscount: Double?
    var shippingCharges: Double?
    var productDiscountTotal: Double?
    var couponDiscountTotal: Double?
    var usedVoucherAmount: Double?
    var totalSavings: Double?
    var usedWalletAmounts: UsedWalletAmountsModel?
    var billingAddressID: Int?
    var shippingAddressID: Int?
    var isUsedSuperCash: Bool?
    var prescriptions: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case id, lines, prescriptions
        case cartStatus = "cart_status"
        case appliedCouponName = "applied_coupons"
        case netPayable = "net_payable"
        case subTotal = "sub_total"
        case shippingDiscount = "shipping_charges_original"
        case shippingCharges = "shipping_charges_final"
        case productDiscountTotal = "product_discount_total"
        case couponDiscountTotal = "coupon_discount_total"
        case usedVoucherAmount = "used_voucher_amount"
        case totalSavings = "total_savings"
        case usedWalletAmounts = "used_wallet_amounts"
        case billingAddressID = "billing_address_id"
        case shippingAddressID = "shipping_address_id"
        case isUsedSuperCash = "use_super_cash"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            lines = (try? values.decode([CartLineInfoModel].self, forKey: .lines)) ?? nil
            cartStatus = (try? values.decode(String.self, forKey: .cartStatus)) ?? ""
            appliedCouponName = (try? values.decode(String.self, forKey: .appliedCouponName)) ?? ""
            netPayable = (try? values.decode(Double.self, forKey: .netPayable)) ?? 0
            subTotal = (try? values.decode(Double.self, forKey: .subTotal)) ?? 0
            shippingDiscount = (try? values.decode(Double.self, forKey: .shippingDiscount)) ?? 0
            shippingCharges = (try? values.decode(Double.self, forKey: .shippingCharges)) ?? 0
            productDiscountTotal = (try? values.decode(Double.self, forKey: .productDiscountTotal)) ?? 0
            couponDiscountTotal = (try? values.decode(Double.self, forKey: .couponDiscountTotal)) ?? 0
            usedVoucherAmount = (try? values.decode(Double.self, forKey: .usedVoucherAmount)) ?? 0
            totalSavings = (try? values.decode(Double.self, forKey: .totalSavings)) ?? 0
            usedWalletAmounts = (try? values.decode(UsedWalletAmountsModel.self, forKey: .usedWalletAmounts)) ?? nil
            billingAddressID = (try? values.decode(Int.self, forKey: .billingAddressID)) ?? 0
            shippingAddressID = (try? values.decode(Int.self, forKey: .shippingAddressID)) ?? 0
            isUsedSuperCash = (try? values.decode(Bool.self, forKey: .isUsedSuperCash)) ?? false
            prescriptions = (try? values.decode([String].self, forKey: .prescriptions)) ?? nil
        } catch {}
    }
}

struct CartLineInfoModel: Codable {
    var cartID: Int?
    var discount: Double?
    var ext: String?
    var mrp: Double?
    var productCode: Int?
    var productSchedule: String?
    var quantity: Int?
    var sellingPrice: Double?
    var availabilityStatus: String?
    var displayName: String?
    var formulationType: String?
    var isColdStorage: Bool?
    var manufacturerName: String?
    var maxQtyInOrder: Int?
    var packSize: String?
    var productImagePath: String?
    var isRxRequired: Bool?
    var schedule: String?
    var stockQty: Int?
    var urlPath: String?
    var isOutOfStock: Bool?
    var unitProductDiscount: Double?
    var lineProductDiscount: Double?
    var unitCouponDiscount: Double?
    var lineCouponDiscount: Double?
    var rate: Double?
    var lineValue: Double?
    var lineMRP: Double?
    var productType: String?
    
    private enum CodingKeys: String, CodingKey {
        case cartID = "cart_id"
        case discount, ext, mrp, schedule, rate
        case productCode = "product_code"
        case productSchedule = "product_schedule"
        case quantity = "qty"
        case sellingPrice = "selling_price"
        case availabilityStatus = "availability_status"
        case displayName = "display_name"
        case formulationType = "formulation_type"
        case isColdStorage = "is_cold_storage"
        case manufacturerName = "manufacturer_name"
        case maxQtyInOrder = "max_qty_in_order"
        case packSize = "pack_size"
        case productImagePath = "product_image_path"
        case isRxRequired = "rx_required"
        case stockQty = "stock_qty"
        case urlPath = "url_path"
        case isOutOfStock = "out_of_stock"
        case unitProductDiscount = "unit_product_discount"
        case lineProductDiscount = "line_product_discount"
        case unitCouponDiscount = "unit_coupon_discount"
        case lineCouponDiscount = "line_coupon_discount"
        case lineValue = "line_value"
        case lineMRP = "line_mrp"
        case productType = "product_type"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cartID = (try? values.decode(Int.self, forKey: .cartID)) ?? 0
            discount = (try? values.decode(Double.self, forKey: .discount)) ?? 0
            ext = (try? values.decode(String.self, forKey: .ext)) ?? ""
            mrp = (try? values.decode(Double.self, forKey: .mrp)) ?? 0
            productCode = (try? values.decode(Int.self, forKey: .productCode)) ?? 0
            productSchedule = (try? values.decode(String.self, forKey: .productSchedule)) ?? ""
            quantity = (try? values.decode(Int.self, forKey: .quantity)) ?? 0
            sellingPrice = (try? values.decode(Double.self, forKey: .sellingPrice)) ?? 0
            availabilityStatus = (try? values.decode(String.self, forKey: .availabilityStatus)) ?? ""
            displayName = (try? values.decode(String.self, forKey: .displayName)) ?? ""
            formulationType = (try? values.decode(String.self, forKey: .formulationType)) ?? ""
            isColdStorage = (try? values.decode(Bool.self, forKey: .isColdStorage)) ?? false
            manufacturerName = (try? values.decode(String.self, forKey: .manufacturerName)) ?? ""
            maxQtyInOrder = (try? values.decode(Int.self, forKey: .maxQtyInOrder)) ?? 0
            packSize = (try? values.decode(String.self, forKey: .packSize)) ?? ""
            productImagePath = (try? values.decode(String.self, forKey: .productImagePath)) ?? ""
            isRxRequired = (try? values.decode(Bool.self, forKey: .isRxRequired)) ?? false
            schedule = (try? values.decode(String.self, forKey: .schedule)) ?? ""
            stockQty = (try? values.decode(Int.self, forKey: .stockQty)) ?? 0
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            isOutOfStock = (try? values.decode(Bool.self, forKey: .isOutOfStock)) ?? false
            unitProductDiscount = (try? values.decode(Double.self, forKey: .unitProductDiscount)) ?? 0
            lineProductDiscount = (try? values.decode(Double.self, forKey: .lineProductDiscount)) ?? 0
            unitCouponDiscount = (try? values.decode(Double.self, forKey: .unitCouponDiscount)) ?? 0
            lineCouponDiscount = (try? values.decode(Double.self, forKey: .lineCouponDiscount)) ?? 0
            lineValue = (try? values.decode(Double.self, forKey: .lineValue)) ?? 0
            lineMRP = (try? values.decode(Double.self, forKey: .lineMRP)) ?? 0
            productType = (try? values.decode(String.self, forKey: .productType)) ?? ""
        } catch {}
    }
}

struct UsedWalletAmountsModel: Codable {
    var nmsCash: Double?
    var walletCash: Double?
    var superCash: Double?
    var total: Double?
    
    private enum CodingKeys: String, CodingKey {
        case nmsCash = "nms_cash"
        case walletCash = "wallet_cash"
        case superCash = "super_cash"
        case total
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            nmsCash = (try? values.decode(Double.self, forKey: .nmsCash)) ?? 0
            walletCash = (try? values.decode(Double.self, forKey: .walletCash)) ?? 0
            superCash = (try? values.decode(Double.self, forKey: .superCash)) ?? 0
            total = (try? values.decode(Double.self, forKey: .total)) ?? 0
        } catch {}
    }
}

struct SuccessModel: Codable {
    var status: String?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct DeliveryEstimationModel: Codable {
    var productCode, sellerName, expiryDate, deliveryDate, sellerAddress: String?
    
    private enum CodingKeys: String, CodingKey {
        case productCode, sellerName, expiryDate, deliveryDate, sellerAddress
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            productCode = (try? values.decode(String.self, forKey: .productCode)) ?? ""
            sellerName = (try? values.decode(String.self, forKey: .sellerName)) ?? ""
            expiryDate = (try? values.decode(String.self, forKey: .expiryDate)) ?? ""
            deliveryDate = (try? values.decode(String.self, forKey: .deliveryDate)) ?? ""
            sellerAddress = (try? values.decode(String.self, forKey: .sellerAddress)) ?? ""
        } catch {}
    }
}

