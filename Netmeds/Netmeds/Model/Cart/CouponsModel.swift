//
//  CouponsModel.swift
//  Netmeds
//
//  Created by Netmedsian on 05/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIWalletBalance: APIBaseRequestResponse {
    
    override func route() -> String {
        return "cart/get_wallet_balances"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return WalletBalanceModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIAllCoupons: APIBaseRequestResponse {
    
    override func route() -> String {
        return "api/v1/offers/couponlist"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return AllCouponsInfoModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIApplyCouponCode: APIBaseRequestResponse {
    
    var couponCode: String = ""

    override func route() -> String {
        return "cart/apply_coupon?coupon_code=\(couponCode)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIUnApplyCouponCode: APIBaseRequestResponse {
    
    var couponCode: String = ""
    
    override func route() -> String {
        return "cart/unapply_coupon?coupon_code=\(couponCode)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIApplySuperCash: APIBaseRequestResponse {
    
    override func route() -> String {
        return "cart/apply_wallet?super_cash=y"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return SuccessModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct WalletBalanceModel: Codable {
    var status: String?
    var result: WalletBalanceInfoModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(WalletBalanceInfoModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct WalletBalanceInfoModel: Codable {
    var walletBalance, cashbackBalance, cashbackUseableBalance: Double?
    var useableBalanceBreakup: [UseableBalanceBreakupModel]?

    public enum CodingKeys: String, CodingKey {
        case walletBalance = "wallet_balance"
        case cashbackBalance = "cashback_balance"
        case cashbackUseableBalance = "cashback_useable_balance"
        case useableBalanceBreakup = "useable_balance_breakup"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            walletBalance = (try? values.decode(Double.self, forKey: .walletBalance)) ?? 0
            cashbackBalance = (try? values.decode(Double.self, forKey: .cashbackBalance)) ?? 0
            cashbackUseableBalance = (try? values.decode(Double.self, forKey: .cashbackUseableBalance)) ?? 0
            useableBalanceBreakup = (try? values.decode([UseableBalanceBreakupModel].self, forKey: .useableBalanceBreakup)) ?? nil
        } catch {}
    }
}

struct UseableBalanceBreakupModel: Codable {
    var balanceType: String?
    var balanceID: Int?
    var balanceAmount: Double?

    public enum CodingKeys: String, CodingKey {
        case balanceType = "balance_type"
        case balanceID = "balance_id"
        case balanceAmount = "balance_amt"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            balanceType = (try? values.decode(String.self, forKey: .balanceType)) ?? ""
            balanceID = (try? values.decode(Int.self, forKey: .balanceID)) ?? 0
            balanceAmount = (try? values.decode(Double.self, forKey: .balanceAmount)) ?? 0
        } catch {}
    }
}

struct AllCouponsInfoModel: Codable {
    var status: String?
    var result: AllCouponsResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(AllCouponsResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct AllCouponsResultModel: Codable {
    var couponList: [AllCouponsModel]?
    var superCash: AllCouponsModel?
    
    public enum CodingKeys: String, CodingKey {
        case couponList,superCash
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            couponList = (try? values.decode([AllCouponsModel].self, forKey: .couponList)) ?? nil
            superCash = (try? values.decode(AllCouponsModel.self, forKey: .superCash)) ?? nil
        } catch {}
    }
}

struct AllCouponsModel: Codable {
    var couponCode, couponDesc, productType, discount: String?
    var minOrderValue, maxOrderValue, activeStatus, activeFrom: String?
    var activeTo, position: String?
//    var isSelected: Bool = false
    
    public enum CodingKeys: String, CodingKey {
        case couponCode, couponDesc, productType, discount
        case minOrderValue, maxOrderValue, activeStatus, activeFrom
        case activeTo, position
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            couponCode = (try? values.decode(String.self, forKey: .couponCode)) ?? ""
            couponDesc = (try? values.decode(String.self, forKey: .couponDesc)) ?? ""
            productType = (try? values.decode(String.self, forKey: .productType)) ?? ""
            discount = (try? values.decode(String.self, forKey: .discount)) ?? ""
            minOrderValue = (try? values.decode(String.self, forKey: .minOrderValue)) ?? ""
            maxOrderValue = (try? values.decode(String.self, forKey: .maxOrderValue)) ?? ""
            activeStatus = (try? values.decode(String.self, forKey: .activeStatus)) ?? ""
            activeFrom = (try? values.decode(String.self, forKey: .activeFrom)) ?? ""
            activeTo = (try? values.decode(String.self, forKey: .activeTo)) ?? ""
            position = (try? values.decode(String.self, forKey: .position)) ?? ""
        } catch {}
    }
}
