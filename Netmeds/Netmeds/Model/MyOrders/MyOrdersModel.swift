//
//  MyOrdersModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 08/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIMyOrders: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "/api/v1/myorders/getmainorders"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }

    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return MyOrdersModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct MyOrdersModel: Codable {
    var status: String?
    var result: MyOrdersResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(MyOrdersResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct MyOrdersResultModel: Codable {
    var orderList: [MyOrdersListModel]?
    var statusList: [MyOrdersStatusListModel]?
    
    public enum CodingKeys: String, CodingKey {
        case orderList, statusList
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderList = (try? values.decode([MyOrdersListModel].self, forKey: .orderList)) ?? nil
            statusList = (try? values.decode([MyOrdersStatusListModel].self, forKey: .statusList)) ?? nil
        } catch {}
    }
}

struct MyOrdersListModel: Codable {
    var orderID: String?
    var name: String?
    var purchasedDate: String?
    var orderAmount: Double?
    var orderStatus: String?
    var statusDesc, displayStatus: String?
    var statusColor: String?
    var item: [String]?
    var items: [MyOrdersListItemsModel]?
    var itemCount: Int?
    var isPrimeMembershipOrder, isPrimeCustomerOrder: Bool?
    
    public enum CodingKeys: String, CodingKey {
        case name, purchasedDate, orderAmount, orderStatus, displayStatus, statusColor, item, items, itemCount, isPrimeMembershipOrder, isPrimeCustomerOrder
        case orderID = "orderId"
        case statusDesc = "statusDescription"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            purchasedDate = (try? values.decode(String.self, forKey: .purchasedDate)) ?? ""
            orderAmount = (try? values.decode(Double.self, forKey: .orderAmount)) ?? 0
            orderStatus = (try? values.decode(String.self, forKey: .orderStatus)) ?? ""
            statusDesc = (try? values.decode(String.self, forKey: .statusDesc)) ?? ""
            displayStatus = (try? values.decode(String.self, forKey: .displayStatus)) ?? ""
            statusColor = (try? values.decode(String.self, forKey: .statusColor)) ?? ""
            item = (try? values.decode([String].self, forKey: .item)) ?? []
            items = (try? values.decode([MyOrdersListItemsModel].self, forKey: .items)) ?? nil
            itemCount = (try? values.decode(Int.self, forKey: .itemCount)) ?? 0
            isPrimeMembershipOrder = (try? values.decode(Bool.self, forKey: .isPrimeMembershipOrder)) ?? false
            isPrimeCustomerOrder = (try? values.decode(Bool.self, forKey: .isPrimeCustomerOrder)) ?? false
        } catch {}
    }
}

struct MyOrdersListItemsModel: Codable {
    var sku: Int?
    var brandName: String?
    
    public enum CodingKeys: String, CodingKey {
        case sku, brandName
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            sku = (try? values.decode(Int.self, forKey: .sku)) ?? 0
            brandName = (try? values.decode(String.self, forKey: .brandName)) ?? ""
        } catch {}
    }
}

struct MyOrdersStatusListModel: Codable {
    var value: String?
    var title: String?
    var count: String?
    
    public enum CodingKeys: String, CodingKey {
        case value, title, count
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            value = (try? values.decode(String.self, forKey: .value)) ?? ""
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            count = (try? values.decode(String.self, forKey: .count)) ?? ""
        } catch {}
    }
}
