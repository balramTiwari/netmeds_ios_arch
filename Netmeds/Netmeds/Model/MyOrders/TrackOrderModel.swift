//
//  TrackOrderModel.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APITrackOrderDetails: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/myorders/getordertracking"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }

    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return TrackOrderInfoModel.decode(data: jsonResult.dataWithJSONObject())
    }
}


struct TrackOrderInfoModel: Codable {
    var status: String?
    var result: TrackOrderResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(TrackOrderResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct TrackOrderResultModel: Codable {
    var orderTracking: TrackOrderModel?
    
    public enum CodingKeys: String, CodingKey {
        case orderTracking
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderTracking = (try? values.decode(TrackOrderModel.self, forKey: .orderTracking)) ?? nil
        } catch {}
    }
}

struct TrackOrderModel: Codable {
    var orderID, paymentMode, orderDate, orderStatusCode, orderStatus, promisedDeliveryDate, expectedDeliveryDate: String?
    var isCancelStatus: Bool?
    var orderBillingInfo: MyOrderBillingInfoModel?
    var trackDetails: [TrackOrderDetailsModel]?

    public enum CodingKeys: String, CodingKey {
        case paymentMode, orderDate, orderStatusCode, orderStatus, promisedDeliveryDate
        case orderBillingInfo
        case trackDetails
        case orderID = "orderId"
        case expectedDeliveryDate = "expectedDeliveryDateString"
        case isCancelStatus = "cancelStatus"

    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            paymentMode = (try? values.decode(String.self, forKey: .paymentMode)) ?? ""
            orderDate = (try? values.decode(String.self, forKey: .orderDate)) ?? ""
            orderStatusCode = (try? values.decode(String.self, forKey: .orderStatusCode)) ?? ""
            orderStatus = (try? values.decode(String.self, forKey: .orderStatus)) ?? ""
            promisedDeliveryDate = (try? values.decode(String.self, forKey: .promisedDeliveryDate)) ?? ""
            expectedDeliveryDate = (try? values.decode(String.self, forKey: .expectedDeliveryDate)) ?? ""
            isCancelStatus = (try? values.decode(Bool.self, forKey: .isCancelStatus)) ?? false
            orderBillingInfo = (try? values.decode(MyOrderBillingInfoModel.self, forKey: .orderBillingInfo)) ?? nil
            trackDetails = (try? values.decode([TrackOrderDetailsModel].self, forKey: .trackDetails)) ?? nil

        } catch {}
    }
}

struct TrackOrderDetailsModel: Codable {
    var orderID, shipLocation, drugStatus, drugStatusCode, productCount: String?
    var trackingNumber, trackingURL, trackingCompany, clickPostID, trackShortStatus: String?
    var trackStatusDetails: [TrackOrderStatusDetailModel]?

    public enum CodingKeys: String, CodingKey {
        case shipLocation, drugStatus, drugStatusCode, productCount
        case trackingNumber, trackingCompany, trackShortStatus
        case trackStatusDetails
        case orderID = "orderId"
        case trackingURL = "trackingUrl"
        case clickPostID = "clickPostId"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            shipLocation = (try? values.decode(String.self, forKey: .shipLocation)) ?? ""
            drugStatus = (try? values.decode(String.self, forKey: .drugStatus)) ?? ""
            drugStatusCode = (try? values.decode(String.self, forKey: .drugStatusCode)) ?? ""
            productCount = (try? values.decode(String.self, forKey: .productCount)) ?? ""
            trackingNumber = (try? values.decode(String.self, forKey: .trackingNumber)) ?? ""
            trackingURL = (try? values.decode(String.self, forKey: .trackingURL)) ?? ""
            trackingCompany = (try? values.decode(String.self, forKey: .trackingCompany)) ?? ""
            clickPostID = (try? values.decode(String.self, forKey: .clickPostID)) ?? ""
            trackShortStatus = (try? values.decode(String.self, forKey: .trackShortStatus)) ?? ""
            trackStatusDetails = (try? values.decode([TrackOrderStatusDetailModel].self, forKey: .trackStatusDetails)) ?? nil

        } catch {}
    }
}

struct TrackOrderStatusDetailModel: Codable {
    var date, shortStatus, status: String?

    public enum CodingKeys: String, CodingKey {
        case date, shortStatus, status
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            date = (try? values.decode(String.self, forKey: .date)) ?? ""
            shortStatus = (try? values.decode(String.self, forKey: .shortStatus)) ?? ""
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
        } catch {}
    }
}
