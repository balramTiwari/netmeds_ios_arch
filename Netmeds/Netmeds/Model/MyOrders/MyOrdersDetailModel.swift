//
//  MyOrdersDetailModel.swift
//  Netmeds
//
//  Created by Netmedsian on 11/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIMyOrdersDetail: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "/api/v1/myorders/getorderdetails"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }

    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return MyOrdersDetailInfoModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct MyOrdersDetailInfoModel: Codable {
    var status: String?
    var result: MyOrdersDetailResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(MyOrdersDetailResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct MyOrdersDetailResultModel: Codable {
    var orderDetailsData: MyOrdersDetailDataModel?
    
    public enum CodingKeys: String, CodingKey {
        case orderDetailsData = "orderDetails"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderDetailsData = (try? values.decode(MyOrdersDetailDataModel.self, forKey: .orderDetailsData)) ?? nil
        } catch {}
    }
}

struct MyOrdersDetailDataModel: Codable {
    var orderID, paymentMode, orderDate, orderStatusCode, orderStatus: String?
    var expectedDeliveryDate, promisedDeliveryDate, expressStatus: String?
    var isPrimeMembershipOrder, isPrimeCustomerOrder, isCancelStatus: Bool?
    var orderBillingInfo: MyOrderBillingInfoModel?
    var orderDetail: [MyOrdersDetailModel]?
    var cancelledItems: [MyOrdersCancelledItemsModel]?
    var productAmount, couponDiscount, shippingAmount, codCharge: Double?
    var totalAmount, voucherAmount, usedWalletAmount, transactionAmount: Double?
    var discountAmount, savingsAmount: Double?
    var rxIDs: [String]?

    public enum CodingKeys: String, CodingKey {
        case paymentMode, orderDate, orderStatusCode, orderStatus
        case expectedDeliveryDate, promisedDeliveryDate, expressStatus
        case isPrimeMembershipOrder, isPrimeCustomerOrder
        case orderBillingInfo
        case productAmount, couponDiscount, shippingAmount, codCharge
        case totalAmount, voucherAmount, usedWalletAmount, transactionAmount
        case discountAmount, savingsAmount
        case orderID = "orderId"
        case isCancelStatus = "cancelStatus"
        case orderDetail = "orderdetail"
        case rxIDs = "rxId"
        case cancelledItems = "canceledItemsDetails"

    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            paymentMode = (try? values.decode(String.self, forKey: .paymentMode)) ?? ""
            orderDate = (try? values.decode(String.self, forKey: .orderDate)) ?? ""
            orderStatusCode = (try? values.decode(String.self, forKey: .orderStatusCode)) ?? ""
            orderStatus = (try? values.decode(String.self, forKey: .orderStatus)) ?? ""
            expectedDeliveryDate = (try? values.decode(String.self, forKey: .expectedDeliveryDate)) ?? ""
            promisedDeliveryDate = (try? values.decode(String.self, forKey: .promisedDeliveryDate)) ?? ""
            expressStatus = (try? values.decode(String.self, forKey: .expressStatus)) ?? ""
            isPrimeMembershipOrder = (try? values.decode(Bool.self, forKey: .isPrimeMembershipOrder)) ?? false
            isPrimeCustomerOrder = (try? values.decode(Bool.self, forKey: .isPrimeCustomerOrder)) ?? false
            isCancelStatus = (try? values.decode(Bool.self, forKey: .isCancelStatus)) ?? false
            orderBillingInfo = (try? values.decode(MyOrderBillingInfoModel.self, forKey: .orderBillingInfo)) ?? nil
            orderDetail = (try? values.decode([MyOrdersDetailModel].self, forKey: .orderDetail)) ?? nil
            cancelledItems = (try? values.decode([MyOrdersCancelledItemsModel].self, forKey: .cancelledItems)) ?? nil
            productAmount = (try? values.decode(Double.self, forKey: .productAmount)) ?? 0.0
            couponDiscount = (try? values.decode(Double.self, forKey: .couponDiscount)) ?? 0.0
            shippingAmount = (try? values.decode(Double.self, forKey: .shippingAmount)) ?? 0.0
            codCharge = (try? values.decode(Double.self, forKey: .codCharge)) ?? 0.0
            totalAmount = (try? values.decode(Double.self, forKey: .totalAmount)) ?? 0.0
            voucherAmount = (try? values.decode(Double.self, forKey: .voucherAmount)) ?? 0.0
            usedWalletAmount = (try? values.decode(Double.self, forKey: .usedWalletAmount)) ?? 0.0
            transactionAmount = (try? values.decode(Double.self, forKey: .transactionAmount)) ?? 0.0
            discountAmount = (try? values.decode(Double.self, forKey: .discountAmount)) ?? 0.0
            savingsAmount = (try? values.decode(Double.self, forKey: .savingsAmount)) ?? 0.0
            rxIDs = (try? values.decode([String].self, forKey: .rxIDs)) ?? []
        } catch {}
    }
}

struct MyOrderBillingInfoModel: Codable {
    var firstName, lastName, address1, address2: String?
    var city, zipCode, state, country, phoneNumber: String?

    public enum CodingKeys: String, CodingKey {
        case firstName = "billingFirstName"
        case lastName = "billingLastName"
        case address1 = "billingAddress1"
        case address2 = "billingAddress2"
        case city = "billingCity"
        case zipCode = "billingZipCode"
        case state = "billingState"
        case country = "billingCountry"
        case phoneNumber
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            firstName = (try? values.decode(String.self, forKey: .firstName)) ?? ""
            lastName = (try? values.decode(String.self, forKey: .lastName)) ?? ""
            address1 = (try? values.decode(String.self, forKey: .address1)) ?? ""
            address2 = (try? values.decode(String.self, forKey: .address2)) ?? ""
            city = (try? values.decode(String.self, forKey: .city)) ?? ""
            zipCode = (try? values.decode(String.self, forKey: .zipCode)) ?? ""
            state = (try? values.decode(String.self, forKey: .state)) ?? ""
            country = (try? values.decode(String.self, forKey: .country)) ?? ""
            phoneNumber = (try? values.decode(String.self, forKey: .phoneNumber)) ?? ""
        } catch {}
    }
}

struct MyOrdersDetailModel: Codable {
    var orderID, shipLocation, drugStatus, drugStatusCode, displayStatus: String?
    var trackingNumber, trackingURL, trackingCompany, clickPostID: String?
    var promisedDeliveryDateFrom, promisedDeliveryDate, trackShortStatus: String?
    var drugDetails: [MyOrdersDrugDetailsModel]?
    var trackStatus: [MyOrdersTrackStatusModel]?

    public enum CodingKeys: String, CodingKey {
        case shipLocation, drugStatus, drugStatusCode, displayStatus
        case trackingNumber, trackingCompany, drugDetails, trackStatus
        case promisedDeliveryDateFrom, promisedDeliveryDate, trackShortStatus
        case orderID = "orderId"
        case trackingURL = "trackingUrl"
        case clickPostID = "clickPostId"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            shipLocation = (try? values.decode(String.self, forKey: .shipLocation)) ?? ""
            drugStatus = (try? values.decode(String.self, forKey: .drugStatus)) ?? ""
            drugStatusCode = (try? values.decode(String.self, forKey: .drugStatusCode)) ?? ""
            displayStatus = (try? values.decode(String.self, forKey: .displayStatus)) ?? ""
            trackingNumber = (try? values.decode(String.self, forKey: .trackingNumber)) ?? ""
            trackingURL = (try? values.decode(String.self, forKey: .trackingURL)) ?? ""
            trackingCompany = (try? values.decode(String.self, forKey: .trackingCompany)) ?? ""
            clickPostID = (try? values.decode(String.self, forKey: .clickPostID)) ?? ""
            promisedDeliveryDateFrom = (try? values.decode(String.self, forKey: .promisedDeliveryDateFrom)) ?? ""
            promisedDeliveryDate = (try? values.decode(String.self, forKey: .promisedDeliveryDate)) ?? ""
            trackShortStatus = (try? values.decode(String.self, forKey: .trackShortStatus)) ?? ""
            drugDetails = (try? values.decode([MyOrdersDrugDetailsModel].self, forKey: .drugDetails)) ?? nil
            trackStatus = (try? values.decode([MyOrdersTrackStatusModel].self, forKey: .trackStatus)) ?? nil
        } catch {}
    }
}

struct MyOrdersDrugDetailsModel: Codable {
    var genericName, brandName, productStauts, pageType, prescriptionNeeded: String?
    var additionalShippingChargers, subscriptionDiscountStatus: String?
    var drugCode, purchaseQuantity, minimumQuantity, alternativeDrugCode: Int?
    var purchasePrice, productDiscount, couponDiscount, discount: Double?

    public enum CodingKeys: String, CodingKey {
        case genericName, brandName, productStauts, pageType, prescriptionNeeded
        case additionalShippingChargers, subscriptionDiscountStatus
        case drugCode, purchaseQuantity, minimumQuantity, alternativeDrugCode
        case purchasePrice, productDiscount, couponDiscount, discount
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            genericName = (try? values.decode(String.self, forKey: .genericName)) ?? ""
            brandName = (try? values.decode(String.self, forKey: .brandName)) ?? ""
            productStauts = (try? values.decode(String.self, forKey: .productStauts)) ?? ""
            pageType = (try? values.decode(String.self, forKey: .pageType)) ?? ""
            prescriptionNeeded = (try? values.decode(String.self, forKey: .prescriptionNeeded)) ?? ""
            additionalShippingChargers = (try? values.decode(String.self, forKey: .additionalShippingChargers)) ?? ""
            subscriptionDiscountStatus = (try? values.decode(String.self, forKey: .subscriptionDiscountStatus)) ?? ""
            drugCode = (try? values.decode(Int.self, forKey: .drugCode)) ?? 0
            purchaseQuantity = (try? values.decode(Int.self, forKey: .purchaseQuantity)) ?? 0
            minimumQuantity = (try? values.decode(Int.self, forKey: .minimumQuantity)) ?? 0
            alternativeDrugCode = (try? values.decode(Int.self, forKey: .alternativeDrugCode)) ?? 0
            purchasePrice = (try? values.decode(Double.self, forKey: .purchasePrice)) ?? 0
            productDiscount = (try? values.decode(Double.self, forKey: .productDiscount)) ?? 0
            couponDiscount = (try? values.decode(Double.self, forKey: .couponDiscount)) ?? 0
            discount = (try? values.decode(Double.self, forKey: .discount)) ?? 0
        } catch {}
    }
}

struct MyOrdersTrackStatusModel: Codable {
    var status, shortStatus, date: String?

    public enum CodingKeys: String, CodingKey {
        case status, shortStatus, date
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            shortStatus = (try? values.decode(String.self, forKey: .shortStatus)) ?? ""
            date = (try? values.decode(String.self, forKey: .date)) ?? ""
        } catch {}
    }
}

struct MyOrdersCancelledItemsModel: Codable {
    var drugCode, drugName, orderID, quantity: String?
    var refundValue, mrp, statusDesc: String?

    public enum CodingKeys: String, CodingKey {
        case drugCode, drugName, quantity
        case refundValue, mrp
        case orderID = "orderId"
        case statusDesc = "statusDescription"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            drugCode = (try? values.decode(String.self, forKey: .drugCode)) ?? ""
            drugName = (try? values.decode(String.self, forKey: .drugName)) ?? ""
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            quantity = (try? values.decode(String.self, forKey: .quantity)) ?? ""
            refundValue = (try? values.decode(String.self, forKey: .refundValue)) ?? ""
            mrp = (try? values.decode(String.self, forKey: .mrp)) ?? ""
            statusDesc = (try? values.decode(String.self, forKey: .statusDesc)) ?? ""
        } catch {}
    }
}
