//
//  DownloadImageModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APIDownloadImage: APIBaseRequestResponse {
    
    var urlString: String = ""
    override func route() -> String {
        return urlString
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let data = response as? Data else { return nil }
        return data
    }
}
