//
//  GoogleAnalyticsModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 17/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

struct GoogleAnalytics {
    static func trackEvent(withScreen screen: String, category: String, label: String, action: String, value: Int? = nil) {
        guard
            let tracker = GAI.sharedInstance()?.tracker(withTrackingId:kGoogleAnalyticsTrackingId),
            let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: NSNumber(integerLiteral: value ?? 0))
            else { return }
        
        
        tracker.set(screen, value: screen)
        
        if let params = GAIDictionaryBuilder.createScreenView()?.build() as? [AnyHashable : Any] {
            tracker.send(params)
        }
        
        if let params = builder.build() as? [AnyHashable : Any] {
            tracker.send(params)
        }
    }
}
