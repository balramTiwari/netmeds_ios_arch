//
//  AllOffersModel.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIAllOffers: APIBaseRequestResponse {
    
    var pageID: String = ""
    
    override func route() -> String {
        return "api/v1/offers/getalloffers?id=\(pageID)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        guard let result = jsonResult["result"] as? JSONDictAny, let offersDetail = result["alloffers"] as? JSONArrayOfDictAny else { return nil }
        return [AllOffersModel].decode(data: offersDetail.dataWithJSONObject())
    }
}

struct AllOffersModel: Codable {
    var title: String?
    var couponDesc: String?
    var bannerImg: String?
    var couponCode: String?
    var eligibility: String?
    var howToGet: String?
    var conditions: String?
    var btnText: String?
    var image: String?
    var offerType: String?
    var pageID: String?
    var sectionType: String?
    var shopURL: String?
    var url: String?
    var endDate: String?
    var offerList: [AllOffersModel]?
    
    private enum CodingKeys: String, CodingKey {
        case title, bannerImg, couponCode, eligibility, howToGet, conditions, btnText, image, offerType, sectionType, url, endDate, offerList
        case couponDesc = "couponDescription"
        case pageID = "pageId"
        case shopURL = "shopUrl"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? values.decode(String.self, forKey: .title)) ?? ""
            couponDesc = (try? values.decode(String.self, forKey: .couponDesc)) ?? ""
            bannerImg = (try? values.decode(String.self, forKey: .bannerImg)) ?? ""
            couponCode = (try? values.decode(String.self, forKey: .couponCode)) ?? ""
            eligibility = (try? values.decode(String.self, forKey: .eligibility)) ?? ""
            howToGet = (try? values.decode(String.self, forKey: .howToGet)) ?? ""
            conditions = (try? values.decode(String.self, forKey: .conditions)) ?? ""
            btnText = (try? values.decode(String.self, forKey: .btnText)) ?? ""
            image = (try? values.decode(String.self, forKey: .image)) ?? ""
            offerType = (try? values.decode(String.self, forKey: .offerType)) ?? ""
            pageID = (try? values.decode(String.self, forKey: .pageID)) ?? ""
            sectionType = (try? values.decode(String.self, forKey: .sectionType)) ?? ""
            shopURL = (try? values.decode(String.self, forKey: .shopURL)) ?? ""
            url = (try? values.decode(String.self, forKey: .url)) ?? ""
            endDate = (try? values.decode(String.self, forKey: .endDate)) ?? ""
            offerList = (try? values.decode([AllOffersModel].self, forKey: .offerList)) ?? nil
        } catch {}
    }
}
