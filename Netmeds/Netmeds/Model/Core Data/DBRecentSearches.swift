//
//  DBRecentSearches.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation
import CoreData

class DBRecentSearches: NSManagedObject {
    @NSManaged var hitItem : [String: Any]
    @NSManaged var date : Date
    @NSManaged var productCode : String
}
