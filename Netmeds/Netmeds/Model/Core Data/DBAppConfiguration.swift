//
//  DBAppConfiguration.swift
//  Netmeds
//
//  Created by SANKARLAL on 27/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation
import CoreData

class DBAppConfiguration: NSManagedObject {
    @NSManaged var response : [String: Any]
}
