//
//  ProductDetailsModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APIProductDetails: APIBaseRequestResponse {
    
    var productID: Int = 0
    
    override func route() -> String {
        return "catalog/product/get/\(productID)?product_fieldset_name=all"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return ProductDetailsModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIBrainSinsInfo: APIBaseRequestResponse {
    var productID: Int = 0
    var brainsinsKey: String = ""
    var templateID: String = ""
    var customerID: String = ""
    
    override func route() -> String {
        return "idItem/\(productID)/idUser/\(customerID).json?token=\(brainsinsKey)&idRec=\(templateID)&lang=mobile"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny , let list = jsonResult["list"] as? [[String: Any]] else { return nil }
        return [BrainSinsModel].decode(data: list.dataWithJSONObject())
    }
}


struct ProductDetailsModel: Codable {
    var status: String?
    var result: ProductDetailsResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(ProductDetailsResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct ProductDetailsResultModel: Codable {
    var product: ProductsModel?
    
    public enum CodingKeys: String, CodingKey {
        case product
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            product = (try? values.decode(ProductsModel.self, forKey: .product)) ?? nil
        } catch {}
    }
}

struct ProductsModel: Codable {
    var availabilityStatus: String?
    var brandName: String?
    var categories: [ProductCategoriesModel]?
    var discount: Int?
    var displayName, formulationType: String?
    var generic: ProductGenericsModel?
    var genericID, genericWithDosageID: Int?
    var genericWithDosage: ProductGenericsModel?
    var imagePaths: [String]?
    var isColdStorage, isDpco: Bool?
    var manufacturerID: Int?
    var manufacturer: ProductGenericsModel?
    var brand: ProductGenericsModel?
    var manufacturerName, manufacturerURL: String?
    var maxQtyInOrder: Int?
    var packLabel: String?
    //    var multipleQtyRules: PackOfNRules?
    var packSize: String?
    var productCode: Int?
    var productType: String?
    var rxRequired: Bool?
    var schedule: String?
    var sellingPrice, mrp, discountRate: Double?
    var similarProducts: String?
    var stockQty: Int?
    var typeOfPackage: String?
    var urlPath: String?
    
    public enum CodingKeys: String, CodingKey {
        case availabilityStatus = "availability_status"
        case brandName = "brand_name"
        case discount, mrp, schedule
        case categories, generic, manufacturer, brand
        case displayName = "display_name"
        case formulationType = "formulation_type"
        case genericID = "generic_id"
        case genericWithDosageID = "generic_with_dosage_id"
        case imagePaths = "image_paths"
        case isColdStorage = "is_cold_storage"
        case isDpco = "is_dpco"
        case manufacturerID = "manufacturer_id"
        case manufacturerName = "manufacturer_name"
        case manufacturerURL = "manufacturer_url"
        case maxQtyInOrder = "max_qty_in_order"
        case packLabel = "pack_label"
        case packSize = "pack_size"
        case productCode = "product_code"
        case productType = "product_type"
        case rxRequired = "rx_required"
        case sellingPrice = "selling_price"
        case discountRate = "discount_rate"
        case similarProducts = "similar_products"
        case stockQty = "stock_qty"
        case typeOfPackage = "type_of_package"
        case urlPath = "url_path"
        case genericWithDosage = "generic_with_dosage"
        //        case multipleQtyRules = "multiple_qty_rules"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            availabilityStatus = (try? values.decode(String.self, forKey: .availabilityStatus)) ?? ""
            brandName = (try? values.decode(String.self, forKey: .brandName)) ?? ""
            discount = (try? values.decode(Int.self, forKey: .discount)) ?? 0
            displayName = (try? values.decode(String.self, forKey: .displayName)) ?? ""
            formulationType = (try? values.decode(String.self, forKey: .formulationType)) ?? ""
            genericID = (try? values.decode(Int.self, forKey: .genericID)) ?? 0
            genericWithDosageID = (try? values.decode(Int.self, forKey: .genericWithDosageID)) ?? 0
            imagePaths = (try? values.decode([String].self, forKey: .imagePaths)) ?? []
            isColdStorage = (try? values.decode(Bool.self, forKey: .isColdStorage)) ?? false
            isDpco = (try? values.decode(Bool.self, forKey: .isDpco)) ?? false
            manufacturerID = (try? values.decode(Int.self, forKey: .manufacturerID)) ?? 0
            manufacturerName = (try? values.decode(String.self, forKey: .manufacturerName)) ?? ""
            manufacturerURL = (try? values.decode(String.self, forKey: .manufacturerURL)) ?? ""
            maxQtyInOrder = (try? values.decode(Int.self, forKey: .maxQtyInOrder)) ?? 0
            packLabel = (try? values.decode(String.self, forKey: .packLabel)) ?? ""
            packSize = (try? values.decode(String.self, forKey: .packSize)) ?? ""
            productCode = (try? values.decode(Int.self, forKey: .productCode)) ?? 0
            productType = (try? values.decode(String.self, forKey: .productType)) ?? ""
            rxRequired = (try? values.decode(Bool.self, forKey: .rxRequired)) ?? false
            schedule = (try? values.decode(String.self, forKey: .schedule)) ?? ""
            sellingPrice = (try? values.decode(Double.self, forKey: .sellingPrice)) ?? 0.0
            mrp = (try? values.decode(Double.self, forKey: .mrp)) ?? 0.0
            discountRate = (try? values.decode(Double.self, forKey: .discountRate)) ?? 0.0
            similarProducts = (try? values.decode(String.self, forKey: .similarProducts)) ?? ""
            stockQty = (try? values.decode(Int.self, forKey: .stockQty)) ?? 0
            typeOfPackage = (try? values.decode(String.self, forKey: .typeOfPackage)) ?? ""
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            categories = (try? values.decode([ProductCategoriesModel].self, forKey: .categories)) ?? nil
            generic = (try? values.decode(ProductGenericsModel.self, forKey: .generic)) ?? nil
            genericWithDosage = (try? values.decode(ProductGenericsModel.self, forKey: .genericWithDosage)) ?? nil
            manufacturer = (try? values.decode(ProductGenericsModel.self, forKey: .manufacturer)) ?? nil
            brand = (try? values.decode(ProductGenericsModel.self, forKey: .brand)) ?? nil
        } catch {}
    }
}

struct ProductCategoriesModel: Codable {
    var isActive: Bool?
    var name: String?
    var id: Int?
    var breadCrumbs: [ProductBreadCrumbsModel]?
    var urlPath: String?
    
    public enum CodingKeys: String, CodingKey {
        case isActive = "is_active"
        case name,id
        case breadCrumbs = "bread_crumbs"
        case urlPath = "url_path"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            isActive = (try? values.decode(Bool.self, forKey: .isActive)) ?? false
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            breadCrumbs = (try? values.decode([ProductBreadCrumbsModel].self, forKey: .breadCrumbs)) ?? nil
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
        } catch {}
    }
}

struct ProductBreadCrumbsModel: Codable {
    var level: Int?
    var name: String?
    var id: Int?
    var urlPath: String?
    
    public enum CodingKeys: String, CodingKey {
        case level, name, id
        case urlPath = "url_path"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            level = (try? values.decode(Int.self, forKey: .level)) ?? 0
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            
        } catch {}
    }
}

struct ProductGenericsModel: Codable {
    var name,url: String?
    var id: Int?
    
    public enum CodingKeys: String, CodingKey {
        case name, url, id
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            url = (try? values.decode(String.self, forKey: .url)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
        } catch {}
    }
}

struct BrainSinsModel: Codable {
    var productCode: String?
    var displayName: String?
    var sellingPrice: String?
    var imageURL: String?
    var categories: String?
    var misc:String?
    var price: String? = ""

    private enum CodingKeys: String, CodingKey {
        case productCode = "id"
        case displayName = "name"
        case sellingPrice = "price"
        case imageURL = "imageUrl"
        case categories = "categories"
        case misc = "misc"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            productCode = (try? values.decodeIfPresent(String.self, forKey: .productCode)) ?? ""
            displayName = (try? values.decodeIfPresent(String.self, forKey: .displayName)) ?? ""
            sellingPrice = (try? values.decodeIfPresent(String.self, forKey: .sellingPrice)) ?? ""
            imageURL = (try? values.decodeIfPresent(String.self, forKey: .imageURL)) ?? ""
            categories = (try? values.decodeIfPresent(String.self, forKey: .categories)) ?? ""
            misc = (try? values.decodeIfPresent(String.self, forKey: .misc)) ?? ""
            if let dict = convertStringToDictionary(text: misc ?? "") {
                price = (dict["original_price"] as? String ?? "").replace(string: ",", replacement: "")
            }
        } catch {}
    }
    
   private func convertStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            return data.jsonObjectWithData() as?  [String: Any]
        }
        return [:]
    }
}
