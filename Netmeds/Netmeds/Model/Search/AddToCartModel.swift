//
//  AddToCartModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APICreateCart: APIBaseRequestResponse {
            
    override func route() -> String {
        return "cart/create"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return CreateCartModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIAddToCart: APIBaseRequestResponse {

    var productID: Int = 0
    var quantity: Int = 0
    var cartID: Int = 0
    
    override func route() -> String {
        let urlString: String = "cart/add_item?product_code=\(productID)&qty=\(quantity)"
        guard cartID > 0 else { return urlString }
        return "\(urlString)&cart_id=\(cartID)"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return AddToCartModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct CreateCartModel: Codable {
    var status: String?
    var result: CreateCartResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(CreateCartResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct CreateCartResultModel: Codable {
    var cartID: Int?
    
    public enum CodingKeys: String, CodingKey {
        case cartID = "cart_id"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cartID = (try? values.decode(Int.self, forKey: .cartID)) ?? 0
            UserDefaultsModel.setObject(cartID, forKey: kCartIDKey)
        } catch {}
    }
}

struct AddToCartModel: Codable {
    var status: String?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}
