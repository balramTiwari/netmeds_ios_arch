//
//  AlgoliaClientModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import InstantSearchClient


class APIAlgoliaSearch: APIBaseRequestResponse {
    
    var query: Query?
    var index: String = ""
    
    override func algoliaQuery() -> Query? {
        return query
    }
    
    override func algoliaIndex() -> String {
        return index
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return AlgoliaSearchModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct AlgoliaSearchModel: Codable {
    var hits: [AlgoliaSearchHitsModel]?
    var totalPage: Int?
    var queryID: String?
    var nbHits: Int?
    var facets: [String: [String: Int]]?
    private enum CodingKeys: String, CodingKey {
        case hits, queryID, nbHits, facets
        case totalPage = "nbPages"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            hits = (try? values.decode([AlgoliaSearchHitsModel].self, forKey: .hits)) ?? nil
            totalPage = (try? values.decode(Int.self, forKey: .totalPage)) ?? 0
            queryID = (try? values.decode(String.self, forKey: .queryID)) ?? ""
            nbHits = (try? values.decode(Int.self, forKey: .nbHits)) ?? 0
            facets = (try? values.decode([String: [String: Int]].self, forKey: .facets)) ?? nil
        } catch {}
    }
}

struct AlgoliaSearchHitsModel: Codable {
    var productCode: Int?
    var displayName: String?
    var manufacturerID: Int?
    var manufacturerName: String?
    var availabilityStatus: String?
    var schedule: String?
    var maxQtyInOrder: Int?
    var price: Float?
    var sellingPrice: Float?
    var inStock: Int?
    var productType: String?
    var formulationType: String?
    var brandID: Int?
    var brand: String?
    var urlPath: String?
    var imageURL: String?
    var thumbnailURL: String?
    var categories: [String]?
    var rxRequired: Int?
    var packLabel: String?
    
    private enum CodingKeys: String, CodingKey {
        case productCode = "product_code"
        case displayName = "display_name"
        case manufacturerID = "manufacturer_id"
        case manufacturerName = "manufacturer_name"
        case availabilityStatus = "availability_status"
        case schedule = "schedule"
        case maxQtyInOrder = "max_qty_in_order"
        case price = "mrp"
        case sellingPrice = "selling_price"
        case inStock = "in_stock"
        case productType = "product_type"
        case formulationType = "formulation_type"
        case brandID = "brand_id"
        case brand = "brand"
        case urlPath = "url_path"
        case imageURL = "image_url"
        case thumbnailURL = "thumbnail_url"
        case categories = "categories"
        case rxRequired = "rx_required"
        case packLabel = "pack_label"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            productCode = (try? values.decode(Int.self, forKey: .productCode)) ?? 0
            displayName = (try? values.decode(String.self, forKey: .displayName)) ?? ""
            manufacturerID = (try? values.decode(Int.self, forKey: .manufacturerID)) ?? 0
            manufacturerName = (try? values.decode(String.self, forKey: .manufacturerName)) ?? ""
            availabilityStatus = (try? values.decode(String.self, forKey: .availabilityStatus)) ?? ""
            schedule = (try? values.decode(String.self, forKey: .schedule)) ?? ""
            maxQtyInOrder = (try? values.decode(Int.self, forKey: .maxQtyInOrder)) ?? 0
            price = (try? values.decode(Float.self, forKey: .price)) ?? 0
            sellingPrice = (try? values.decode(Float.self, forKey: .sellingPrice)) ?? 0
            inStock = (try? values.decode(Int.self, forKey: .inStock)) ?? 0
            productType = (try? values.decode(String.self, forKey: .productType)) ?? ""
            formulationType = (try? values.decode(String.self, forKey: .formulationType)) ?? ""
            brandID = (try? values.decode(Int.self, forKey: .brandID)) ?? 0
            brand = (try? values.decode(String.self, forKey: .brand)) ?? ""
            urlPath = (try? values.decode(String.self, forKey: .urlPath)) ?? ""
            imageURL = (try? values.decode(String.self, forKey: .imageURL)) ?? ""
            thumbnailURL = (try? values.decode(String.self, forKey: .thumbnailURL)) ?? ""
            categories = (try? values.decode([String].self, forKey: .categories)) ?? [String]()
            rxRequired = (try? values.decode(Int.self, forKey: .rxRequired)) ?? 0
            packLabel = (try? values.decode(String.self, forKey: .packLabel)) ?? ""
        } catch {}
    }
}

//struct AlgoliaClientModel {
//    static func algoliaClientIndex(index: String = "") -> Index {
//        let client = Client(appID: "algoliaInfo.appId", apiKey: "algoliaInfo.apiKey")
//        return client.index(withName: "algoliaIndex")
//    }
//}
