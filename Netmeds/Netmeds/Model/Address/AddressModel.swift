//
//  AddressModel.swift
//  Netmeds
//
//  Created by Netmeds1 on 26/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation

class APIAllAddress: APIBaseRequestResponse {
    
    override func route() -> String {
        return "address/get/all"
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return AllAddressModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

struct AllAddressModel: Codable {
    var status: String?
    var result: AllAddressResultModel?
    var errorInfo: ErrorInfoModel?
    
    public enum CodingKeys: String, CodingKey {
        case status,result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(AllAddressResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct AllAddressResultModel: Codable {
    var addressList: [AllAddressInfoModel]?
    
    public enum CodingKeys: String, CodingKey {
        case addressList = "address_list"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            addressList = (try? values.decode([AllAddressInfoModel].self, forKey: .addressList)) ?? nil
        } catch {}
    }
}

struct AllAddressInfoModel: Codable {
    var firstName: String?
    var isActive: Bool?
    var city: String?
    var mobileNo: String?
    var lastName: String?
    var pinCode: String?
    var street: String?
    var name: String?
    var id: Int?
    var state: String?
    var customerID: Int?
    var landMark: String?
    var line1: String?
    var line3: String?
    var isSelected: Bool = false
    
    public enum CodingKeys: String, CodingKey {
        case city, street, name, id, state, line1, line3
        case firstName = "firstname"
        case isActive = "is_active"
        case mobileNo = "mobile_no"
        case lastName = "lastname"
        case pinCode = "pin"
        case customerID = "customer_id"
        case landMark = "landmark"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            firstName = (try? values.decode(String.self, forKey: .firstName)) ?? ""
            isActive = (try? values.decode(Bool.self, forKey: .isActive)) ?? false
            city = (try? values.decode(String.self, forKey: .city)) ?? ""
            mobileNo = (try? values.decode(String.self, forKey: .mobileNo)) ?? ""
            lastName = (try? values.decode(String.self, forKey: .lastName)) ?? ""
            pinCode = (try? values.decode(String.self, forKey: .pinCode)) ?? ""
            street = (try? values.decode(String.self, forKey: .street)) ?? ""
            name = (try? values.decode(String.self, forKey: .name)) ?? ""
            id = (try? values.decode(Int.self, forKey: .id)) ?? 0
            state = (try? values.decode(String.self, forKey: .state)) ?? ""
            customerID = (try? values.decode(Int.self, forKey: .customerID)) ?? 0
            landMark = (try? values.decode(String.self, forKey: .landMark)) ?? ""
            line1 = (try? values.decode(String.self, forKey: .line1)) ?? ""
            line3 = (try? values.decode(String.self, forKey: .line3)) ?? ""
        } catch {}
    }
    
    static func sequenceBySelected(lhs: AllAddressInfoModel, rhs: AllAddressInfoModel) -> Bool {
          return lhs.isSelected
      }
}
