//
//  JusPayModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

class APIJusPayOrderCreate: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/juspay/ordercreate"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return JusPayOrderCreateModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIJusPayOrderStatus: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/juspay/orderstatus"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return JusPayOrderStatusModel.decode(data: jsonResult.dataWithJSONObject())
    }
}

class APIJusPayOrderComplete: APIBaseRequestResponse {
    
    var multiPartParams: JSONArrayOfDictString?
    
    override func route() -> String {
        return "api/v1/completeorder/yourreceipt"
    }
    
    override func multiPartParameters() -> JSONArrayOfDictString? {
        return multiPartParams
    }
    
    override func parseResponse(response: Any?) -> Any? {
        guard let jsonResult = response as? JSONDictAny else { return nil }
        return jsonResult["status"]
    }
}

struct JusPayOrderCreateModel: Codable {
    var status: String?
    var result: JusPayOrderCreateResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(JusPayOrderCreateResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct JusPayOrderCreateResultModel: Codable {
    var phoneNumber, customerID, emailID: String?
    var amount: Double?
    var authInfo: JusPayOrderCreateAuthInfoModel?
    var orderID: String?
    var merchantID: String?
    var returnURL: String?
    var transactionID: String?
    var environment: String = "prod"//"sandbox"
    var service: String = "in.juspay.ec"
    var clientID: String = "netmeds_ios"
    var redirectAfterPayment: Bool = true
    
    private enum CodingKeys: String, CodingKey {
        case amount
        case phoneNumber = "customer_phone"
        case customerID = "customer_id"
        case emailID = "customer_email"
        case authInfo = "juspay"
        case orderID = "order_id"
        case merchantID = "merchant_id"
        case returnURL = "return_url"
        case transactionID = "id"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            amount = (try? values.decode(Double.self, forKey: .amount)) ?? 0
            phoneNumber = (try? values.decode(String.self, forKey: .phoneNumber)) ?? ""
            customerID = (try? values.decode(String.self, forKey: .customerID)) ?? ""
            emailID = (try? values.decode(String.self, forKey: .emailID)) ?? ""
            orderID = (try? values.decode(String.self, forKey: .orderID)) ?? ""
            merchantID = (try? values.decode(String.self, forKey: .merchantID)) ?? ""
            returnURL = (try? values.decode(String.self, forKey: .returnURL)) ?? ""
            transactionID = (try? values.decode(String.self, forKey: .transactionID)) ?? ""
            authInfo = (try? values.decode(JusPayOrderCreateAuthInfoModel.self, forKey: .authInfo)) ?? nil
        } catch {}
    }
}

struct JusPayOrderCreateAuthInfoModel: Codable {
    var authToken : String?
    
    private enum CodingKeys: String, CodingKey {
        case authToken = "client_auth_token"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            authToken = (try? values.decode(String.self, forKey: .authToken)) ?? ""
        } catch {}
    }
}

struct JusPayOrderStatusModel: Codable {
    var status: String?
    var result: JusPayOrderStatusResultModel?
    var errorInfo: ErrorInfoModel?
    
    private enum CodingKeys: String, CodingKey {
        case status, result
        case errorInfo = "reason"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            status = (try? values.decode(String.self, forKey: .status)) ?? ""
            result = (try? values.decode(JusPayOrderStatusResultModel.self, forKey: .result)) ?? nil
            errorInfo = (try? values.decode(ErrorInfoModel.self, forKey: .errorInfo)) ?? nil
        } catch {}
    }
}

struct JusPayOrderStatusResultModel: Codable {
    var paymentMethodType, paymentMethod: String?
    var statusID: Int?
    
    private enum CodingKeys: String, CodingKey {
        case paymentMethodType = "payment_method_type"
        case paymentMethod = "payment_method"
        case statusID = "status_id"
    }
    
    init(from decoder: Decoder) {
        do {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            paymentMethodType = (try? values.decode(String.self, forKey: .paymentMethodType)) ?? ""
            paymentMethod = (try? values.decode(String.self, forKey: .paymentMethod)) ?? ""
            statusID = (try? values.decode(Int.self, forKey: .statusID)) ?? 0
        } catch {}
    }
}
