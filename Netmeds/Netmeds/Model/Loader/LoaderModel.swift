//
//  LoaderModel.swift
//  Netmeds
//
//  Created by SANKARLAL on 17/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import Foundation
import SVProgressHUD

func showLoader() {
    SVProgressHUD.show()
}

func hideLoader() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        SVProgressHUD.dismiss()
    }
}
