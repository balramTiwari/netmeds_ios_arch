//
//  NMEditProfileTableViewCell.swift
//  Netmeds
//
//  Created by NETMEDS on 28/01/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

class NMEditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
