//
//  APIRequestFactory.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation

class APIRequestFactory: NSObject {
    class func reqestFrom(type: RequestType) -> APIBaseRequestResponse? {
        switch type {
        case .appConfig:
            return APIAppConfig(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .configURLPaths:
            return APIConfigURLPaths(requestType: type)
        case .logIn:
            return APILogIn(requestType: type)
        case .socialLogIn:
            return APISocialLogIn(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .customerDetails:
            return APICustomerDetails(requestType: type)
        case .logInViaPassword:
            return APILogInViaPassword(requestType: type, requestMethod: .post)
        case .sendOTP:
            return APISendOTP(requestType: type)
        case .reSendOTP:
            return APIReSendOTP(requestType: type)
        case .initiateChangePassword:
            return APIInitiateChangePassword(requestType: type, requestMethod: .post)
        case .completeChangePassword:
            return APICompleteChangePassword(requestType: type, requestMethod: .post)
        case .logInViaOTP:
            return APILogInViaOTP(requestType: type)
        case .signUp:
            return APISignUp(requestType: type, requestMethod: .post)
        case .homeOffersBanner:
            return APIHomeOffersBanner(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .homeBigSalesBanner:
            return APIHomeBigSalesBanner(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .homeWellnessBanner:
            return APIHomeWellnessBanner(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .homeCashBackBanner:
            return APIHomeCashBackBanner(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .homeHealthExperts:
            return APIHomeHealthExpertsBanner(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .downloadImage:
            return APIDownloadImage(requestType: type, apiEndPointsType: .none)
        case .algoliaSearch:
            return APIAlgoliaSearch(requestType: type, apiEndPointsType: .none)
        case .productDetails:
            return APIProductDetails(requestType: type)
        case .brainSins:
            return APIBrainSinsInfo(requestType: type, apiEndPointsType: .brainSinsBaseURL)
        case .createCart:
            return APICreateCart(requestType: type)
        case .addToCart:
            return APIAddToCart(requestType: type)
        case .getCart:
            return APICart(requestType: type)
        case .removeItemFromCart:
            return APIRemoveItemFromCart(requestType: type)
        case .addItemToCart:
            return APIAddItemToCart(requestType: type)
        case .getWalletBalance:
            return APIWalletBalance(requestType: type)
        case .getAllCoupons:
            return APIAllCoupons(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .applyCouponCode:
            return APIApplyCouponCode(requestType: type)
        case .unApplyCouponCode:
            return APIUnApplyCouponCode(requestType: type)
        case .applySuperCash:
            return APIApplySuperCash(requestType: type)
        case .deliveryEstimation:
            return APIDeliveryEstimation(requestType: type, requestMethod: .post, apiEndPointsType: .none)
        case .getAllAddress:
            return APIAllAddress(requestType: type)
        case .paymentGateWayDetails:
            return APIPaymentGateWay(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .allNetBankingDetails:
            return APIAllNetBanking(requestType: type, apiEndPointsType: .none)
        case .getCategoryProductList:
            return APICategoryProductList(requestType: type)
        case .getManufacturerProductList:
            return APIManufacturerProductList(requestType: type)
        case .getCategoryProductBanners:
            return APICategoryProductBanners(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .getManufacturerProductBanners:
            return APIManufacturerProductBanners(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .allOffers:
            return APIAllOffers(requestType: type, apiEndPointsType: .customAPIBaseURL)
        case .getMyOrders:
            return APIMyOrders(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .getMyOrdersDetail:
            return APIMyOrdersDetail(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .getTrackOrderDetails:
            return APITrackOrderDetails(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .searchPhoto:
            return APISearchPhoto(requestType: .searchPhoto)
        case .method2Carts:
            return APIMethod2Carts(requestType: type)
        case .uploadSelectedPrescriptions:
            return APIUploadSelectedPrescriptions(requestType: type, requestMethod: .post)
        case .pastPrescriptions:
            return APIPastPrescriptions(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .detachPrescriptions:
            return APIDetachPrescriptions(requestType: type)
        case .jusPayOrderCreate:
            return APIJusPayOrderCreate(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .juspayOrderStatus:
            return APIJusPayOrderStatus(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        case .juspayOrderComplete:
            return APIJusPayOrderComplete(requestType: type, requestMethod: .post, apiEndPointsType: .customAPIBaseURL)
        default:
            return nil
        }
    }
}
