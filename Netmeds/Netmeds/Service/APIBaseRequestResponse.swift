//
//  APIBaseRequestResponse.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import InstantSearchClient

typealias RequestCallBackClosure = (_ error: Error?, _ response: Any?) -> Void

enum APIEndPointsType {
    case baseURL
    case customAPIBaseURL
    case diagnosticsJustPayBaseURL
    case consultationPaymentBaseURL
    case brainSinsBaseURL
    case none
}

class APIBaseRequestResponse: NSObject {
    var requestType: RequestType = .none
    var requestMethod: RequestMethod = .get
    var apiEndPointsType: APIEndPointsType = .baseURL
    
    init(requestType: RequestType, requestMethod: RequestMethod = .get, apiEndPointsType: APIEndPointsType = .baseURL) {
        super.init()
        self.requestType = requestType
        self.requestMethod = requestMethod
        self.apiEndPointsType = apiEndPointsType
    }
    
    private var baseURL: String {
        get {
            switch apiEndPointsType {
            case .baseURL:
                return APIEndPointConstants.baseURL
            case .customAPIBaseURL:
                return APIEndPointConstants.customAPIBaseURL
            case .diagnosticsJustPayBaseURL:
                return APIEndPointConstants.diagnosticsJustPayBaseURL
            case .consultationPaymentBaseURL:
                return APIEndPointConstants.consultationPaymentBaseURL
            case .brainSinsBaseURL:
                return APIEndPointConstants.brainSinsBaseURL
            case .none:
                return ""
            }
        }
    }
    
    func route() -> String {
        return ""
    }
    
    private func urlString() -> String {
        let url: String = baseURL + route()
        return url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
    
    func parameters() -> JSONDictAny? {
        return nil
    }
    
    func multiPartParameters() -> JSONArrayOfDictString? {
        return nil
    }
    
    func uploadPrescriptionParameters() -> [Any]? {
        return nil
    }
    
    private func headers() -> JSONDictString {
        let userID = UserDefaultsModel.getUserID()
        let authToken = UserDefaultsModel.getSessionID()
        if requestType == .deliveryEstimation {
            return [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
        }
        guard userID.isValidString, authToken.isValidString else {
            return [
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            ]
        }
        return [
            "authtoken": authToken,
            "userid": userID
        ]
    }
    
    func parseResponse(response: Any?) -> Any? {
        return nil
    }
    
    func algoliaQuery() -> Query? {
        return nil
    }
    
    func algoliaIndex() -> String {
        return ""
    }
    
    func executeRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let url: URL = URL(string: urlString()) else { completionHandler(nil, nil); return }
        showLoader()
        var urlRequest: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = requestMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers()
        urlRequest.httpBody = parameters()?.dataWithJSONObject()
        let session = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                self.handleJSONResponse(data: data, error: error, completionHandler: completionHandler)
            }
        }.resume()
    }
    
    func executeQueryRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let url: URL = URL(string: urlString()) else { completionHandler(nil, nil); return }
        showLoader()
        let postData = NSMutableData()
        for (key,value) in parameters() ?? [:] {
            postData.append("&\(key)=\(value)".data(using: String.Encoding.utf8)!)
        }
        var urlRequest: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = requestMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers()
        urlRequest.httpBody = postData as Data
        let session = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                self.handleJSONResponse(data: data, error: error, completionHandler: completionHandler)
            }
        }.resume()
    }
    
    func executeMultiPartRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let url: URL = URL(string: urlString()) else { completionHandler(nil, nil); return }
        showLoader()
        var urlRequest: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = requestMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers()
        let boundary = generateBoundaryString()
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = getMultiPartBody(boundary: boundary)
        let session = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                self.handleJSONResponse(data: data, error: error, completionHandler: completionHandler)
            }
        }.resume()
    }
    
    func executeDownloadImageRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let url: URL = URL(string: urlString()) else { completionHandler(nil, nil); return }
        var urlRequest: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = requestMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers()
        urlRequest.httpBody = parameters()?.dataWithJSONObject()
        let session = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                self.handleDataResponse(data: data, error: error, completionHandler: completionHandler)
            }
        }.resume()
    }
    
    func executeUploadPrescriptionsRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let url: URL = URL(string: urlString()) else { completionHandler(nil, nil); return }
        showLoader()
        var urlRequest: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = requestMethod.rawValue
        urlRequest.allHTTPHeaderFields = headers()
        let boundary = generateBoundaryString()
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = getPrescriptionBody(boundary: boundary)
        let session = URLSession.shared
        session.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                self.handleJSONResponse(data: data, error: error, completionHandler: completionHandler)
            }
        }.resume()
    }
    
    func executeSearchAlogliaQueryRequest(completionHandler: @escaping RequestCallBackClosure) {
        guard let query = self.algoliaQuery(), let algoliaInfo = ConfigManager.shared.configDetails?.algolia else { completionHandler(nil, nil); return }
        showLoader()
        let appIDDecodedKey = algoliaInfo.appId!.decodeKey()
        let apiKeyDecodedKey = algoliaInfo.medDetail1!.decodeKey()
        let client = Client(appID: appIDDecodedKey, apiKey: apiKeyDecodedKey)
        let indexName = self.algoliaIndex().isValidString ? self.algoliaIndex(): algoliaInfo.medIndex!
        let index = client.index(withName: indexName)
        index.search(query) { (responseJSON, error) in
            self.handleAlgoliaResponse(response: responseJSON, error: error, completionHandler: completionHandler)
        }
    }
    
    // TODO:- Response From Data to Dict and return error if not Valid Data
    private func handleJSONResponse(data: Data?, error: Error?, completionHandler: @escaping RequestCallBackClosure) {
        hideLoader()
        guard error == nil else { completionHandler(error, nil); return }
        guard let validData = data else { completionHandler(error, nil); return }
        print("RESPONSE", validData.jsonObjectWithData() as Any)
        guard let jsonResponse = self.parseResponse(response: validData.jsonObjectWithData()) else { completionHandler(error, nil); return }
        completionHandler(nil, jsonResponse)
    }
    
    private func handleDataResponse(data: Data?, error: Error?, completionHandler: @escaping RequestCallBackClosure) {
        guard error == nil else { completionHandler(error, nil); return }
        completionHandler(nil, data)
    }
    
    private func handleAlgoliaResponse(response: JSONDictAny?, error: Error?, completionHandler: @escaping RequestCallBackClosure) {
        hideLoader()
        guard error == nil else { completionHandler(error, nil); return }
        print("ALGOLIA RESPONSE", response as Any)
        guard let jsonResponse = self.parseResponse(response: response) else { completionHandler(error, nil); return }
        completionHandler(nil, jsonResponse)
    }
}

extension APIBaseRequestResponse {
    private func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    private func getMultiPartBody(boundary: String) -> Data {
        var body = Data()
        if let multiPartParameters = multiPartParameters() {
            for itemDict in multiPartParameters {
                let lineBreak = "\r\n"
                for (key, value) in itemDict {
                    body.append("--\(boundary + lineBreak)")
                    let keyFinal = key
                    body.append("Content-Disposition: form-data; name=\"\(keyFinal)\"\(lineBreak + lineBreak)")
                    body.append("\(value + lineBreak)")
                    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
                }
            }
        }
        return body
    }
    
    private func getPrescriptionBody(boundary: String) -> Data {
        var body = Data()
        if let parameters = uploadPrescriptionParameters() {
            var imageNumber = 1
            var rxIDNumber = 1
            let mimeType = "image/jpeg"
            for param in parameters {
                if let imgObj = param as? UIImage {
                    var keyFinal = ""
                    let filename = "IMG_000\(imageNumber).jpg"
                    let imageData = imgObj.jpeg(.high)
                    if let data = imageData {
                        body.append("--\(boundary)\r\n")
                        if imageNumber > 9 {
                            keyFinal = "\("pimg")\(imageNumber)"
                            body.append("Content-Disposition: form-data; name=\"\(keyFinal)\"; filename=\"\(filename)\"\r\n")
                        } else {
                            keyFinal = "\("pimg")0\(imageNumber)"
                            body.append("Content-Disposition: form-data; name=\"\(keyFinal)\"; filename=\"\(filename)\"\r\n")
                        }
                        body.append("Content-Type: \(mimeType)\r\n\r\n")
                        body.append(data)
                        body.append("\r\n")
                    }
                    imageNumber += 1
                }
                if let rxIDs = param as? String {
                    let lineBreak = "\r\n"
                    var keyFinal = ""
                    body.append("--\(boundary + lineBreak)")
                    if rxIDNumber > 9 {
                        keyFinal = "pid\(rxIDNumber)"
                        body.append("Content-Disposition: form-data; name=\"\(keyFinal)\"\(lineBreak + lineBreak)")
                    } else {
                        keyFinal = "pid0\(rxIDNumber)"
                        body.append("Content-Disposition: form-data; name=\"\(keyFinal)\"\(lineBreak + lineBreak)")
                    }
                    body.append("\(rxIDs + lineBreak)")
                    rxIDNumber += 1
                }
            }
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        return body
    }
}
