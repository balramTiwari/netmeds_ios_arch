//
//  NetWorkManager.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import InstantSearchClient

class NetworkManager: NSObject {
    static let shared = NetworkManager()
}

extension NetworkManager {
    
    func fetchAppConfigInfoService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .appConfig) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func fetchConfigURLPathsService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .configURLPaths) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }

    func fetchPhotosFrom(searchText: String, completionHandler: @escaping RequestCallBackClosure) {
        let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .searchPhoto)!
        if let request = requestResponse as? APISearchPhoto {
            request.searchText = searchText
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- LogIn Services
extension NetworkManager {
    
    func logInService(mobileNo: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .logIn) else { return }
        if let request = requestResponse as? APILogIn {
            request.mobileNo = mobileNo
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func socialLogInService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .socialLogIn) else { return }
        if let request = requestResponse as? APISocialLogIn {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func customerDetailsService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .customerDetails) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
}

// MARK:- LogIn Via Password / OTP Services
extension NetworkManager {
    func logInViaPasswordService(params: JSONDictAny, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .logInViaPassword) else { return }
        if let request = requestResponse as? APILogInViaPassword {
            request.params = params
        }
        requestResponse.executeQueryRequest(completionHandler: completionHandler)
    }
    
    func sendOTPService(mobileNo: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .sendOTP) else { return }
        if let request = requestResponse as? APISendOTP {
            request.mobileNo = mobileNo
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func reSendOTPService(randomKey: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .reSendOTP) else { return }
        if let request = requestResponse as? APIReSendOTP {
            request.randomKey = randomKey
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func initiateChangePasswordService(params: JSONDictAny, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .initiateChangePassword) else { return }
        if let request = requestResponse as? APIInitiateChangePassword {
            request.params = params
        }
        requestResponse.executeQueryRequest(completionHandler: completionHandler)
    }
    
    func completeChangePasswordService(params: JSONDictAny, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .completeChangePassword) else { return }
        if let request = requestResponse as? APICompleteChangePassword {
            request.params = params
        }
        requestResponse.executeQueryRequest(completionHandler: completionHandler)
    }
    
}

// MARK:- LogIn Via Password / OTP Services
extension NetworkManager {
    func logInViaOTPService(mobileNo: String, randomKey: String, otp: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .logInViaOTP) else { return }
        if let request = requestResponse as? APILogInViaOTP {
            request.mobileNo = mobileNo
            request.randomKey = randomKey
            request.otp = otp
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- SignUp Services
extension NetworkManager {
    func signUpService(params: JSONDictAny, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .signUp) else { return }
        if let request = requestResponse as? APISignUp {
            request.params = params
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- Home Services
extension NetworkManager {
    func homeOffersBannerService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .homeOffersBanner) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func homeBigSalesBannerService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .homeBigSalesBanner) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func homeWellnessBannerService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .homeWellnessBanner) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func homeCashBackBannerService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .homeCashBackBanner) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func homeHealthExpertsBannerService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .homeHealthExperts) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func downloadImageService(urlString: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .downloadImage) else { return }
        if let request = requestResponse as? APIDownloadImage {
            request.urlString = urlString
        }
        requestResponse.executeDownloadImageRequest(completionHandler: completionHandler)
    }
}

// MARK:- Search Services
extension NetworkManager {
    func algoliaSearchService(index: String = "", query: Query?, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .algoliaSearch) else { return }
        if let request = requestResponse as? APIAlgoliaSearch {
            request.query = query
            request.index = index
        }
        requestResponse.executeSearchAlogliaQueryRequest(completionHandler: completionHandler)
    }
    
    func productDetailsService(productID: Int, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .productDetails) else { return }
        if let request = requestResponse as? APIProductDetails {
            request.productID = productID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
}

// MARK:- Add To Cart Services
extension NetworkManager {
    func createCartService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .createCart) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func addToCartService(productID: Int, quantity: Int, cartID: Int = 0, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .addToCart) else { return }
        if let request = requestResponse as? APIAddToCart {
            request.productID = productID
            request.quantity = quantity
            request.cartID = cartID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- Cart And Order Review Services
extension NetworkManager {
    func getCartService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getCart) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func removeItemFromCartService(productCode: String, quantity: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .removeItemFromCart) else { return }
        if let request = requestResponse as? APIRemoveItemFromCart {
            request.productCode = productCode
            request.quantity = quantity
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func addItemToCartService(productCode: String, quantity: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .addItemToCart) else { return }
        if let request = requestResponse as? APIAddItemToCart {
            request.productCode = productCode
            request.quantity = quantity
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func getWalletBalanceService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getWalletBalance) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func getAllCouponsService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getAllCoupons) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func applyCouponCodeService(couponCode: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .applyCouponCode) else { return }
        if let request = requestResponse as? APIApplyCouponCode {
            request.couponCode = couponCode
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func unApplyCouponCodeService(couponCode: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .unApplyCouponCode) else { return }
        if let request = requestResponse as? APIUnApplyCouponCode {
            request.couponCode = couponCode
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func applySuperCashService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .applySuperCash) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }

    func getDeliveryEstimationDateService(params: JSONDictAny, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .deliveryEstimation) else { return }
        if let request = requestResponse as? APIDeliveryEstimation {
            request.params = params
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- Payment GateWay Services
extension NetworkManager {
    func paymentGateWayService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .paymentGateWayDetails) else { return }
        if let request = requestResponse as? APIPaymentGateWay {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func allNetBankingServcie(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .allNetBankingDetails) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }

}

// MARK:- Address Services
extension NetworkManager {
    func getAllAddressService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getAllAddress) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- Product List Services
extension NetworkManager {
    
    func getCategoryListService(categoryID: Int, pageNo: Int, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getCategoryProductList) else { return }
        if let request = requestResponse as? APICategoryProductList {
            request.categoryID = categoryID
            request.pageNo = pageNo
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func getManufacturerListService(manufacturerID: Int, pageNo: Int, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getManufacturerProductList) else { return }
        if let request = requestResponse as? APIManufacturerProductList {
            request.manufacturerID = manufacturerID
            request.pageNo = pageNo
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func getCategoryBannersService(categoryID: Int, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getCategoryProductBanners) else { return }
        if let request = requestResponse as? APICategoryProductBanners {
            request.categoryID = categoryID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func getManufacturerBannersService(manufacturerID: Int, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getManufacturerProductBanners) else { return }
        if let request = requestResponse as? APIManufacturerProductBanners {
            request.manufacturerID = manufacturerID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- All Offers Services
extension NetworkManager {
    func getAllOffersService(pageID: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .allOffers) else { return }
        if let request = requestResponse as? APIAllOffers {
            request.pageID = pageID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- My Orders Services
extension NetworkManager {
    func getMyOrdersService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getMyOrders) else { return }
        if let request = requestResponse as? APIMyOrders {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func getMyOrdersDetailService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getMyOrdersDetail) else { return }
        if let request = requestResponse as? APIMyOrdersDetail {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func getTrackOrderDetailsService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .getTrackOrderDetails) else { return }
        if let request = requestResponse as? APITrackOrderDetails {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func brainSinsInfoService(productID: Int, brainsinsKey: String, templateID: String, customerID: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .brainSins) else { return }
        if let request = requestResponse as? APIBrainSinsInfo {
            request.productID = productID
            request.brainsinsKey = brainsinsKey
            request.templateID = templateID
            request.customerID = customerID
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- Upload Prescriptions Services
extension NetworkManager {
    func method2CartsService(completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .method2Carts) else { return }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
    
    func pastPrescriptionsService(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .pastPrescriptions) else { return }
        if let request = requestResponse as? APIPastPrescriptions {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func uploadSelectedPrescriptionsService(params: [Any]?, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .uploadSelectedPrescriptions) else { return }
        if let request = requestResponse as? APIUploadSelectedPrescriptions {
            request.params = params
        }
        requestResponse.executeUploadPrescriptionsRequest(completionHandler: completionHandler)
    }
    
    func detachPrescriptionsService(detatchPrescriptions: String, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .detachPrescriptions) else { return }
        if let request = requestResponse as? APIDetachPrescriptions {
            request.detatchPrescriptions = detatchPrescriptions
        }
        requestResponse.executeRequest(completionHandler: completionHandler)
    }
}

// MARK:- JusPay Services
extension NetworkManager {
    func jusPayOrderCreateServcie(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .jusPayOrderCreate) else { return }
        if let request = requestResponse as? APIJusPayOrderCreate {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func jusPayOrderStatusServcie(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .juspayOrderStatus) else { return }
        if let request = requestResponse as? APIJusPayOrderStatus {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
    
    func jusPayOrderCompleteServcie(params: JSONArrayOfDictString, completionHandler: @escaping RequestCallBackClosure) {
        guard let requestResponse: APIBaseRequestResponse = APIRequestFactory.reqestFrom(type: .juspayOrderComplete) else { return }
        if let request = requestResponse as? APIJusPayOrderComplete {
            request.multiPartParams = params
        }
        requestResponse.executeMultiPartRequest(completionHandler: completionHandler)
    }
}
