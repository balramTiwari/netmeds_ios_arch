//
//  UITextFieldSingleLine.swift
//  Netmeds
//
//  Created by SANKARLAL on 06/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class UITextFieldSingleLine: UITextField {
  func singleLineTextField() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.NMSTextColor().cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//        self.layer.shadowOpacity = 1.0
//        self.layer.shadowRadius = 0.0
    }
}
