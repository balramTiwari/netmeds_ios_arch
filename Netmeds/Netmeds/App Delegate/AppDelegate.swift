//
//  AppDelegate.swift
//  Netmeds
//
//  Created by Netmeds on 28/11/18.
//  Copyright © 2018 Netmeds. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import IQKeyboardManagerSwift
import SVProgressHUD
import HyperSDK
import WebEngage
import UserNotifications
import Firebase
import Tune
//import InstantSearchClient
import KapChat

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // MARK: Life Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setUpUserInterface(application, didFinishLaunchingWithOptions: launchOptions)
        if #available(iOS 13.0, *) { window!.overrideUserInterfaceStyle = .light }
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken:Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        /* send the device token to your server */
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            "app_open": "app_open"
        ])
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        Tune.measureSession()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if self.universalLink(userActivity: userActivity) {
            return true
        }
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let regex = try? NSRegularExpression(pattern: "amzn-.*|juspay.*", options:[])
        if (regex?.numberOfMatches(in: url.absoluteString, options: [], range:
            NSRange(location: 0, length: url.absoluteString.count)) ?? 0) > 0 {
            return Hyper.handleRedirectURL(url, sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String)
        }else if url.scheme == kSchemesIdentifier  {
            var userInfo = [AnyHashable: Any]()
            userInfo["deeplink"] = url.absoluteString
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                //                           NotificationCenter.default.post(name: Notification.Name(kDeepLinkPostNotification), object: userInfo)
            }
            return true
        }else if let appId = FBSDKSettings.appID(), url.scheme == "fb\(appId)" {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        } else {
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
    }
}

// MARK: All Functions
extension AppDelegate {
    private func setUpUserInterface(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        initialSetUp(application, didFinishLaunchingWithOptions: launchOptions)
        saveUUIDToKeychain()
        ConfigManager.shared.updateConfigDetails()
        UserManager.shared.updateUserDetails()
        getAppConfigInfoService()
        getConfigURLPathsService()
        if UserDefaultsModel.isUserLoggedIn() {
            getUserInfoService()
            setHomeRootViewController()
        }
    }
    
    private func initialSetUp(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        // Configure Fire Base
        #if PRODUCTION
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
        }
        #endif
        // Kapture Chat InitialSetUp
        if let XMPPSetupClass = XMPPSetup.sharedInstance() as? XMPPSetup {
            XMPPSetupClass.initialSetup()
        }
        // Tune Initializer
        Tune.initialize(withTuneAdvertiserId: kTuneAdvertiserID, tuneConversionKey: kTuneConversionKey)
        Tune.setAppAdTrackingEnabled(true)
        Tune.appleAdvertisingIdentifier()
        Tune.setDebugLogVerbose(true)
        // SV Progress HUD Default Style SetUp
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        // IQ Keyboard SetUp
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        // FB Initializer
        DispatchQueue.global(qos: .background).async {
            FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)
            if let gai = GAI.sharedInstance() {
                gai.tracker(withTrackingId: kGoogleAnalyticsTrackingId)
                gai.trackUncaughtExceptions = true
                gai.defaultTracker.allowIDFACollection = true
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    gai.defaultTracker.set(kGAIAppVersion, value: version)
                }
            }
        }
        // WebEngage SetUp
        UNUserNotificationCenter.current().delegate = self
        WebEngage.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions, notificationDelegate: self)
    }
    
    private func saveUUIDToKeychain() {
        let keychainStorage = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: KeychainConfiguration.accountName, accessGroup: KeychainConfiguration.accessGroup)
        var storedUUIDString: String = ""
        do { storedUUIDString = try keychainStorage.readPassword() } catch {}
        guard storedUUIDString.count == 0 else { return }
        let uuidString: String = UIDevice.current.identifierForVendor?.uuidString ?? ""
        do { try keychainStorage.savePassword(uuidString) } catch {}
    }
    
    private func universalLink(userActivity: NSUserActivity) -> Bool {
        var status = false
        if let urls = userActivity.webpageURL?.absoluteString, userActivity.activityType == NSUserActivityTypeBrowsingWeb, let sep = urls.components(separatedBy: "/") as NSArray? {
            let predicator = NSPredicate(format: "SELF contains[cd] 'www.netmeds.com' OR SELF contains[cd] 'm.netmeds.com' OR SELF contains[cd] 'labs.netmeds.com' OR Self contains[cd] 'www.labs.netmeds.com' OR Self contains[cd] 'consult.netmeds.com'")
            let filter = sep.filtered(using: predicator)
            
            var userInfo = [AnyHashable: Any]()
            userInfo["UniversalLink"] = urls
            //            NotificationCenter.default.post(name: Notification.Name(kUniversalLinkPostNotification), object: userInfo)
            status  = (filter.count > 0) ? (true) :  (false)
        }
        return status
    }
        
    func setHomeRootViewController() {
        let viewController = UITabBarController.instantiate(storyboardName: .home)
        self.window?.rootViewController = viewController
    }

    func navigateToKaptureChatVC() {
        
    }
    
    func wegHandleDeeplink(_ deeplink: String, userData data: [AnyHashable: Any]) {
        var userInfo = [AnyHashable: Any]()
        userInfo["deeplink"] = deeplink
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            NotificationCenter.default.post(name: Notification.Name(kDeepLinkPostNotification), object: userInfo)
        }
    }
}

// MARK: All API Functions
extension AppDelegate {
    private func getAppConfigInfoService() {
        NetworkManager.shared.fetchAppConfigInfoService { (error, response) in
            guard error == nil, let configInfo = response as? ConfigDetailsModel else { return }
            ConfigManager.shared.resetConfigDetails(configInfo: configInfo)
        }
    }
    
    private func getUserInfoService() {
        NetworkManager.shared.customerDetailsService { (error, response) in
            guard error == nil, let customerInfo = response as? CustomerInfoModel, let customerDetails = customerInfo.result?.details else { return }
            UserManager.shared.resetUserDetails(userInfo: customerDetails)
        }
    }
    
    private func getConfigURLPathsService() {
        NetworkManager.shared.fetchConfigURLPathsService { (error, response) in
            guard error == nil, let configURLPathsInfo = response as? ConfigURLPathsModel, let result = configURLPathsInfo.result else { return }
            ConfigManager.shared.configURLPaths = result
        }
    }
}

// MARK: ALL DELEGATE FUNCTIONS
// MARK: UNUser Notification Center Delegate Functions
extension AppDelegate:  UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        WEGManualIntegration.userNotificationCenter(center, didReceive: response)
        let userInfo = response.notification.request.content.userInfo
        if let dataFrom = userInfo["dataFrom"] as? String, dataFrom.lowercased() == kPushMessageTypeForKapChat.lowercased() {
            navigateToKaptureChatVC()
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        WEGManualIntegration.userNotificationCenter(center, willPresent: notification)
        completionHandler( [.alert,.sound])
    }
}

// MARK: WEGInApp Notification Protocol Functions
extension AppDelegate : WEGInAppNotificationProtocol {
    func notificationPrepared(_ inAppNotificationData: [AnyHashable : Any]!, shouldStop stopRendering: UnsafeMutablePointer<ObjCBool>!) -> [AnyHashable : Any]! {
        return inAppNotificationData
    }
    
    func notificationShown(_ inAppNotificationData: [AnyHashable : Any]!) {}
    
    func notification(_ inAppNotificationData: [AnyHashable : Any]!, clickedWithAction actionId: String!) {
        if let actions = inAppNotificationData["actions"] as? NSArray {
            let predicate = NSPredicate(format: "%K MATCHES[cd] %@", "type", "DEEP_LINK")
            let filter = actions.filtered(using: predicate)
            if filter.count > 0, let item = filter.first as? [AnyHashable : Any], let actionLink = item["actionLink"] as? String {
                var userInfo = [AnyHashable: Any]()
                userInfo["deeplink"] = actionLink
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    //                    NotificationCenter.default.post(name: Notification.Name(kDeepLinkPostNotification), object: userInfo)
                }
            }
        }
    }
    
    func notificationDismissed(_ inAppNotificationData: [AnyHashable : Any]!) {}
}

// MARK: Chat Message Delegate Functions
extension AppDelegate: ChatMessageDelegate {
    func backBtnClicked(_ button: UIBarButtonItem!) {
        if let topViewCtrl = UIApplication.topViewController() {
            topViewCtrl.dismiss(animated: true, completion: nil)
        }
        IQKeyboardManager.shared.enable = true
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
