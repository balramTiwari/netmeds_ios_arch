//
//  RoundButton.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            setCorners(with: cornerRadius)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            setBorder(with: borderWidth)
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            setBorderColor(with: borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        setCorners(with: cornerRadius)
    }
    
    func setCorners(with value: CGFloat) {
        layer.cornerRadius = value
    }
    
    func setBorder(with value: CGFloat) {
        layer.borderWidth = value
    }
    
    func setBorderColor(with value: UIColor) {
        layer.borderColor = value.cgColor
    }
    
}
