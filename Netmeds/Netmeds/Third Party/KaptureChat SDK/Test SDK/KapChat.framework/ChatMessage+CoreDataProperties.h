//
//  ChatMessage+CoreDataProperties.h
//  
//
//  Created by ios-Developer on 23/01/20.
//
//  This file was automatically generated and should not be edited.
//

#import "ChatMessage+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatMessage (CoreDataProperties)

+ (NSFetchRequest<ChatMessage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *chat_id;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, retain) NSData *imgData;
@property (nonatomic) BOOL isActivity;
@property (nonatomic) BOOL isDownloaded;
@property (nonatomic) BOOL isExpired;
@property (nonatomic) BOOL isLocal;
@property (nonatomic) BOOL isSynced;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *notification;
@property (nullable, nonatomic, copy) NSString *persist;
@property (nullable, nonatomic, copy) NSString *sender;
@property (nullable, nonatomic, copy) NSDate *sentDate;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
