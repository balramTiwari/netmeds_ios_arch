//
//  iImagePickerController.h
//  iimagePickerContoller
//
//  Created by Rajesh on 9/11/15.
//  Copyright (c) 2015 Org. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iImagePickerController : UIViewController

//@property (weak, nonatomic) IBOutlet UIColor *backItemColor;
//
//@property (weak, nonatomic) IBOutlet UIColor *titleColor;
//@property (strong, nonatomic) UIImage *backItemImage;

@property (weak, nonatomic) IBOutlet UIColor *backItemColor;
@property (strong, nonatomic) UIImage *backItemImage;
@property (weak, nonatomic) IBOutlet UIColor *barColor;
@property (strong, nonatomic) NSString *imageTitle;
@property (weak, nonatomic) IBOutlet UIColor *titleColor;
@property (weak, nonatomic) IBOutlet UIView *cameraImgView;

@property (weak, nonatomic) IBOutlet UIView *topView;
- (IBAction)backBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *kapTitle;

@end
