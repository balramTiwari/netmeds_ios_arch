//
//  ChatHandlerVC.h
//  test
//
//  Created by ios-Developer on 18/03/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatMessageVC.h"
#import "XMPPSetup.h"

@import XMPPFramework;

@class ChatMessageVC;


@protocol updateUIDelegate

//-(void)changeHeaderViewColor:(ChatMessageVC *)VC color:(UIColor *)color;

@end

@interface ChatHandlerVC : UIViewController<updateUIDelegate>

extern NSString *const chatUserName;
extern NSString *const chatsid;
extern NSString *const chatPassword;
extern NSString *const personName;
extern NSString *const sName;
extern NSString *const userResource;
extern NSString *const userJID;
extern NSString *const onScreen;
extern NSString *const ivValue;
extern NSString *const leadid;
extern NSString *const resourcePrefix;
//extern NSString *const S3_BUCKET_NAME;

@property (weak, nonatomic) ChatMessageVC *messageVC;
@property (nonatomic, retain) id <updateUIDelegate> delegate;
@property (nonatomic, copy) void(^completionHandler)(ChatMessageVC *vc, BOOL success);

+ (id)sharedInstance;
-(void)changeHeaderViewColor:(ChatMessageVC *)VC color:(UIColor *)color;
-(void)sendMessageToServer:(NSString *)type mime:(NSString *)mime messageId:(NSString *)messageId chatMessage:(Message *)chatMessage;
-(void)sendChatRequestToserver:(NSString *)customerCode supportKey:(NSString *)supportKey encryptionKey:(NSString *)encryptionKey withCompletion:(void (^)(ChatMessageVC *vc, BOOL success))completion;
-(void)getInstanceChatMessage:(ChatMessageVC *)vc;

@end
