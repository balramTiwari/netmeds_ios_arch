//
//  KapChat.h
//  KapChat
//
//  Created by ios-Developer on 02/05/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KapChat.
FOUNDATION_EXPORT double KapChatVersionNumber;

//! Project version string for KapChat.
FOUNDATION_EXPORT const unsigned char KapChatVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KapChat/PublicHeader.h>

#import <KapChat/XMPPSetup.h>
#import <KapChat/ChatMessageVC.h>
#import <KapChat/iImagePickerController.h>
#import <KapChat/TakePhotoVC.h>
#import <KapChat/ImageVC.h>
#import <KapChat/HPGrowingTextView.h>
#import <KapChat/HPTextViewInternal.h>
#import <KapChat/AESCipher.h>
#import <KapChat/Inputbar.h>
//#import <KapChat/DAKeyboardControl.h>
#import <KapChat/MBProgressHUD.h>
#import <KapChat/ChatHandlerVC.h>
#import <KapChat/MessageGateway.h>
#import <KapChat/Image+Compression.h>
#import <KapChat/LocalStorage.h>
#import <KapChat/TableHandler.h>
#import <KapChat/MessageCell.h>
#import <KapChat/imageCell.h>
#import <KapChat/Message.h>



