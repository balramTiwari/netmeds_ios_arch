//
//  TableHandler.h
//  BAChat
//
//  Created by ios-Developer on 28/05/18.
//  Copyright © 2018 ios-Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"

@interface TableHandler : NSObject

-(void)addObject:(Message *)message;
-(void)addObjectsFromArray:(NSArray *)messages;
-(void)replaceMessage:(Message *)message replaceObjectIndex:(long)index;
-(NSInteger)numberOfMessages;
-(NSInteger)numberOfSections;
-(NSInteger)numberOfMessagesInSection:(NSInteger)section;
-(NSString *)titleForSection:(NSInteger)section;
-(Message *)objectAtIndexPath:(NSIndexPath *)indexPath;
-(Message *)lastObject;
-(NSIndexPath *)indexPathForLastMessage;
-(NSIndexPath *)indexPathForMessage:(Message *)message;
-(void)removeMessage:(Message *)message;

@end
