//
//  MessageGateway.h
//  BAChat
//
//  Created by ios-Developer on 28/05/18.
//  Copyright © 2018 ios-Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"
#import "ChatMessageVC.h"
@protocol MessageGatewayDelegate;



@interface MessageGateway : NSObject

@property (assign, nonatomic) id<MessageGatewayDelegate,ChatMessageDelegate> delegate;

+(id)sharedInstance;
-(void)loadOldMessages;
-(void)sendMessage:(Message *)message;
-(void)dismiss;

@end

@protocol MessageGatewayDelegate <NSObject>
-(void)gatewayDidUpdateStatusForMessage:(Message *)message;
-(void)gatewayDidReceiveMessages:(NSArray *)array;
@end
