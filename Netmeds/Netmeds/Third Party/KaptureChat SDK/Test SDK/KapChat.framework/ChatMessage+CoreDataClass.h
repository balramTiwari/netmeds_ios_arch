//
//  ChatMessage+CoreDataClass.h
//  
//
//  Created by ios-Developer on 23/01/20.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessage : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatMessage+CoreDataProperties.h"
