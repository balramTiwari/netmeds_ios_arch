//
//  Image+Compression.h
//  
//
//  Created by ios-Developer on 23/03/17.
//
//

#import <UIKit/UIKit.h>

@interface Image_Compression : UIImage

+ (id)sharedInstance;

- (UIImage *)compressImage:(UIImage *)image;

@end
