//
//  ImageVC.h
//  BBChatSample
//
//  Created by ios-Developer on 29/05/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *myImgView;

@property (strong, nonatomic) NSData *imgData;
@property (strong, nonatomic) NSString *imgUrlString;

@property (strong, nonatomic) NSString *chatTitle;
@property (strong, nonatomic) UIColor *imageThemeColor;
@property (strong, nonatomic) UIColor *imageThemeTextColor;
@property (strong, nonatomic) UIImage *backBtnImage;

@property (strong, nonatomic) UIColor *backItemColor;

- (IBAction)backBtn:(UIButton *)sender;

@end
