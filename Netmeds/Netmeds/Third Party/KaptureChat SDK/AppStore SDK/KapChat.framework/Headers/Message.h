//
//  Message.h
//  BAChat
//
//  Created by ios-Developer on 28/05/18.
//  Copyright © 2018 ios-Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MessageStatus)
{
//    MessageStatusSending,
//    MessageStatusReceived,
//    MessageStatusRead,
    MessageStatusSent = 0,
//    MessageStatusFailed = 1
    MessageStatusNotSent = 1
};

typedef NS_ENUM(NSInteger, MessageSender)
{
    MessageSenderMyself = 0,
    MessageSenderSomeone = 1
};

@interface Message : NSObject

@property (assign, nonatomic) MessageSender sender;
@property (assign, nonatomic) MessageStatus status;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *chat_id;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSDate *sentDate;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *persist;
@property (strong, nonatomic) NSString *notification;
@property (strong, nonatomic) NSData *imgData;
@property (nonatomic) BOOL isDownloaded;
@property (nonatomic) BOOL isSynced;
@property (nonatomic) BOOL isExpired;
@property (nonatomic) BOOL isLocal;
@property (nonatomic) BOOL isActivity;

@property (assign, nonatomic) CGFloat height;

+(Message *)messageFromDictionary:(NSDictionary *)dictionary;

@end
