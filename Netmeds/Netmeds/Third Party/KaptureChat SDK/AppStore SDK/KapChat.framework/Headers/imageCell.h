//
//  imageCell.h
//  ChatTest
//
//  Created by ios-Developer on 09/03/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *imageText;
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
- (IBAction)downloadBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *userDownloadBtn;
- (IBAction)userDownloadBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *userImgText;
@property (weak, nonatomic) IBOutlet UIButton *statusIcon;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *userIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dataIndicator;

@property (strong, nonatomic) IBOutlet UIColor *rMsgThemeColor;
@property (strong, nonatomic) IBOutlet UIColor *rMsgThemeTextcolor;
@property (strong, nonatomic) IBOutlet UIColor *sMsgThemeColor;
@property (strong, nonatomic) IBOutlet UIColor *sMsgThemeTextcolor;

@property (nonatomic) CGFloat chatFontSize;
@property (strong, nonatomic) NSString* chatFontName;
@property (nonatomic) CGFloat timeFontSize;

@property (weak, nonatomic) IBOutlet UIColor *chatBorderColor;
@property (nonatomic) CGFloat chatBorderWidth;

@end
