//
//  LocalStorage.h
//  BAChat
//
//  Created by ios-Developer on 28/05/18.
//  Copyright © 2018 ios-Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"

@interface LocalStorage : NSObject

+(id)sharedInstance;
+(void)saveMessage:(Message *)message;
+(void)saveMessages:(NSArray *)messages;
-(NSArray *)queryMessagesForChatID:(NSString *)chatId;

@end
