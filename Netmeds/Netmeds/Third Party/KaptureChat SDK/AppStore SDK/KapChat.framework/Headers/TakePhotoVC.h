//
//  TakePhotoVC.h
//  KapChat
//
//  Created by ios-Developer on 04/10/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TakePhotoVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)uploadPhoto:(UIButton *)sender;

@property (strong, nonatomic) NSData *imgData;
@property (strong, nonatomic) NSString *imgUrlString;
@property (weak, nonatomic) IBOutlet UIColor *backItemColor;
@property (strong, nonatomic) UIImage *backItemImage;
@property (weak, nonatomic) IBOutlet UIColor *barColor;
@property (strong, nonatomic) NSString *imageTitle;
@property (weak, nonatomic) IBOutlet UIColor *titleColor;

@property (strong, nonatomic) NSString *imgName;
@property (strong, nonatomic) NSString *imgSize;
@property (strong, nonatomic) NSString *imgPath;

@property (weak, nonatomic) IBOutlet UIButton *uploadPhoto;

@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
- (IBAction)backBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *dataView;

@end
