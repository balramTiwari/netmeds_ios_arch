//
//  ChatMessageVC.h
//  BBChatSample
//
//  Created by ios-Developer on 05/05/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Message.h"


@protocol ChatMessageDelegate

@optional
-(void)addChatData:(Message *)chatMessage;
-(void)backBtnClicked:(UIBarButtonItem *)button;

@end

@interface ChatMessageVC : UIViewController<ChatMessageDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, weak) id <ChatMessageDelegate> delegate;
//@property (strong, nonatomic) IBOutlet UIColor *topViewColor;
@property (nonatomic) NSInteger statusBarTag;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *kapTitle;

/* To Change font and size*/
-(void)changeFont:(NSString *)fontName fontSize:(CGFloat)fontSize timeFontSize:(CGFloat)timeFontSize;
//-(void)setChatBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth;

-(void)changeHeaderColor:(UIColor *)redcolor;
-(void)isChatHeaderShown:(BOOL)isShown;
-(void)changeHeaderColorWithRGB:(CGFloat)value1 value2:(CGFloat)value2 value3:(CGFloat)value3;
-(void)chatHeaderTitle:(NSString*)Title titleColor:(UIColor *)titleColor leftItemImage:(UIImage *)leftImage rightItemImage:(UIImage *)rightImage;
-(void)setChatRightItem:(UIImage *)rightImage rightItemColor:(UIColor *)rightItemColor;
- (IBAction)backBtn:(UIButton *)sender;
-(void)chatMessageReceiverCellHexColor:(int)cellColor withTextHexColor:(int)textColor;
- (IBAction)clearChatBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *clearChatBtn;

@property (strong, nonatomic) IBOutlet UIColor *chatThemeColor;
@property (strong, nonatomic) IBOutlet UIColor *chatThemeTextColor;
@property (strong, nonatomic) IBOutlet UIColor *sThemeColor;
@property (strong, nonatomic) IBOutlet UIColor *sThemeTextcolor;
@property (strong, nonatomic) IBOutlet UIImage *backgroundImage;
@property (strong, nonatomic) IBOutlet UIColor *vcBackgroundColor;
@property (strong, nonatomic) NSString *chatTitle;
@property (strong, nonatomic) NSString *rightImageText;

@end

/*ImageVC *hp =[[ImageVC alloc] initWithNibName:@"ImageVC" bundle:[NSBundle bundleForClass:ImageVC.self]];
 hp.imgData=data;
 hp.backItemColor=self.leftItemColor;
 hp.backItemImage=self.leftItemImage;
 hp.imgUrlString=imgUrlString;
 [self.navigationController pushViewController:hp animated:YES];
 
 
 NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
 NSString *trimmedString = [self.chatTextView.text stringByTrimmingCharactersInSet:charSet];
 
 if(![trimmedString isEqualToString:@""])
 {
 NSString *messageID=[[[XMPPSetup sharedInstance] xmppStream] generateUUID];
 
 msgString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
 
 [[ChatHandlerVC sharedInstance] sendMessageToServer:msgString type:@"type" url:@"" textlabel:@"" mime:@"" toChat:@"" messageId:messageID];
*/
