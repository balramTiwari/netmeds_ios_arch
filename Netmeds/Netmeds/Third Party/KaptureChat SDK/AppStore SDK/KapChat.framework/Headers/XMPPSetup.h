//
//  XMPPSetup.h
//  chatFramework
//
//  Created by ios-Developer on 17/03/17.
//  Copyright © 2017 ios-Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@import XMPPFramework;
#import "XMPPMessageArchiveManagement.h"
#import "ChatMessageVC.h"

@interface XMPPSetup : NSObject<XMPPRosterDelegate,ChatMessageDelegate,XMPPMessageArchiveManagementDelegate,XMPPParserDelegate,NSXMLParserDelegate>
{
    XMPPStream *xmppStream;
    XMPPReconnect *xmppReconnect;
    XMPPRoster *xmppRoster;
    XMPPRosterCoreDataStorage *xmppRosterStorage;
    XMPPvCardCoreDataStorage *xmppvCardStorage;
    XMPPvCardTempModule *xmppvCardTempModule;
    XMPPvCardAvatarModule *xmppvCardAvatarModule;
    XMPPCapabilities *xmppCapabilities;
    XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts;
    XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingStorage;
    XMPPMessageArchiving *xmppMessageArchivingModule;
    
    NSString *password;
    BOOL customCertEvaluation;
    BOOL isXmppConnected;
    BOOL isOpen;
    
    ChatMessageVC *chatViewController;
}
@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong, readonly) XMPPRoster *xmppRoster;
@property (nonatomic, strong, readonly) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (nonatomic, strong, readonly) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, strong, readonly) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, strong, readonly) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong, readonly) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;

@property (readwrite, copy) NSXMLElement *preferences;
- (NSManagedObjectContext *)managedObjectContext_roster;
- (NSManagedObjectContext *)managedObjectContext_capabilities;

@property(strong, nonatomic)NSManagedObjectContext *managedObjectContext;
@property(strong, nonatomic)NSManagedObjectModel *managedObjectModel;
@property(strong, nonatomic)NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (void)saveContext;
-(NSURL *)applicationDocumentsDirectory;

- (BOOL)connect;
- (void)disconnect;

+ (id)sharedInstance;

-(void)initialSetup;

- (void)setupStream;

- (void)goOnline;
- (void)goOffline;

- (void)teardownStream;

@end
