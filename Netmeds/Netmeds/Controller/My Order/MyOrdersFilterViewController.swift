//
//  MyOrdersFilterViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class MyOrdersFilterViewController: UIViewController {
    @IBOutlet weak private var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var tblViewFilter: UITableView!
    private var completionHandler: ((String) -> ())?
    var selectedStatus: String = "Recent"
    var statusList: [String] = [String]()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension MyOrdersFilterViewController {
    private func setUpUserInterface() {
        registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        tblViewFilter.tableFooterView = UIView()
        tblViewFilter.registerNibForCell(MyOrdersFilterTblViewCell.self)
    }
    
    func onCompletionHandler(For completionHandler: @escaping (_ selectedStatus: String) -> () ) {
        self.completionHandler = completionHandler
    }
}

// MARK: All Button Functions
extension MyOrdersFilterViewController {
    @IBAction private func performCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func performClearAllButton() {
        selectedStatus = "Recent"
        self.dismiss(animated: true) {
            self.completionHandler?(self.selectedStatus) ?? ()
        }
    }
    
    @IBAction private func performApplyButton() {
        self.dismiss(animated: true) {
            self.completionHandler?(self.selectedStatus) ?? ()
        }
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension MyOrdersFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewHeightConstraint.constant = tblViewFilter.contentSize.height + 40 //+ 25 + 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let status = statusList[indexPath.row]
        let statusCell: MyOrdersFilterTblViewCell = tblViewFilter.dequeueReusableCellForIndexPath(indexPath)
        statusCell.configCell(status: status, selectedStatus: selectedStatus)
        return statusCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedStatus = statusList[indexPath.row]
        tblViewFilter.reloadData()
    }
}

