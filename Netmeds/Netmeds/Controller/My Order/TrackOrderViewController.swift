//
//  TrackOrderViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 12/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol TrackOrdersDetailDelegateHandlers: class {
    func successProcess()
}

class TrackOrderViewController: BaseViewController {

    @IBOutlet weak private var tblViewTrackOrder: UITableView!
    private var trackOrderViewModel = TrackOrderViewModel()
    var orderID: String = ""

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension TrackOrderViewController {
    private func setUpUserInterface() {
        trackOrderViewModel.delegate = self
        registerTableViewCells()
        getTrackOrdersDetailService()
    }
    
    private func registerTableViewCells() {
        tblViewTrackOrder.tableFooterView = UIView()
        tblViewTrackOrder.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewTrackOrder.registerNibForCell(TrackOrderTblViewCell.self)
        tblViewTrackOrder.registerNibForCell(DeliveryAddressTblViewCell.self)
    }
}

// MARK: All Button Functions
extension TrackOrderViewController {
}

// MARK: All API Functions
extension TrackOrderViewController {
    func getTrackOrdersDetailService() {
        let params: JSONArrayOfDictString = [["orderId": orderID]]
        trackOrderViewModel.getTrackOrderDetailsService(params: params)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension TrackOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return trackOrderViewModel.trackOrderHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackOrderViewModel.trackOrderHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = trackOrderViewModel.trackOrderHeaderModel[section]
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewTrackOrder.frame.size.width, height: 60.0))
        headerView.backgroundColor = .clear
        guard let headerTblCell: CartHeaderTblViewCell = tblViewTrackOrder.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return headerView }
        headerTblCell.contentView.frame = headerView.frame
        headerTblCell.configCell(title: headerModel.name)
        headerView.addSubview(headerTblCell.contentView)
        return headerView
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let headerModel = homeViewModel.homeHeaderModel[indexPath.section]
    //        let headerHeight = headerModel.headerHeight
    //        var byRoundingCorners: UIRectCorner = headerHeight > 0 ? [.bottomLeft, .bottomRight]: [.allCorners]
    //        if headerModel.type == .uploadPrescriptions || headerModel.type == .firstMembership || headerModel.type == .onlineConsultation {
    //            byRoundingCorners = [.allCorners]
    //        }
    //        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: byRoundingCorners, cornerRadii: CGSize(width: 10, height: 10))
    //        let maskLayer = CAShapeLayer()
    //        maskLayer.frame = cell.layer.bounds
    //        maskLayer.path = maskPath.cgPath
    //        cell.layer.mask = maskLayer
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = trackOrderViewModel.trackOrderHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .banner:
            return UITableViewCell()
        case .orderStatus:
            let orderStatusCell: TrackOrderTblViewCell = tblViewTrackOrder.dequeueReusableCellForIndexPath(indexPath)
            orderStatusCell.configCell(rowModel: rowModel, row: indexPath.row, totalCount: headerModel.rowModel.count)
            return orderStatusCell
        case .deliveryAddress:
            let deliveryAddressCell: DeliveryAddressTblViewCell = tblViewTrackOrder.dequeueReusableCellForIndexPath(indexPath)
            deliveryAddressCell.configCell(rowModel: rowModel)
            return deliveryAddressCell
        default:
            return UITableViewCell()
        }
    }
}

//MARK:- Track Orders Detail Delegate Handlers
extension TrackOrderViewController: TrackOrdersDetailDelegateHandlers {
    func successProcess() {
        tblViewTrackOrder.reloadData()
    }
}
