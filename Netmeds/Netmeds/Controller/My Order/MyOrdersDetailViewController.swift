//
//  MyOrdersDetailViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 11/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

protocol MyOrdersDetailDelegateHandlers: class {
    func successProcess()
}

import UIKit

class MyOrdersDetailViewController: BaseViewController {
    
    @IBOutlet weak private var tblViewMyOrdersDetail: UITableView!
    private var myOrdersDetailViewModel = MyOrdersDetailViewModel()
    var orderID: String = ""
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension MyOrdersDetailViewController {
    private func setUpUserInterface() {
        myOrdersDetailViewModel.delegate = self
        registerTableViewCells()
        getMyOrdersDetailService()
    }
    
    private func registerTableViewCells() {
        tblViewMyOrdersDetail.tableFooterView = UIView()
        tblViewMyOrdersDetail.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewMyOrdersDetail.registerNibForCell(PaymentDetailsTblViewCell.self)
        tblViewMyOrdersDetail.registerNibForCell(OrderReviewProductTblViewCell.self)
        tblViewMyOrdersDetail.registerNibForCell(DeliveryAddressTblViewCell.self)
        tblViewMyOrdersDetail.registerNibForCell(MyOrdersDetailFooterTblViewCell.self)
        tblViewMyOrdersDetail.registerNibForCell(MyOrdersDetailCancelledItemsTblViewCell.self)
    }
}

// MARK: All Button Functions
extension MyOrdersDetailViewController {
}

// MARK: All API Functions
extension MyOrdersDetailViewController {
    func getMyOrdersDetailService() {
        let params: JSONArrayOfDictString = [["orderId": orderID]]
        myOrdersDetailViewModel.getMyOrdersDetailService(params: params)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension MyOrdersDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return myOrdersDetailViewModel.myOrdersDetailHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrdersDetailViewModel.myOrdersDetailHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let footerModel = myOrdersDetailViewModel.myOrdersDetailHeaderModel[section].footerModel
        guard footerModel.name.isValidString else { return CGFloat.leastNormalMagnitude }
        return 45.0
    }
    
    //    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    //        let headerModel = homeViewModel.homeHeaderModel[section]
    //        guard headerModel.headerHeight > 0 else { return }
    //        let maskPath = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10))
    //        let maskLayer = CAShapeLayer()
    //        maskLayer.frame = view.bounds
    //        maskLayer.path = maskPath.cgPath
    //        view.layer.mask = maskLayer
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = myOrdersDetailViewModel.myOrdersDetailHeaderModel[section]
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewMyOrdersDetail.frame.size.width, height: 60.0))
        headerView.backgroundColor = .clear
        guard let headerTblCell: CartHeaderTblViewCell = tblViewMyOrdersDetail.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return headerView }
        headerTblCell.contentView.frame = headerView.frame
        headerTblCell.configCell(title: headerModel.name)
        headerView.addSubview(headerTblCell.contentView)
        return headerView
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let headerModel = homeViewModel.homeHeaderModel[indexPath.section]
    //        let headerHeight = headerModel.headerHeight
    //        var byRoundingCorners: UIRectCorner = headerHeight > 0 ? [.bottomLeft, .bottomRight]: [.allCorners]
    //        if headerModel.type == .uploadPrescriptions || headerModel.type == .firstMembership || headerModel.type == .onlineConsultation {
    //            byRoundingCorners = [.allCorners]
    //        }
    //        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: byRoundingCorners, cornerRadii: CGSize(width: 10, height: 10))
    //        let maskLayer = CAShapeLayer()
    //        maskLayer.frame = cell.layer.bounds
    //        maskLayer.path = maskPath.cgPath
    //        cell.layer.mask = maskLayer
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = myOrdersDetailViewModel.myOrdersDetailHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .banner:
            return UITableViewCell()
        case .orderDetails:
            let orderDetailsCell: PaymentDetailsTblViewCell = tblViewMyOrdersDetail.dequeueReusableCellForIndexPath(indexPath)
            orderDetailsCell.configCell(rowModel: rowModel, row: indexPath.row)
            return orderDetailsCell
        case .orders:
            let ordersCell: OrderReviewProductTblViewCell = tblViewMyOrdersDetail.dequeueReusableCellForIndexPath(indexPath)
            ordersCell.configCell(rowModel: rowModel)
            return ordersCell
        case .prescriptons:
            return UITableViewCell()
        case .paymentDetails:
            let paymentDetailsCell: PaymentDetailsTblViewCell = tblViewMyOrdersDetail.dequeueReusableCellForIndexPath(indexPath)
            paymentDetailsCell.configCell(rowModel: rowModel, row: indexPath.row)
            return paymentDetailsCell
        case .cancelledItems:
            let cancelledItemsCell: MyOrdersDetailCancelledItemsTblViewCell = tblViewMyOrdersDetail.dequeueReusableCellForIndexPath(indexPath)
            cancelledItemsCell.configCell(rowModel: rowModel)
            return cancelledItemsCell
        case .deliveryAddress:
            let deliveryAddressCell: DeliveryAddressTblViewCell = tblViewMyOrdersDetail.dequeueReusableCellForIndexPath(indexPath)
            deliveryAddressCell.configCell(rowModel: rowModel)
            return deliveryAddressCell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerModel = myOrdersDetailViewModel.myOrdersDetailHeaderModel[section].footerModel
        let footerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewMyOrdersDetail.frame.size.width, height: CGFloat.leastNormalMagnitude))
        guard footerModel.name.isValidString else { return footerView }
        footerView.backgroundColor = .white
        guard let footerTblCell: MyOrdersDetailFooterTblViewCell = tblViewMyOrdersDetail.dequeueReusableCell(withIdentifier: "MyOrdersDetailFooterTblViewCell") as? MyOrdersDetailFooterTblViewCell else { return footerView }
        footerView.frame = CGRect(x: 0, y: 0, width: tblViewMyOrdersDetail.frame.size.width, height: 45.0)
        footerTblCell.contentView.frame = footerView.frame
        footerTblCell.configCell(footerModel: footerModel)
        footerView.addSubview(footerTblCell.contentView)
        return footerView
    }
}

//MARK:- MyOrders Detail Delegate Handlers
extension MyOrdersDetailViewController: MyOrdersDetailDelegateHandlers {
    func successProcess() {
        tblViewMyOrdersDetail.reloadData()
    }
}
