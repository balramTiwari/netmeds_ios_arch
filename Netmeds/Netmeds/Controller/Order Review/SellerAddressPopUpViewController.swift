//
//  SellerAddressPopUpViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class SellerAddressPopUpViewController: UIViewController {
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblAddress: UILabel!
    @IBOutlet weak private var cornerView: UIView!
    var addressInfo: (title: String, address: String) = ("", "")

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpUserInterface()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cornerView.roundCorners([.topLeft, .topRight], radius: 16)
    }
}

// MARK: All Functions
extension SellerAddressPopUpViewController {
    private func setUpUserInterface() {
        setUpTitleInfo()
        setUpAddressInfo()
    }
    
    private func setUpTitleInfo() {
        lblTitle.text = addressInfo.title
    }
    
    private func setUpAddressInfo() {
        lblAddress.text = addressInfo.address
        guard addressInfo.address.isValidString else { return }
        guard addressInfo.address.contains("<br/>"), let htmlData = addressInfo.address.data(using: String.Encoding.unicode) else { return }
        let attrStr = try? NSMutableAttributedString (data: htmlData, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        let range = NSRange(location: 0, length: attrStr?.length ?? 100)
        attrStr?.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.NMSTextColor(with: 0.6)], range: range)
        attrStr?.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Lato-Regular", size: 14) as Any, range: range)
        lblAddress.attributedText = attrStr
    }
}

// MARK: All Button Functions
extension SellerAddressPopUpViewController {
    @IBAction private func performCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
}

