//
//  OrderReviewViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 05/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class OrderReviewViewController: BaseViewController {
    @IBOutlet weak private var tblViewOrderReview: UITableView!
    @IBOutlet weak private var bottomView: UIView!
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var btnProceed: UIButton!
    @IBOutlet weak private var lblTotalAmount: UILabel!
    private var cartViewModel = CartViewModel()
    var selectedAddress: AllAddressInfoModel?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension OrderReviewViewController {
    private func setUpUserInterface() {
        cartViewModel.delegate = self
        registerTableViewCells()
        getCartDetailsService()
        setUpRefreshControl()
    }
    
    private func registerTableViewCells() {
        tblViewOrderReview.tableFooterView = UIView()
        tblViewOrderReview.registerNibForCell(OrderReviewProductTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewOrderReview.registerNibForCell(PaymentDetailsTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartBecomeFirstMemberTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartUnDeliverableTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartOutOfStockTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartCouponCodeTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartNMSSuperCashTblViewCell.self)
        tblViewOrderReview.registerNibForCell(CartSeparatorTblViewCell.self)
        tblViewOrderReview.registerNibForCell(DisclaimerTblViewCell.self)
        tblViewOrderReview.registerNibForCell(DeliveryAddressTblViewCell.self)
        tblViewOrderReview.rowHeight = UITableView.automaticDimension
        tblViewOrderReview.estimatedRowHeight = 100
        tblViewOrderReview.sectionHeaderHeight = UITableView.automaticDimension
        tblViewOrderReview.estimatedSectionHeaderHeight = 100
    }
    
    private func setUpRefreshControl() {
        refreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.netmedsGreenColor()
        tblViewOrderReview.addSubview(refreshControl)
    }
    
    @objc private func handleRefresh() {
        self.getCartDetailsService()
    }
}

// MARK: All Button Functions
extension OrderReviewViewController {
    @IBAction private func performProceedButton() {
        let totalAmount: String = lblTotalAmount.text!.replace(string: "₹ ", replacement: "")
        self.navigateToPaymentGateWayVC(cartID: String(cartViewModel.cartID), orderValue: totalAmount)
    }
    
    @objc private func performSellerNameButton(_ sender: UIButton) {
        guard let index = cartViewModel.cartHeaderModel.index( where:{ $0.type == .products }) else { return }
        let sellerNameInfo = cartViewModel.cartHeaderModel[index].rowModel[sender.tag]
        let addressInfo: (title: String, address: String) = (sellerNameInfo.sellerName, sellerNameInfo.sellerAddress)
        self.navigateToSellerAddressPopUpVC(addressInfo: addressInfo)
    }
}

// MARK: All API Functions
extension OrderReviewViewController {
    private func getCartDetailsService() {
        cartViewModel.getCartInfoService(pageType: .orderReview)
        //        cartViewModel.getCartDetailsService(deliveryAddress: selectedAddress)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension OrderReviewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cartViewModel.cartHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartViewModel.cartHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerModel = cartViewModel.cartHeaderModel[section]
        guard headerModel.title.isValidString else { return CGFloat.leastNormalMagnitude }
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        let headerModel = cartViewModel.cartHeaderModel[section]
    //        var height: CGFloat = 0.0
    ////        var height: CGFloat = (headerModel.type == .separatorView || headerModel.type == .superCash || headerModel.type == .disclaimer) ? CGFloat.leastNormalMagnitude: 10.0
    //        guard headerModel.title.isValidString else { return height }
    //        height = 60.0
    //        return height
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        //        let headerModel = cartViewModel.cartHeaderModel[section]
    //        //        guard headerModel.type == .separatorView || headerModel.type == .promoCode || headerModel.type == .paymentDetails else { return 10.0 }
    //        return CGFloat.leastNormalMagnitude
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = cartViewModel.cartHeaderModel[section]
        guard headerModel.title.isValidString else { return UIView() }
        guard let headerTblCell: CartHeaderTblViewCell = tblViewOrderReview.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return UIView() }
        headerTblCell.configCell(title: headerModel.title)
        return headerTblCell
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerModel = cartViewModel.cartHeaderModel[section]
    //        var height: CGFloat = 0.0
    ////        var height: CGFloat = (headerModel.type == .separatorView || headerModel.type == .superCash || headerModel.type == .disclaimer) ? 0.0: 10.0
    //        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewOrderReview.frame.size.width, height: height))
    //        headerView.backgroundColor = .clear
    //        guard headerModel.title.isValidString else { return headerView }
    //        guard let headerTblCell: CartHeaderTblViewCell = tblViewOrderReview.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return headerView }
    //        height = 60.0
    //        headerView.frame = CGRect(x: 0, y: 0, width: tblViewOrderReview.frame.size.width, height: height)
    //        headerTblCell.contentView.frame = headerView.frame
    //        headerTblCell.configCell(title: headerModel.title)
    //        headerView.addSubview(headerTblCell.contentView)
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowModel = cartViewModel.cartHeaderModel[indexPath.section].rowModel
        let cornerRadius: CGFloat = indexPath.row == rowModel.count - 1 ? 10.0: 0.0
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let rowModel = cartViewModel.cartHeaderModel[indexPath.section].rowModel
    //        if indexPath.row == rowModel.count - 1 {
    //            let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 10, height: 10))
    //            let maskLayer = CAShapeLayer()
    //            maskLayer.frame = cell.layer.bounds
    //            maskLayer.path = maskPath.cgPath
    //            cell.layer.mask = maskLayer
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = cartViewModel.cartHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .becomeFirstMember:
            let firstMemberCell: CartBecomeFirstMemberTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            firstMemberCell.configCell(title: rowModel.title, subTitle: rowModel.subTitle)
            return firstMemberCell
        case .undeliverable:
            let unDeliverableCell: CartUnDeliverableTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            unDeliverableCell.configCell(title: rowModel.title, subTitle: rowModel.subTitle, btnTitle: rowModel.btnTitle)
            return unDeliverableCell
        case .medicinesOutOfStock:
            let outOfStockCell: CartOutOfStockTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            outOfStockCell.configCell(title: rowModel.title, btnTitle: rowModel.btnTitle)
            return outOfStockCell
        case .products:
            let cartCell: OrderReviewProductTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            cartCell.configCell(rowModel: rowModel, index: indexPath.row)
            cartCell.btnSellerName.addTarget(self, action: #selector(performSellerNameButton(_:)), for: .touchUpInside)
            return cartCell
        case .promoCode:
            let couponCodeCell: CartCouponCodeTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            couponCodeCell.configCell(title: rowModel.title, desc: rowModel.desc, youSaveText: rowModel.sellerName, isCouponApplied: rowModel.isOutOfStock, isHideRightArrowImg: true)
            return couponCodeCell
        case .paymentDetails:
            let paymentDetailsCell: PaymentDetailsTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            paymentDetailsCell.configCell(title: rowModel.productName, price: rowModel.paymentDetailsPrice, type: rowModel.type)
            return paymentDetailsCell
        case .disclaimer:
            let disclaimerCell: DisclaimerTblViewCell = tblViewOrderReview.dequeueReusableCellForIndexPath(indexPath)
            return disclaimerCell
        default:
            return UITableViewCell()
        }
    }
}

//MARK:- Cart Delegate Handlers
extension OrderReviewViewController : CartDelegateHandlers {
    func successProcess(totalAmount: String, shippingAddressID: Int) {
        //        self.shippingAddressID = shippingAddressID
        lblTotalAmount.text = totalAmount
        tblViewOrderReview.reloadData()
        refreshControl.endRefreshing()
        bottomView.isHidden = false
    }
    
    func deliveryEstimationSuccessProcess() {
        tblViewOrderReview.reloadData()
        refreshControl.endRefreshing()
    }
    
    func applySuperCashProcess() {}
    func failureProcess(errorMessage message: String) {}
    func emptyCartProcess() {}
}
