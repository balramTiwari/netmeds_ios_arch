//
//  AddToCartViewController.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol AddToCartDelegateHandlers: class {
    func productDetailsSuccessProcess()
    func addToCartSuccessProcess()
    func failureProcess(errorMessage message: String)
}

class AddToCartViewController: BaseViewController {
    
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var imgView: UIImageView!
    @IBOutlet weak private var lblCategories: UILabel!
    @IBOutlet weak private var lblDrugType: UILabel!
    @IBOutlet weak private var lblPrice: UILabel!
    @IBOutlet weak private var lblStrikePrice: UILabel!
    @IBOutlet weak private var lblPackSizeType: UILabel!
    @IBOutlet weak private var lblManufacturerName: UILabel!
    @IBOutlet weak private var btnAddToCart: UIButton!
    @IBOutlet weak private var lblTotalAmount: UILabel!
    @IBOutlet weak private var lblCartCount: UILabel!
    @IBOutlet weak private var cornerView: UIView!
    var productID: Int = 0
    private var addToCartViewModel = AddToCartViewModel()
    private var maxSaleQty = 1
    private var productQuantity = 1
    private var addCartCompletionHandler: (() -> Void)?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cornerView.roundCorners([.topLeft, .topRight], radius: 10)
    }
}

// MARK: All Functions
extension AddToCartViewController {
    private func setUpUserInterface() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        addToCartViewModel.delegate = self
        addToCartViewModel.productDetailsService(productID: productID)
    }
    
    private func updateUI() {
        guard let product = addToCartViewModel.productDetails?.result?.product else { return }
        if let maxQty = product.maxQtyInOrder, let stockQty = product.stockQty {
            maxSaleQty = maxQty < stockQty ? maxQty : stockQty
        }
        lblDrugType.isHidden = !product.rxRequired!
        self.lblManufacturerName.text = ""
        if let manufacturer = product.manufacturer, let name = manufacturer.name, name.isValidString {
            self.lblManufacturerName.text = "By " + name
        }
        lblName.text = product.displayName!
        lblPrice.text = "₹ 0.00"
        if let productPrice = product.sellingPrice {
            self.lblPrice.text = "₹ \(NSNumber(value: productPrice).formatFractionDigits())"
        }
        self.lblStrikePrice.isHidden = true
        if  let mrp = product.mrp, let discountRate = product.discountRate, discountRate != 0.0 {
            self.lblStrikePrice.isHidden = false
            let numberValue = NSNumber(value: mrp)
            self.lblStrikePrice.attributedText = numberValue.formatFractionDigits().strikeEffect()
        } else {
            self.lblStrikePrice.isHidden = true
            if let productPrice = product.sellingPrice {
                lblPrice.attributedText = (NSNumber(value: productPrice).formatFractionDigits()).setPriceWithMRPText()
            }
        }
        self.lblCategories.isHidden = true
        if let categories = product.categories, let firstCategory = categories.first, let breadCrumbs = firstCategory.breadCrumbs {
            var names = [String]()
            for value in breadCrumbs {
                if value.level == 1 || value.level == 2 {
                    if let name = value.name {
                        names.append(name)
                    }
                }
            }
            self.lblCategories.text = names.joined(separator: " ")
            self.lblCategories.isHidden = !(names.count > 0)
        }
        self.imgView.image = UIImage(named: "no_experience")
        //        let imagePath = NMUser.shared.configUrl?.result?.product_image_url_base_path
        //        let imgPX = "120x120/"
        //        if let imgStr = item.image_paths?[0], let imagePath = imagePath, let url = URL.init(string: "\(imagePath)\(imgPX)\(imgStr)") {
        //            self.thumbnailImage.kf.setImage(with: url, placeholder: UIImage(named: "no_experience"))
        //        }
        if let availableStatus = product.availabilityStatus, availableStatus == "A" {
            btnAddToCart.isEnabled = true
        } else {
            self.btnAddToCart.setTitleColor(UIColor.lightGray, for: .normal)
            btnAddToCart.isEnabled = false
        }
        updateTotalAmount()
    }
    
    private func updateTotalAmount() {
        guard let product = addToCartViewModel.productDetails?.result?.product, let price =  product.sellingPrice else { return }
        let total =  NSNumber(value: price * Double(productQuantity))
        self.lblTotalAmount.text = "₹ \(total.formatFractionDigits())"
    }
    
    func actionAddCartHandle(_ completionHandler: @escaping () -> Void) {
        addCartCompletionHandler = completionHandler
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}


// MARK: All Button Functions
extension AddToCartViewController {
    @IBAction private func performCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func perfromIncrementCartButton() {
        if productQuantity < maxSaleQty {
            productQuantity += 1
            self.lblCartCount.text = "\(productQuantity)"
            updateTotalAmount()
        }
    }
    
    @IBAction private func performDecrementCartButton() {
        if productQuantity > 1 {
            productQuantity -= 1
            self.lblCartCount.text = "\(productQuantity)"
            updateTotalAmount()
        }
    }
    
    @IBAction private func performAddToCartButton() {
        let quantity = Int(self.lblCartCount.text ?? "0") ?? 0
        addToCartViewModel.addToCartService(productID: productID, quantity: quantity)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
//extension AddToCartViewController: UITableViewDelegate, UITableViewDataSource {
//}

//MARK:- Add To Cart Delegate Handlers
extension AddToCartViewController: AddToCartDelegateHandlers {
    func productDetailsSuccessProcess() {
        updateUI()
    }
    
    func addToCartSuccessProcess() {
        UIDevice.vibrate()
        self.dismiss(animated: true) {
            self.addCartCompletionHandler?()
        }
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
