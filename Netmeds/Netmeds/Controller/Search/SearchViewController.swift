//
//  SearchViewController.swift
//  Netmeds
//
//  Created by Netmeds1 on 22/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol SearchDelegateHandlers: class {
    func recentSearchItemsSuccessProcess()
    func algoliaHitsSuccessProcess()
    func algoliaNoHitsProcess()
}

protocol SearchTblViewCellDelegateHandlers: class {
    func btnAddToCartTapped(cell: SearchTblViewCell)
    func btnManufacturerNameTapped(cell: SearchTblViewCell)
}

class SearchViewController: BaseViewController {
    @IBOutlet weak private var txtFldSearch: UITextField!
    @IBOutlet weak private var tblViewSearch: UITableView!
    private var searchViewModel = SearchViewModel()
    private var currentPage: UInt = 0
    private var isDataLoading: Bool = false
    var searchText: String = ""
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarWithCartIcon()
    }
}

// MARK: All Functions
extension SearchViewController {
    private func setUpUserInterface() {
        searchViewModel.delegate = self
        registerTableViewCells()
        setUpSearchTxtFld()
        setUpSearchIcon()
        fetchRecentSearchItemsFromDB()
    }
    
    private func registerTableViewCells() {
        tblViewSearch.tableFooterView = UIView()
        tblViewSearch.registerNibForCell(SearchTblViewCell.self)
        tblViewSearch.registerNibForCell(SearchFooterTblViewCell.self)
        tblViewSearch.registerNibForCell(RecentSearchTblHeaderCell.self)
        tblViewSearch.sectionHeaderHeight = UITableView.automaticDimension
        tblViewSearch.estimatedSectionHeaderHeight = 100
        tblViewSearch.sectionFooterHeight = UITableView.automaticDimension
        tblViewSearch.estimatedSectionFooterHeight = 100
    }
    
    private func setUpSearchTxtFld() {
        txtFldSearch.autocorrectionType = .no
        if searchText.isValidString  {
            self.txtFldSearch.text = searchText
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.algoliaSearchService()
            }
        }
        self.txtFldSearch.attributedPlaceholder = NSAttributedString(string: "Search here...",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }
    
    private func setUpSearchIcon() {
        self.txtFldSearch.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "search")
        imageView.image = image
        self.txtFldSearch.rightView = imageView
    }
    
    private func removeSearchIcon() {
        self.txtFldSearch.rightView = nil
        self.txtFldSearch.clearButtonMode = .always
    }
    
    private func removealgoliaSearchInfo() {
        searchViewModel.algoliaSearchModel.removeAll()
        resetSearchTextAndPage()
        tblViewSearch.reloadData()
    }
    
    private func resetSearchTextAndPage() {
        searchText = ""
        currentPage = 0
    }        
}

// MARK: All Button Functions
extension SearchViewController {
    @objc private func performClearAllButton() {
        searchViewModel.deleteAllRecentSearchInfoFromDB()
    }
}

// MARK: All API Functions
extension SearchViewController {
    private func fetchRecentSearchItemsFromDB() {
        searchViewModel.fetechRecentSearchItemsFromDB()
    }
    
    private func algoliaSearchService() {
        self.isDataLoading = true
        guard searchText.isValidString else { return }
        searchViewModel.algoliaSearchService(searchText: searchText, page: currentPage)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchViewModel.algoliaSearchModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard searchViewModel.hasRecentSearchInfo else { return CGFloat.leastNormalMagnitude}
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard !searchText.isValidString else { return CGFloat.leastNormalMagnitude}
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard searchViewModel.hasRecentSearchInfo else { return UIView() }
        guard let headerTblCell: RecentSearchTblHeaderCell = tblViewSearch.dequeueReusableCell(withIdentifier: "RecentSearchTblHeaderCell") as? RecentSearchTblHeaderCell else { return UIView() }
        headerTblCell.btnClearAll.addTarget(self, action: #selector(self.performClearAllButton), for: .touchUpInside)
        return headerTblCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == searchViewModel.algoliaSearchModel.count - 5, currentPage < searchViewModel.totalPages, !isDataLoading {
            currentPage += 1
            algoliaSearchService()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let alogliaSearchModel = searchViewModel.algoliaSearchModel[indexPath.row]
        let searchCell: SearchTblViewCell = tblViewSearch.dequeueReusableCellForIndexPath(indexPath)
        searchCell.configCell(alogliaSearchModel: alogliaSearchModel)
        searchCell.delegate = self
        return searchCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard !searchText.isValidString else { return UIView() }
        guard let footerTblCell: SearchFooterTblViewCell = tblViewSearch.dequeueReusableCell(withIdentifier: "SearchFooterTblViewCell") as? SearchFooterTblViewCell else { return UIView() }
        return footerTblCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alogliaSearchModel = searchViewModel.algoliaSearchModel[indexPath.row]
        if let productCode = alogliaSearchModel.productCode {
            self.navigateToProductDetailsVC(productCode: productCode)
        }
        searchViewModel.addRecentSearchIntoDB(item: alogliaSearchModel)
    }
}

//MARK:- TextField Delegate Functions
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        setUpSearchIcon()
        removealgoliaSearchInfo()
        fetchRecentSearchItemsFromDB()
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Google Analytics Tracker
        GoogleAnalytics.trackEvent(withScreen: "Search Page", category: "Other Actions", label: "Search Page", action: "Product Searched")
        let nsString:NSString? = textField.text as NSString?
        let text = nsString?.replacingCharacters(in:range, with:string).trimmingCharacters(in: CharacterSet.whitespaces)
        removealgoliaSearchInfo()
        searchText = text ?? ""
        searchText.isValidString ? removeSearchIcon(): setUpSearchIcon()
        algoliaSearchService()
        return true
    }
}

//MARK:- Search Delegate Handlers
extension SearchViewController: SearchDelegateHandlers {
    
    func recentSearchItemsSuccessProcess() {
        tblViewSearch.reloadData()
    }
    
    func algoliaHitsSuccessProcess() {
        guard searchText.isValidString else { removealgoliaSearchInfo(); return }
        tblViewSearch.reloadData()
        self.isDataLoading = false
    }
    
    func algoliaNoHitsProcess() {
        guard currentPage == 0 else { return }
        removealgoliaSearchInfo()
    }
}

//MARK:- Search TblView Cell Delegate Handlers
extension SearchViewController: SearchTblViewCellDelegateHandlers {
    func btnAddToCartTapped(cell: SearchTblViewCell) {
        guard let indexPath = tblViewSearch.indexPath(for: cell) else { return }
        let productID: Int = searchViewModel.algoliaSearchModel[indexPath.row].productCode!
        self.navigateToAddToCartVC(productID: productID)
    }
    
    func btnManufacturerNameTapped(cell: SearchTblViewCell) {
        
    }
}

