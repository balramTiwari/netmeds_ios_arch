//
//  BaseViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit
import WebEngage

class BaseViewController: AnalyticsViewController {
    lazy var refreshControl = UIRefreshControl()
    lazy var wegAnalytics: WEGAnalytics = WebEngage.sharedInstance().analytics

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    deinit {
        print("DEINIT - ", type(of: self))
    }
}

// MARK: Show Toast Message
extension BaseViewController {
    func createToastWith(message: String) {
        self.view.hideAllToasts()
        self.view.makeToast(message, duration: kToastViewDuration, point: self.view.center, title: nil, image: nil, style: ToastStyle.init(), completion: nil)
    }
    
    func addSuccessViewCart() {
        let height = CGFloat(48)
        let y = self.view.frame.size.height - height
        let viewCart = CartToastView(frame: CGRect(x: 0, y: y, width: self.view.bounds.size.width, height: height))
        viewCart.alpha = 0
        self.view.addSubview(viewCart)
        viewCart.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            NSLayoutConstraint.activate([
                viewCart.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                viewCart.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
                viewCart.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -height),
                viewCart.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            ])
        } else {
            // Fallback on earlier versions
            NSLayoutConstraint.activate([
                viewCart.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor),
                viewCart.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
                viewCart.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor,constant: -height),
                viewCart.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor),
            ])
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            viewCart.alpha = 1
        }) { (_) in }
        viewCart.actionViewCartHandle(controlEvents: .touchUpInside) {
            viewCart.removeFromSuperview()
            self.performCartBarButton()
        }
    }
}

// MARK: SetUp Bar Button Items
extension BaseViewController {
    func setUpOfferBarBtnItem() -> UIBarButtonItem {
        let btnOffer = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        btnOffer.setImage(UIImage(named: "offer_outline"), for: .normal)
        btnOffer.addTarget(self, action: #selector(performOfferBarButton), for: .touchUpInside)
        return UIBarButtonItem(customView: btnOffer)
    }

    func setUpBackBarBtnItem() -> UIBarButtonItem {
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 35, height: 35)))
        button.setBackgroundImage(UIImage(named: "arrowBack.png"), for: .normal)
        button.addTarget(self, action: #selector(self.performBackBarButton), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }

    func setUpCartBarBtnItem() -> UIBarButtonItem {
        let btnCart = SSBadgeButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        btnCart.setImage(UIImage(named: "shoppingCart"), for: .normal)
        btnCart.addTarget(self, action: #selector(performCartBarButton), for: .touchUpInside)
        return UIBarButtonItem(customView: btnCart)
    }

    func setUpSearchBarBtnItem() -> UIBarButtonItem {
        let searchButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        searchButton.addTarget(self, action: #selector(performSearchBarButton), for: .touchUpInside)
        return UIBarButtonItem(customView: searchButton)
    }

    func setUpNavigationTitleView() {
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 115, height: 38))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "netmeds_logo_full")
        imageView.image = image
        imageView.isUserInteractionEnabled = true
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(performTitleViewLogoGesture))
        mytapGestureRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(mytapGestureRecognizer)
    }

    func navigationBarWithSearchIcon() {
        hideNavigationBarShadow(with: .white)
        self.navigationItem.leftBarButtonItem = setUpBackBarBtnItem()
        self.navigationItem.rightBarButtonItems = [setUpSearchBarBtnItem()]
    }

    func navigationBarWithCartIcon() {
        hideNavigationBarShadow(with: .white)
        self.navigationItem.leftBarButtonItem = setUpBackBarBtnItem()
        self.navigationItem.rightBarButtonItems = [setUpCartBarBtnItem()]
    }

    func navigationBarForChildViewController() {
        hideNavigationBarShadow(with: .white)
        self.navigationItem.rightBarButtonItems = nil
        self.navigationItem.leftBarButtonItem = setUpBackBarBtnItem()
    }

    func navigationBarWithSearchCartIcons() {
        hideNavigationBarShadow(with: .white)
        self.navigationItem.leftBarButtonItem = setUpBackBarBtnItem()
        self.navigationItem.rightBarButtonItems = [setUpCartBarBtnItem(), setUpSearchBarBtnItem()]
    }
    
    func navigationBarForTabViewController() {
        hideNavigationBarShadow(with: .white)
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
        self.tabBarController?.navigationItem.rightBarButtonItems = nil
    }
    
    func hideNavigationBarShadow(with barTintColor: UIColor) {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = barTintColor
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    func navigationBarForLoginViewController() {
        hideNavigationBarShadow(with: .white)
        self.navigationItem.leftBarButtonItem = nil
        //        self.navigationItem.rightBarButtonItems = [createSkipBarButtonItem()]
    }
    
    func navigationBarForHomeViewController() {
        hideNavigationBarShadow(with: UIColor.NMLogoGreenColor())
        let btnBadge = SSBadgeButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        btnBadge.setImage(UIImage(named: "shopping_cart_white"), for: .normal)
        btnBadge.addTarget(self, action: #selector(performCartBarButton), for: .touchUpInside)
        let barBtnCart = UIBarButtonItem(customView: btnBadge)
        self.navigationItem.rightBarButtonItems = [barBtnCart, setUpOfferBarBtnItem()]
        let btnLogo = UIButton(type: .custom)
        btnLogo.setImage(UIImage(named: "netmeds_final_logo_white.png"), for: .normal)
        btnLogo.isUserInteractionEnabled = false
        let barBtnLogo = UIBarButtonItem(customView: btnLogo)
        self.navigationItem.leftBarButtonItem = barBtnLogo
    }
}

// MARK: All Navigation Functions
extension BaseViewController {
    @objc private func performCartBarButton() {
        self.navigateToCartVC()
    }
    
    @objc private func performOfferBarButton() {
        self.navigateToAllOffersVC()
    }

    @objc private func performBackBarButton() {
        self.popViewController()
    }
    
    @objc private func performTitleViewLogoGesture() {
        self.popToRootViewController()
    }

    @objc private func performSearchBarButton() {
        self.navigateToSearchVC()
    }
}

// MARK: Show All Empty View Functions
extension BaseViewController {
    func showCartEmptyView() {
        let yPosition: CGFloat = 43.0
        if let emptyView = EmptyView.instanceFromNib() as? EmptyView {
            emptyView.updateUI(type: .cart)
            emptyView.frame = CGRect.init(x: 0 ,y: yPosition, width: screenWidth, height: self.view.frame.height - yPosition)
            self.view.addSubview(emptyView)
        }
    }
}
