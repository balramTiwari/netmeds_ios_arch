//
//  ChangePasswordViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 19/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol ChangePasswordDelegateHandlers: class {
    func resendOTPSuccessProcess(randomKey key: String)
    func changePasswordSuccessProcess()
    func failureProcess(errorMessage message: String)
}

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak private var verifyTitleLabel: UILabel!
    @IBOutlet weak private var verifySubTitleLabel: UILabel!
    @IBOutlet weak private var phoneNumberTextField: UITextField!
    @IBOutlet weak private var phoneNumberLabel: UILabel!
    @IBOutlet weak private var changePhoneNumberButton: UIButton!
    @IBOutlet weak private var verifyButton: UIButton!
    @IBOutlet weak private var resendOTPButton: UIButton!
    @IBOutlet weak private var otpSentLabel: UILabel!
    @IBOutlet weak private var otpTimerLabel: UILabel!
    @IBOutlet weak private var otpCountDownLabel: UILabel!
    @IBOutlet weak private var otpView: OTPView!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var passwordConfirmTextField: UITextField!
    @IBOutlet weak private var showPasswordImagePass: UIImageView!
    @IBOutlet weak private var showPasswordImageConfirmPass: UIImageView!
    @IBOutlet weak private var passwordView: UIView!
    @IBOutlet weak private var separatorView: UIView!
    @IBOutlet weak private var passwordWarningLabel: UILabel!
    @IBOutlet weak private var passwordLabel: UILabel!
    @IBOutlet weak private var closeViewHeightConstraint: NSLayoutConstraint!
    private var timerSeconds = kOTPTimerMaxSeconds
    private var timer: Timer!
    var randomKey: String = ""
    private var changePasswordViewModel = ChangePasswordViewModel()
    var isFromEditProfile = false

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        phoneNumberTextField.endEditing(true)
    }
}

// MARK: All Functions
extension ChangePasswordViewController {
    private func setUpUserInterface() {
        changePasswordViewModel.delegate = self
        enableDisableResendOTPButton(isEnabled: false)
        scheduledTimer()
        setUpUIText()
    }
    
    private func setUpUIText() {
        closeViewHeightConstraint.constant = isFromEditProfile ? 66 : 0
        passwordTextField.returnKeyType = .next
        passwordConfirmTextField.returnKeyType = .done
        phoneNumberLabel.text = kPhoneNumberText
        changePhoneNumberButton.setTitle(kChangeText, for: .normal)
        verifyButton.setTitle(kVerifyText, for: .normal)
        verifyTitleLabel.text = kVerifyText.capitalized
        verifySubTitleLabel.text = kVerifySubTitle
    }
    
    private func enableDisableResendOTPButton(isEnabled: Bool) {
        resendOTPButton.isEnabled = isEnabled
        resendOTPButton.tintColor = UIColor.mediumPinkTextColor()
        resendOTPButton.alpha = isEnabled ? 1.0 : 0.5
    }
    
    private func scheduledTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateOTPTimer), userInfo: nil, repeats: true)
    }
    
    @objc private func updateOTPTimer() {
        enableDisableResendOTPButton(isEnabled: (timerSeconds == 0) ? true : false)
        otpCountDownLabel.text = otpView.updateOTPTimer(with: timerSeconds)
        if timerSeconds == 0 {
            timer.invalidate()
        }
        timerSeconds -= 1
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension ChangePasswordViewController {
    @IBAction func handleShowPasswordAction(_ sender: Any) {
        showPasswordImagePass.alpha = (passwordTextField.isSecureTextEntry) ? 1.0 : 0.4
        passwordTextField.isSecureTextEntry = (passwordTextField.isSecureTextEntry) ? false : true
    }
    
    @IBAction func handleShowPasswordActionCPass(_ sender: Any) {
        showPasswordImageConfirmPass.alpha = (passwordConfirmTextField.isSecureTextEntry) ? 1.0 : 0.4
        passwordConfirmTextField.isSecureTextEntry = (passwordConfirmTextField.isSecureTextEntry) ? false : true
    }
    
    //    func handleVerifyMobileNumberSuccess(For action:@escaping (_ isValidPhoneNumber: Bool) -> () ) {
    //        self.action = action
    //    }
    
    @IBAction func handleCloseButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleChangePhoneNumberAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func handleVerifyButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        guard otpView.otpText().count > 5 else {
            self.createToastWith(message: kEnterOTPText); return }
        guard passwordTextField.text!.isValidPassword else {
            self.createToastWith(message: kPasswordWarningText); return }
        guard passwordTextField.text == passwordConfirmTextField.text else {
            self.createToastWith(message: kPwdAndConfirmPwdNotMatchWarningText); return }
        let params = [
            "mobile_no" : self.phoneNumberTextField.text! ,
            "rk": randomKey,
            "otp":otpView.otpText(),
            "new_passwd":passwordTextField.text!
        ]
        changePasswordViewModel.completeChangePasswordService(params: params)
    }
    
    @IBAction func handleResendButtonAction(_ sender: Any) {
        timerSeconds = kOTPTimerMaxSeconds
        otpView.clearTextField()
        scheduledTimer()
        changePasswordViewModel.reSendOTPService(randomKey: randomKey)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension ChangePasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            return (textField.text ?? "").limitCharactersLength(range: range, replacement: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            passwordConfirmTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == passwordTextField {
            if !passwordTextField.text!.isValidPassword {
                self.createToastWith(message:kPasswordWarningText)
            }
        } else if textField == passwordConfirmTextField {
            if passwordConfirmTextField.text != passwordTextField.text {
                self.createToastWith(message: kPwdAndConfirmPwdNotMatchWarningText)
            }
        }
    }
}

//MARK:- Change Passwor dDelegate Handlers
extension ChangePasswordViewController: ChangePasswordDelegateHandlers {
    func resendOTPSuccessProcess(randomKey key: String) {
        randomKey = key
    }
    
    func changePasswordSuccessProcess() {
        self.createToastWith(message: "Password changed successfully")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
