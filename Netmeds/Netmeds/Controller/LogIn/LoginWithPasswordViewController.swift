//
//  LoginWithPasswordViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 16/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol LogInViaPasswordDelegateHandlers: class {
    func otpSuccessProcess(randomKey key: String, pageSourceType: PageSourceType)
    func homePageProcess()
    func failureProcess(errorMessage message: String)
}

class LoginWithPasswordViewController: BaseViewController {
    
    @IBOutlet weak var signinLabel: UILabel!
    @IBOutlet weak var signInSubTitleLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var changePhoneNumberButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginUsingOTPButtomn: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var separatorView: UIView!
    private var logInViaPasswordViewModel = LogInViaPasswordViewModel()
    var phoneNo: String = ""
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
        phoneNumberTextField.text = phoneNo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension LoginWithPasswordViewController {
    private func setUpUserInterface() {
        logInViaPasswordViewModel.delegate = self
        setUpUIText()
    }
    
    private func setUpUIText() {
        loginButton.setTitle(kLogInText, for: .normal)
        loginUsingOTPButtomn.setTitle(kLogInUsingOTPText, for: .normal)
        orLabel.text = kORText
        phoneNumberLabel.text = kPhoneNumberText
        passwordLabel.text = kPasswordText
        changePhoneNumberButton.setTitle(kChangeText, for: .normal)
        forgotPasswordButton.setTitle(kForgotPasswordText, for: .normal)
    }
    
    private func updatePasswordErrorMsgColor(password: String) {
        if passwordTextField.isFirstResponder {
            passwordView.backgroundColor =  UIColor.white
            separatorView.backgroundColor = UIColor.separatorViewColor()
            passwordLabel.textColor = UIColor.netmedsGreenColor()
        } else {
            passwordView.backgroundColor = !(password.isEmpty) ? UIColor.white : UIColor.warningRedColor(with: 0.08)
            separatorView.backgroundColor = !(password.isEmpty) ? UIColor.separatorViewColor() : UIColor.warningRedColor(with: 1.0)
            passwordLabel.textColor = !(password.isEmpty) ? UIColor.netmedsGreenColor() : UIColor.warningRedColor(with: 1.0)
        }
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension LoginWithPasswordViewController {
    @IBAction func handleChangePhoneNumberAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleForgotPasswordAction(_ sender: Any) {
        let params = [
            "userid" : self.phoneNumberTextField.text!
        ]
        logInViaPasswordViewModel.initiateChangePasswordService(params: params)
    }
    
    @IBAction func handleLoginUsingOTPButton(_ sender: Any) {
        GoogleAnalytics.trackEvent(withScreen: "Sign In with OTP", category: "Login", label: "Login with OTP", action: "Login with OTP - Click")
        logInViaPasswordViewModel.sendOTPService(mobileNo: phoneNumberTextField.text!)
    }
    
    @IBAction func handleLoginButtonAction(_ sender: Any) {
        GoogleAnalytics.trackEvent(withScreen: "Sign In with Password", category: "Login", label: "Login with Password", action: "Password Entered - Proceed")
        passwordTextField.resignFirstResponder()
        guard let password = passwordTextField.text, password.isValidString else {
            self.createToastWith(message: kEnterPasswordText)
            self.updatePasswordErrorMsgColor(password: "")
            return
        }
        let params = [
            "userid" : phoneNumberTextField.text!,
            "passwd" : passwordTextField.text!,
            "channel" : kSourceApp
        ]
        logInViaPasswordViewModel.logInViaPasswordService(params: params)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension LoginWithPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTextField, let password = textField.text  {
            updatePasswordErrorMsgColor(password: password)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            return (textField.text ?? "").limitCharactersLength(range: range, replacement: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:- LogIn Pwd / OTP Delegate Functions
extension LoginWithPasswordViewController: LogInViaPasswordDelegateHandlers {
    
    func otpSuccessProcess(randomKey key: String, pageSourceType: PageSourceType) {
        switch pageSourceType {
        case .otp:
            self.navigateToLoginWithOTPVC(randomKey: key)
        case .changePassword:
            self.navigateToChangePasswordVC(randomKey: key)
        default:
            break
        }
    }
    
    func homePageProcess() {
        self.setHomeRootVC()
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
