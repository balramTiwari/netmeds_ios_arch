//
//  VerifyPhoneNumberViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol VerifyPhoneNoDelegateHandlers: class {
    func successProcess(randomKey key: String)
    func failureProcess(errorMessage message: String)
}

class VerifyPhoneNumberViewController: BaseViewController {
    
    @IBOutlet weak private var signinLabel: UILabel!
    @IBOutlet weak private var signInSubTitleLabel: UILabel!
    @IBOutlet weak private var phoneNumberTextField: UITextField!
    @IBOutlet weak private var termsAndServiceTextView: UITextView!
    @IBOutlet weak private var privacyAndPolicyTextView: UITextView!
    @IBOutlet weak private var phoneNumberLabel: UILabel!
    @IBOutlet weak private var continueButton: RoundButton!
    private let configManager = ConfigManager.shared
    private var privacyPolicyUrl: String?
    private var verifyPhoneNumberViewModel = VerifyPhoneNumberViewModel()
    var socialLogInInfo: SocialLogInInfoViewModel = SocialLogInInfoViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        phoneNumberTextField.endEditing(true)
    }
}

// MARK: All Functions
extension VerifyPhoneNumberViewController {
    private func setUpUserInterface() {
        verifyPhoneNumberViewModel.delegate = self
        self.privacyPolicyUrl = configManager.configDetails?.privacy ?? ""
        privacyAndPolicyTextView.handlePrivacyPolicyTextView(privacyPolicyUrl: configManager.configDetails?.privacy ?? "")
        termsAndServiceTextView.handleTermsAndServiceTextView(termsAndConditionUrl: configManager.configDetails?.terms ?? "")
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension VerifyPhoneNumberViewController {
    @IBAction private func handleLoginButtonAction(_ sender: Any) {
        guard let mobileNumber = phoneNumberTextField.text else {
            createToastWith(message: kEnterMobileNoText); return }
        guard mobileNumber.isValidPhoneNumber else {
            createToastWith(message: kEnterValidMobileNoText); return }
        socialLogInInfo.mobileNumber = mobileNumber
        verifyPhoneNumberViewModel.socialLogInService(socialLogInInfo: socialLogInInfo)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension VerifyPhoneNumberViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField.text ?? "").limitCharactersLength(range: range, replacement: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:- TextView Delegate Function
//extension VerifyPhoneNumberViewController: UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        let vc = WebViewViewController.instantiate(appStoryboard: .WebView) as! WebViewViewController
//        var dict = [String: Any]()
//        dict[kName] = (URL.absoluteString == self.privacyPolicyUrl) ? "Privacy Policy" : "Terms and Condition"
//        dict[kUrl] = URL.absoluteString
//        if let article = ArticleList.decode(json: dict as AnyObject) {
//            vc.requestUrlModel = article
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        return false
//    }
//}

//MARK:- LogIn Delegate Functions
extension VerifyPhoneNumberViewController: VerifyPhoneNoDelegateHandlers {
    func successProcess(randomKey key: String) {
        self.navigateToLoginWithOTPVC(randomKey: key, isFromVerifyPhoneNumber: true, socialLogInInfo: socialLogInInfo)
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
