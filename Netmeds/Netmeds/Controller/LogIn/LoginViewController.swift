//
//  LoginViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

protocol LogInDelegateHandlers: class {
    func logInSuccessProcess()
    func signUpProcessUsing(randomKey key: String)
    func socialLogInSuccessProcess()
    func homePageProcess()
    func failureProcess(errorMessage message: String)
}

class LoginViewController: BaseViewController {
    
    @IBOutlet weak private var signinLabel: UILabel!
    @IBOutlet weak private var signInSubTitleLabel: UILabel!
    @IBOutlet weak private var phoneNumberTextField: UITextField!
    @IBOutlet weak private var termsAndServiceTextView: UITextView!
    @IBOutlet weak private var privacyAndPolicyTextView: UITextView!
    @IBOutlet weak private var orLabel: UILabel!
    @IBOutlet weak private var googleButton: RoundButton!
    @IBOutlet weak private var facebookButton: RoundButton!
    @IBOutlet weak private var phoneNumberLabel: UILabel!
    @IBOutlet weak private var continueButton: RoundButton!
    private let configManager = ConfigManager.shared
    private var privacyPolicyUrl: String = ""
    private var logInViewModel = LogInViewModel()
    private var socialLogInInfo: SocialLogInInfoViewModel = SocialLogInInfoViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .white
        self.navigationBarForLoginViewController()
    }
}

// MARK: All Functions
extension LoginViewController {
    private func setUpUserInterface() {
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.setRightBarButton(setUpSkipBarButtonItem(), animated: true)
        logInViewModel.delegate = self
        setUpGoogleSign()
        setUpUIText()
    }
    
    func setUpGoogleSign() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance()?.clientID = kGoogleClientID
        GIDSignIn.sharedInstance()?.shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance()?.scopes = ["profile"]
    }
    
    func setUpSkipBarButtonItem() -> UIBarButtonItem {
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 70, height: 40)))
        button.setTitle("SKIP", for: .normal)
        button.titleLabel?.textAlignment = .right
        button.contentMode = .right
        button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
        button.setTitleColor(UIColor.NMSTextColor(), for: .normal)
        button.addTarget(self, action: #selector(self.skipButtonAction), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        barButton.customView?.contentMode = .right
        return barButton
    }
    
    private func setUpUIText() {
        googleButton.setTitle(kGoogleText, for: .normal)
        let title = NSAttributedString(string: kFaceBookText)
        facebookButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        facebookButton.setAttributedTitle(title, for: .normal)
        facebookButton.tintColor = UIColor.NMSTextColor(with: 1.0)
        orLabel.text = kORText
        phoneNumberLabel.text = kPhoneNumberText
        self.privacyPolicyUrl = configManager.configDetails?.privacy ?? ""
        privacyAndPolicyTextView.handlePrivacyPolicyTextView(privacyPolicyUrl: configManager.configDetails?.privacy ?? "")
        termsAndServiceTextView.handleTermsAndServiceTextView(termsAndConditionUrl: configManager.configDetails?.terms ?? "")
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension LoginViewController {
    @IBAction func handleLoginButtonAction(_ sender: Any) {
        GoogleAnalytics.trackEvent(withScreen: "Sign in / Sign up", category: "Login", label: "Login/Registration", action: "Mobile Number Entered")
        guard let phoneNumber = phoneNumberTextField.text, phoneNumber.isValidString else {
            createToastWith(message: kEnterMobileNoText); return }
        guard phoneNumber.isValidPhoneNumber else {
            createToastWith(message: kEnterValidMobileNoText); return }
        logInViewModel.loginService(mobileNo: phoneNumber)
    }
    
    @IBAction func handleGoogleSigninAction(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func handleFacebookSigninAction(_ sender: Any) {
        fbReadPermissions()
    }
    
    @objc public func skipButtonAction() {
    }
}

// MARK: All API Functions
extension LoginViewController {
    private func fbReadPermissions() {
        if let _ =  FBSDKAccessToken.current() {
            self.fetchFBUser()
        } else {
            FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile", "user_birthday"], from: self) { (result, error) in
                guard let _ = result?.grantedPermissions else {
                    self.createToastWith(message: kPermissionDeniedText)
                    return
                }
                self.fetchFBUser()
            }
        }
    }
    
    private func fetchFBUser() {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, birthday, first_name, last_name, gender"]).start(completionHandler: { (connection, result, error) -> Void in
            guard let _ = result as? JSONDictAny, let token = FBSDKAccessToken.current()?.tokenString else { return }
            self.socialLogInInfo.accessToken = token
            self.socialLogInInfo.socialFlag = kFBSocialFlag
            self.logInViewModel.socialLogInService(socialLogInInfo: self.socialLogInInfo)
        })
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField.text ?? "").limitCharactersLength(range: range, replacement: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//extension LoginViewController: UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        let vc = WebViewViewController.instantiate(appStoryboard: .WebView) as! WebViewViewController
//        var dict = [String: Any]()
//        dict[kName] = (URL.absoluteString == self.privacyPolicyUrl) ? "Privacy Policy" : "Terms and Condition"
//        dict[kUrl] = URL.absoluteString
//        vc.requestUrl = dict
//        if let article = ArticleList.decode(json: dict as AnyObject) {
//            vc.requestUrlModel = article
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        return false
//    }
//}

//MARK:- Google Delegate Functions
extension LoginViewController: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else {
            self.createToastWith(message: kPermissionDeniedText)
            return
        }
        self.socialLogInInfo.accessToken = user.authentication.idToken
        self.socialLogInInfo.socialFlag = kGoogleSocialFlag
        self.logInViewModel.socialLogInService(socialLogInInfo: self.socialLogInInfo)
    }
}

//MARK:- LogIn Delegate Functions
extension LoginViewController: LogInDelegateHandlers {
    func logInSuccessProcess() {
        self.navigateToLoginWithPasswordVC(phoneNo: phoneNumberTextField.text!)
    }
    
    func signUpProcessUsing(randomKey key: String) {
        self.navigateToSignUpVC(randomKey: key)
    }
    
    func socialLogInSuccessProcess() {
        self.navigateToVerifyPhoneNumberVC(socialLogInInfo: socialLogInInfo)
    }
    
    func homePageProcess() {
        self.setHomeRootVC()
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
