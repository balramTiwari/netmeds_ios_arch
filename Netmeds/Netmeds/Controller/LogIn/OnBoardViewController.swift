//
//  OnBoardViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 12/12/19.
//  Copyright © 2019 NETMEDS. All rights reserved.
//

import UIKit

class OnBoardViewController: BaseViewController {
    @IBOutlet weak private var collectionViewOnBoard: UICollectionView!
    @IBOutlet weak private var pageControl: UIPageControl!
    var arrayOnBoarding: [ConfigOnBoardingModel] = [ConfigOnBoardingModel]()
    private var configManager = ConfigManager.shared
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpUserInterface()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageControl.customPageControl(dotWidth: 12)
    }
}

// MARK: All Functions
extension OnBoardViewController {
    private func setUpUserInterface() {
        registerCells()
    }
    
    private func updateOnBoardingDetails() {
        guard let onBoardingInfo = configManager.configDetails?.onBoarding, onBoardingInfo.count > 0 else { return }
        arrayOnBoarding = onBoardingInfo
        pageControl.numberOfPages = arrayOnBoarding.count
        collectionViewOnBoard.reloadData()
    }
    
    private func registerCells() {
        collectionViewOnBoard.registerNibForCell(OnBoardingCollectionViewCell.self)
        //        if let flowLayout = collectionViewOnBoard.collectionViewLayout as? UICollectionViewFlowLayout {
        //            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        //        }
    }
}

// MARK: All Button Functions
extension OnBoardViewController {
    
}

// MARK: All API Functions
extension OnBoardViewController {
    
}

// MARK: ALL DELEGATE FUNCTIONS
// MARK: Collection View Delegate And DataSource Functions
extension OnBoardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOnBoarding.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCollectionViewCell", for: indexPath) as? OnBoardingCollectionViewCell else { return UICollectionViewCell() }
        let onBoarding = arrayOnBoarding[indexPath.row]
        cell.configCell(onBoarding: onBoarding)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width , height: collectionView.frame.size.height)
    }
}
