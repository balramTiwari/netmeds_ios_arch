//
//  LoginWithOTPViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 17/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol LogInViaOTPDelegateHandlers: class {
    func resendOTPSuccessProcess(randomKey key: String)
    func homePageProcess()
    func failureProcess(errorMessage message: String)
}

class LoginWithOTPViewController: BaseViewController {
    
    @IBOutlet weak private var signinLabel: UILabel!
    @IBOutlet weak private var signInSubTitleLabel: UILabel!
    @IBOutlet weak private var phoneNumberTextField: UITextField!
    @IBOutlet weak private var orLabel: UILabel!
    @IBOutlet weak private var phoneNumberLabel: UILabel!
    @IBOutlet weak private var changePhoneNumberButton: UIButton!
    @IBOutlet weak private var verifyButton: UIButton!
    @IBOutlet weak private var loginUsingPasswordButton: UIButton!
    @IBOutlet weak private var resendOTPButton: UIButton!
    @IBOutlet weak private var otpSentLabel: UILabel!
    @IBOutlet weak private var otpTimerLabel: UILabel!
    @IBOutlet weak private var otpCountDownLabel: UILabel!
    @IBOutlet weak private var otpView: OTPView!
    @IBOutlet weak private var orView: UIView!
    private var timerSeconds = kOTPTimerMaxSeconds
    private var timer: Timer!
    private var logInViaOTPViewModel = LogInViaOTPViewModel()
    var socialLogInInfo: SocialLogInInfoViewModel = SocialLogInInfoViewModel()
    var randomKey: String = ""
    var isFromVerifyPhoneNumber = false
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTextField.text = "9789537536"
        setUpUserInterface()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        phoneNumberTextField.endEditing(true)
    }
}

// MARK: All Functions
extension LoginWithOTPViewController {
    private func setUpUserInterface() {
        logInViaOTPViewModel.delegate = self
        enableDisableResendOTPButton(isEnabled: false)
        scheduledTimer()
        setUpUIText()
        //        //Dummy args
        //        let loginAttributes: [String:Any] = ["User Name" : "Prasath",
        //                                             "Password" : "xxxx",
        //                                             "Loginned" : true]
        //        self.weAnalytics.trackEvent(withName: "Login With OTP", andValue: loginAttributes)
        //Google Analytics Tracker
        GoogleAnalytics.trackEvent(withScreen: "Sign In with OTP", category: "Login", label: "Login with OTP", action: "Login with OTP - Successful")
    }
    
    private func setUpUIText() {
        loginUsingPasswordButton.isHidden = isFromVerifyPhoneNumber ? true : false
        orView.isHidden = isFromVerifyPhoneNumber ? true : false
        phoneNumberLabel.text = kPhoneNumberText
        changePhoneNumberButton.setTitle(kChangeText, for: .normal)
        verifyButton.setTitle(kVerifyText, for: .normal)
        if isFromVerifyPhoneNumber {
            signinLabel.text = kVerifyText.capitalized
            signInSubTitleLabel.text = kVerifySubTitle
        } else {
            signinLabel.text = kSignInText
            signInSubTitleLabel.text = kSignInSubTitle
            loginUsingPasswordButton.setTitle(kLogInUsingPasswordText, for: .normal)
            orLabel.text = kORText
        }
    }
    
    private func enableDisableResendOTPButton(isEnabled: Bool) {
        resendOTPButton.isEnabled = isEnabled
        resendOTPButton.tintColor = UIColor.mediumPinkTextColor()
        resendOTPButton.alpha = isEnabled ? 1.0 : 0.5
    }
    
    private func scheduledTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateOTPTimer), userInfo: nil, repeats: true)
    }
    
    @objc private func updateOTPTimer() {
        enableDisableResendOTPButton(isEnabled: (timerSeconds == 0) ? true : false)
        otpCountDownLabel.text = otpView.updateOTPTimer(with: timerSeconds)
        if timerSeconds == 0 {
            timer.invalidate()
        }
        timerSeconds -= 1
    }
    
    private func navigateToHomeVC() {
        guard let appDelegate  = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.setHomeRootViewController()
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension LoginWithOTPViewController {
    @IBAction func handleChangePhoneNumberAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func handleLoginUsingPasswordButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleVerifyButtonAction(_ sender: Any) {
        guard otpView.otpText().count != 0 else  {
            createToastWith(message: kEnterOTPText); return }
        guard !isFromVerifyPhoneNumber else {
            let appendRandomKey = randomKey + "+" + otpView.otpText()
            let encodedRandomKey = appendRandomKey.toBase64()
            socialLogInInfo.randomkey = encodedRandomKey
            logInViaOTPViewModel.socialLogInService(socialLogInInfo: socialLogInInfo)
            return
        }
        logInViaOTPViewModel.logInViaOTP(mobileNo: phoneNumberTextField.text!, randomKey: randomKey, otp: otpView.otpText())
    }
    
    @IBAction func handleResendButtonAction(_ sender: Any) {
        timerSeconds = kOTPTimerMaxSeconds
        otpView.clearTextField()
        scheduledTimer()
        guard !isFromVerifyPhoneNumber else {
            socialLogInInfo.randomkey = ""
            logInViaOTPViewModel.socialLogInService(socialLogInInfo: socialLogInInfo)
            return
        }
        logInViaOTPViewModel.reSendOTPService(randomKey: randomKey)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension LoginWithOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            return (textField.text ?? "").limitCharactersLength(range: range, replacement: string)
        }
        return true
    }
}

//MARK:- LogIn OTP Delegate Functions
extension LoginWithOTPViewController: LogInViaOTPDelegateHandlers {
    func resendOTPSuccessProcess(randomKey key: String) {
        randomKey = key
    }
    
    func homePageProcess() {
        navigateToHomeVC()
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
