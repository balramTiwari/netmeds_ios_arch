//
//  SignUpViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol SignUpDelegateHandlers: class {
    func resendOTPSuccessProcess(randomKey key: String)
    func homePageProcess()
    func failureProcess(errorMessage message: String)
}

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak private var createAccountLabel: UILabel!
    @IBOutlet weak private var emailLabel: UILabel!
    @IBOutlet weak private var emailTextField: UITextField!
    @IBOutlet weak private var firstNameTextField: UITextField!
    @IBOutlet weak private var resendOTPButton: UIButton!
    @IBOutlet weak private var changePhoneNumberButton: UIButton!
    @IBOutlet weak private var otpSentLabel: UILabel!
    @IBOutlet weak private var otpTimerLabel: UILabel!
    @IBOutlet weak private var otpCountDownLabel: UILabel!
    @IBOutlet weak private var phoneNumberLabel: UILabel!
    @IBOutlet weak private var verifyButton: UIButton!
    @IBOutlet weak private var otpView: OTPView!
    @IBOutlet weak private var emailIDView: UIView!
    @IBOutlet weak private var emailSeparatorView: UIView!
    @IBOutlet weak private var lastNameTextField: UITextField!
    @IBOutlet weak private var firstNameView: UIView!
    @IBOutlet weak private var firstNameSeparatorView: UIView!
    @IBOutlet weak private var firstNameLabel: UILabel!
    @IBOutlet weak private var lastNameLabel: UILabel!
    @IBOutlet weak private var lastNameView: UIView!
    @IBOutlet weak private var lastNameSeparatorView: UIView!
    @IBOutlet weak private var otpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var verifyBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var passwordConfirmTextField: UITextField!
    @IBOutlet weak private var showPasswordImagePass: UIImageView!
    @IBOutlet weak private var showPasswordImageConfirmPass: UIImageView!
    @IBOutlet weak private var passwordView: UIView!
    @IBOutlet weak private var separatorView: UIView!
    @IBOutlet weak private var passwordWarningLabel: UILabel!
    @IBOutlet weak private var passwordLabel: UILabel!
    @IBOutlet weak private var passwordConfirmLabel: UILabel!
    private var timerSeconds = kOTPTimerMaxSeconds
    private var timer: Timer!
    private var signUpViewModel = SignUpViewModel()
    var randomKey: String = ""

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
}

// MARK: All Functions
extension SignUpViewController {
    private func setUpUserInterface() {
        signUpViewModel.delegate = self
        setUpUIText()
    }
    
    private func setUpUIText() {
        passwordWarningLabel.text = kPasswordWarningText
        verifyButton.setTitle(kVerifyText, for: .normal)
        changePhoneNumberButton.setTitle(kChangeText, for: .normal)
        //        if let email = NMUser.shared.user?.customer_email {
        //            emailTextField.text = email
        //            emailLabel.isHidden = false
        //        } else {
        //            emailLabel.isHidden = true
        //        }
        //        if let fName = NMUser.shared.user?.firstname {
        //            firstNameTextField.text = fName
        //            firstNameLabel.isHidden = false
        //        } else {
        //            firstNameLabel.isHidden = true
        //        }
        //        if let lName = NMUser.shared.user?.lastname {
        //            lastNameTextField.text = lName
        //            lastNameLabel.isHidden = false
        //        } else {
        //            lastNameLabel.isHidden = true
        //        }
        //        if let phoneNumber = NMUser.shared.user?.customer_mobileno {
        //            phoneNumberLabel.text = "+91-"+phoneNumber
        //        }
        passwordLabel.isHidden = true
        passwordConfirmLabel.isHidden = true
        otpView.isHidden = false
        otpViewHeightConstraint.constant = 166
        verifyBtnTopConstraint.constant = 30
        enableDisableResendOTPButton(isEnabled: false)
        scheduledTimer()
    }
    
    private func enableDisableResendOTPButton(isEnabled: Bool) {
        resendOTPButton.isEnabled = isEnabled
        resendOTPButton.tintColor = UIColor.mediumPinkTextColor()
        resendOTPButton.alpha = isEnabled ? 1.0 : 0.5
    }
    
    private func scheduledTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateOTPTimer), userInfo: nil, repeats: true)
    }
    
    @objc private func updateOTPTimer() {
        enableDisableResendOTPButton(isEnabled: (timerSeconds == 0) ? true : false)
        otpCountDownLabel.text = otpView.updateOTPTimer(with: timerSeconds)
        if timerSeconds == 0 {
            timer.invalidate()
        }
        timerSeconds -= 1
    }
    
    func validatePassword(password: String) {
        if passwordTextField.isFirstResponder {
            passwordView.backgroundColor =  UIColor.white
            separatorView.backgroundColor = UIColor.separatorViewColor()
            passwordWarningLabel.textColor = UIColor.NMSTextColor()
            passwordLabel.textColor = UIColor.NMSTextColor()
        } else {
            //                NMUser.shared.user?.password = password
            passwordTextField.resignFirstResponder()
        }
    }
    
    func validateEmail(email: String?) {
        guard let email = email, email.count > 0 else {
            createToastWith(message: kEnterEmailIDText)
            return
        }
        if emailTextField.isFirstResponder {
            emailIDView.backgroundColor =  UIColor.white
            emailSeparatorView.backgroundColor = UIColor.separatorViewColor()
            emailLabel.textColor = UIColor.netmedsGreenColor()
        } else {
            if email.count == 0 {
                createToastWith(message: kEnterEmailIDText)
            } else if !(email.isValidEmail) {
                createToastWith(message: kEnterValidEmailIDText)
            }
        }
    }
    
    func validateFirstName(firstName: String?) {
        guard let firstName = firstName else {
            createToastWith(message: kEnterFirstNameText)
            return
        }
        if firstNameTextField.isFirstResponder {
            firstNameView.backgroundColor =  UIColor.white
            firstNameSeparatorView.backgroundColor = UIColor.separatorViewColor()
            firstNameLabel.textColor = UIColor.netmedsGreenColor()
        } else {
            if !(firstName.isValidName) {
                createToastWith(message: kEnterValidFirstNameText)
            } else if (firstName.count <= 0){
                createToastWith(message: kEnterFirstNameText)
            }
        }
    }
    
    func validateLastName(lastName: String?) {
        guard let lastName = lastName, lastName.count > 0 else {
            createToastWith(message: kEnterLastNameText)
            return
        }
        if lastNameTextField.isFirstResponder {
            lastNameView.backgroundColor =  UIColor.white
            lastNameSeparatorView.backgroundColor = UIColor.separatorViewColor()
            lastNameLabel.textColor = UIColor.netmedsGreenColor()
        } else {
            if !(lastName.isValidName) {
                createToastWith(message: kEnterValidLastNameText)
            } else if (lastName.count <= 0) {
                createToastWith(message: kEnterLastNameText)
            }
        }
    }
    
    private func navigateToHomeVC() {
        guard let appDelegate  = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.setHomeRootViewController()
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }

}

// MARK: All Button Functions
extension SignUpViewController {
    @IBAction func handleShowPasswordAction(_ sender: Any) {
        showPasswordImagePass.alpha = (passwordTextField.isSecureTextEntry) ? 1.0 : 0.4
        passwordTextField.isSecureTextEntry = (passwordTextField.isSecureTextEntry) ? false : true
    }
    
    @IBAction func handleShowPasswordActionCPass(_ sender: Any) {
        showPasswordImageConfirmPass.alpha = (passwordConfirmTextField.isSecureTextEntry) ? 1.0 : 0.4
        passwordConfirmTextField.isSecureTextEntry = (passwordConfirmTextField.isSecureTextEntry) ? false : true
    }
    
    @IBAction func handleChangePhoneNumberAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func handleVerifyButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        guard let fname = firstNameTextField.text?.isValidName, let lname = lastNameTextField.text?.isValidName, let  _ = emailTextField.text, lname, fname else {
            validateLastName(lastName: lastNameTextField.text)
            validateFirstName(firstName: firstNameTextField.text)
            validateEmail(email: emailTextField.text)
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            validatePassword(password: "")
            self.createToastWith(message: kPasswordWarningText)
            return
        }
        if passwordTextField.text != passwordConfirmTextField.text {
            self.createToastWith(message: kPwdAndConfirmPwdNotMatchWarningText)
            return
        }
        guard password.count > 7 else {
            validatePassword(password: "")
            self.createToastWith(message: kPasswordTooShortText)
            return
        }
        guard password.isValidPassword else {
            validatePassword(password: password)
            return
        }
        guard otpView.otpText().count > 0 else {
            createToastWith(message: kEnterOTPText)
            return
        }
        
//        guard let mobileNo = NMUser.shared.user?.customer_mobileno else {return}
//
        let params = [
            "firstname" : firstNameTextField.text!,
            "lastname" : lastNameTextField.text!,
            "mobile_no" : "",
            "email" : emailTextField.text!,
            "date_of_birth" : "1999-01-01",
            "gender" : "Male",
            "rk" : randomKey,
            "otp" : otpView.otpText(),
            "password" : passwordTextField.text!
            ] as [String : Any]
        signUpViewModel.signUpService(params: params)
    }
    
    @IBAction func handleResendButtonAction(_ sender: Any) {
        timerSeconds = kOTPTimerMaxSeconds
        otpView.clearTextField()
        scheduledTimer()
        signUpViewModel.reSendOTPService(randomKey: randomKey)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TextField Delegate Functions
extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = emailTextField.text, (textField == emailTextField) {
            validateEmail(email: text)
            emailLabel.isHidden = (textField.text?.count == 0) ? true : false
        } else if let text = firstNameTextField.text, (textField == firstNameTextField) {
            validateFirstName(firstName: text)
            firstNameLabel.isHidden = (textField.text?.count == 0) ? true : false
        } else if let text = lastNameTextField.text, (textField == lastNameTextField) {
            validateLastName(lastName: text)
            lastNameLabel.isHidden = (textField.text?.count == 0) ? true : false
        } else if let text = passwordTextField.text, (textField == passwordTextField) {
            validatePassword(password: text)
            passwordLabel.isHidden = (textField.text?.count == 0) ? true : false
        } else if let text = passwordConfirmTextField.text, (textField == passwordConfirmTextField) {
            validatePassword(password: text)
            passwordConfirmLabel.isHidden = (textField.text?.count == 0) ? true : false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == emailTextField) {
            emailLabel.isHidden = false
        } else if (textField == firstNameTextField) {
            firstNameLabel.isHidden = false
        } else if (textField == lastNameTextField) {
            lastNameLabel.isHidden = false
        } else if (textField == passwordTextField) {
            passwordLabel.isHidden = false
        } else if (textField == passwordConfirmTextField) {
            passwordConfirmLabel.isHidden = false
        } else {
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString:NSString? = textField.text as NSString?
        let text = nsString?.replacingCharacters(in:range, with:string)
        if (textField == emailTextField) {
            emailLabel.isHidden = (text?.count == 0)
        } else if (textField == firstNameTextField) {
            firstNameLabel.isHidden = (text?.count == 0)
        } else if (textField == lastNameTextField){
            lastNameLabel.isHidden = (text?.count == 0)
        } else if (textField == passwordTextField){
            passwordLabel.isHidden = (text?.count == 0)
        }
        if textField == firstNameTextField || textField == lastNameTextField {
            if string == " " {
                if range.location == 0 {
                    return false
                }
                if let value = textField.text, value.last == " " {
                    return false
                }
                if let value = textField.text {
                    let spaceCount = value.filter{$0 == " "}.count
                    if spaceCount > 2 {
                        return false
                    }
                }
            }
            let regex = try! NSRegularExpression(pattern: "[a-zA-Z\\s]+", options: [])
            let textRange = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
            let isTextLimitted = (textField.text ?? "").limitCharactersLength(maxLength: 30, range: range, replacement: string)
            return (textRange.length == string.count) && isTextLimitted
        }
        if textField == passwordTextField || textField == passwordConfirmTextField {
            let isTextLimitted = (textField.text ?? "").limitCharactersLength(maxLength: 30, range: range, replacement: string)
            return isTextLimitted
        }
        return true
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- SignUp Delegate Handlers
extension SignUpViewController: SignUpDelegateHandlers {
    func resendOTPSuccessProcess(randomKey key: String) {
        randomKey = key
    }
    
    func homePageProcess() {
        navigateToHomeVC()
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}
