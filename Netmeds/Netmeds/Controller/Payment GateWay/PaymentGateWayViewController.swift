//
//  PaymentGateWayViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 09/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit
import HyperSDK

enum PaymentGateWayType {
    case freePay
    case payPal
    case phonePe
    case mobiKwik
    case freeCharge
    case paytm
    case amazonPay
    case olaPostPaid
    case creditAndDebitCards
    case netBanking
    case cashOnDelivery
    case googlePay
    case none
}

protocol PaymentGateWayDelegateHandlers: class {
    func successProcess()
    func failureProcess(errorMessage message: String)
}

class PaymentGateWayViewController: BaseViewController {
    @IBOutlet weak private var tblViewPaymentGateWay: UITableView!
    @IBOutlet weak private var bottomView: UIView!
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var btnPlaceOrder: UIButton!
    @IBOutlet weak private var lblTotalAmount: UILabel!
    private var paymentGateWayViewModel = PaymentGateWayViewModel()
    private var selectedPaymentGateWay: PaymentGateWayRowModel = PaymentGateWayRowModel()
    // WARNING: TO BE START REMOVED
    var cartID: String = ""
    var orderValue: String = ""
    var appVersion: String = ""
    // WARNING: TO BE END REMOVED
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarForChildViewController()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        setUpUserInterface()
    }
}

// MARK: All Functions
extension PaymentGateWayViewController {
    private func setUpUserInterface() {
        Hyper().preFetch("netmeds_ios")
        paymentGateWayViewModel.delegate = self
        registerTableViewCells()
        paymentGateWayService()
    }
    
    private func registerTableViewCells() {
        tblViewPaymentGateWay.tableFooterView = UIView()
        tblViewPaymentGateWay.registerNibForCell(PaymentGateWayTblCell.self)
        tblViewPaymentGateWay.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewPaymentGateWay.registerNibForCell(LabelTblViewCell.self)
        tblViewPaymentGateWay.rowHeight = UITableView.automaticDimension
        tblViewPaymentGateWay.estimatedRowHeight = 100
        tblViewPaymentGateWay.sectionHeaderHeight = UITableView.automaticDimension
        tblViewPaymentGateWay.estimatedSectionHeaderHeight = 100
    }
    
    private func resetSelectedPaymentGateWay() {
        paymentGateWayViewModel.resetSelectedPaymentGateWay()
    }
}

// MARK: All Button Functions
extension PaymentGateWayViewController {
    @IBAction private func performPlaceOrderButton() {
        let jusPayTransaction = JusPayTransaction()
        switch selectedPaymentGateWay.paymentGateWayType {
        case .payPal:
            print("payPal")
        //            self.performSegue(withIdentifier: "NMPaypalForNetmedsViewController", sender: nil)
        case .paytm:
            print("paytm")
        case .cashOnDelivery:
            print("cashOnDelivery")
        case .phonePe, .olaPostPaid, .creditAndDebitCards, .netBanking, .googlePay:
            jusPayTransaction.generateAuthToken(viewController: self, paymentGateWayType: selectedPaymentGateWay.paymentGateWayType, paymentMethod: selectedPaymentGateWay.id, cartID: cartID)
        case .mobiKwik, .freeCharge:
            if selectedPaymentGateWay.isLinked {
                jusPayTransaction.generateAuthToken(viewController: self, paymentGateWayType: selectedPaymentGateWay.paymentGateWayType, paymentMethod: selectedPaymentGateWay.id, cartID: cartID)
            } else {
                //                self.performSegue(withIdentifier: "NMPaymentLinkAccountNetmedsViewController", sender: nil)
            }
        case .amazonPay:
            if selectedPaymentGateWay.isLinked {
                jusPayTransaction.generateAuthToken(viewController: self, paymentGateWayType: selectedPaymentGateWay.paymentGateWayType, paymentMethod: selectedPaymentGateWay.id, cartID: cartID)
            } else {
                //                self.juspayCallForAmazon()
            }
        default: break
        }
    }
}

// MARK: All API Functions
extension PaymentGateWayViewController {
    private func paymentGateWayService() {
        let params: JSONArrayOfDictString = [
            ["orderValue" : orderValue.replace(string: ",", replacement: "")],
            ["appSource" : "iOS"],
            ["appVersion" : appVersion],
            ["amazonFlag" : "false"],
            ["cartId": cartID]//Need to check Sexual product in Cart
        ]
        paymentGateWayViewModel.paymentGateWayService(params: params)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension PaymentGateWayViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return paymentGateWayViewModel.paymentGateWayHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentGateWayViewModel.paymentGateWayHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerModel = paymentGateWayViewModel.paymentGateWayHeaderModel[section]
        guard headerModel.title.isValidString else { return CGFloat.leastNormalMagnitude }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = paymentGateWayViewModel.paymentGateWayHeaderModel[section]
        guard headerModel.title.isValidString else { return UIView() }
        guard let headerTblCell: CartHeaderTblViewCell = tblViewPaymentGateWay.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return UIView() }
        headerTblCell.configCell(title: headerModel.title)
        return headerTblCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowModel = paymentGateWayViewModel.paymentGateWayHeaderModel[indexPath.section].rowModel
        let cornerRadius: CGFloat = indexPath.row == rowModel.count - 1 ? 10.0: 0.0
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = paymentGateWayViewModel.paymentGateWayHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch rowModel.type {
        case .paymentGateWay:
            let gateWayCell: PaymentGateWayTblCell = tblViewPaymentGateWay.dequeueReusableCellForIndexPath(indexPath)
            gateWayCell.configCell(rowModel: rowModel)
            return gateWayCell
        case .selectOtherNetbanks:
            let otherBanksCell: LabelTblViewCell = tblViewPaymentGateWay.dequeueReusableCellForIndexPath(indexPath)
            otherBanksCell.configCell(title: rowModel.displayName, textAlignment: .right, textColor: UIColor.NMLogoGreenColor())
            return otherBanksCell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetSelectedPaymentGateWay()
        let headerModel = paymentGateWayViewModel.paymentGateWayHeaderModel[indexPath.section]
        var rowModel = headerModel.rowModel[indexPath.row]
        switch rowModel.type {
        case .selectOtherNetbanks:
            self.navigateToAllNetBankingVC()
        default:
            rowModel.isSelected = !rowModel.isSelected
            paymentGateWayViewModel.paymentGateWayHeaderModel[indexPath.section].rowModel[indexPath.row] = rowModel
            selectedPaymentGateWay = rowModel
            tblViewPaymentGateWay.reloadData()
        }
    }
}

//MARK:- Payment GateWay Delegate Handlers
extension PaymentGateWayViewController : PaymentGateWayDelegateHandlers {
    func successProcess() {
        tblViewPaymentGateWay.reloadData()
        lblTotalAmount.text = "₹ " + orderValue
        bottomView.isHidden = false
    }
    
    func failureProcess(errorMessage message: String) {
        self.createToastWith(message: message)
    }
    
}
