//
//  HomeViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit
let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.size.width
let screenHeight = screenSize.size.height

protocol HomeDelegateHandlers: class {
    func successProcess()
}

class HomeViewController: BaseViewController {
    @IBOutlet weak private var tblViewHome: UITableView!
    @IBOutlet weak private var appUpdateView: UIView!
    @IBOutlet weak private var imgViewAppIcon: UIImageView!
    @IBOutlet weak private var lblAppUpdateTitle: UILabel!
    @IBOutlet weak private var lblAppUpdateDesc: UILabel!
    private var homeViewModel = HomeViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForHomeViewController()
        MedicineFlowManager.shared.flow = .universalCart
        MedicineFlowManager.shared.cartID = 0
    }
}

// MARK: All Functions
extension HomeViewController {
    private func setUpUserInterface() {
        UserDefaultsModel.setObject(true, forKey: kUserLoggedInKey)
        homeViewModel.delegate = self
        registerTableViewCells()
        handleAppUpdateBanner()
        getHomePageInfo()
        setUpRefreshControl()
    }
    
    private func registerTableViewCells() {
        tblViewHome.tableFooterView = UIView()
        tblViewHome.registerNibForCell(TblCellWithCollectionView.self)
        tblViewHome.registerNibForCell(HomeUploadPrescriptionTblCell.self)
        tblViewHome.registerNibForCell(HomeHeaderTblCell.self)
        tblViewHome.registerNibForCell(FirstMembershipTblCell.self)
        tblViewHome.registerNibForCell(HomeWinterHeaderTblCell.self)
        tblViewHome.rowHeight = UITableView.automaticDimension
        tblViewHome.estimatedRowHeight = 100
        tblViewHome.sectionHeaderHeight = UITableView.automaticDimension
        tblViewHome.estimatedSectionHeaderHeight = 100
    }
    
    private func setUpRefreshControl() {
        refreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.netmedsGreenColor()
        tblViewHome.addSubview(refreshControl)
    }
    
    private func handleAppUpdateBanner()  {
        appUpdateView.isHidden = true
        let configDetails = ConfigManager.shared.configDetails
        guard let isInAppSoftUpdateEnabled = configDetails?.isInAppSoftUpdateEnabled, isInAppSoftUpdateEnabled, getAppStoreVersion() else { return }
        guard let title = configDetails?.inAppSoftUpdateTitle, let desc = configDetails?.inAppSoftUpdateDesc else { return }
        lblAppUpdateTitle.text = title
        lblAppUpdateDesc.text = desc
        appUpdateView.isHidden = false
    }
    
    private func getAppStoreVersion() -> Bool {
        guard let info = Bundle.main.infoDictionary else { return false }
        let currentVersion = info["CFBundleShortVersionString"] as? String
        let identifier = info["CFBundleIdentifier"] as? String
        let url = URL(string: "\(kAppDetailsURL)\(identifier!)")
        do {
            let data = try Data(contentsOf: url!)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                if let result = (json!["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
                    if version != currentVersion {
                        return true
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return false
    }
    
    @objc private func handleRefresh() {
        self.getHomePageInfo()
    }
}

// MARK: All Button Functions
extension HomeViewController {
    @IBAction private func performSearchButton() {
        navigateToSearchVC()
    }
    
    @IBAction private func performLaterButton () {
        appUpdateView.isHidden = true
    }
    
    @IBAction private func performUpdateNowButton() {
        appUpdateView.isHidden = true
        if let url = URL(string: kITUNESAPPURL),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc private func performViewAllButton(_ sender: UIButton) {
        switch sender.tag {
        case HomeBannersType.healthExperts.rawValue:
            navigateToWebVC(title: "Health Experts", urlString: "\(kBaseURL)health-library")
        default: break
        }
    }
}

// MARK: All API Functions
extension HomeViewController {
    private func getHomePageInfo() {
        homeViewModel.addAllBanners()
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return homeViewModel.homeHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerModel = homeViewModel.homeHeaderModel[section]
        guard headerModel.hasShowHeader else { return CGFloat.leastNormalMagnitude }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let headerModel = homeViewModel.homeHeaderModel[section]
        switch headerModel.type {
        case .offers, .bigSales, .consultation, .cashBackOffers:
            return 2
        default: return 10
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let maskPath = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = view.bounds
        maskLayer.path = maskPath.cgPath
        view.layer.mask = maskLayer
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = homeViewModel.homeHeaderModel[section]
        guard headerModel.hasShowHeader else { return UIView() }
        guard headerModel.type != .winterSales else {
            guard let winterHeaderTblCell: HomeWinterHeaderTblCell = tblViewHome.dequeueReusableCell(withIdentifier: "HomeWinterHeaderTblCell") as? HomeWinterHeaderTblCell else { return UIView() }
            winterHeaderTblCell.configCell(headerModel: headerModel)
            return winterHeaderTblCell
        }
        guard let headerTblCell: HomeHeaderTblCell = tblViewHome.dequeueReusableCell(withIdentifier: "HomeHeaderTblCell") as? HomeHeaderTblCell else { return UIView() }
        headerTblCell.configCell(headerModel: headerModel, type: headerModel.type)
        headerTblCell.btnViewAll.addTarget(self, action: #selector(performViewAllButton(_:)), for: .touchUpInside)
        return headerTblCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let headerModel = homeViewModel.homeHeaderModel[indexPath.section]
        let hasShowHeader = headerModel.hasShowHeader
        var byRoundingCorners: UIRectCorner = hasShowHeader ? [.bottomLeft, .bottomRight]: [.allCorners]
        if headerModel.type == .uploadPrescriptions || headerModel.type == .firstMembership || headerModel.type == .onlineConsultation {
            byRoundingCorners = [.allCorners]
        }
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: byRoundingCorners, cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = homeViewModel.homeHeaderModel[indexPath.section]
        switch headerModel.type {
        case .uploadPrescriptions, .onlineConsultation:
            let uploadPrescriptionsCell: HomeUploadPrescriptionTblCell = tblViewHome.dequeueReusableCellForIndexPath(indexPath)
            uploadPrescriptionsCell.configCell(headerModel: headerModel)
            return uploadPrescriptionsCell
        case .firstMembership:
            let firstMembershipCell: FirstMembershipTblCell = tblViewHome.dequeueReusableCellForIndexPath(indexPath)
            firstMembershipCell.configCell(headerModel: headerModel)
            return firstMembershipCell
        default:
            let offersCell: TblCellWithCollectionView = tblViewHome.dequeueReusableCellForIndexPath(indexPath)
            offersCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section, headerModel: headerModel)
            return offersCell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let headerModel = homeViewModel.homeHeaderModel[indexPath.section]
        switch headerModel.type {
        case .uploadPrescriptions:
            self.navigateToUploadPrescriptionsVC()
        default: break
            
        }
    }
}

//MARK:- CollectionView Delegate and DataSource Functions
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeViewModel.homeHeaderModel[collectionView.tag].rowModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let headerModel = homeViewModel.homeHeaderModel[collectionView.tag]
        let height = headerModel.rowHeight
        var width = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        width = headerModel.rowModel.count > 1 ? width - 16: width
        switch headerModel.type {
        case .offers:
            return CGSize(width: width - 40, height: height)
        case .cashBackOffers:
            return CGSize(width: width / 1.2, height: height)
        case .categories:
            return CGSize(width: width / 4 - 10, height: height)
        case .bigSales:
            return CGSize(width: 130.0, height: height)
        case .consultationPopularConcerns, .diagnosticsPopularConcerns:
            return CGSize(width: 156.0, height: height)
        case .wellness, .healthConcerns:
            return CGSize(width: height, height: height)
        case .healthExperts:
            return CGSize(width: width / 1.7, height: height)
        default:
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let headerModel = homeViewModel.homeHeaderModel[collectionView.tag]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .offers, .bigSales, .consultation, .consultationPopularConcerns, .diagnosticsPopularConcerns:
            let offersCell: CollectionViewCellWithImage = collectionView.dequeueReusableCellForIndexPath(indexPath)
            offersCell.configCell(imgURL: rowModel.imgURL)
            return offersCell
        case .wellness, .healthConcerns:
            let wellnessCell: CollectionViewCellWithImageAndLbl = collectionView.dequeueReusableCellForIndexPath(indexPath)
            wellnessCell.configCell(rowModel: rowModel)
            return wellnessCell
        case .categories:
            let categoriesCell: HomeCategoriesCollectionViewCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            categoriesCell.configCell(title: rowModel.title, imgName: rowModel.imgName)
            return categoriesCell
        case .rewards:
            let rewardsCell: HomeRewardsCollectionViewCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            rewardsCell.configCell(rowModel: rowModel)
            return rewardsCell
        case .winterSales:
            let winterSalesCell: HomeWinterCollectionViewCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            winterSalesCell.configCell(rowModel: rowModel)
            return winterSalesCell
        case .cashBackOffers:
            let cashBackOffersCell: CashBackOffersCollectionViewCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            cashBackOffersCell.configCell(rowModel: rowModel)
            return cashBackOffersCell
        case .healthExperts:
            let healthExpertsCell: CollectionViewCellWithVerticalImageAndLbl = collectionView.dequeueReusableCellForIndexPath(indexPath)
            healthExpertsCell.configCell(rowModel: rowModel)
            return healthExpertsCell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let headerModel = homeViewModel.homeHeaderModel[collectionView.tag]
        switch headerModel.type {
        case .offers, .bigSales, .consultation, .cashBackOffers, .rewards:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let headerModel = homeViewModel.homeHeaderModel[collectionView.tag]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .cashBackOffers:
            navigateToOffersDetailVC(pageID: rowModel.pageID)
        case .healthConcerns, .wellness:
            if let productID = Int(rowModel.pageID) {
                navigateToProductListVC(productID: productID, title: rowModel.title)
            }
        case .healthExperts:
            navigateToWebVC(title: "", urlString: rowModel.url)
        default:
            switch rowModel.type {
            case .medicine:
                navigateToSearchVC()
            case .others:
                self.view.appLinkBannerRedirection(linkType: rowModel.linkType, pageID: rowModel.pageID, url: rowModel.url)
            default: break
            }
        }
    }
}

//MARK:- Home Delegate Handlers
extension HomeViewController : HomeDelegateHandlers {
    func successProcess() {
        homeViewModel.homeHeaderModel.sort(by: HomeHeaderModel.sequenceByType)
        tblViewHome.reloadData()
        refreshControl.endRefreshing()
    }
}
