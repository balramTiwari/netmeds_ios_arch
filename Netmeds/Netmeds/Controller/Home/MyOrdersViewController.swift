//
//  MyOrdersViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 18/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol MyOrdersDelegateHandlers: class {
    func successProcess()
}

class MyOrdersViewController: BaseViewController {
    
    @IBOutlet weak private var tblViewMyOrders : UITableView!
    @IBOutlet weak private var filterView : UIView!
    private var myOrdersViewModel = MyOrdersViewModel()
    private var currentPage = 1
    private var selectedStatus: String = "Recent"
    private var isDataLoading: Bool = false
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDataLoading = true
        navigationBarForTabViewController()
        getMyOrdersService()
    }
}

// MARK: All Functions
extension MyOrdersViewController {
    private func setUpUserInterface() {
        myOrdersViewModel.delegate = self
        registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        tblViewMyOrders.tableFooterView = UIView()
        tblViewMyOrders.registerNibForCell(MyOrdersHeaderTblViewCell.self)
        tblViewMyOrders.registerNibForCell(MyOrdersTblViewCell.self)
        tblViewMyOrders.registerNibForCell(MyOrdersFooterTblViewCell.self)
    }
    
    private func getStatus(status: String) -> String {
        switch status {
        case "All":
            return "All"
        case "Recent":
            return "Recent"
        case "Processing":
            return "S"
        case "InTransit":
            return "I"
        case "Delivered":
            return "D"
        case "Cancelled":
            return "C"
        case "Subscription Delivery Pending":
            return "K"
        default:
            return "All"
        }
    }
    
    private func getParams() -> JSONArrayOfDictString {
        return [["pageIndex": "\(currentPage)"], ["pageSize": "15"], ["orderStatusText": "\(getStatus(status: selectedStatus))"]]
    }
}

// MARK: All Button Functions
extension MyOrdersViewController {
    @IBAction private func performFilterButton() {
        guard let vc = MyOrdersFilterViewController.instantiate(storyboardName: .myOrders) as? MyOrdersFilterViewController else { return }
        vc.selectedStatus = self.selectedStatus
        vc.statusList = myOrdersViewModel.myOrdersStatusModel
        vc.onCompletionHandler { [weak self] (selectedStatus) in
            self?.currentPage = 1
            self?.selectedStatus = selectedStatus
            self?.getMyOrdersFilterService()
        }
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc private func performViewDetailsButton(_ sender: UIButton) {
        let headerModel = myOrdersViewModel.myOrdersHeaderModel[sender.tag]
        self.navigatToOrdersDetailVC(orderID: headerModel.orderID)
    }

    @objc private func performTrackOrderButton(_ sender: UIButton) {
        let headerModel = myOrdersViewModel.myOrdersHeaderModel[sender.tag]
        self.navigatToTrackOrderVC(orderID: headerModel.orderID)
    }
}

// MARK: All API Functions
extension MyOrdersViewController {
    private func getMyOrdersService() {
        isDataLoading = false
        myOrdersViewModel.getMyOrdersService(params: getParams())
    }
    
    private func getMyOrdersFilterService() {
        isDataLoading = false
        myOrdersViewModel.getMyOrdersFilterService(params: getParams())
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return myOrdersViewModel.myOrdersHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrdersViewModel.myOrdersHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = myOrdersViewModel.myOrdersHeaderModel[section]
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewMyOrders.frame.size.width, height: 75.0))
        guard let headerTblCell: MyOrdersHeaderTblViewCell = tblViewMyOrders.dequeueReusableCell(withIdentifier: "MyOrdersHeaderTblViewCell") as? MyOrdersHeaderTblViewCell else { return headerView }
        headerTblCell.contentView.frame = headerView.frame
        headerTblCell.configCell(headerModel: headerModel)
        headerView.addSubview(headerTblCell.contentView)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("INDEX PATH", indexPath.section, myOrdersViewModel.myOrdersHeaderModel.count)
        let headerModel = myOrdersViewModel.myOrdersHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        let myOrdersCell: MyOrdersTblViewCell = tblViewMyOrders.dequeueReusableCellForIndexPath(indexPath)
        myOrdersCell.configCell(rowModel: rowModel)
        return myOrdersCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerModel = myOrdersViewModel.myOrdersHeaderModel[section].footerModel
        let footerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewMyOrders.frame.size.width, height: 60.0))
        guard let footerTblCell: MyOrdersFooterTblViewCell = tblViewMyOrders.dequeueReusableCell(withIdentifier: "MyOrdersFooterTblViewCell") as? MyOrdersFooterTblViewCell else { return footerView }
        footerTblCell.contentView.frame = footerView.frame
        footerTblCell.configCell(footerModel: footerModel, section: section)
        footerTblCell.btnViewDetails.addTarget(self, action: #selector(performViewDetailsButton(_:)), for: .touchUpInside)
        footerTblCell.btnTrackOrder.addTarget(self, action: #selector(performTrackOrderButton(_:)), for: .touchUpInside)
        footerView.addSubview(footerTblCell.contentView)
        return footerView
    }
}

//MARK:- Scroll View Delegate
extension MyOrdersViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: self.view)
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - (scrollView.frame.size.height + 200), translation.y < 0 {
            if !isDataLoading {
                if myOrdersViewModel.totalOrders > myOrdersViewModel.myOrdersHeaderModel.count {
                    isDataLoading = true
                    currentPage += 1
                    getMyOrdersService()
                }
            }
        }
    }
}

//MARK:- MyOrders Delegate Handlers
extension MyOrdersViewController: MyOrdersDelegateHandlers {
    func successProcess() {
        tblViewMyOrders.reloadData()
    }
}

