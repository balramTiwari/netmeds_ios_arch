//
//  AllNetBankingViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol AllNetBankingDelegateHandlers: class {
    func successProcess()
}

class AllNetBankingViewController: BaseViewController {
    @IBOutlet weak private var sreachBar: UISearchBar!
    @IBOutlet weak private var tblViewAllNetBanking: UITableView!
    private var allNetBankingViewModel = AllNetBankingViewModel()
    private var isSearch: Bool = false

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarForChildViewController()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension AllNetBankingViewController {
    private func setUpUserInterface() {
        allNetBankingViewModel.delegate = self
        registerTableViewCells()
        allNetbankingService()
    }
    
    private func registerTableViewCells() {
        tblViewAllNetBanking.tableFooterView = UIView()
        tblViewAllNetBanking.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewAllNetBanking.registerNibForCell(AllNetBankingTblCell.self)
        tblViewAllNetBanking.rowHeight = UITableView.automaticDimension
        tblViewAllNetBanking.estimatedRowHeight = 100
        tblViewAllNetBanking.sectionHeaderHeight = UITableView.automaticDimension
        tblViewAllNetBanking.estimatedSectionHeaderHeight = 100
    }
}

// MARK: All API Functions
extension AllNetBankingViewController {
    private func allNetbankingService() {
        allNetBankingViewModel.allNetbankingService()
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension AllNetBankingViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return isSearch ? allNetBankingViewModel.searchNetBankingHeaderModel.count: allNetBankingViewModel.allNetBankingHeaderModel.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearch ? allNetBankingViewModel.searchNetBankingHeaderModel[section].rowModel.count: allNetBankingViewModel.allNetBankingHeaderModel[section].rowModel.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = isSearch ? allNetBankingViewModel.searchNetBankingHeaderModel[section]: allNetBankingViewModel.allNetBankingHeaderModel[section]
        guard let headerTblCell: CartHeaderTblViewCell = tblViewAllNetBanking.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return UIView() }
        headerTblCell.configCell(title: headerModel.title)
        return headerTblCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowModel = isSearch ? allNetBankingViewModel.searchNetBankingHeaderModel[indexPath.section].rowModel: allNetBankingViewModel.allNetBankingHeaderModel[indexPath.section].rowModel
        let cornerRadius: CGFloat = indexPath.row == rowModel.count - 1 ? 10.0: 0.0
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = isSearch ? allNetBankingViewModel.searchNetBankingHeaderModel[indexPath.section]: allNetBankingViewModel.allNetBankingHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        let otherBanksCell: AllNetBankingTblCell = tblViewAllNetBanking.dequeueReusableCellForIndexPath(indexPath)
        otherBanksCell.configCell(name: rowModel.displayName)
        return otherBanksCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: tblViewAllNetBanking.frame.size.width, height: 20.0))
        footerView.backgroundColor = .clear
        return footerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension AllNetBankingViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        isSearch = true
//        self.createInputAccessoryView()
//        sreachBar.inputAccessoryView = inputAccessory
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        isSearch = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        isSearch = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        isSearch = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText.isValidString else { return }
//        isSearch = true
//        allNetBankingViewModel.searchNetBankNameFrom(text: searchText)
//        if (searchText.count == 0) {
//            isSearch = false
//            nobanksView.isHidden = true
//            tblView_NetBanking.isHidden = false
//            self.netBankingService()
//        } else {
//
//            let predicate = NSPredicate(format: "description contains[cd] %@", searchText)
//            let arrayValues  = (paymentMethods! as NSArray).filtered(using: predicate) as? [[String:Any]]
//            if arrayValues!.count > 0 {
//                tblView_NetBanking.isHidden = false
//                nobanksView.isHidden = true
//                self.paymentMethodsNameAdd(paymentMethods:arrayValues!)
//            } else {
//                //Set No Record found
//                keysTest.removeAll()
//                nobanksView.isHidden = false
//                tblView_NetBanking.isHidden = true
//            }
//        }
    }
}

//MARK:- All NetBanking Delegate Handlers
extension AllNetBankingViewController : AllNetBankingDelegateHandlers {
    func successProcess() {
        tblViewAllNetBanking.reloadData()
    }
}
