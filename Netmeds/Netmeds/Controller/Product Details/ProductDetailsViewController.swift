//
//  ProductDetailsViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit
import WebKit

protocol ProductDetailsDelegateHandlers: class {
    func successProcess()
    func failureProcess(errorMessage message: String)
}

class ProductDetailsViewController: BaseViewController {
    
    @IBOutlet weak private var tblViewProductDetails: UITableView!
    private var productDetailsViewModel: ProductDetailsViewModel = ProductDetailsViewModel()
    private var webViewHeight: CGFloat = 0.0
    private var isWebViewLoaded: Bool = false

    var productCode: Int = 0
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarWithSearchCartIcons()
    }
    
    //    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    //        guard let keyPath = keyPath else { return }
    //        if keyPath == "contentSize" {
    //            if let webView: WKWebView = self.view.viewWithTag(5000) as? WKWebView {
    //                let height = webView.scrollView.contentSize.height
    //                if height != webViewHeight {
    //                    webViewHeight = height
    //                    tblViewProductDetails.reloadData()
    //                }
    //            }
    //        }
    //    }
    
    //    deinit {
    //        removeObserver(self, forKeyPath: "contentSize", context: nil)
    //    }
}

// MARK: All Functions
extension ProductDetailsViewController {
    private func setUpUserInterface() {
        productDetailsViewModel.delegate = self
        registerTableViewCells()
        getProductDetailsInfo()
    }
    
    private func registerTableViewCells() {
        tblViewProductDetails.tableFooterView = UIView()
        tblViewProductDetails.registerNibForCell(LblAndCollectionViewTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsCollectionTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsPriceTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsHeaderTblCell.self)
        tblViewProductDetails.registerNibForCell(SearchTblViewCell.self)
        tblViewProductDetails.registerNibForCell(WebViewTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsOutOfStockTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsNotifyMeTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsCartTblCell.self)
        tblViewProductDetails.registerNibForCell(HighPriceMedicinesTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsOffersHeaderTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsOffersTblCell.self)
        tblViewProductDetails.registerNibForCell(ProductDetailsExpiryTblCell.self)
        tblViewProductDetails.rowHeight = UITableView.automaticDimension
        tblViewProductDetails.estimatedRowHeight = 100
        tblViewProductDetails.sectionHeaderHeight = UITableView.automaticDimension
        tblViewProductDetails.estimatedSectionHeaderHeight = 100
    }
    
    //    private func addObserver(webView: WKWebView) {
    //        webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    //    }
}

// MARK: All API Functions
extension ProductDetailsViewController {
    private func getProductDetailsInfo() {
        productDetailsViewModel.productDetailsService(productID: productCode)
    }
}
//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension ProductDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return productDetailsViewModel.productDetailsHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[section]
        switch headerModel.type {
        case .alternateSalts, .details:
            return headerModel.rowModel.count
        default: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[section]
        guard headerModel.title.isValidString else { return CGFloat.leastNormalMagnitude }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[indexPath.section]
        switch headerModel.type {
        case .webView:
            return webViewHeight
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[section]
        guard headerModel.title.isValidString else { return UIView() }
        switch headerModel.type {
        case .offers:
            guard let offersHeaderTblCell: ProductDetailsOffersHeaderTblCell = tblViewProductDetails.dequeueReusableCell(withIdentifier: "ProductDetailsOffersHeaderTblCell") as? ProductDetailsOffersHeaderTblCell else { return UIView() }
            offersHeaderTblCell.configCell(title: headerModel.title)
            return offersHeaderTblCell
        default:
            guard let headerTblCell: ProductDetailsHeaderTblCell = tblViewProductDetails.dequeueReusableCell(withIdentifier: "ProductDetailsHeaderTblCell") as? ProductDetailsHeaderTblCell else { return UIView() }
            headerTblCell.configCell(title: headerModel.title, viewAll: "")
            return headerTblCell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .title:
            let titleCell: LblAndCollectionViewTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            titleCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section, rowModel: rowModel)
            return titleCell
        case .categories, .otcProductImages:
            let collectionViewCell: ProductDetailsCollectionTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            collectionViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forSection: indexPath.section, headerModel: headerModel)
            return collectionViewCell
        case .priceInfo:
            let priceInfoCell: ProductDetailsPriceTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            priceInfoCell.configCell(rowModel: rowModel)
            return priceInfoCell
        case .cart:
            let cartCell: ProductDetailsCartTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            cartCell.configCell(cartCount: rowModel.title)
            return cartCell
        case .outOfStock:
            let outOfStockCell: ProductDetailsOutOfStockTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            outOfStockCell.configCell(title: rowModel.title, message: rowModel.subTitle)
            return outOfStockCell
        case .notifyMe:
            let notifyMeCell: ProductDetailsNotifyMeTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            notifyMeCell.configCell(title: rowModel.title)
            return notifyMeCell
        case .highPriceMedicines:
            let highPriceMedicinesCell: HighPriceMedicinesTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            return highPriceMedicinesCell
        case .details:
            let detailsCell: ProductDetailsExpiryTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            detailsCell.configCell(imgName: rowModel.imgName, title: rowModel.title, subTitle: rowModel.subTitle)
            return detailsCell
        case .offers:
            let offersCell: ProductDetailsOffersTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            offersCell.configCell(couponCode: rowModel.title, discount: rowModel.subTitle, message: rowModel.message)
            return offersCell
        case .alternateSalts:
            let searchCell: SearchTblViewCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            searchCell.configCell(alogliaSearchModel: rowModel.algoliaSearchHitsModel!)
            searchCell.delegate = self
            return searchCell
        case .webView:
            let webViewCell: WebViewTblCell = tblViewProductDetails.dequeueReusableCellForIndexPath(indexPath)
            if !isWebViewLoaded {
                isWebViewLoaded = true
                webViewCell.setWebViewDelegate(delegate: self, urlString: rowModel.urlString)
            }
            //            if !isWebViewLoaded {
            //                isWebViewLoaded = true
            //                webViewCell.setWebViewDelegate(delegate: self, urlString: rowModel.urlString)
            //                self.addObserver(webView: webViewCell.webView)
            //            }
            return webViewCell
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

//MARK:- CollectionView Delegate and DataSource Functions
extension ProductDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productDetailsViewModel.productDetailsHeaderModel[collectionView.tag].rowModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[collectionView.tag]
        let rowModel = headerModel.rowModel[indexPath.row]
        let width: CGFloat = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        switch headerModel.type {
        case .title:
            return CGSize(width: 50, height: 50)
        case .categories:
            let label = UILabel(frame: CGRect.zero)
            label.text = rowModel.urlString
            label.font = UIFont(name: "Lato-Regular", size: 12)
            label.sizeToFit()
            return CGSize(width: label.frame.width + 10.0, height: 33)
        case .otcProductImages:
            return CGSize(width: 250, height: 140)
        default:
            return CGSize(width: width - 40, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[collectionView.tag]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .title, .otcProductImages:
            let titleCell: CollectionViewCellWithImage = collectionView.dequeueReusableCellForIndexPath(indexPath)
            titleCell.configCell(imgURL: rowModel.urlString)
            return titleCell
        case .categories:
            let categoryCell: LblCollectionCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            categoryCell.configCell(name: rowModel.urlString)
            return categoryCell
        default: return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[collectionView.tag]
        switch headerModel.type {
        case .categories:
            return UIEdgeInsets(top: 0, left: 8.0, bottom: 0, right: 8.0)
//        case .otcProductImages:
//            let totalCellWidth = 250
////            let totalCellWidth = 250 * collectionView.numberOfItems(inSection: 0)
//            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
////            let leftInset = max(0.0, (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)))
//            var leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
////            if leftInset < 0 { leftInset = 0 }
//            let rightInset = leftInset
//            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        default:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[collectionView.tag]
        switch headerModel.type {
        case .categories, .otcProductImages:
            return 10.0
        default:
            return 0.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

//MARK:- Product Details Delegate Handlers
extension ProductDetailsViewController: ProductDetailsDelegateHandlers {
    func successProcess() {
        productDetailsViewModel.productDetailsHeaderModel.sort(by: ProductDetailsHeaderModel.sequenceByType)
        tblViewProductDetails.reloadData()
    }
    
    func failureProcess(errorMessage message: String) {
        //        showErrorMessage(errorMessage: message)
    }
}

//MARK:- Search TblView Cell Delegate Handlers
extension ProductDetailsViewController: SearchTblViewCellDelegateHandlers {
    func btnAddToCartTapped(cell: SearchTblViewCell) {
        guard let indexPath = tblViewProductDetails.indexPath(for: cell) else { return }
        let headerModel = productDetailsViewModel.productDetailsHeaderModel[indexPath.section]
        let productID: Int = headerModel.rowModel[indexPath.row].algoliaSearchHitsModel?.productCode ?? 0
        self.navigateToAddToCartVC(productID: productID)
    }
    
    func btnManufacturerNameTapped(cell: SearchTblViewCell) {
        
    }
}

//MARK:- WebView Delegate Methods
extension ProductDetailsViewController: WKNavigationDelegate {
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        guard webViewHeight == 0.0 else { return }
        webViewHeight = webView.scrollView.contentSize.height
//        guard let index = productDetailsViewModel.productDetailsHeaderModel.index( where:{ $0.type == .webView }) else { return }
//        tblViewProductDetails.reloadSections([index], with: .none)
        tblViewProductDetails.reloadData()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
}

