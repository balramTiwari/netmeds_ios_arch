//
//  JusPayTransaction.swift
//  Netmeds
//
//  Created by SANKARLAL on 10/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import Foundation

import HyperSDK

protocol JusPayDelegateHandlers: class {
    func successProcess()
    func failureProcess(errorMessage message: String)
}

class JusPayTransaction {
    private var paymentGateWayType: PaymentGateWayType = .none
    private var paymentMethod: String = ""
    private var jusPayResultModel: JusPayOrderCreateResultModel?
    private var directWalletToken: String = ""
    private var cartID: String = ""
    weak var delegate: JusPayDelegateHandlers?
}

extension JusPayTransaction {
    
    func generateAuthToken(viewController: UIViewController, paymentGateWayType: PaymentGateWayType, paymentMethod: String, cartID: String) {
        self.cartID = cartID
        var params = JSONArrayOfDictString()
        params = [["cartId": cartID]]
        NetworkManager.shared.jusPayOrderCreateServcie(params: params) { (error, response) in
            guard error == nil, let orderCreate = response as? JusPayOrderCreateModel else { return }
            guard orderCreate.status?.lowercased() == ResponseStatus.success.rawValue else { self.parseErrorInfo(errorInfo: orderCreate.errorInfo); return }
            self.jusPayResultModel = orderCreate.result
            self.start(viewController: viewController, paymentGateWayType: paymentGateWayType, paymentMethod: paymentMethod)
        }
    }
    
    func start(viewController: UIViewController, paymentGateWayType: PaymentGateWayType, paymentMethod: String) {
        self.paymentGateWayType = paymentGateWayType
        self.paymentMethod = paymentMethod
        var paymentParams = getPaymentParams()
        guard let payloadString = getPayload().dataWithJSONObject().utf8Encoding() else { return }
        paymentParams["payload"] = payloadString
        let hyper = Hyper()
        hyper.start(viewController, data: paymentParams) { (status, response, error) in
            print("Juspay SDK Response", status, error, response)
//            self.juspayOrderStatusService()
        }
    }
    
    func juspayOrderStatusService() {
        var params = JSONArrayOfDictString()
        params = [["orderId": jusPayResultModel?.orderID ?? ""]]
        NetworkManager.shared.jusPayOrderStatusServcie(params: params) { (error, response) in
            guard error == nil, let orderStatus = response as? JusPayOrderStatusModel else { return }
            guard let statusID = orderStatus.result?.statusID, statusID == 21 else { return }
//            self.delegate?.successProcess()
        }
    }
    
    func juspayOrderCompleteService() {
        var params = JSONArrayOfDictString()
        params = [
            ["cartId": cartID,
             "paymentMethod": paymentMethod,
             "paypalPayId": "",
             "paytmCheckSum": ""
            ]
        ]
        NetworkManager.shared.jusPayOrderCompleteServcie(params: params) { (error, response) in
            guard error == nil, let status = response as? String else { return }
            guard status.lowercased() == ResponseStatus.success.rawValue else { return }
            self.delegate?.successProcess()
        }
    }
}

extension JusPayTransaction {
    private func parseErrorInfo(errorInfo: ErrorInfoModel?) {
        //        guard let errorMessage = errorInfo?.message, errorMessage.isValidString else { return }
    }
}

extension JusPayTransaction {
    private func getPayload() -> JSONDictAny {
        var payLoad = JSONDictAny()
        switch paymentGateWayType {
        case .phonePe:
            payLoad = getCommonPayload()
        case .mobiKwik, .freeCharge, .olaPostPaid, .googlePay:
            payLoad = getCommonPayload()
            payLoad["directWalletToken"] = directWalletToken
        case .netBanking:
            payLoad["opName"] = "nbTxn"
            payLoad["paymentMethodType"] = "NB"
            payLoad["paymentMethod"] = paymentMethod
            payLoad["redirectAfterPayment"] = "false"
        default: break
        }
        return payLoad
    }
    
    private func getCommonPayload() -> JSONDictAny {
        var payLoad = JSONDictAny()
        payLoad["opName"] = "walletTxn"
        payLoad["paymentMethodType"] = "Wallet"
        payLoad["paymentMethod"] = paymentMethod
        return payLoad
    }
    
    private func getPaymentParams() -> JSONDictAny {
        var paymentParams = JSONDictAny()
        guard let jusPayModel = jusPayResultModel else { return paymentParams }
        paymentParams["merchant_id"] = jusPayModel.merchantID
        paymentParams["client_id"] = jusPayModel.clientID
        paymentParams["customer_id"] = jusPayModel.customerID
        paymentParams["customer_email"] = jusPayModel.emailID
        paymentParams["customer_phone_number"] = jusPayModel.phoneNumber
        paymentParams["environment"] = jusPayModel.environment
        paymentParams["service"] =  jusPayModel.service
        paymentParams["session_token"] = jusPayModel.authInfo?.authToken
        paymentParams["end_urls"] = [jusPayModel.returnURL]
        //"https://s1-app.netmeds.com/pub/pg/juspay/returnurl.php?"
        //juspayModelData.returnjuspayUrl //"https://s1-meds.netmeds.com/pub/pg/juspay/returnurl.php"
        //"https://s1-apiv2.netmeds.com/pub/pg/juspay/returnurl.php"
        paymentParams["order_id"] = jusPayModel.orderID
        paymentParams["amount"] = jusPayModel.amount
        paymentParams["transaction_id"] = jusPayModel.transactionID
        paymentParams["redirectAfterPayment"] = jusPayModel.redirectAfterPayment
        return paymentParams
    }
}
