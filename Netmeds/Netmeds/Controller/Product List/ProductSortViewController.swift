//
//  ProductSortViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol ProductSortDelegateHandlers: class {
    func productSortSuccessProcess()
}

enum SortDisplayNameType: String {
    case popularity = "Popularity"
    case discount = "Discount"
    case priceLowtoHigh = "Price Low to High"
    case priceHightoLow = "Price High to Low"
    case nameAtoZ = "Name A to Z"
    case nameZtoA = "Name Z to A"
    case none = "none"
}

class ProductSortViewController: BaseViewController {
    @IBOutlet weak private var collectionViewSort: UICollectionView!
    private var productSortViewModel: ProductSortViewModel = ProductSortViewModel()
    private var onCompletionHandler: ((_ sortItems: [ProductSortModel]?, _ hasAlgoliaSortSearch: Bool) -> ())?
    private var noSortValue:(()->())?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension ProductSortViewController {
    private func setUpUserInterface() {
        productSortViewModel.delegate = self
        registerCollectionViewCells()
        productSortViewModel.setUpSortData()
    }
    
    private func registerCollectionViewCells() {
        collectionViewSort.registerNibForCell(ProductSortCollectionViewCell.self)
    }
    
    func assignSelectedSortItemsInfo(selectedSortItems: [ProductSortModel]?, completionHandler:@escaping(_ sortItems: [ProductSortModel]?, _ hasAlgoliaSortSearch: Bool)->()){
        productSortViewModel.selectedSortItems = selectedSortItems ?? [ProductSortModel]()
        self.onCompletionHandler = completionHandler
    }
    
    func noSortAvailable(action:@escaping (()->())) {
        self.noSortValue = action
    }
    
    private func closeTheView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension ProductSortViewController {
    @IBAction private func performCloseButton(_ sender: Any) {
        closeTheView()
    }
    
    @IBAction private func performClearAllButton(_ sender: Any) {
        onCompletionHandler?(nil, false)
        closeTheView()
    }
    
    @IBAction private func performApplyFilterButton(_ sender: Any) {
        if productSortViewModel.hasSelectedSortItems() {
            onCompletionHandler?(productSortViewModel.sortItems, true)
            closeTheView()
        } else {
            self.noSortValue?() ?? ()
        }
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- CollectionView Delegate and DataSource Functions
extension ProductSortViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productSortViewModel.sortItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sortCell: ProductSortCollectionViewCell = collectionViewSort.dequeueReusableCellForIndexPath(indexPath)
        let sortModel = productSortViewModel.sortItems[indexPath.row]
        sortCell.configCell(sortModel: sortModel)
        return sortCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        productSortViewModel.resetSelectedItems()
        let sortModel = productSortViewModel.sortItems[indexPath.row]
        sortModel.isSelected = !sortModel.isSelected
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.blue.cgColor
        cell?.layer.borderWidth = 1
    }
}

//MARK:- Product Sort Delegate Handlers
extension ProductSortViewController: ProductSortDelegateHandlers {
    func productSortSuccessProcess() {
        collectionViewSort.reloadData()
    }
}
