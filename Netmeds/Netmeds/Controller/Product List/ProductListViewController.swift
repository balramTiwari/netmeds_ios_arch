//
//  ProductListViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol ProductListDelegateHandlers: class {
    func productListSuccessProcess()
    func algoliaHitsSuccessProcess()
    func productListBannersSuccessProcess()
    func failureProcess(errorMessage message: String)
}

class ProductListViewController: BaseViewController {
    
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var tblViewProductList: UITableView!
    private var productListViewModel: ProductListViewModel = ProductListViewModel()
    private var currentPage: Int = 0
    private var isDataLoading: Bool = false
    // Algolia Filter
    private var hasAlgoliaSearchForFilter: Bool = false
    private var algoliaSearchQueryFilterString = ""
    private var selectedFacets: [ProductFilterModel]?
    // Algolia Sort
    private var hasAlgoliaSearchForSort: Bool = false
    private var selectedSortItems: [ProductSortModel]?
    private var algoliaSearchIndex = kAlgoliaSortPopularitySearchIndex
    
    var productID: Int = 0
    var isCategoryProduct: Bool = true
    var titleString: String = ""
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarWithSearchCartIcons()
        setUpNavigationTitleView()
    }
}

// MARK: All Functions
extension ProductListViewController {
    private func setUpUserInterface() {
        productListViewModel.delegate = self
        lblTitle.text = titleString
        registerTableViewCells()
        getProductListService()
    }
    
    private func registerTableViewCells() {
        tblViewProductList.tableFooterView = UIView()
        tblViewProductList.registerNibForCell(SearchTblViewCell.self)
        tblViewProductList.registerNibForCell(TblCellWithCollectionView.self)
    }
    
    private func setUpTableHeaderView() -> UIView {
        guard let offersCell: TblCellWithCollectionView = tblViewProductList.dequeueReusableCell(withIdentifier: "TblCellWithCollectionView") as? TblCellWithCollectionView, productListViewModel.productListBanners.count > 0 else { return UIView() }
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewProductList.frame.size.width, height: 145.0))
        offersCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, headerModel: HomeHeaderModel())
        offersCell.contentView.frame = headerView.frame
        headerView.addSubview(offersCell.contentView)
        return headerView
    }
    
    private func getDataFromAlgoliaSearchForSort() {
        guard let selectedItems = selectedSortItems else { return }
        for item in selectedItems where item.isSelected == true {
            algoliaSearchIndex = item.index
        }
        applySelectedListToAlgolia()
    }
    
    private func getDataFromAlgoliaSearchForFilter(selectedFacets: [ProductFilterModel]?) {
        var filterQueryString: String = ""
        guard let selectedFilter = selectedFacets else { return }
        for filter in selectedFilter {
            let selectedFilters = getSelectedFilters(filter: filter)
            if !selectedFilters.isEmpty && !filterQueryString.isEmpty { filterQueryString = filterQueryString + " AND "}
            filterQueryString = filterQueryString + selectedFilters
        }
        algoliaSearchQueryFilterString = filterQueryString
        applySelectedListToAlgolia()
    }
    
    private func getSelectedFilters(filter: ProductFilterModel) -> String {
        var filterQueryString: String = ""
        for values in filter.values where values.isSelected == true {
            filterQueryString = filterQueryString.isEmpty ? "" : (filterQueryString + " OR ")
            if filter.key == FacetsAlgoliaSearchKeysType.sellingPrice.rawValue {
                filterQueryString = filterQueryString + "\(filter.key)" + ":" + "\(values.name.replace(string: "Rs.", replacement: "").replace(string: "-", replacement: "TO"))"
            } else if filter.key == FacetsAlgoliaSearchKeysType.discountPrice.rawValue {
                filterQueryString = filterQueryString + "\(filter.key)" + ":" + "\(values.name.replace(string: "%", replacement: "").replace(string: "-", replacement: "TO"))"
            } else if filter.key == FacetsAlgoliaSearchKeysType.inStock.rawValue  {
                filterQueryString = filterQueryString + "\"\(filter.key)\"" + ":" + "\"\(values.key)\""
            } else {
                filterQueryString = filterQueryString + "\"\(filter.key)\"" + ":" + "\"\(values.name)\""
            }
        }
        return filterQueryString
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension ProductListViewController {
    @IBAction func performFilterButton() {
        guard let vc = ProductFilterViewController.instantiate(storyboardName: .products) as? ProductFilterViewController else { return }
        vc.productID = productID
        vc.assignSelectedFacetsInfo(selectedFacets: self.selectedFacets, subCategories: self.productListViewModel.categoryListDetails?.subCategories ?? [SubCategoriesModel]()) { [weak self] (selectedFacets, hasAlgoliaSearch) in
            //                            self?.listEmpty.isHidden = true
            self?.selectedFacets = selectedFacets
            self?.hasAlgoliaSearchForFilter = hasAlgoliaSearch
            self?.currentPage = 1
            self?.isDataLoading = true
            self?.productListViewModel.algoliaSearchList.removeAll()
            self?.productListViewModel.productList.removeAll()
            self?.tblViewProductList.reloadData()
            if !(self?.hasAlgoliaSearchForFilter ?? false), !(self?.hasAlgoliaSearchForSort ?? false) {
                self?.getProductListService()
            } else if (self?.hasAlgoliaSearchForFilter ?? false){
                self?.getDataFromAlgoliaSearchForFilter(selectedFacets: selectedFacets)
            } else {
                self?.algoliaSearchQueryFilterString = ""
                self?.getDataFromAlgoliaSearchForSort()
            }
        }
        vc.noFilerAvailable {
            //                            self.addNotificationView(textlabel: "No filter options available")
        }
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func performSortButton() {
        guard let vc = ProductSortViewController.instantiate(storyboardName: .products) as? ProductSortViewController else { return }
        vc.assignSelectedSortItemsInfo(selectedSortItems: self.selectedSortItems) { [weak self] (sortItems, hasAlgoliaSortSearch) in
            //                            self?.listEmpty.isHidden = true
            self?.selectedSortItems = hasAlgoliaSortSearch ? sortItems: nil
            self?.hasAlgoliaSearchForSort = hasAlgoliaSortSearch
            self?.currentPage = 1
            self?.isDataLoading = true
            self?.productListViewModel.algoliaSearchList.removeAll()
            self?.productListViewModel.productList.removeAll()
            self?.tblViewProductList.reloadData()
            if !(self?.hasAlgoliaSearchForFilter ?? false), !(self?.hasAlgoliaSearchForSort ?? false) {
                self?.getProductListService()
            } else if (self?.hasAlgoliaSearchForSort ?? false){
                self?.getDataFromAlgoliaSearchForSort()
            } else {
                self?.algoliaSearchIndex = kAlgoliaSortPopularitySearchIndex
                self?.getDataFromAlgoliaSearchForFilter(selectedFacets: self?.selectedFacets)
            }
        }
        vc.noSortAvailable {
            //                            self.addNotificationView(textlabel: "No sort option available")
        }
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

// MARK: All API Functions
extension ProductListViewController {
    private func getProductListService() {
        self.algoliaSearchQueryFilterString = ""
        algoliaSearchIndex = kAlgoliaSortPopularitySearchIndex
        currentPage = 1
        guard isCategoryProduct else {
            productListViewModel.getManufacturerProductListService(manufacturerID: productID, pageNo: currentPage); return }
        productListViewModel.getCategoryProductListService(categoryID: productID, pageNo: currentPage)
    }
    
    private func applySelectedListToAlgolia() {
        let facetFiltersKey = isCategoryProduct ? "category_ids": "manufacturer_id"
        let facetFilters: [Any]? = ["\(facetFiltersKey):\(productID)"]
        productListViewModel.algoliaSearchService(index: algoliaSearchIndex, filters: algoliaSearchQueryFilterString, facetFilters: facetFilters, page: UInt(currentPage - 1))
    }
    
    private func getProductListBannersService() {
        guard isCategoryProduct else {
            productListViewModel.getManufacturerProductBannersService(manufacturerID: productID); return }
        productListViewModel.getCategoryProductBannersService(categoryID: productID)
    }
}


//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension ProductListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard !hasAlgoliaSearchForFilter, !hasAlgoliaSearchForSort else { return productListViewModel.algoliaSearchList.count }
        return productListViewModel.productList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productListCell: SearchTblViewCell = tblViewProductList.dequeueReusableCellForIndexPath(indexPath)
        if hasAlgoliaSearchForFilter || hasAlgoliaSearchForSort {
            let alogliaSearchModel  = productListViewModel.algoliaSearchList[indexPath.row]
            productListCell.configCell(alogliaSearchModel: alogliaSearchModel)
        } else {
            let productModel = productListViewModel.productList[indexPath.row]
            productListCell.configCellFromProductList(product: productModel)
        }
        productListCell.delegate = self
        return productListCell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let _ = scrollView as? UITableView else { return }
        let translation = scrollView.panGestureRecognizer.translation(in: self.view)
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - (scrollView.frame.size.height + 400), translation.y < 0 {
            if !isDataLoading, productListViewModel.totalCount > 0, currentPage < (productListViewModel.totalCount / 10) {
                isDataLoading = true
                currentPage += 1
                guard !hasAlgoliaSearchForFilter, !hasAlgoliaSearchForSort else { applySelectedListToAlgolia(); return }
                getProductListService()
            }
        }
    }
}

//MARK:- CollectionView Delegate and DataSource Functions
extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productListViewModel.productListBanners.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 145.0
        var width: CGFloat = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        if productListViewModel.productListBanners.count > 1 {
            width -= 16
        }
        return CGSize(width: width - 40, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let banner = productListViewModel.productListBanners[indexPath.row]
        let offersCell: CollectionViewCellWithImage = collectionView.dequeueReusableCellForIndexPath(indexPath)
        offersCell.configCell(imgURL: banner.imageURL!)
        return offersCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let banner = productListViewModel.productListBanners[indexPath.row]
        bannerRedirection(linkType: banner.linkType!, url: banner.url!)
    }
}

//MARK:- Product List Delegate Handlers
extension ProductListViewController: ProductListDelegateHandlers {
    func productListSuccessProcess() {
        getProductListBannersService()
        isDataLoading = false
        tblViewProductList.reloadData()
    }
    
    func algoliaHitsSuccessProcess() {
        isDataLoading = false
        if productListViewModel.algoliaSearchList.count == 0 {
            //            self.listEmpty.isHidden = false
        }
        tblViewProductList.reloadData()
    }
    
    func productListBannersSuccessProcess() {
        tblViewProductList.tableHeaderView = setUpTableHeaderView()
    }
    
    func failureProcess(errorMessage message: String) {
        showErrorMessage(errorMessage: message)
    }
}

//MARK:- Search TblView Cell Delegate Handlers
extension ProductListViewController: SearchTblViewCellDelegateHandlers {
    func btnAddToCartTapped(cell: SearchTblViewCell) {
        //        guard let indexPath = tblViewSearch.indexPath(for: cell) else { return }
        //        let productID: Int = searchViewModel.algoliaSearchModel[indexPath.row].productCode!
        //        navigateToAddToCartVC(productID: productID)
    }
    
    func btnManufacturerNameTapped(cell: SearchTblViewCell) {
        
    }
    
}

