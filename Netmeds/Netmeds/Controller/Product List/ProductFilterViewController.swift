//
//  ProductFilterViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 31/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

enum FacetsAlgoliaSearchKeysType: String {
    case brand = "brand"
    case categories = "categories"
    case discountPrice = "discount_pct"
    case sellingPrice = "selling_price"
    case manufacturer = "manufacturer_name"
    case inStock = "in_stock"
    case none = "none"
}

enum FacetsParseKeysType: String {
    case brand = "brand"
    case categories = "categories"
    case discountPrice = "discount_pct"
    case sellingPrice = "selling_price"
    case manufacturer = "manufacturer_name"
    case inStock = "in_stock"
    case none = "none"
}

enum FacetsDisplayNameType: String {
    case brand = "Brands"
    case categories = "Sub_Category"
    case discountPrice = "Discount"
    case sellingPrice = "Price"
    case manufacturer = "Manufacturer"
    case inStock = "Availability"
    case none = "none"
}

protocol ProductFilterDelegateHandlers: class {
    func algoliaHitsSuccessProcess()
    func productSearchFilterSuccessProcess()
}

class ProductFilterViewController: BaseViewController {
    @IBOutlet weak private var txtFldSearchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var txtFldSearch: UITextField!
    @IBOutlet weak private var tblViewCategory: UITableView!
    @IBOutlet weak private var tblViewSubCategory: UITableView!
    private var selectedIndex: Int = 0 {
        didSet {
            tblViewSubCategory.reloadData()
        }
    }
    private var searchedText = ""
    private var onCompletionHandler: ((_ facets: [ProductFilterModel]?, _ hasAlgoliaSearch: Bool) -> ())?
    private var noFilterValue:(()->())?
    private var productFilterViewModel = ProductFilterViewModel()
    var productID: Int = 0
    var isCategoryProduct: Bool = true
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension ProductFilterViewController {
    private func setUpUserInterface() {
        productFilterViewModel.delegate = self
        registerTableViewCells()
        setUpTextField()
        applySelectedListToAlgolia()
    }
    
    private func registerTableViewCells() {
        tblViewCategory.tableFooterView = UIView()
        tblViewSubCategory.tableFooterView = UIView()
        tblViewCategory.registerNibForCell(ProductFilterTblViewCell.self)
        tblViewSubCategory.registerNibForCell(ProductFilterTblViewCell.self)
    }
    
    private func setUpTextField() {
        txtFldSearch.delegate = self
        txtFldSearch.layer.borderColor = UIColor.lightGray.cgColor
        txtFldSearch.autocorrectionType = .no
        txtFldSearchHeightConstraint.constant = 0
        txtFldSearch.leftViewMode = .always
        txtFldSearch.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 40.0))
    }
    
    private func updateUI() {
        tblViewCategory.reloadData()
        guard productFilterViewModel.filterItems.count > 0 else { return }
        let indexPath = IndexPath(row: 0, section: 0);
        tblViewCategory.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        self.tableView(tblViewCategory, didSelectRowAt: indexPath)
    }
    
    func assignSelectedFacetsInfo(selectedFacets: [ProductFilterModel]?, subCategories: [SubCategoriesModel], completionHandler:@escaping(_ facets: [ProductFilterModel]?, _ hasAlgoliaSearch: Bool)->()) {
        productFilterViewModel.assignSelectedFacetsInfo(selectedFacets: selectedFacets, subCategories: subCategories)
        self.onCompletionHandler = completionHandler
    }
    
    func noFilerAvailable(action:@escaping (()->())) {
        self.noFilterValue = action
    }
    
    private func cloaeTheView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
}

// MARK: All Button Functions
extension ProductFilterViewController {
    
    @IBAction private func performCloseButton(_ sender: Any) {
        cloaeTheView()
    }
    
    @IBAction private func performClearAllButton(_ sender: Any) {
        productFilterViewModel.resetSelectedFilter()
        onCompletionHandler?(productFilterViewModel.filterItems, false)
        cloaeTheView()
    }
    
    @IBAction private func performApplyFilterButton(_ sender: Any) {
        if productFilterViewModel.hasSelectedFilter() {
            onCompletionHandler?(productFilterViewModel.filterItems, true)
            cloaeTheView()
        } else {
            self.noFilterValue?() ?? ()
        }
    }
}

// MARK: All API Functions
extension ProductFilterViewController {
    private func applySelectedListToAlgolia() {
        let facetFiltersKey = isCategoryProduct ? "category_ids": "manufacturer_id"
        let facetFilters: [Any]? = ["\(facetFiltersKey):\(productID)"]
        productFilterViewModel.algoliaSearchService(facetFilters: facetFilters)
    }
}

// MARK:- All Delegate Functions
// MARK:- TableView Delegate and DataSource Functions
extension ProductFilterViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewCategory {
            return productFilterViewModel.filterItems.count
        }
        guard searchedText.isEmpty else { return productFilterViewModel.searchFilterItems.count }
        return productFilterViewModel.filterItems.count > 0 ? productFilterViewModel.filterItems[selectedIndex].values.count: 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if tableView == tblViewCategory {
            return 60.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblViewCategory {
            let categoryCell: ProductFilterTblViewCell = tblViewCategory.dequeueReusableCellForIndexPath(indexPath)
            let filterModel = productFilterViewModel.filterItems[indexPath.row]
            categoryCell.configCell(filterModel: filterModel)
            let selectionColor: UIColor = UIColor(red: 21.0/255.0, green: 27.0/255.0, blue: 57.0/255.0, alpha: 0.1)
            categoryCell.backgroundColor = selectedIndex == indexPath.row ? selectionColor: .white
            categoryCell.contentView.backgroundColor = selectedIndex == indexPath.row ? selectionColor: .white
            return categoryCell
        } else {
            let subCategoryCell: ProductFilterTblViewCell = tblViewSubCategory.dequeueReusableCellForIndexPath(indexPath)
            let filterValuesModel = searchedText.isEmpty ? productFilterViewModel.filterItems[selectedIndex].values[indexPath.row]: productFilterViewModel.searchFilterItems[indexPath.row]
            subCategoryCell.configCell(filterValuesModel: filterValuesModel)
            return subCategoryCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView == tblViewSubCategory else {
            self.view.endEditing(true)
            searchedText = ""
            txtFldSearch.text = ""
            productFilterViewModel.searchFilterItems.removeAll()
            selectedIndex = indexPath.row
            let filterModel = productFilterViewModel.filterItems[indexPath.row]
            txtFldSearchHeightConstraint.constant = 0
            if filterModel.name == FacetsDisplayNameType.manufacturer.rawValue || filterModel.name == FacetsDisplayNameType.brand.rawValue || filterModel.name == FacetsDisplayNameType.categories.rawValue {
                self.txtFldSearchHeightConstraint.constant = 40
            }
            tblViewCategory.reloadData()
            tblViewSubCategory.scrollToRow(at: IndexPath(item: 0, section: 0), at: .none, animated: false)
            return
        }
        let subCategoryValues = searchedText.isEmpty ? productFilterViewModel.filterItems[selectedIndex].values[indexPath.row]: productFilterViewModel.searchFilterItems[indexPath.row]
        subCategoryValues.isSelected = !subCategoryValues.isSelected
        let indexPath = IndexPath(item: indexPath.row, section: 0)
        tblViewSubCategory.reloadRows(at: [indexPath], with: .none)
    }
}

// MARK:- TextField Delegate Function
extension ProductFilterViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let text = (currentText as NSString).replacingCharacters(in: range, with: string)
        searchedText = text
        guard !text.isEmpty else { productFilterViewModel.searchFilterItems.removeAll(); return true }
        productFilterViewModel.searchFilterBy(name: text, selectedIndex: selectedIndex)
        return true
    }
}

//MARK:- Product Sort Delegate Handlers
extension ProductFilterViewController: ProductFilterDelegateHandlers {
    func algoliaHitsSuccessProcess() {
        updateUI()
    }
    
    func productSearchFilterSuccessProcess() {
        tblViewSubCategory.reloadData()
    }
}
