//
//  PromoCodeListPopUpViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 06/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol PromoCodeListDelegateHandlers: class {
    func successProcess(isAppliedPromoCode: Bool)
    func failureProcess(errorMessage message: String)
}

class PromoCodeListPopUpViewController: BaseViewController {
    
    @IBOutlet weak private var tblViewPromoCode: UITableView!
    @IBOutlet weak private var bottomHolderView: UIView!
    @IBOutlet weak private var bottomHolderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var txtFldPromoCode: UITextField!
    @IBOutlet weak private var btnPromocodeApply: UIButton!
    private var allCoupons = [AllCouponsModel]()
    private var completionHandler: ((_ isAppliedPromoCode: Bool) -> ())?
    private var isRemovedFromBtn: Bool = true
    private var appliedCouponName: String = ""
    var cartViewModel = CartViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bottomHolderView.roundCorners([.topLeft, .topRight], radius: 16)
    }
}

// MARK: All Functions
extension PromoCodeListPopUpViewController {
    private func setUpUserInterface() {
        appliedCouponName = cartViewModel.appliedCouponName
        cartViewModel.promoCodeListDelegate = self
        allCoupons = cartViewModel.allCoupons
        updateUI()
        txtFldPromoCode.setBottomBorder()
        registerTableViewCells()
    }
    
    //    private func updateUI() {
    //        let filtered = allCoupons.filter { $0.isSelected == true }
    //        if filtered.count > 0 {
    //            txtFldPromoCode.text = filtered.first?.couponCode
    //            btnPromocodeApply.setTitle("REMOVE", for: .normal)
    //            txtFldPromoCode.isEnabled = false
    //        } else {
    //            txtFldPromoCode.text = ""
    //            txtFldPromoCode.isEnabled = true
    //            btnPromocodeApply.setTitle("APPLY", for: .normal)
    //        }
    //    }
    
    private func updateUI() {
        if appliedCouponName.isValidString {
            txtFldPromoCode.text = appliedCouponName
            btnPromocodeApply.setTitle("REMOVE", for: .normal)
            txtFldPromoCode.isEnabled = false
        } else {
            txtFldPromoCode.text = ""
            txtFldPromoCode.isEnabled = true
            btnPromocodeApply.setTitle("APPLY", for: .normal)
        }
    }
    
    private func registerTableViewCells() {
        tblViewPromoCode.tableFooterView = UIView()
        tblViewPromoCode.rowHeight = UITableView.automaticDimension
        tblViewPromoCode.estimatedRowHeight = 100
        tblViewPromoCode.registerNibForCell(PromoCodeListTblViewCell.self)
        tblViewPromoCode.reloadData()
    }
    
    //    private func resetSelectedItem() {
    //        for (index, _) in allCoupons.enumerated() {
    //            allCoupons[index].isSelected = false
    //        }
    //        tblViewPromoCode.reloadData()
    //    }
    
    private func resetSelectedItem() {
        appliedCouponName = ""
        tblViewPromoCode.reloadData()
    }
    
    func onCompletionHandler(_ completionHandler:@escaping (_ isAppliedPromoCode: Bool) -> () ) {
        self.completionHandler = completionHandler
    }
    
    private func showErrorMessage(errorMessage message: String) {
        self.createToastWith(message: message)
    }
    
    private func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: All Button Functions
extension PromoCodeListPopUpViewController {
    @IBAction private func performApplyButton() {
        isRemovedFromBtn = true
        guard appliedCouponName.isValidString else {
            guard txtFldPromoCode.text!.isValidString else {
                showErrorMessage(errorMessage: kPromoCodeEnterValidText)
                return }
            applyCouponCodeService(couponCode: txtFldPromoCode.text!); return }
        unApplyCouponCodeService(couponCode: appliedCouponName)
        
        //        isRemovedFromBtn = true
        //        let filtered = allCoupons.filter { $0.isSelected == true }
        //        guard filtered.count > 0 else {
        //            guard txtFldPromoCode.text!.isValidString else {
        //                showErrorMessage(errorMessage: kPromoCodeEnterValidText)
        //                return }
        //            applyCouponCodeService(couponCode: txtFldPromoCode.text!); return }
        //        unApplyCouponCodeService(couponCode: filtered[0].couponCode!)
    }
    
    @IBAction private func performCloseButton() {
        dismissVC()
    }
}

// MARK: All API Functions
extension PromoCodeListPopUpViewController {
    private func applyCouponCodeService(couponCode: String) {
        cartViewModel.applyCouponCodeService(couponCode: couponCode)
    }
    
    private func unApplyCouponCodeService(couponCode: String) {
        cartViewModel.unApplyCouponCodeService(couponCode: couponCode)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension PromoCodeListPopUpViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allCoupons.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if allCoupons.count < 3 {
            bottomHolderViewHeightConstraint.constant = tableView.contentSize.height + 114.0
            self.tableViewHeightConstraint.constant = tableView.contentSize.height
        } else {
            bottomHolderViewHeightConstraint.constant = 420
            self.tableViewHeightConstraint.constant = 420 - 114
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let coupon = allCoupons[indexPath.section]
        let cell: PromoCodeListTblViewCell = tblViewPromoCode.dequeueReusableCellForIndexPath(indexPath)
        cell.configCell(coupon: coupon, appliedCouponName: self.appliedCouponName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewPromoCode.frame.size.width, height: 10.0))
        footerView.backgroundColor = .white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isRemovedFromBtn = false
        let coupon = allCoupons[indexPath.section]
        if appliedCouponName.isValidString {
            if appliedCouponName == coupon.couponCode { dismissVC(); return }
            unApplyCouponCodeService(couponCode: appliedCouponName)
            resetSelectedItem()
        } else {
            applyCouponCodeService(couponCode: coupon.couponCode!)
        }
        appliedCouponName = coupon.couponCode!
        tblViewPromoCode.reloadData()
        
        //        isRemovedFromBtn = false
        //        let coupon = allCoupons[indexPath.section]
        //        let filtered = allCoupons.filter { $0.isSelected == true }
        //        if filtered.count > 0 {
        //            if filtered[0].couponCode! == coupon.couponCode! { dismissVC(); return }
        //            unApplyCouponCodeService(couponCode: filtered[0].couponCode!)
        //           resetSelectedItem()
        //        } else {
        //            applyCouponCodeService(couponCode: coupon.couponCode!)
        //        }
        //        allCoupons[indexPath.section].isSelected = true
        //        tblViewPromoCode.reloadData()
    }
}

//MARK:- TextField Delegate Functions
extension PromoCodeListPopUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:- TextField Delegate Functions
extension PromoCodeListPopUpViewController: PromoCodeListDelegateHandlers {
    func successProcess(isAppliedPromoCode: Bool) {
        if !isAppliedPromoCode, !self.isRemovedFromBtn {
            if appliedCouponName.isValidString {
                applyCouponCodeService(couponCode: appliedCouponName)
            }
            return
        }
        dismiss(animated: true) {
            self.completionHandler?(isAppliedPromoCode)
        }
    }
    
    //    func successProcess(isAppliedPromoCode: Bool) {
    //        if !isAppliedPromoCode, !self.isRemovedFromBtn {
    //            let filtered = allCoupons.filter { $0.isSelected == true }
    //            if filtered.count > 0 {
    //                 applyCouponCodeService(couponCode: filtered[0].couponCode!);
    //            }
    //            return
    //        }
    //        dismiss(animated: true) {
    //            self.completionHandler?(isAppliedPromoCode)
    //        }
    //    }
    
    func failureProcess(errorMessage message: String) {
        resetSelectedItem()
        updateUI()
        showErrorMessage(errorMessage: message)
        self.completionHandler?(false)
    }
}
