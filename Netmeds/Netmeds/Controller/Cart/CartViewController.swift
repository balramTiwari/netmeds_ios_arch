//
//  CartViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 24/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol CartDelegateHandlers: class {
    func successProcess(totalAmount: String, shippingAddressID: Int)
    func applySuperCashProcess()
    func deliveryEstimationSuccessProcess()
    func failureProcess(errorMessage message: String)
    func emptyCartProcess()
}

protocol CartProductTblCellDelegateHandlers: class {
    func buttonTapped(cell: CartProductTblViewCell, type: CartProductTblCellButtonType, sender: UIButton)
}

class CartViewController: BaseViewController {
    @IBOutlet weak private var tblViewCart: UITableView!
    @IBOutlet weak private var bottomView: UIView!
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var btnProceed: UIButton!
    @IBOutlet weak private var lblTotalAmount: UILabel!
    private var cartViewModel = CartViewModel()
    private var shippingAddressID: Int = 0
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarWithSearchIcon()
        getCartDetailsService()
    }
}

// MARK: All Functions
extension CartViewController {
    private func setUpUserInterface() {
        cartViewModel.delegate = self
        registerTableViewCells()
        setUpRefreshControl()
    }
    
    private func registerTableViewCells() {
        tblViewCart.tableFooterView = UIView()
        tblViewCart.registerNibForCell(CartProductTblViewCell.self)
        tblViewCart.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewCart.registerNibForCell(PaymentDetailsTblViewCell.self)
        tblViewCart.registerNibForCell(CartBecomeFirstMemberTblViewCell.self)
        tblViewCart.registerNibForCell(CartUnDeliverableTblViewCell.self)
        tblViewCart.registerNibForCell(CartOutOfStockTblViewCell.self)
        tblViewCart.registerNibForCell(CartCouponCodeTblViewCell.self)
        tblViewCart.registerNibForCell(CartNMSSuperCashTblViewCell.self)
        tblViewCart.registerNibForCell(CartSeparatorTblViewCell.self)
        tblViewCart.registerNibForCell(DisclaimerTblViewCell.self)
        tblViewCart.rowHeight = UITableView.automaticDimension
        tblViewCart.estimatedRowHeight = 100
        tblViewCart.sectionHeaderHeight = UITableView.automaticDimension
        tblViewCart.estimatedSectionHeaderHeight = 100
    }
    
    private func setUpRefreshControl() {
        refreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.netmedsGreenColor()
        tblViewCart.addSubview(refreshControl)
    }
        
    @objc private func handleRefresh() {
        self.getCartDetailsService()
    }
}

// MARK: All Navigation Functions
extension CartViewController {
    private func navigateToQuantityListPopUpVC(rowModel: CartRowModel, maximumQty: NSNumber, sender: UIButton) {
        guard let vc = QuantityListPoUpViewController.instantiate(storyboardName: .cart) as? QuantityListPoUpViewController else { return }
        var maxQty = maximumQty
        var itemqty = NSNumber(value: 0)
        if let qty = rowModel.quantity as NSNumber? {
            itemqty = qty
            vc.selectedQuantity = qty.stringValue
        }
        if maxQty.intValue == 0 {
            vc.maxQuantity = itemqty
        } else {
            vc.maxQuantity = NSNumber(value: rowModel.maxQtyInOrder)
        }
        
        if itemqty.intValue >= maxQty.intValue {
            vc.selectedQuantity = "\(rowModel.quantity)" // max qty
        } else {
            vc.selectedQuantity = itemqty.stringValue // qty
        }
        var height = 300
        maxQty = NSNumber(value: rowModel.maxQtyInOrder)
        if maxQty.intValue == 0 {
            height = (itemqty.intValue * 44)
        } else if maxQty.intValue < 10 {
            height = (maxQty.intValue * 44)
        }
        let currentQuantity = rowModel.quantity
        let productCode = String(rowModel.productCode)
        vc.handleQuantitySelectionAction { [weak self] newQty in
            let newQuantity = Int(truncating: newQty)
            if currentQuantity > newQuantity {
                let toBeRemoveQty = currentQuantity - (currentQuantity - newQuantity)
                let deleteItemQty = currentQuantity - toBeRemoveQty
                self?.removeItemFromCartService(productCode: productCode, quantity: String(deleteItemQty))
            } else {
                let toBeAddQty = (newQuantity - currentQuantity) + currentQuantity
                let addItemQty = toBeAddQty - currentQuantity
                self?.addItemToCartService(productCode: productCode, quantity: String(addItemQty))
            }
        }
        vc.preferredContentSize = CGSize(width: 170, height: CGFloat(height))
        vc.modalPresentationStyle = .popover
        let popOver = vc.popoverPresentationController
        popOver?.delegate = self
        self.present(vc, animated: true, completion: nil)
        popOver?.sourceView = self.view
        popOver?.barButtonItem = UIBarButtonItem(customView: sender)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblViewCart)
        popOver?.sourceRect = CGRect(x: buttonPosition.x, y: buttonPosition.y, width: 170, height: CGFloat(height))
    }
    
    private func navigateToPromoCodeListVC() {
        guard let vc = PromoCodeListPopUpViewController.instantiate(storyboardName: .cart) as? PromoCodeListPopUpViewController else { return }
        vc.cartViewModel = cartViewModel
        vc.onCompletionHandler { (isAppliedPromoCode) in
            if isAppliedPromoCode {
                self.createToastWith(message: kPromoCodeAppliedSuccessText)
            } else {
                self.createToastWith(message: kPromoRemovedSuccessText)
            }
            self.getCartDetailsService()
        }
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

// MARK: All Button Functions
extension CartViewController {
    @IBAction private func performProceedButton() {
        guard !cartViewModel.hasRxProduct else { return }
        guard !cartViewModel.hasOutOfStockProduct else { return }
        self.navigateToOrderReviewVC()
//        self.navigateToSelectAddressVC(shippingAddressID: shippingAddressID)
    }
}

// MARK: All API Functions
extension CartViewController {
    private func getCartDetailsService() {
        cartViewModel.getCartInfoService(pageType: .cart)
    }
    
    private func removeItemFromCartService(productCode: String, quantity: String) {
        cartViewModel.removeItemFromCartService(productCode: productCode, quantity: quantity)
    }
    
    private func addItemToCartService(productCode: String, quantity: String) {
        cartViewModel.addItemToCartService(productCode: productCode, quantity: quantity)
    }
    
    private func applySuperCashService() {
        guard !cartViewModel.isUsedSuperCash else { return }
        cartViewModel.applySuperCashService()
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cartViewModel.cartHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartViewModel.cartHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerModel = cartViewModel.cartHeaderModel[section]
        guard headerModel.title.isValidString else { return CGFloat.leastNormalMagnitude }
        return UITableView.automaticDimension
        //        let headerModel = cartViewModel.cartHeaderModel[section]
        //        var height: CGFloat = (headerModel.type == .separatorView || headerModel.type == .superCash || headerModel.type == .disclaimer) ? CGFloat.leastNormalMagnitude: 10.0
        //        guard headerModel.title.isValidString else { return height }
        //        height = 60.0
        //        return height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
        //        let headerModel = cartViewModel.cartHeaderModel[section]
        //        guard headerModel.type == .separatorView || headerModel.type == .promoCode || headerModel.type == .paymentDetails else { return 10.0 }
        //        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = cartViewModel.cartHeaderModel[section]
        guard headerModel.title.isValidString else { return UIView() }
        guard let headerTblCell: CartHeaderTblViewCell = tblViewCart.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return UIView() }
        headerTblCell.configCell(title: headerModel.title)
        return headerTblCell
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerModel = cartViewModel.cartHeaderModel[section]
    //        var height: CGFloat = (headerModel.type == .separatorView || headerModel.type == .superCash || headerModel.type == .disclaimer) ? 0.0: 10.0
    //        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewCart.frame.size.width, height: height))
    //        headerView.backgroundColor = .clear
    //        guard headerModel.title.isValidString else { return headerView }
    //        guard let headerTblCell: CartHeaderTblViewCell = tblViewCart.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return headerView }
    //        height = 60.0
    //        headerView.frame = CGRect(x: 0, y: 0, width: tblViewCart.frame.size.width, height: height)
    //        headerTblCell.contentView.frame = headerView.frame
    //        headerTblCell.configCell(headerModel: headerModel)
    //        headerView.addSubview(headerTblCell.contentView)
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowModel = cartViewModel.cartHeaderModel[indexPath.section].rowModel
        let cornerRadius: CGFloat = indexPath.row == rowModel.count - 1 ? 10.0: 0.0
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = cartViewModel.cartHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .becomeFirstMember:
            let firstMemberCell: CartBecomeFirstMemberTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            firstMemberCell.configCell(title: rowModel.title, subTitle: rowModel.subTitle)
            return firstMemberCell
        case .undeliverable:
            let unDeliverableCell: CartUnDeliverableTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            unDeliverableCell.configCell(title: rowModel.title, subTitle: rowModel.subTitle, btnTitle: rowModel.btnTitle)
            return unDeliverableCell
        case .medicinesOutOfStock:
            let outOfStockCell: CartOutOfStockTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            outOfStockCell.configCell(title: rowModel.title, btnTitle: rowModel.btnTitle)
            return outOfStockCell
        case .products:
            let cartCell: CartProductTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            cartCell.configCell(rowModel: rowModel, isShowColdStorage: false)
            cartCell.delegate = self
            //            cartCell.lblSeparatorLine.isHidden = indexPath.row == headerModel.rowModel.count - 1 ? true: false
            return cartCell
        case .promoCode:
            switch rowModel.type {
            case .promoCode:
                let couponCodeCell: CartCouponCodeTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
                couponCodeCell.configCell(title: rowModel.title, desc: rowModel.desc, youSaveText: rowModel.sellerName, isCouponApplied: rowModel.isOutOfStock)
                return couponCodeCell
            case .separatorView:
                let separatorCell: CartSeparatorTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
                return separatorCell
            case .superCash:
                let superCashCell: CartNMSSuperCashTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
                superCashCell.configCell(title: rowModel.title, balance: rowModel.price, desc: rowModel.desc, isCouponApplied: rowModel.isOutOfStock)
                return superCashCell
            default:
                return UITableViewCell()
            }
        case .paymentDetails:
            let paymentDetailsCell: PaymentDetailsTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            paymentDetailsCell.configCell(title: rowModel.productName, price: rowModel.paymentDetailsPrice, type: rowModel.type)
            return paymentDetailsCell
        case .disclaimer:
            let disclaimerCell: DisclaimerTblViewCell = tblViewCart.dequeueReusableCellForIndexPath(indexPath)
            return disclaimerCell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let headerModel = cartViewModel.cartHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch headerModel.type {
        case .promoCode:
            switch rowModel.type {
            case .promoCode:
                navigateToPromoCodeListVC()
            case .superCash:
                applySuperCashService()
            default: break
            }
        default: break
        }
    }
    //    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        let headerModel = cartViewModel.cartHeaderModel[section]
    //        let footerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    //        guard headerModel.type == .separatorView || headerModel.type == .promoCode else {
    //            footerView.frame =  CGRect(x: 0, y: 0, width: 0, height: 10)
    //            footerView.backgroundColor = .clear
    //            return footerView
    //        }
    //        footerView.frame =  CGRect(x: 0, y: 0, width: 0, height: 1.0)
    //        footerView.backgroundColor = .white
    //        return footerView
    //    }
}

//MARK:- Cart Delegate Handlers
extension CartViewController : CartDelegateHandlers {
    func successProcess(totalAmount: String, shippingAddressID: Int) {
        self.shippingAddressID = shippingAddressID
        lblTotalAmount.text = totalAmount
        tblViewCart.reloadData()
        refreshControl.endRefreshing()
        bottomView.isHidden = false
        guard let index = cartViewModel.cartHeaderModel.index( where:{ $0.type == .products }) else { return }
        lblTitle.text = "\(cartViewModel.cartHeaderModel[index].rowModel.count) Item(s) in Cart"
    }
    
    func applySuperCashProcess() {
        self.getCartDetailsService()
    }
    
    func deliveryEstimationSuccessProcess() {}
    
    func failureProcess(errorMessage message: String) {
        self.createToastWith(message: message)
    }
    
    func emptyCartProcess() {
        lblTitle.text = "My Cart"
        self.showCartEmptyView()
    }
}

//MARK:- Cart Product Tbl View Cell Delegate Handlers
extension CartViewController: CartProductTblCellDelegateHandlers {
    func buttonTapped(cell: CartProductTblViewCell, type: CartProductTblCellButtonType, sender: UIButton) {
        guard let indexPath = tblViewCart.indexPath(for: cell) else { return }
        let headerModel = cartViewModel.cartHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        let productCode = String(rowModel.productCode)
        let quantity = String(rowModel.quantity)
        switch type {
        case .chooseQuantity:
            navigateToQuantityListPopUpVC(rowModel: rowModel, maximumQty: cell.maxQty, sender: sender)
        case .removeItem:
            removeItemFromCartService(productCode: productCode, quantity: quantity)
        case .viewAlternative:
            print("viewAlternative")
        }
    }
}

//MARK:- Popover Presentation Controller Delegate
extension CartViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
