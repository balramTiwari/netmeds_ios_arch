//
//  QuantityListPoUpViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 05/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class QuantityListPoUpViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var action :((_ quantity: NSNumber) -> ())?
    var maxQuantity = NSNumber(value: 1)
    var selectedQuantity = "1"
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension QuantityListPoUpViewController {
    private func setUpUserInterface() {
        self.tableView.tableFooterView = UIView()
        if Int(truncating: maxQuantity) > 3{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                if let intValue = Int(self.selectedQuantity) {
                    let row = intValue - 1
                    let indexPath = IndexPath(row: row, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
                }
            }
        }
    }
    
    func handleQuantitySelectionAction(ForAction action:@escaping (_ quantity: NSNumber) -> () ) {
        self.action = action
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension QuantityListPoUpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return maxQuantity.intValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuantityCell", for: indexPath)
        cell.textLabel?.text = "\(indexPath.row + 1)"
        cell.accessoryType = .none
        cell.backgroundColor = .white
        if (selectedQuantity == "\(indexPath.row + 1)") {
            let button = UIButton(type: .custom)
            button.setImage(UIImage(named: "checkCircle"), for: .normal)
            button.tag = indexPath.row
            button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            cell.accessoryView = button
        } else {
            cell.accessoryView = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.action?(NSNumber(value: indexPath.row + 1)) ?? ()
        self.dismiss(animated: true, completion: nil)
    }
}
