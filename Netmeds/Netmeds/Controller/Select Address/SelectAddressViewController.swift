//
//  SelectAddressViewController.swift
//  Netmeds
//
//  Created by Netmeds1 on 26/12/19.
//  Copyright © 2019 Netmeds. All rights reserved.
//

import UIKit

protocol SelectAddressDelegateHandlers: class {
    func allAddressSuccessProcess()
}

class SelectAddressViewController: BaseViewController {

    @IBOutlet weak private var tblViewSelectAddress: UITableView!
    var allAddressViewModel = AllAddressViewModel()
    var shippingAddressID: Int = 0
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension SelectAddressViewController {
    private func setUpUserInterface() {
        allAddressViewModel.delegate = self
        registerTableViewCells()
        getAllAddressService()
    }
    
    private func registerTableViewCells() {
        tblViewSelectAddress.tableFooterView = UIView()
        tblViewSelectAddress.registerNibForCell(SelectAddressTblViewCell.self)
        tblViewSelectAddress.registerNibForCell(AddNewAddressTblViewCell.self)
    }
    
    private func deSelectAddress() {
        for (index, _) in allAddressViewModel.addressList.enumerated() {
            allAddressViewModel.addressList[index].isSelected = false
        }
    }
    
    private func setDefaultAddress() {
        deSelectAddress()
        for (index, list) in allAddressViewModel.addressList.enumerated() {
            if list.id! == shippingAddressID {
                allAddressViewModel.addressList[index].isSelected = true
                break
            }
        }
        allAddressViewModel.addressList.sort(by: AllAddressInfoModel.sequenceBySelected)
        tblViewSelectAddress.reloadData()
    }
}

// MARK: All Button Functions
extension SelectAddressViewController {
    @IBAction private func performOrderReviewButton() {
        self.navigateToOrderReviewVC(selectedAddress: allAddressViewModel.addressList.first)
    }
}

// MARK: All API Functions
extension SelectAddressViewController {
    private func getAllAddressService() {
        allAddressViewModel.getAllAddressService()
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension SelectAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allAddressViewModel.addressList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 76.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tblViewSelectAddress.frame.size.width, height: 76.0))
        headerView.backgroundColor = .clear
        guard let headerTblCell: AddNewAddressTblViewCell = tblViewSelectAddress.dequeueReusableCell(withIdentifier: "AddNewAddressTblViewCell") as? AddNewAddressTblViewCell else { return headerView }
        headerTblCell.contentView.frame = headerView.frame
        headerView.addSubview(headerTblCell.contentView)
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let list = allAddressViewModel.addressList[indexPath.row]
        let selectAddressCell: SelectAddressTblViewCell = tblViewSelectAddress.dequeueReusableCellForIndexPath(indexPath)
        selectAddressCell.configCell(list: list)
        return selectAddressCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let list = allAddressViewModel.addressList[indexPath.row]
        guard !list.isSelected else { return }
        shippingAddressID = list.id!
        setDefaultAddress()
    }
}

//MARK:- Select Address Delegate Handlers
extension SelectAddressViewController: SelectAddressDelegateHandlers {
    func allAddressSuccessProcess() {
        setDefaultAddress()
    }
}
