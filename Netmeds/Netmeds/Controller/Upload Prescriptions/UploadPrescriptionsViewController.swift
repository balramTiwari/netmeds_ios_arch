//
//  UploadPrescriptionsViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 13/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol UploadPrescriptionsDelegateHandlers: class {
    func successProcess(isEnabled: Bool)
}

protocol UploadPrescriptionsCollectionViewCellDelegateHandlers: class {
    func btnRemovePrescriptionTapped(cell: UploadPrescriptionsCollectionCell, tag: Int)
}

class UploadPrescriptionsViewController: BaseViewController {
    
    @IBOutlet weak private var tblViewPrescription: UITableView!
    @IBOutlet weak private var btnContinue: RoundButton!
    
    private var prescriptionsViewModel = UploadPrescriptionsViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension UploadPrescriptionsViewController {
    private func setUpUserInterface() {
        MedicineFlowManager.shared.flow = .methodTwo
        MedicineFlowManager.shared.cartID = 0
        prescriptionsViewModel.delegate = self
        registerTableViewCells()
        updateContinueButton()
        addDefaultInfo()
        method2CartsService()
    }
    
    private func registerTableViewCells() {
        tblViewPrescription.tableFooterView = UIView()
        tblViewPrescription.registerNibForCell(CartHeaderTblViewCell.self)
        tblViewPrescription.registerNibForCell(LabelTblViewCell.self)
        tblViewPrescription.registerNibForCell(UploadPrescriptionsCollectionTblCell.self)
        tblViewPrescription.registerNibForCell(UploadPrescriptionsGuideTblCell.self)
        tblViewPrescription.rowHeight = UITableView.automaticDimension
        tblViewPrescription.estimatedRowHeight = 100
        tblViewPrescription.sectionHeaderHeight = UITableView.automaticDimension
        tblViewPrescription.estimatedSectionHeaderHeight = 100
    }
    
    private func addPrescription(image: UIImage? = nil, rxID: String = "") {
        prescriptionsViewModel.addPrescription(image: image, rxID: rxID)
    }
    
    private func getPrescriptionsRowModelFrom(index: Int) -> UploadPrescriptionsRowModel? {
        return prescriptionsViewModel.getPrescriptionsRowModelFrom(index: index)
    }
    
    private func removePrescription(index: Int) {
        prescriptionsViewModel.removePrescription(index: index)
    }
    
    private func resetSelectedPrescriptions(rxIDs: [String]) {
        prescriptionsViewModel.resetSelectedPrescriptions(rxIDs: rxIDs)
    }
    
    private func updateContinueButton(isEnabled: Bool = false) {
        btnContinue.backgroundColor = isEnabled ? UIColor.btnEnableNetmedsGreenColor(): UIColor.btndisableAshColor()
        btnContinue.isEnabled = isEnabled
    }
    
    private func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            //            DispatchQueue.main.async {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.allowsEditing = false
            self.present(imagePickerController, animated: true, completion: nil)
            //            }
        }
    }
}

// MARK: All Navigations Functions
extension UploadPrescriptionsViewController {
    private func navigateToPastPrescriptionsVC() {
        guard let vc = PastPrescriptionsViewController.instantiate(storyboardName: .uploadPrescriptions) as? PastPrescriptionsViewController else { return }
        vc.delegate = self
        if let subRowModel = prescriptionsViewModel.getAllRxIDsPrescriptions() {
            vc.subRowModel = subRowModel
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

// MARK: All Button Functions
extension UploadPrescriptionsViewController {
    @IBAction private func performContinueButton() {
        guard let rowModel = prescriptionsViewModel.getPrescriptionsRowModelFrom() else { return }
        var params = [Any]()
        for subRowModel in rowModel.subRowModel {
            if let image = subRowModel.image {
                params.append(image)
            }
            if subRowModel.imgName.isValidString {
                params.append(subRowModel.imgName)
            }
        }
        uploadSelectedPrescriptionsService(params: params)
    }
    
    private func performOpenGalleyButton() {
        showImagePickerController(sourceType: .photoLibrary)
    }
    
    private func performOpenCameraButton() {
        showImagePickerController(sourceType: .camera)
    }
}

// MARK: All API Functions
extension UploadPrescriptionsViewController {
    private func addDefaultInfo() {
        prescriptionsViewModel.addDefaultInfo()
    }
    
    private func method2CartsService() {
        prescriptionsViewModel.method2CartsService()
    }
    
    private func uploadSelectedPrescriptionsService(params: [Any]?) {
        prescriptionsViewModel.detachAndUploadPrescriptions(params: params)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension UploadPrescriptionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return prescriptionsViewModel.uploadPrescriptionsHeaderModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prescriptionsViewModel.uploadPrescriptionsHeaderModel[section].rowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let headerModel = prescriptionsViewModel.uploadPrescriptionsHeaderModel[indexPath.section]
        //        let rowModel = headerModel.rowModel[indexPath.row]
        //        if rowModel.type == .prescriptionsGuide { return 220.0 }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerModel = prescriptionsViewModel.uploadPrescriptionsHeaderModel[section]
        guard let headerTblCell: CartHeaderTblViewCell = tblViewPrescription.dequeueReusableCell(withIdentifier: "CartHeaderTblViewCell") as? CartHeaderTblViewCell else { return UIView() }
        let bgColor: UIColor = headerModel.type == .prescriptionsGuide ? .clear: .white
        headerTblCell.configCell(title: headerModel.title, bgColor: bgColor, bottomInset: headerModel.type == .prescriptionsGuide ? 2.0: 16.0, textColor: headerModel.type == .prescriptionsGuide ? UIColor.black.withAlphaComponent(0.6): UIColor.NMSTextColor(with: 0.6))
        return headerTblCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowModel = prescriptionsViewModel.uploadPrescriptionsHeaderModel[indexPath.section].rowModel
        let cornerRadius: CGFloat = indexPath.row == rowModel.count - 1 ? 10.0: 0.0
        let maskPath = UIBezierPath(roundedRect: cell.layer.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = cell.layer.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headerModel = prescriptionsViewModel.uploadPrescriptionsHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        switch rowModel.type {
        case .title:
            let titleCell: LabelTblViewCell = tblViewPrescription.dequeueReusableCellForIndexPath(indexPath)
            let bgColor: UIColor = headerModel.type == .prescriptionsGuide ? .clear: .white
            titleCell.configCell(title: rowModel.title, textColor: headerModel.type == .prescriptionsGuide ? UIColor.NMSTextColor(with: 0.6): UIColor.NMSTextColor(with: 1.0), bgColor: bgColor, titleFont: headerModel.type == .prescriptionsGuide ? UIFont(name: "Lato-Regular", size: 12)!: UIFont(name: "Lato-Regular", size: 14)!)
            return titleCell
        case .prescriptionsGuide:
            let prescriptionsGuideCell: UploadPrescriptionsGuideTblCell = tblViewPrescription.dequeueReusableCellForIndexPath(indexPath)
            return prescriptionsGuideCell
        default:
            let prescriptionsCell: UploadPrescriptionsCollectionTblCell = tblViewPrescription.dequeueReusableCellForIndexPath(indexPath)
            prescriptionsCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, rowModel: rowModel)
            return prescriptionsCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

//MARK:- CollectionView Delegate and DataSource Functions
extension UploadPrescriptionsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let rowModel = getPrescriptionsRowModelFrom(index: collectionView.tag) else { return 0 }
        return rowModel.subRowModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let rowModel = getPrescriptionsRowModelFrom(index: collectionView.tag) else { return CGSize(width: 0.0, height: 0.0) }
        var width = screenWidth - 16 - (collectionView.contentInset.left + collectionView.contentInset.right)
        width = rowModel.subRowModel.count > 1 ? width - 16: width
        switch rowModel.type {
        case .options:
            let noOfCellsInRow: CGFloat = CGFloat(rowModel.subRowModel.count)
            //               let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            //               let totalSpace = flowLayout.sectionInset.left
            //                   + flowLayout.sectionInset.right
            //                   + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
            //               let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
            //           return CGSize(width: size, height: size)
            return CGSize(width: width / noOfCellsInRow - 10, height: 105.0)
            
        default:
            return CGSize(width: 78.0, height: 112.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let rowModel = getPrescriptionsRowModelFrom(index: collectionView.tag) else { return UICollectionViewCell() }
        let subRowModel = rowModel.subRowModel[indexPath.row]
        switch rowModel.type {
        case .options:
            let optionsCell: UploadPrescriptionsOptionsCollectionCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            optionsCell.configCell(title: subRowModel.title, imgName: subRowModel.imgName)
            return optionsCell
        case .prescriptions:
            let prescriptionsCell: UploadPrescriptionsCollectionCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
            prescriptionsCell.configCell(imgURL: subRowModel.imgName, image: subRowModel.image, btnTag: collectionView.tag)
            prescriptionsCell.delegate = self
            return prescriptionsCell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let rowModel = getPrescriptionsRowModelFrom(index: collectionView.tag) else { return }
        let subRowModel = rowModel.subRowModel[indexPath.row]
        switch subRowModel.type {
        case .gallery:
            performOpenGalleyButton()
        case .camera:
            performOpenCameraButton()
        case .pastRx:
            self.navigateToPastPrescriptionsVC()
        default: break
        }
    }
}

//MARK:- UIImage Picker Controller Delegate Functions
extension UploadPrescriptionsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else { return }
        //        let data = selectedImage.jpegData(compressionQuality: 1.0)
        addPrescription(image: selectedImage)
        dismiss(animated: true, completion: nil)
    }
}


//MARK:- Upload Prescriptions Delegate Handlers
extension UploadPrescriptionsViewController : UploadPrescriptionsDelegateHandlers {
    func successProcess(isEnabled: Bool) {
        self.tblViewPrescription.reloadData()
        updateContinueButton(isEnabled: isEnabled)
    }
}

//MARK:- Upload Prescriptions CollectionViewCell Delegate Handlers
extension UploadPrescriptionsViewController : UploadPrescriptionsCollectionViewCellDelegateHandlers {
    func btnRemovePrescriptionTapped(cell: UploadPrescriptionsCollectionCell, tag: Int) {
        guard let collectionView = self.view.viewWithTag(tag) as? UICollectionView else { return }
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        removePrescription(index: indexPath.row)
        collectionView.deleteItems(at: [indexPath])
    }
}

//MARK:- Selected Prescriptions Delegate Handlers
extension UploadPrescriptionsViewController : SelectedPrescriptionsDelegateHandlers {
    func selectedPrescriptions(prescriptions: [String]) {
        resetSelectedPrescriptions(rxIDs: prescriptions)
    }
}

