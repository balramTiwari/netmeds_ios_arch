//
//  PastPrescriptionsViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 11/02/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol PastPrescriptionsDelegateHandlers: class {
    func successProcess()
}

protocol SelectedPrescriptionsDelegateHandlers: class {
    func selectedPrescriptions(prescriptions: [String])
}

protocol PastPrescriptionsCollectionViewCellDelegateHandlers: class {
    func btnChoosePrescriptionTapped(cell: PastPrescriptionsCollectionCell)
}

class PastPrescriptionsViewController: BaseViewController {
    
    @IBOutlet weak private var collectionViewPastPrescriptions: UICollectionView!
    var subRowModel = [UploadPrescriptionsSubRowModel]()
    weak var delegate: SelectedPrescriptionsDelegateHandlers?
    private var pastPrescriptionsViewModel = PastPrescriptionsViewModel()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
}

// MARK: All Functions
extension PastPrescriptionsViewController {
    private func setUpUserInterface() {
        pastPrescriptionsViewModel.delegate = self
        registerCollectionnViewCells()
        pastPrescriptionsService()
    }
    
    private func registerCollectionnViewCells() {
        collectionViewPastPrescriptions.registerNibForCell(PastPrescriptionsCollectionCell.self)
        collectionViewPastPrescriptions.registerNibForSupplementaryView(PastPrescriptionsCollectionHeaderCell.self, of: UICollectionView.elementKindSectionHeader)
    }
    
    private func updateSelectedPrescription(section: Int, row: Int) {
        pastPrescriptionsViewModel.updateSelectedPrescription(section: section, row: row)
    }
}

// MARK: All Button Functions
extension PastPrescriptionsViewController {
    
    @IBAction private func performCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func performDoneButton() {
        var selectedRxIDs: [String] = [String]()
        for headerModel in pastPrescriptionsViewModel.pastPrescriptionsHeaderModel {
            for rowModel in headerModel.rowModel where rowModel.isSelected == true {
                selectedRxIDs.append(rowModel.imgURL)
            }
        }
        self.delegate?.selectedPrescriptions(prescriptions: selectedRxIDs)
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: All API Functions
extension PastPrescriptionsViewController {
    
    func pastPrescriptionsService() {
        let params = [
            ["pageIndex":"1"],
            ["pageSize":"12"]
        ]
        pastPrescriptionsViewModel.pastPrescriptionsService(params: params, subRowModel: subRowModel)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- CollectionView Delegate and DataSource Functions
extension PastPrescriptionsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return pastPrescriptionsViewModel.pastPrescriptionsHeaderModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pastPrescriptionsViewModel.pastPrescriptionsHeaderModel[section].rowModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100.0, height: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionHeader else { return UICollectionReusableView() }
        let headerModel = pastPrescriptionsViewModel.pastPrescriptionsHeaderModel[indexPath.section]
        let headerView: PastPrescriptionsCollectionHeaderCell = collectionView.dequeueReusableSupplementaryViewForIndexPath(indexPath, of: kind)
        headerView.configCell(title: headerModel.title)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let headerModel = pastPrescriptionsViewModel.pastPrescriptionsHeaderModel[indexPath.section]
        let rowModel = headerModel.rowModel[indexPath.row]
        let pastPrescriptionsCell: PastPrescriptionsCollectionCell = collectionView.dequeueReusableCellForIndexPath(indexPath)
        pastPrescriptionsCell.configCell(imgURL: rowModel.imgURL, isSelected: rowModel.isSelected)
        pastPrescriptionsCell.delegate = self
        return pastPrescriptionsCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

//MARK:- Past Prescriptions Delegate Handlers
extension PastPrescriptionsViewController: PastPrescriptionsCollectionViewCellDelegateHandlers {
    func btnChoosePrescriptionTapped(cell: PastPrescriptionsCollectionCell) {
        guard let indexPath = collectionViewPastPrescriptions.indexPath(for: cell) else { return }
        updateSelectedPrescription(section: indexPath.section, row: indexPath.row)
//        let headerModel = pastPrescriptionsViewModel.pastPrescriptionsHeaderModel[indexPath.section]
//        var rowModel = headerModel.rowModel[indexPath.row]
//        rowModel.isSelected = !rowModel.isSelected
//        pastPrescriptionsViewModel.pastPrescriptionsHeaderModel[indexPath.section].rowModel[indexPath.row] = rowModel
//        collectionViewPastPrescriptions.reloadData()
    }
}

//MARK:- Past Prescriptions Delegate Handlers
extension PastPrescriptionsViewController: PastPrescriptionsDelegateHandlers {
    func successProcess() {
        self.collectionViewPastPrescriptions.reloadData()
    }
}

