//
//  AllOffersViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol AllOffersDelegateHandlers: class {
    func successProcess()
}

class AllOffersViewController: BaseViewController {
    @IBOutlet weak private var tblViewAllOffers: UITableView!
    @IBOutlet weak private var segmentedControlView: CustomSegmentedControl!
    private var allOffersViewModel  = AllOffersViewModel()
    var selectedIndex = 0
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension AllOffersViewController {
    private func setUpUserInterface() {
        allOffersViewModel.delegate = self
        registerTableViewCells()
        updateUI()
        getAllOffers()
    }
    
    private func registerTableViewCells() {
        tblViewAllOffers.tableFooterView = UIView()
        tblViewAllOffers.registerNibForCell(AllOffersTblViewCell.self)
    }
    
    private func updateUI() {
        segmentedControlView.isHidden = false
        segmentedControlView.isFromOffersPage = true
        segmentedControlView.segmentCount = 3
        segmentedControlView.selectedIndex = self.selectedIndex
        segmentedControlView.loadView()
        segmentedControlView.handleSegmentedControlValueChangedAction { [weak self] (value) in
            self?.selectedIndex = value
            var type: AllOffersSelectionType = .medicine
            if value == 1 {
                type = .diagnostics
            } else if value == 2 {
                type = .consultation
            }
            self?.allOffersViewModel.filterBy(selectionType: type)
        }
    }
}

// MARK: All Button Functions
extension AllOffersViewController {
}

// MARK: All API Functions
extension AllOffersViewController {
    private func getAllOffers() {
        allOffersViewModel.getAllOffersService()
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension AllOffersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allOffersViewModel.allOffersModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let allOffers = allOffersViewModel.allOffersModel[indexPath.row]
        let allOffersCell: AllOffersTblViewCell = tblViewAllOffers.dequeueReusableCellForIndexPath(indexPath)
        allOffersCell.configCell(offers: allOffers)
        return allOffersCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let allOffers = allOffersViewModel.allOffersModel[indexPath.row]
        if let offerList = allOffers.offerList, offerList.count > 0 {
            self.navigateToOfferListVC(offers: offerList, title: allOffers.title ?? "")
        } else {
            self.navigateToOffersDetailVC(offers: allOffers)
        }
    }
}

//MARK:- All Offers Delegate Handlers
extension AllOffersViewController : AllOffersDelegateHandlers {
    func successProcess() {
        tblViewAllOffers.reloadData()
    }
}
