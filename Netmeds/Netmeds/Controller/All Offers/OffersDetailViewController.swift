//
//  OffersDetailViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 04/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

protocol OffersDetailDelegateHandlers: class {
    func successProcess(offersDetailsModel: AllOffersModel?)
    func failureProcess()
}

class OffersDetailViewController: BaseViewController {
    @IBOutlet weak private var lblTitle : UILabel!
    @IBOutlet weak private var lblDesc : UILabel!
    @IBOutlet weak private var tblViewOffersDetail: UITableView!
    @IBOutlet weak private var btnCopyCode : UIButton!
    @IBOutlet weak private var imgView: UIImageView!
    private var offersDetailViewModel : OffersDetailViewModel = OffersDetailViewModel()
    var pageID: String = "0"
    var offersDetailsModel: AllOffersModel?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension OffersDetailViewController {
    private func setUpUserInterface() {
        offersDetailViewModel.delegate = self
        registerTableViewCells()
        guard let offersDetails = offersDetailsModel else { getOffersDetail(); return }
        offersDetailViewModel.parseOffersDetail(offersDetailInfo: offersDetails)
    }
    
    private func updateUI() {
        lblTitle.text = offersDetailsModel?.title
        lblDesc.text =  offersDetailsModel?.couponDesc
        if let couponCode = offersDetailsModel?.couponCode, couponCode.isValidString {
            btnCopyCode.setTitle("COPY CODE AND SHOP", for: .normal)
        } else {
            btnCopyCode.setTitle("SHOP", for: .normal)
        }
        if let btnText = offersDetailsModel?.btnText {
            btnCopyCode.setTitle(btnText, for: .normal)
        }
        if let imgURL = offersDetailsModel?.image {
            imgView.downloadImage(from: imgURL) { [weak self] (image) in
                self?.imgView.image = image
            }
        }
        tblViewOffersDetail.reloadData()
    }
    
    private func registerTableViewCells() {
        tblViewOffersDetail.tableFooterView = UIView()
        tblViewOffersDetail.registerNibForCell(VerticalTwoLabelsTblViewCell.self)
        tblViewOffersDetail.registerNibForCell(TblViewCellWithImage.self)
    }    
}

// MARK: All Button Functions
extension OffersDetailViewController {
    @IBAction private func performShopButton() {
        if let shopUrl = offersDetailsModel?.shopURL, shopUrl.isValidString {
            if let couponCode = offersDetailsModel?.couponCode {
                UIPasteboard.general.string = couponCode
                createToastWith(message: "Copied to clipboard")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.bannerRedirection(linkType: LinkType.custom.rawValue, url: shopUrl)
                }
            } else {
                self.bannerRedirection(linkType: LinkType.custom.rawValue, url: shopUrl)
            }
        } else if let couponCode = offersDetailsModel?.couponCode {
            UIPasteboard.general.string = couponCode
            createToastWith(message: "Copied to clipboard")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.navigateToSearchVC()
            }
        } else {
            navigateToSearchVC()
        }
    }
}

// MARK: All API Functions
extension OffersDetailViewController {
    private func getOffersDetail() {
        offersDetailViewModel.getOffersDetailService(pageID: pageID)
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension OffersDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offersDetailViewModel.offersDetailCustomModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let offersDetail = offersDetailViewModel.offersDetailCustomModel[indexPath.row]
        return offersDetail.type == .image ? 180.0: UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offersDetail = offersDetailViewModel.offersDetailCustomModel[indexPath.row]
        switch offersDetail.type {
        case .image:
            let imageCell: TblViewCellWithImage = tblViewOffersDetail.dequeueReusableCellForIndexPath(indexPath)
            imageCell.configCell(imgURL: offersDetail.desc)
            return imageCell
        default:
            let lblCell: VerticalTwoLabelsTblViewCell = tblViewOffersDetail.dequeueReusableCellForIndexPath(indexPath)
            lblCell.configCell(offersDetail: offersDetail)
            return lblCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let offersDetail = offersDetailViewModel.offersDetailCustomModel[indexPath.row]
        if offersDetail.type == .moreDetails {
            if let title = offersDetailsModel?.title, let urlString = offersDetailsModel?.url {
                navigateToWebVC(title: title, urlString: urlString)
            }
        }
    }
}

//MARK:- Offers Details Delegate Handlers
extension OffersDetailViewController : OffersDetailDelegateHandlers {
    func successProcess(offersDetailsModel: AllOffersModel?) {
        self.offersDetailsModel = offersDetailsModel
        updateUI()
    }
    
    func failureProcess() {
        self.navigationController?.popToRootViewController(animated: false)
        self.navigateToAllOffersVC()
    }
}
