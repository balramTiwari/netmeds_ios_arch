//
//  OfferListViewController.swift
//  Netmeds
//
//  Created by SANKARLAL on 30/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit

class OfferListViewController: BaseViewController {
    @IBOutlet weak private var lblTitle : UILabel!
    @IBOutlet weak private var tblViewOfferList: UITableView!
    var offerName: String = ""
    var offerListModel = [AllOffersModel]()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension OfferListViewController {
    private func setUpUserInterface() {
        registerTableViewCells()
        updateUI()
    }
    
    private func registerTableViewCells() {
        tblViewOfferList.tableFooterView = UIView()
        tblViewOfferList.registerNibForCell(AllOffersTblViewCell.self)
    }
        
    private func updateUI() {
        lblTitle.text = offerName
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- TableView Delegate and DataSource Functions
extension OfferListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerListModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let allOffers = offerListModel[indexPath.row]
        let allOffersCell: AllOffersTblViewCell = tblViewOfferList.dequeueReusableCellForIndexPath(indexPath)
        allOffersCell.configCell(offers: allOffers)
        return allOffersCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let allOffers = offerListModel[indexPath.row]
        self.navigateToOffersDetailVC(offers: allOffers)
    }
}
