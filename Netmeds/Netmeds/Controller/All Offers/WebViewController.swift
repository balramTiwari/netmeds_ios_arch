//
//  WebViewController.swift
//  Netmeds
//
//  Created by Netmedsian on 05/01/20.
//  Copyright © 2020 Netmeds. All rights reserved.
//

import UIKit
import JavaScriptCore
import WebKit

class WebViewController: BaseViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var activeIndicator: UIActivityIndicatorView!
    var titleString: String = ""
    var urlString: String = ""
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarForChildViewController()
    }
}

// MARK: All Functions
extension WebViewController {
    private func setUpUserInterface() {
        lblTitle.text = titleString
        setUpWebViewConfiguration()
        loadRequest()
    }
    
    private func setUpWebViewConfiguration() {
        webView.configuration.userContentController.add(self, name: "launchProductDetail")
        webView.configuration.userContentController.add(self, name: "launchUrl")
        webView.configuration.userContentController.add(self, name: "launchExternalBrowser")
    }
 
    private func loadRequest() {
        if urlString.isValidString, let url = URL.init(string: urlString) {
            self.activeIndicator.startAnimating()
            let request = URLRequest(url: url)
            self.webView.load(request)
        }
    }
}

//MARK:- ALL DELEGATE FUCNTIONS
//MARK:- WebView WKUIDelegate Functions
extension WebViewController: WKUIDelegate, WKScriptMessageHandler {
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (Bool) -> Void) {}
   
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: () -> Void) {
        print("runJavaScriptAlertPanelWithMessage - \(message)");
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: (String?) -> Void) {
        print("runJavaScriptTextInputPanelWithPrompt - \(prompt)");
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "launchProductDetail" {
            print(message.body)
            if let productCode = message.body as? String, let intproductCode = Int(productCode),intproductCode >= 0 {
//                let viewController: NMOTCProductDetailViewController = NMOTCProductDetailViewController.instantiate(appStoryboard: .search)
//                viewController.productCode = intproductCode
//                self.navigationController?.pushViewController(viewController, animated: true)
            }
        } else if message.name == "launchExternalBrowser" {
            print(message.body)
            if let url = URL(string: (message.body as? String)!) {
                UIApplication.shared.open(url)
            }
        }  else if message.name == "launchUrl" {
            self.view.appLinkBannerRedirection(linkType: LinkType.custom.rawValue, url: message.body as? String ?? "")
        }
    }
}

//MARK:- WebView WKNavigationDelegate Functions
extension WebViewController: WKNavigationDelegate {
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
        self.activeIndicator.stopAnimating()
        webView.scrollView.subviews.forEach { subview in
            subview.gestureRecognizers?.forEach { recognizer in
                if let tapRecognizer = recognizer as? UITapGestureRecognizer,
                    tapRecognizer.numberOfTapsRequired == 2 && tapRecognizer.numberOfTouchesRequired == 1 {
                    subview.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(#function)
        self.activeIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        print(#function)
        self.activeIndicator.stopAnimating()
        completionHandler(.performDefaultHandling,nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            if let url = navigationAction.request.url {
                print(url)
                if !self.bannerRedirection(linkType: LinkType.custom.rawValue, url: url.absoluteString) {
                    self.activeIndicator.startAnimating()
                    let request = URLRequest(url: url)
                    self.webView.load(request)
                }
                decisionHandler(.allow)
                return
            }
        }
        decisionHandler(.cancel)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        print(#function)
        decisionHandler(.allow)
    }
}
